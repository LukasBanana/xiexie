/*
 * Main file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ConsoleOutput.h"
#include "StringMod.h"
#include "CompilerOptions.h"
#include "XieXieCompiler.h"
#include "AsmCompiler.h"
#include "VirtualMachine.h"

#include <iostream>
#include <chrono>

#if 0//!!!
#include "BigInteger.h"
#endif


namespace Settings
{

bool EnablePause        = false;
bool ShowAST            = false;
bool UseASM             = false;
bool ShowNameVisTable   = false;

} // /namespace Settings

namespace Feedback
{

bool GotInput       = false;

} // /namespace Feedback

static unsigned int GetCompilerOptions()
{
    unsigned int Options = 0;

    if (Settings::ShowAST)
        Options |= XieXieCompiler::Flags::ShowAST;
    if (Settings::UseASM)
        Options |= XieXieCompiler::Flags::UseASM;
    if (Settings::ShowNameVisTable)
        Options |= XieXieCompiler::Flags::ShowNameVisTable;

    return Options;
}

static bool FileExists(const std::string& Filename)
{
    /* Check if the file can be read (used C++11's ifstream bool operator) */
    std::ifstream File(Filename);
    return File ? true : false;
}

static bool CompileSingleFile(const std::string& Filename)
{
    Feedback::GotInput = true;

    /* Compile single source file */
    XieXieCompiler Compiler;
    return Compiler.CompileSingleFile(Filename, GetCompilerOptions());
}

static bool AssembleFile(const std::string& Filename)
{
    Feedback::GotInput = true;

    /* Assemble source file */
    XieXieAssembler Assembler;
    return Assembler.AssembleFile(Filename);
}

static bool DisassembleFile(const std::string& Filename)
{
    Feedback::GotInput = true;

    /* Read XieXie byte-code from file */
    ByteCode Program;
    if (!Program.ReadFromFile(Filename))
        return false;

    /* Disassemble to new file */
    return Program.Disassemble(
        Filename + ".disassembled.xasm",
        ByteCode::DisassembleModes::DecLineNumbers
    );
}

static bool RunByteCodeFromFile(const std::string& Filename)
{
    Feedback::GotInput = true;

    /* Load byte-code from file */
    XVM::ByteCode Program;
    if (!Program.ReadFromFile(Filename))
        return false;

    /* Run program in the virtual-machine */
    ConsoleOutput::Message("Run program inside virtual-machine (" + xxStr(Program.NumInstructions()) + " instructions)");

    XVM::VirtualMachine VM;

    const auto StartTime = std::chrono::system_clock::now();
    const auto Err = VM.Execute(Program);
    const auto EndTime = std::chrono::system_clock::now();

    /* Check for errors */
    if (Err == 0)
    {
        /* Print execution briefing message */
        std::string ExeBrief = "Program terminated successful after " + xxStr(VM.NumCycles()) + " cycles and ";

        auto Duration = std::chrono::duration_cast<std::chrono::milliseconds>(EndTime - StartTime).count();

        if (Duration >= 10000)
        {
            Duration = std::chrono::duration_cast<std::chrono::seconds>(EndTime - StartTime).count();
            ExeBrief += xxStr(Duration) + " sec.";
        }
        else
            ExeBrief += xxStr(Duration) + " ms.";

        ConsoleOutput::Success(ExeBrief);
    }
    else
        ConsoleOutput::Error("Program terminated with error code " + xxStr(Err));

    return true;
}

static bool RunProgram(const std::string& Filename)
{
    Settings::UseASM = true;

    return
        CompileSingleFile(Filename) &&
        AssembleFile(Filename + ".xasm") &&
        RunByteCodeFromFile(Filename + ".xasm.xbc");
}

static bool EvaluateExpression(const std::string& ExprStr)
{
    Feedback::GotInput = true;

    /* Evaluate single expression string */
    XieXieCompiler Compiler;
    return Compiler.EvaluateExpression(ExprStr, GetCompilerOptions());
}

static bool AnalyzeFile(const std::string& Filename)
{
    /* Analyte file by its extension */
    auto Ext = xxFileExt(Filename);

    if (Ext == "xx")
        RunProgram(Filename);
    else if (Ext == "xasm")
        AssembleFile(Filename);
    else if (Ext == "xbc")
        RunByteCodeFromFile(Filename);
    else
        return false;

    return true;
}

static std::string GetNextArg(int& argc, const char**& argv)
{
    --argc;
    ++argv;
    return std::string(*argv);
}

static bool GetNextArgFilename(int& argc, const char**& argv, std::string& Filename)
{
    /* Get filename from argument list and assemble source file */
    if (argc > 1)
    {
        Filename = GetNextArg(argc, argv);
        return true;
    }

    /* Print error of missing filename */
    ConsoleOutput::Error("Missing filename argument");

    return false;
}

static void ParseArgument(int argc, const char** argv)
{
    /* Get current command line argument */
    const std::string Param = *argv;
    std::string Filename;

    /* --- Print information --- */
    if (Param == "-v" || Param == "--version")
    {
        ConsoleOutput::PrintVersion();
        Feedback::GotInput = true;
    }
    else if (Param == "-h" || Param == "--help")
    {
        ConsoleOutput::PrintHelp();
        Feedback::GotInput = true;
    }
    else if (Param == "-i" || Param == "--info")
    {
        ConsoleOutput::PrintInfo();
        Feedback::GotInput = true;
    }
    else if (Param == "-k" || Param == "--keywords")
    {
        ConsoleOutput::PrintKeywords();
        Feedback::GotInput = true;
    }

    /* --- Internal settings --- */
    else if (Param == "-p" || Param == "--pause")
        Settings::EnablePause = true;
    else if (Param == "-ast")
        Settings::ShowAST = true;
    else if (Param == "-nv-table")
        Settings::ShowNameVisTable = true;
    else if (Param == "-gen-ASM")
        Settings::UseASM = true;

    /* --- Other settings --- */
    else if (Param == "-w" || Param == "--window")
        ConsoleOutput::OpenWindow();
    else if (Param == "--auto-cref")
    {
        CompilerOptions::Settings.AutoConstRef = true;
        ConsoleOutput::Warning("Automatic constant reference generation ('--auto-cref' option) may cause problems");
    }

    /* --- Compilation commands --- */
    else if (Param == "-e" || Param == "--expr")
    {
        std::string ExprStr;

        if (argc == 1)
        {
            while (true)
            {
                /* Let user enter an expression */
                ConsoleOutput::Message("Enter expression (or 'q' to quit):");
                ConsoleOutput::UpperIndent();

                std::getline(std::cin, ExprStr);

                if (ExprStr == "q")
                    break;

                EvaluateExpression(ExprStr);

                ConsoleOutput::LowerIndent();
            }
        }
        else
        {
            /* Get expression string from argument list and evaluate single expression string */
            ExprStr = GetNextArg(argc, argv);
            EvaluateExpression(ExprStr);
        }
    }
    else if (Param == "-f" || Param == "--file")
    {
        if (GetNextArgFilename(argc, argv, Filename))
            CompileSingleFile(Filename);
    }
    else if (Param == "-asm" || Param == "--assemble")
    {
        if (GetNextArgFilename(argc, argv, Filename))
            AssembleFile(Filename);
    }
    else if (Param == "-dasm" || Param == "--disassemble")
    {
        if (GetNextArgFilename(argc, argv, Filename))
            DisassembleFile(Filename);
    }
    else if (Param == "-xvm" || Param == "--virtual-machine")
    {
        if (GetNextArgFilename(argc, argv, Filename))
            RunByteCodeFromFile(Filename);
    }
    else if (Param == "-run")
    {
        if (argc > 1)
        {
            /* Get filename from argument list and run program (compile, assemble and execute) */
            const auto Filename = GetNextArg(argc, argv);
            RunProgram(Filename);
        }
        else
            ConsoleOutput::Error("Missing filename argument");
    }
    else
    {
        /* Try to configure other settings */
        if (!CompilerOptions::Settings.Setup(Param, true))
        {
            /* Otherwise try to anaylze file by its extension */
            if (FileExists(Param))
            {
                Settings::EnablePause = true;
                if (!AnalyzeFile(Param))
                    ConsoleOutput::Error("Unknown file extension \"" + xxFileExt(Param) + "\"");
            }
            else
                ConsoleOutput::Error("Unknown argument \"" + Param + "\"");
        }
    }

    /* Parse next command line argument */
    if (argc > 1)
        ParseArgument(argc - 1, argv + 1);
}

int main(int argc, const char** argv)
{
    #if 0 //!DEBUGGING! --> BitInteger class

    #   if 0
    
    BigInteger i("FE", 16), j("12", 8), k("31ef", 16), l("12", 16);

    std::cout << "i = " << i.Bits().front() << std::endl;
    std::cout << "j = " << j.Bits().front() << std::endl;
    std::cout << "k = " << k.Bits().front() << std::endl;
    std::cout << "l = " << l.Bits().front() << std::endl;

    #   elif 0

    BigInteger i("123456789abcdef", 16);

    for (int j = 0; j < 32; ++j)
        i += i;

    std::cout << "i = " << i.Str(2) << " (Binary)" << std::endl;
    std::cout << "i = " << i.Str(8) << " (Octal)" << std::endl;
    std::cout << "i = " << i.Str(16) << " (Hex)" << std::endl;

    #   else

    BigInteger a("84c78723ff983283782f7783487367bc7872337328373487337f8eaeabff9877882823", 16), b("fff987788eabc7872334873847fff987788eabc7872332837823348736723732837823", 16);
    
    auto c = a + b;

    std::cout << "a   = " << a.Str(16) << std::endl;
    std::cout << "b   = " << b.Str(16) << std::endl;
    std::cout << "a+b = " << c.Str(16) << std::endl;
    
    #   endif

    system("pause");
    return 0;

    #endif

    try
    {
        /* Get program path from argument list */
        const std::string AppPath = *argv;
        --argc;
        ++argv;

        /* Get program arguments */
        if (argc == 0)
        {
            ConsoleOutput::PrintInfo();
            return 0;
        }

        /* Parse argument list */
        ParseArgument(argc, argv);

        /* Update events for window frame is enabled */
        ConsoleOutput::UpdateWindow();
        ConsoleOutput::CloseWindow();
    }
    catch (const std::string& Err)
    {
        ConsoleOutput::Error(Err);
    }

    if (!Feedback::GotInput)
    {
        ConsoleOutput::Message(
            "Enter \"xxc -h\" or \"xxc --help\" for information about how to use the XieXie compiler"
        );
    }

    /* Check if pause has been enabled */
    if (Settings::EnablePause)
        ConsoleOutput::Wait();

    return 0;
}



// ================================================================================