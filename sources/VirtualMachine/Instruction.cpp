/*
 * Instruction file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Instruction.h"
#include "StringMod.h"


namespace XVM
{


/*
 * Registers structure
 */

const char* Instruction::Registers::Name(const reg_t& Index)
{
    switch (Index)
    {
        /* Integral registers */
        case i0: return "i0";
        case i1: return "i1";
        case i2: return "i2";
        case i3: return "i3";

        /* Reserved registers */
        case cf: return "cf"; // <-- Conditional flags register
        case lb: return "lb"; // <-- Local base pointer
        case sp: return "sp"; // <-- Stack pointer
        case pc: return "pc"; // <-- Program counter

        /* Floating-point registers */
        case f0: return "f0";
        case f1: return "f1";
        case f2: return "f2";
        case f3: return "f3";
        case f4: return "f4";
        case f5: return "f5";
        case f6: return "f6";
        case f7: return "f7";
    }
    return nullptr;
}

const char* Instruction::OpCodes::Mnemonic(const opcode_t& OpCode)
{
    switch (OpCode)
    {
        case Reg1::MOV:
        case Reg2::MOV:     return "MOV";
        case Reg2::NOT:     return "NOT";
        case Reg1::AND:
        case Reg2::AND:     return "AND";
        case Reg1::OR:
        case Reg2::OR:      return "OR";
        case Reg1::XOR:
        case Reg2::XOR:     return "XOR";
        case Reg1::ADD:
        case Reg2::ADD:     return "ADD";
        case Reg1::SUB:
        case Reg2::SUB:     return "SUB";
        case Reg1::MUL:
        case Reg2::MUL:     return "MUL";
        case Reg1::DIV:
        case Reg2::DIV:     return "DIV";
        case Reg1::MOD:
        case Reg2::MOD:     return "MOD";
        case Reg1::SLL:
        case Reg2::SLL:     return "SLL";
        case Reg1::SLR:
        case Reg2::SLR:     return "SLR";
        case Reg2::CMP:     return "CMP";
        case Reg2::FTI:     return "FTI";
        case Reg2::ITF:     return "ITF";
        case Reg1::PUSH:    return "PUSH";
        case Reg1::POP:     return "POP";
        case Reg1::INC:     return "INC";
        case Reg1::DEC:     return "DEC";

        case Jump::JMP:     return "JMP";
        case Jump::JE:      return "JE";
        case Jump::JNE:     return "JNE";
        case Jump::JG:      return "JG";
        case Jump::JL:      return "JL";
        case Jump::JGE:     return "JGE";
        case Jump::JLE:     return "JLE";
        case Jump::CALL:    return "CALL";

        case Special::STOP: return "STOP";
        case Special::RET:  return "RET";
        case Special::PUSH: return "PUSH";

        case Mem::LDB:
        case MemOff::LDB:   return "LDB";
        case Mem::STB:
        case MemOff::STB:   return "STB";
        case Mem::LDW:
        case MemOff::LDW:   return "LDW";
        case Mem::STW:
        case MemOff::STW:   return "STW";
    }
    return nullptr;
}


/*
 * Instruction class
 */

Instruction::Instruction() :
    Code_(0)
{
}
Instruction::Instruction(const code_t Code) :
    Code_(Code)
{
}
Instruction::~Instruction()
{
}

void Instruction::BackPatchAddress(const unsigned int Address)
{
    if (IsSpecialInstr())
    {
        /* Insert address as 26-bit operand for special instruction (e.g. "PUSH data_field_address") */
        Code_ = (Code_ & 0xfc000000) | (Address & 0x03ffffff);
    }
    else if (IsLdStOffInstr())
    {
        /* Insert address as 18-bit operand for load/store (with offset) instruction (e.g. "LDW i0, (lb) 8") */
        Code_ = (Code_ & 0xfffc0000) | (Address & 0x0003ffff);
    }
    else
    {
        /* Insert address as 22-bit operand for jump instruction (e.g. "JMP (pc) 42") */
        Code_ = (Code_ & 0xffc00000) | (Address & 0x003fffff);
    }
}

Instruction Instruction::CreateReg2(
    const OpCodes::opcode_t OpCode, const Registers::reg_t Reg1, const Registers::reg_t Reg2, const unsigned int Flags)
{
    return Instruction(
        (code_t(OpCode & 0x3f      ) << 26) |
        (code_t(Reg1   & 0x0f      ) << 22) |
        (code_t(Reg2   & 0x0f      ) << 18) |
               (Flags  & 0x0003ffff)
    );
}

Instruction Instruction::CreateReg1(
    const OpCodes::opcode_t OpCode, const Registers::reg_t Reg, const unsigned int Value)
{
    return Instruction(
        (code_t(OpCode & 0x3f      ) << 26) |
        (code_t(Reg    & 0x0f      ) << 22) |
               (Value  & 0x003fffff)
    );
}

Instruction Instruction::CreateJump(
    const OpCodes::opcode_t OpCode, const Registers::reg_t Reg, const unsigned int Offset)
{
    return CreateReg1(OpCode, Reg, Offset);
}

Instruction Instruction::CreateLdSt(
    const OpCodes::opcode_t OpCode, const Registers::reg_t Reg, const unsigned int Address)
{
    return CreateReg1(OpCode, Reg, Address);
}

Instruction Instruction::CreateLdStOff(
    const OpCodes::opcode_t OpCode, const Registers::reg_t Reg1, const Registers::reg_t Reg2, const unsigned int Offset)
{
    return CreateReg2(OpCode, Reg1, Reg2, Offset);
}

Instruction Instruction::CreateSpecial(const OpCodes::opcode_t OpCode, const unsigned int Value)
{
    return Instruction(
        (code_t(OpCode & 0x3f      ) << 26) |
               (Value  & 0x03ffffff)
    );
}

Instruction Instruction::CreateSpecial(
    const OpCodes::opcode_t OpCode, unsigned int ResultSize, unsigned int ArgSize)
{
    return Instruction(
        (code_t(OpCode     & 0x3f      ) << 26) |
        (      (ResultSize & 0x000000ff) << 18) |
               (ArgSize    & 0x0003ffff)
    );
}

int Instruction::SgnValue26() const
{
    unsigned int Val = Value26();

    /* Sign-extension */
    if ((Val & 0x02000000) != 0)
        Val |= 0xfc000000;

    return static_cast<int>(Val);
}

int Instruction::SgnValue22() const
{
    unsigned int Val = Value22();

    /* Sign-extension */
    if ((Val & 0x00200000) != 0)
        Val |= 0xffc00000;

    return static_cast<int>(Val);
}

int Instruction::SgnValue18() const
{
    unsigned int Val = Value18();

    /* Sign-extension */
    if ((Val & 0x00020000) != 0)
        Val |= 0xfffc0000;

    return static_cast<int>(Val);
}

std::string Instruction::Disassemble() const
{
    std::string Instr;

    /* Append mnemonic */
    const auto InstrOpCode = OpCode();

    const char* Mnemonic = OpCodes::Mnemonic(InstrOpCode);
    if (Mnemonic == nullptr)
        return "";

    Instr += std::string(Mnemonic);

    if ( ( InstrOpCode >= OpCodes::Reg2::MOV && InstrOpCode <= OpCodes::Jump::CALL ) || InstrOpCode >= OpCodes::Mem::LDB )
    {
        /* Append 1st operand */
        const char* Reg0Name = Registers::Name(Reg1());
        if (Reg0Name == nullptr)
            return "";

        const bool IsJmp = IsJumpInstr();

        if (IsJmp)
            Instr += " (" + std::string(Reg0Name) + ")";
        else
            Instr += " " + std::string(Reg0Name);

        if (InstrOpCode <= OpCodes::Reg2::ITF)
        {
            /* Append 2nd operand */
            const char* Reg1Name = Registers::Name(Reg2());
            if (Reg1Name == nullptr)
                return "";

            Instr += ", " + std::string(Reg1Name);
        }
        else if (InstrOpCode >= OpCodes::MemOff::LDB)
        {
            /* Append 2nd operand and offset */
            const char* Reg1Name = Registers::Name(Reg2());
            if (Reg1Name == nullptr)
                return "";

            Instr += ", (" + std::string(Reg1Name) + ") " + xxStr(SgnValue18());
        }
        else if ( InstrOpCode >= OpCodes::Reg1::MOV && ( InstrOpCode < OpCodes::Reg1::PUSH || InstrOpCode > OpCodes::Reg1::DEC ) )
        {
            /* Append value */
            if (IsJmp)
                Instr += " " + xxStr(SgnValue22());
            else
                Instr += ", " + xxStr(SgnValue22());
        }
    }
    else if (InstrOpCode == OpCodes::Special::RET)
    {
        /* Append 1st- and 2nd operand */
        Instr += " (" + xxStr(ExtraValue8()) + ") " + xxStr(Value18());
    }
    else if (InstrOpCode == OpCodes::Special::PUSH)
    {
        /* Append value */
        Instr += " " + xxStr(SgnValue26());
    }

    return Instr;
}

unsigned int Instruction::FormatVersion()
{
    return 100u; // 1.00
}


} // /namespace XVM



// ================================================================================
