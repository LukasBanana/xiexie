/*
 * Heap manager file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "HeapManager.h"
#include "ConsoleOutput.h"
#include "StringMod.h"


namespace XVM
{


HeapManager::HeapManager() :
    MemSize_(0)
{
}
HeapManager::~HeapManager()
{
    ClearLeaks();
}

HeapManager::ptr_t HeapManager::AllocMem(unsigned int Size)
{
    /* Allocate memory block */
    void* Addr = malloc(Size);
    if (Addr == nullptr)
        throw std::string("Out of memory");

    /* Increase used memory size */
    MemSize_ += Size;

    /* Store pointer in hash-map */
    ptr_t Ptr = reinterpret_cast<ptr_t>(Addr);
    MemTable_[Ptr] = Size;

    return Ptr;
}

void HeapManager::FreeMem(ptr_t Ptr)
{
    /* Check if the memory block is still allive */
    auto it = MemTable_.find(Ptr);
    if (it == MemTable_.end())
        throw std::string("Memory deletion violation");

    /* Decrease used memory size */
    MemSize_ -= it->second;

    /* Delete memory block */
    void* Addr = reinterpret_cast<void*>(Ptr);
    free(Addr);
    MemTable_.erase(it);
}

void HeapManager::ClearLeaks()
{
    if (!MemTable_.empty())
    {
        for (auto it : MemTable_)
        {
            void* Addr = reinterpret_cast<void*>(it.first);
            free(Addr);
        }
        ConsoleOutput::Warning(xxStr(MemTable_.size()) + " memory leaks detected (" + xxStr(MemSize_) + " bytes)");
        MemTable_.clear();
    }
}


} // /namespace XVM



// ================================================================================