/*
 * Instruction header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_INSTRUCTION_H__
#define __XX_INSTRUCTION_H__


#include <string>


namespace XVM
{


/**
32-bit instruction.

\htmlonly
<pre>
Registers:
--------------------

i0 -> Integral register 0.
i1 -> Integral register 1.
i2 -> Integral register 2.
i3 -> Integral register 3.

cf -> Conditional flags register (used for jump conditions).
lb -> Local base pointer.
sp -> Stack pointer.
pc -> Program counter.

f0 -> Floating-point register 0.
f1 -> Floating-point register 1.
f2 -> Floating-point register 2.
f3 -> Floating-point register 3.
f4 -> Floating-point register 4.
f5 -> Floating-point register 5.
f6 -> Floating-point register 6.
f7 -> Floating-point register 7.


-------------------------------------------------------------------------------------------------------------------------------------------------
|                                                        2 Register Instruction Opcodes:                                                        |
-------------------------------------------------------------------------------------------------------------------------------------------------
| Mnemonic | Opcode      | Dest.   | Source  | Flags                               | Description                                                |
-------------------------------------------------------------------------------------------------------------------------------------------------
|          | 31.......26 | 25...22 | 21...18 | 17................................0 |                                                            |
-------------------------------------------------------------------------------------------------------------------------------------------------
| MOV      | 0 0 0 0 0 1 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Move register source to destination.                       |
|          |             |         |         |                                     | If L flag is set, use 64 bit value (otherwise 32 bit).     |
-------------------------------------------------------------------------------------------------------------------------------------------------
| NOT      | 0 0 0 0 1 0 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Bitwise NOT.                                               |
-------------------------------------------------------------------------------------------------------------------------------------------------
| AND      | 0 0 0 0 1 1 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Bitwise AND.                                               |
-------------------------------------------------------------------------------------------------------------------------------------------------
| OR       | 0 0 0 1 0 0 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Bitwise OR.                                                |
-------------------------------------------------------------------------------------------------------------------------------------------------
| XOR      | 0 0 0 1 0 1 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Bitwise XOR.                                               |
-------------------------------------------------------------------------------------------------------------------------------------------------
| ADD      | 0 0 0 1 1 0 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Arithmetic addition.                                       |
-------------------------------------------------------------------------------------------------------------------------------------------------
| SUB      | 0 0 0 1 1 1 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Arithmetic subtraction.                                    |
-------------------------------------------------------------------------------------------------------------------------------------------------
| MUL      | 0 0 1 0 0 0 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Arithmetic multiplication.                                 |
-------------------------------------------------------------------------------------------------------------------------------------------------
| DIV      | 0 0 1 0 0 1 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Arithmetic division.                                       |
-------------------------------------------------------------------------------------------------------------------------------------------------
| MOD      | 0 0 1 0 1 0 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Arithmetic modulo.                                         |
-------------------------------------------------------------------------------------------------------------------------------------------------
| SLL      | 0 0 1 0 1 1 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Shift locial left.                                         |
-------------------------------------------------------------------------------------------------------------------------------------------------
| SLR      | 0 0 1 1 0 0 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Shift locial right.                                        |
-------------------------------------------------------------------------------------------------------------------------------------------------
| CMP      | 0 0 1 1 0 1 | X X X X | Y Y Y Y | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Compares the two registers and stores the result in "cf".  |
-------------------------------------------------------------------------------------------------------------------------------------------------
| FTI      | 0 0 1 1 1 0 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Float to integer.                                          |
-------------------------------------------------------------------------------------------------------------------------------------------------
| ITF      | 0 0 1 1 1 1 | D D D D | S S S S | L 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Integer to float (Flags like FTI).                         |
-------------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------------------------------------
|                                                        1 Register Instruction Opcodes:                                                        |
-------------------------------------------------------------------------------------------------------------------------------------------------
| Mnemonic | Opcode      | Reg.    | Value                                       | Description                                                  |
-------------------------------------------------------------------------------------------------------------------------------------------------
|          | 31.......26 | 25...22 | 21........................................0 |                                                              |
-------------------------------------------------------------------------------------------------------------------------------------------------
| MOV      | 0 1 0 0 0 0 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Move value to register.                                      |
-------------------------------------------------------------------------------------------------------------------------------------------------
| AND      | 0 1 0 0 0 1 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Bitwise AND.                                                 |
-------------------------------------------------------------------------------------------------------------------------------------------------
| OR       | 0 1 0 0 1 0 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Bitwise OR.                                                  |
-------------------------------------------------------------------------------------------------------------------------------------------------
| XOR      | 0 1 0 0 1 1 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Bitwise XOR.                                                 |
-------------------------------------------------------------------------------------------------------------------------------------------------
| ADD      | 0 1 0 1 0 0 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Arithmetic addition.                                         |
-------------------------------------------------------------------------------------------------------------------------------------------------
| SUB      | 0 1 0 1 0 1 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Arithmetic subtraction.                                      |
-------------------------------------------------------------------------------------------------------------------------------------------------
| MUL      | 0 1 0 1 1 0 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Arithmetic multiplication.                                   |
-------------------------------------------------------------------------------------------------------------------------------------------------
| DIV      | 0 1 0 1 1 1 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Arithmetic division.                                         |
-------------------------------------------------------------------------------------------------------------------------------------------------
| MOD      | 0 1 1 0 0 0 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Arithmetic modulo.                                           |
-------------------------------------------------------------------------------------------------------------------------------------------------
| SLL      | 0 1 1 0 0 1 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Shift locial left.                                           |
-------------------------------------------------------------------------------------------------------------------------------------------------
| SLR      | 0 1 1 0 1 0 | D D D D | V V V V V V V V V V V V V V V V V V V V V V | Shift locial right.                                          |
-------------------------------------------------------------------------------------------------------------------------------------------------
| PUSH     | 0 1 1 0 1 1 | S S S S | 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Push source register onto stack.                             |
-------------------------------------------------------------------------------------------------------------------------------------------------
| POP      | 0 1 1 1 0 0 | D D D D | 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Pop destination register from stack.                         |
-------------------------------------------------------------------------------------------------------------------------------------------------
| INC      | 0 1 1 1 0 1 | D D D D | 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Increment integral register.                                 |
-------------------------------------------------------------------------------------------------------------------------------------------------
| DEC      | 0 1 1 1 1 0 | D D D D | 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Decrement integral register.                                 |
-------------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------------------------------------
|                                                           Jump Instruction Opcodes:                                                           |
-------------------------------------------------------------------------------------------------------------------------------------------------
| Mnemonic | Opcode      | Reg.    | Offset                                      | Description                                                  |
-------------------------------------------------------------------------------------------------------------------------------------------------
|          | 31.......26 | 25...22 | 21........................................0 |                                                              |
-------------------------------------------------------------------------------------------------------------------------------------------------
| JMP      | 1 0 0 0 0 0 | S S S S | O O O O O O O O O O O O O O O O O O O O O O | Always jump to the labled address.                           |
-------------------------------------------------------------------------------------------------------------------------------------------------
| JE       | 1 0 0 0 0 1 | S S S S | O O O O O O O O O O O O O O O O O O O O O O | Jump if greater.                                             |
-------------------------------------------------------------------------------------------------------------------------------------------------
| JNE      | 1 0 0 0 1 0 | S S S S | O O O O O O O O O O O O O O O O O O O O O O | Jump if not-equal.                                           |
-------------------------------------------------------------------------------------------------------------------------------------------------
| JG       | 1 0 0 0 1 1 | S S S S | O O O O O O O O O O O O O O O O O O O O O O | Jump if greater.                                             |
-------------------------------------------------------------------------------------------------------------------------------------------------
| JL       | 1 0 0 1 0 0 | S S S S | O O O O O O O O O O O O O O O O O O O O O O | Jump if less.                                                |
-------------------------------------------------------------------------------------------------------------------------------------------------
| JGE      | 1 0 0 1 0 1 | S S S S | O O O O O O O O O O O O O O O O O O O O O O | Jump if greater or equal.                                    |
-------------------------------------------------------------------------------------------------------------------------------------------------
| JLE      | 1 0 0 1 1 0 | S S S S | O O O O O O O O O O O O O O O O O O O O O O | Jump if less or equal.                                       |
-------------------------------------------------------------------------------------------------------------------------------------------------
| CALL     | 1 0 0 1 1 1 | S S S S | O O O O O O O O O O O O O O O O O O O O O O | Calls the routine at the address, stored in the register 'S' |
|          |             |         |                                             | + the offset 'O', and pushes 'lb' and 'pc' onto the stack.   |
-------------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------------------------------------
|                                                        Load/Store Instruction Opcodes:                                                        |
-------------------------------------------------------------------------------------------------------------------------------------------------
| Mnemonic | Opcode      | Reg.    | Address                                     | Description                                                  |
-------------------------------------------------------------------------------------------------------------------------------------------------
|          | 31.......26 | 25...22 | 21........................................0 |                                                              |
-------------------------------------------------------------------------------------------------------------------------------------------------
| LDB      | 1 1 1 0 0 0 | D D D D | A A A A A A A A A A A A A A A A A A A A A A | Load byte from memory to register.                           |
-------------------------------------------------------------------------------------------------------------------------------------------------
| STB      | 1 1 1 0 0 1 | S S S S | A A A A A A A A A A A A A A A A A A A A A A | Store byte from register to memory.                          |
-------------------------------------------------------------------------------------------------------------------------------------------------
| LDW      | 1 1 1 0 1 0 | D D D D | A A A A A A A A A A A A A A A A A A A A A A | Load word from memory to register.                           |
-------------------------------------------------------------------------------------------------------------------------------------------------
| STW      | 1 1 1 0 1 1 | S S S S | A A A A A A A A A A A A A A A A A A A A A A | Store word from register to memory.                          |
-------------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------------------------------------
|                                                   Load/Store (Offset) Instruction Opcodes:                                                    |
-------------------------------------------------------------------------------------------------------------------------------------------------
| Mnemonic | Opcode      | Dest.   | AddrReg | Offset                              | Description                                                |
-------------------------------------------------------------------------------------------------------------------------------------------------
|          | 31.......26 | 25...22 | 21...18 | 17................................0 |                                                            |
-------------------------------------------------------------------------------------------------------------------------------------------------
| LDB      | 1 1 1 1 0 0 | D D D D | A A A A | O O O O O O O O O O O O O O O O O O | Load byte from memory to register.                         |
-------------------------------------------------------------------------------------------------------------------------------------------------
| STB      | 1 1 1 1 0 1 | S S S S | A A A A | O O O O O O O O O O O O O O O O O O | Store byte from register to memory.                        |
-------------------------------------------------------------------------------------------------------------------------------------------------
| LDW      | 1 1 1 1 1 0 | D D D D | A A A A | O O O O O O O O O O O O O O O O O O | Load word from memory to register.                         |
-------------------------------------------------------------------------------------------------------------------------------------------------
| STW      | 1 1 1 1 1 1 | S S S S | A A A A | O O O O O O O O O O O O O O O O O O | Store word from register to memory.                        |
-------------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------------------------------------
|                                                         Special Instruction Opcodes:                                                          |
-------------------------------------------------------------------------------------------------------------------------------------------------
| Mnemonic | Opcode      | Value or Unused                                     | Description                                                    |
-------------------------------------------------------------------------------------------------------------------------------------------------
|          | 31.......26 | 25................................................0 |                                                                |
-------------------------------------------------------------------------------------------------------------------------------------------------
| STOP     | 0 0 0 0 0 0 | 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 | Stops program execution.                                       |
-------------------------------------------------------------------------------------------------------------------------------------------------
| PUSH     | 1 1 0 0 0 1 | V V V V V V V V V V V V V V V V V V V V V V V V V V | Pushes the value onto the stack.                               |
-------------------------------------------------------------------------------------------------------------------------------------------------
| Mnemonic | Opcode      | Result Size     | Arguments Size                      | Description                                                  |
-------------------------------------------------------------------------------------------------------------------------------------------------
|          | 31.......26 | 25...........18 | 17................................0 |                                                              |
-------------------------------------------------------------------------------------------------------------------------------------------------
| RET      | 1 1 0 0 0 0 | R R R R R R R R | A A A A A A A A A A A A A A A A A A | Pops R words from the stack and buffers them. Pops the cur-  |
|          |             |                 |                                     | rent stack frame and pops A argument words from the stack.   |
|          |             |                 |                                     | Then pushes the R words back onto the stack and restores     |
|          |             |                 |                                     | the 'lb' and 'pc' registers.                                 |
-------------------------------------------------------------------------------------------------------------------------------------------------
</pre>
\endhtmlonly
*/
class Instruction
{
    
    public:
        
        /* === Enumerations === */

        //! Instruction code type (32-bit unsigned integer).
        typedef unsigned int code_t;

        /**
        4 32-bit integral registers so that two 64-bit values can be used for computation.
        4 32-bit reserved registers.
        8 32-bit floating-pointer registers so that four 64-bit or two 128-bit values can be used for computation.
        */
        struct Registers
        {
            //! Register code type (8-bit unsigned integer).
            typedef unsigned char reg_t;

            enum : reg_t
            {
                i0 = 0x00, //!< Integral register 0.
                i1 = 0x01, //!< Integral register 1.
                i2 = 0x02, //!< Integral register 2.
                i3 = 0x03, //!< Integral register 3.

                cf = 0x04, //!< Conditional register: used for jump conditions.
                lb = 0x05, //!< Local base pointer: pointer to the current local stack base.
                sp = 0x06, //!< Stack-pointer.
                pc = 0x07, //!< Program-counter.

                f0 = 0x08, //!< Floating-point register 0.
                f1 = 0x09, //!< Floating-point register 1.
                f2 = 0x0a, //!< Floating-point register 2.
                f3 = 0x0b, //!< Floating-point register 3.
                f4 = 0x0c, //!< Floating-point register 4.
                f5 = 0x0d, //!< Floating-point register 5.
                f6 = 0x0e, //!< Floating-point register 6.
                f7 = 0x0f, //!< Floating-point register 7.
            };

            static const char* Name(const reg_t& Index);
        };

        struct OpCodes
        {
            //! Op-code type (8-bit unsigned integer).
            typedef unsigned char opcode_t;

            //! 2 register instruction op-codes.
            struct Reg2
            {
                enum : opcode_t
                {
                    MOV     = 0x01,
                    NOT     = 0x02,
                    AND     = 0x03,
                    OR      = 0x04,
                    XOR     = 0x05,
                    ADD     = 0x06,
                    SUB     = 0x07,
                    MUL     = 0x08,
                    DIV     = 0x09,
                    MOD     = 0x0a,
                    SLL     = 0x0b,
                    SLR     = 0x0c,
                    CMP     = 0x0d,
                    FTI     = 0x0e,
                    ITF     = 0x0f,
                };
            };

            //! 1 register instruction op-codes.
            struct Reg1
            {
                enum : opcode_t
                {
                    MOV     = 0x10,
                    AND     = 0x11,
                    OR      = 0x12,
                    XOR     = 0x13,
                    ADD     = 0x14,
                    SUB     = 0x15,
                    MUL     = 0x16,
                    DIV     = 0x17,
                    MOD     = 0x18,
                    SLL     = 0x19,
                    SLR     = 0x1a,
                    PUSH    = 0x1b,
                    POP     = 0x1c,
                    INC     = 0x1d,
                    DEC     = 0x1e,
                    //Unused = 0x01f,
                };
            };

            //! Jump instruction op-codes.
            struct Jump
            {
                enum : opcode_t
                {
                    JMP     = 0x20,
                    JE      = 0x21,
                    JNE     = 0x22,
                    JG      = 0x23,
                    JL      = 0x24,
                    JGE     = 0x25,
                    JLE     = 0x26,
                    CALL    = 0x27,
                    /*
                    Unused  = 0x28,
                    Unused  = 0x29,
                    Unused  = 0x2a,
                    Unused  = 0x2b,
                    Unused  = 0x2c,
                    Unused  = 0x2d,
                    Unused  = 0x2e,
                    Unused  = 0x2f,
                    */
                };
            };

            // Load/store instruction op-codes.
            struct Mem
            {
                enum : opcode_t
                {
                    LDB     = 0x38,
                    STB     = 0x39,
                    LDW     = 0x3a,
                    STW     = 0x3b,
                };
            };

            // Load/store (offset) instruction op-codes.
            struct MemOff
            {
                enum : opcode_t
                {
                    LDB     = 0x3c,
                    STB     = 0x3d,
                    LDW     = 0x3e,
                    STW     = 0x3f,
                };
            };

            //! Special instruction op-codes.
            struct Special
            {
                enum : opcode_t
                {
                    STOP    = 0x00,
                    RET     = 0x30,
                    PUSH    = 0x31,
                    /*
                    Unused  = 0x32,
                    Unused  = 0x33,
                    Unused  = 0x34,
                    Unused  = 0x35,
                    Unused  = 0x36,
                    Unused  = 0x37,
                    */
                };
            };

            static const char* Mnemonic(const opcode_t& OpCode);
        };

        /* === Constructors & destructor === */

        Instruction();
        Instruction(const code_t Code);
        ~Instruction();

        /* === Functions === */

        //! 'Back-patches' the address for a jump instruction.
        void BackPatchAddress(const unsigned int Address);

        //! Converts the 26-bit unsigned integer into a 32-bit signed integer.
        //int SgnAddress() const;
        int SgnValue26() const;

        //! Converts the 22-bit unsigned integer into a 32-bit signed integer.
        int SgnValue22() const;

        //! Converts the 18-bit unsigned integer into a 32-bit signed integer.
        //int SgnFlags() const;
        int SgnValue18() const;

        /**
        Returns the disassembled instruction as string.
        \note Data-fields can not be reproduced as instruction (e.g. "Math.PI: DATA.float 3.14").
        */
        std::string Disassemble() const;

        /* === Static functions === */

        //! Returns the format version for encoding.
        static unsigned int FormatVersion();

        /**
         * Creates a 2-register instruction.
         * \param[in] OpCode Specifies the 2-register instruction op-code.
         * \param[in] Reg1 Specifies the 1st operand register index.
         * \param[in] Reg2 Specifies the 2nd operand register index.
         * \param[in] Flags Specifies the instruction flags. Maximal bit-width: 18. By default 0.
         * \code
         * Mnemonic Reg1, Reg2
         * Mnemonic [Flags] Reg1, Reg2
         * \endcode
         * \see OpCodes::Reg2
         * \see Registers
         */
        static Instruction CreateReg2(
            const OpCodes::opcode_t OpCode, const Registers::reg_t Reg1, const Registers::reg_t Reg2, const unsigned int Flags = 0u
        );

        /**
         * Creates a 1-register instruction.
         * \param[in] OpCode Specifies the 2-register instruction op-code.
         * \param[in] Reg Specifies the 1st operand register index.
         * \param[in] Value Specifies the 2nd operand value. Maximal bit-width: 22. By default 0.
         * \code
         * Mnemonic Reg, Value
         * \endcode
         * \see OpCodes::Reg1
         * \see Registers
         */
        static Instruction CreateReg1(
            const OpCodes::opcode_t OpCode, const Registers::reg_t Reg, const unsigned int Value = 0u
        );

        /**
         * Creates a jump instruction.
         * \param[in] OpCode Specifies the 2-register instruction op-code.
         * \param[in] Reg Specifies the 1st operand register index.
         * \param[in] Offset Specifies the 2nd operand code address offset. Maximal bit-width: 22.
         * \code
         * Mnemonic (Reg) Offset
         * \endcode
         * \see OpCodes::Jump
         * \see Registers
         */
        static Instruction CreateJump(
            const OpCodes::opcode_t OpCode, const Registers::reg_t Reg, const unsigned int Offset
        );

        /**
         * Creates a load/store instruction.
         * \param[in] OpCode Specifies the load/store instruction op-code.
         * \param[in] Reg Specifies the 1st operand register index.
         * \param[in] Address Specifies the 2nd operand memory address. Maximal bit-width: 22.
         * \code
         * Mnemonic Reg, Address
         * \endcode
         * \see OpCodes::Mem
         * \see Registers
         */
        static Instruction CreateLdSt(
            const OpCodes::opcode_t OpCode, const Registers::reg_t Reg, const unsigned int Address
        );

        /**
         * Creates a load/store-offset instruction.
         * \param[in] OpCode Specifies the load/store-offset instruction op-code.
         * \param[in] Reg1 Specifies the 1st operand register index.
         * \param[in] Reg2 Specifies the 2nd operand register index.
         * \param[in] Offset Specifies the 3rd operand memory address offset (added to the 2nd operand register's value). Maximal bit-width: 18.
         * \code
         * Mnemonic Reg1, (Reg2) Offset
         * \endcode
         * \see OpCodes::MemOff
         * \see Registers
         */
        static Instruction CreateLdStOff(
            const OpCodes::opcode_t OpCode, const Registers::reg_t Reg1, const Registers::reg_t Reg2, const unsigned int Offset
        );

        /**
         * Creates a special instruction.
         * \param[in] OpCode Specifies the special instruction op-code.
         * \param[in] Value Specifies the operand value. Maximal bit-width: 26. By default 0.
         * \code
         * Mnemonic
         * Mnemonic Value
         * \endcode
         * \see OpCodes::Special
         * \see Registers
         */
        static Instruction CreateSpecial(const OpCodes::opcode_t OpCode, const unsigned int Value = 0u);

        /**
         * Creates (another) special instruction.
         * \param[in] OpCode Specifies the special instruction op-code.
         * \param[in] ResultSize Specifies the 1st operand result size (in words -> 4-bytes). Maximal bit-width: 8.
         * \param[in] ArgSize Specifies the 2nd operand arguments size (in words -> 4-bytes). Maximal bit-width: 18.
         * \code
         * Mnemonic (ResultSize) ArgSize
         * \endcode
         * \see OpCodes::Special
         * \see Registers
         */
        static Instruction CreateSpecial(
            const OpCodes::opcode_t OpCode, unsigned int ResultSize, unsigned int ArgSize
        );

        /* === Inline functions === */

        //! Returns the whole binary instruction code (32-bit).
        inline code_t Code() const
        {
            return Code_;
        }

        //! Returns the encoding for the instruction op-code.
        inline OpCodes::opcode_t OpCode() const
        {
            return (Code_ & 0xfc000000) >> 26;
        }

        /**
        Returns the encoding for the 26-bit value (or address) of a jump- or special instruction.
        \see OpCodes::Jump
        \see OpCodes::Special
        */
        //inline unsigned int Address() const
        inline unsigned int Value26() const
        {
            return (Code_ & 0x03ffffff);
        }

        /**
        Returns the encoding for the 22-bit value of a 1-register- or load/store instruction.
        \see OpCodes::Reg1
        \see OpCodes::Mem
        \see OpCodes::Jump
        */
        inline unsigned int Value22() const
        {
            return (Code_ & 0x003fffff);
        }

        /**
        Returns the encoding for the 18-bit value (or flags) of a 2-register-, load/store (with offset)- or special instruction.
        \see OpCodes::Reg2
        \see OpCodes::MemOff
        \see OpCodes::Special
        \see HasFlagLong
        */
        inline unsigned int Value18() const
        {
            return (Code_ & 0x0003ffff);
        }

        /**
        Returns the encoding for the 8-bit extra value of a special instruction.
        \see OpCodes::Special
        */
        inline unsigned int ExtraValue8() const
        {
            return (Code_ & 0x03fc0000) >> 18;
        }

        //! Returns the register encoding for the 1st operand of a 2-register instruction.
        inline Registers::reg_t Reg1() const
        {
            return (Code_ & 0x03c00000) >> 22;
        }
        //! Returns the register encoding for the 2nd operand of a 2-register instruction.
        inline Registers::reg_t Reg2() const
        {
            return (Code_ & 0x003c0000) >> 18;
        }

        //! Returns true if the 'long' flag is set in a 2-register instruction.
        inline bool HasFlagLong() const
        {
            return (Code_ & 0x00020000) != 0;
        }

        //! Returns true if the op-code refers to a jump instruction.
        inline bool IsJumpInstr() const
        {
            return ((Code_ & 0xe0000000) ^ 0x80000000) == 0;
        }
        //! Returns true if the op-code regers to a load/store instruction.
        inline bool IsLdStInstr() const
        {
            return ((Code_ & 0xf0000000) ^ 0xe0000000) == 0;
        }
        //! Returns true if the op-code regers to a load/store (with offset) instruction.
        inline bool IsLdStOffInstr() const
        {
            return ((Code_ & 0xf0000000) ^ 0xf0000000) == 0;
        }
        //! Returns true if the op-code refers to a special instruction.
        inline bool IsSpecialInstr() const
        {
            return ((Code_ & 0xf8000000) ^ 0xc0000000) == 0;
        }

    private:

        /* === Members === */
        
        code_t Code_; //!< 32-bit instruction code.

};


} // /namespace XVM


#endif



// ================================================================================
