/*
 * Heap manager header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_HEAP_MANAGER_H__
#define __XX_HEAP_MANAGER_H__


#include <map>


namespace XVM
{


//! Manager class for the virtual heap of the XieXie vitual-machine.
class HeapManager
{

    public:

        typedef unsigned int ptr_t;

        HeapManager();
        ~HeapManager();

        /**
        Allocates a new memory block with the specified size.
        \throws std::string If the virtual-machine runs out of memory.
        */
        ptr_t AllocMem(unsigned int Size);

        /**
        Deallocates the specified memory block.
        \throws std::string If the specified memory block has already been deleted.
        */
        void FreeMem(ptr_t Ptr);

        //! Clears the whole heap if memory leaks were detected and print a warning message if so.
        void ClearLeaks();

        //! Returns the whole used memory size.
        inline unsigned int UsedMemorySize() const
        {
            return MemSize_;
        }

    private:

        std::map<ptr_t, unsigned int> MemTable_;
        unsigned int MemSize_;

};


} // /namespace XVM


#endif



// ================================================================================