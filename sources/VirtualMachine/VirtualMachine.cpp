/*
 * Virtual machine file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "VirtualMachine.h"
#include "ConsoleOutput.h"
#include "StringMod.h"

#include <string.h>
#include <iostream>
#include <thread>
//#include <cmath>


namespace XVM
{


VirtualMachine::VirtualMachine() :
    ICmpFlags_      (0      ),
    InstrList_      (nullptr),
    InstrCounter_   (0      )
{
    /* Initialize registers */
    ResetRegisters();
}
VirtualMachine::~VirtualMachine()
{
}

VirtualMachine::err_t VirtualMachine::Execute(
    const ByteCode& Program, unsigned int Schedule, bool Reset)
{
    /* Reset execution state */
    if (InstrList_ == nullptr)
        Reset = true;

    if (Reset)
    {
        /* Get instruction list */
        InstrList_ = &(Program.Instructions());

        if (InstrList_->empty())
            return ErrorCodes::NullProgram;

        /* Get first instruction */
        Instr_ = InstrList_->front();

        /* Reset machine registers */
        ResetRegisters();
    }

    InstrCounter_ = 0;

    auto NumInstructions = InstrList_->size();

    try
    {
        /* Instruction execution loop */
        while (true)
        {
            /* Analyze current instruction */
            switch (Instr_.OpCode())
            {
                /* --- 2 register instructions --- */
                case Instruction::OpCodes::Reg2::MOV:
                    MOV(Instr_.Reg1(), Instr_.Reg2());
                    break;

                case Instruction::OpCodes::Reg2::NOT:
                    NOT(Instr_.Reg1(), Instr_.Reg2());
                    break;
                case Instruction::OpCodes::Reg2::AND:
                    AND(Instr_.Reg1(), Instr_.Reg2());
                    break;
                case Instruction::OpCodes::Reg2::OR:
                    OR(Instr_.Reg1(), Instr_.Reg2());
                    break;
                case Instruction::OpCodes::Reg2::XOR:
                    XOR(Instr_.Reg1(), Instr_.Reg2());
                    break;

                case Instruction::OpCodes::Reg2::ADD:
                    ADD(Instr_.Reg1(), Instr_.Reg2());
                    break;
                case Instruction::OpCodes::Reg2::SUB:
                    SUB(Instr_.Reg1(), Instr_.Reg2());
                    break;
                case Instruction::OpCodes::Reg2::MUL:
                    MUL(Instr_.Reg1(), Instr_.Reg2());
                    break;
                case Instruction::OpCodes::Reg2::DIV:
                    DIV(Instr_.Reg1(), Instr_.Reg2());
                    break;
                case Instruction::OpCodes::Reg2::MOD:
                    MOD(Instr_.Reg1(), Instr_.Reg2());
                    break;

                case Instruction::OpCodes::Reg2::SLL:
                    SLL(Instr_.Reg1(), Instr_.Reg2());
                    break;
                case Instruction::OpCodes::Reg2::SLR:
                    SLR(Instr_.Reg1(), Instr_.Reg2());
                    break;

                case Instruction::OpCodes::Reg2::CMP:
                    CMP(Instr_.Reg1(), Instr_.Reg2());
                    break;
                
                case Instruction::OpCodes::Reg2::FTI:
                    FTI(Instr_.Reg1(), Instr_.Reg2(), Instr_.HasFlagLong());
                    break;
                case Instruction::OpCodes::Reg2::ITF:
                    ITF(Instr_.Reg1(), Instr_.Reg2(), Instr_.HasFlagLong());
                    break;

                /* --- 1 register instructions --- */
                case Instruction::OpCodes::Reg1::MOV:
                    MOVC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                    
                case Instruction::OpCodes::Reg1::AND:
                    ANDC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Reg1::OR:
                    ORC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Reg1::XOR:
                    XORC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                    
                case Instruction::OpCodes::Reg1::ADD:
                    ADDC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Reg1::SUB:
                    SUBC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Reg1::MUL:
                    MULC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Reg1::DIV:
                    DIVC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Reg1::MOD:
                    MODC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                    
                case Instruction::OpCodes::Reg1::SLL:
                    SLLC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Reg1::SLR:
                    SLRC(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                    
                case Instruction::OpCodes::Reg1::PUSH:
                    PUSH(Instr_.Reg1());
                    break;
                case Instruction::OpCodes::Reg1::POP:
                    POP(Instr_.Reg1());
                    break;

                case Instruction::OpCodes::Reg1::INC:
                    INC(Instr_.Reg1());
                    break;
                case Instruction::OpCodes::Reg1::DEC:
                    DEC(Instr_.Reg1());
                    break;
                
                /* --- Jump instructions --- */
                case Instruction::OpCodes::Jump::JMP:
                    JMP(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Jump::JE:
                    JE(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Jump::JNE:
                    JNE(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Jump::JG:
                    JG(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Jump::JL:
                    JL(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Jump::JGE:
                    JGE(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Jump::JLE:
                    JLE(Instr_.Reg1(), Instr_.SgnValue22());
                    break;
                case Instruction::OpCodes::Jump::CALL:
                {
                    PushDynamicLink();

                    /* Call procedure */
                    auto Addr = Instr_.SgnValue22();

                    if (Addr >= IntrinsicAddresses::Reserved)
                    {
                        CallIntrinsic(Addr);
                        NextInstr();
                    }
                    else
                        CALL(Instr_.Reg1(), Addr);
                }
                break;

                /* --- Special instructions --- */
                case Instruction::OpCodes::Special::STOP:
                    return StopExecution();
                case Instruction::OpCodes::Special::RET:
                    RET(Instr_.ExtraValue8(), Instr_.Value18());
                    break;
                case Instruction::OpCodes::Special::PUSH:
                    PUSHC(Instr_.SgnValue26());
                    break;

                /* --- Load/store instructions --- */
                case Instruction::OpCodes::Mem::LDB:
                    LDB(Instr_.Reg1(), Instr_.Value22());
                    break;
                case Instruction::OpCodes::Mem::STB:
                    STB(Instr_.Reg1(), Instr_.Value22());
                    break;
                case Instruction::OpCodes::Mem::LDW:
                    LDW(Instr_.Reg1(), Instr_.Value22());
                    break;
                case Instruction::OpCodes::Mem::STW:
                    STW(Instr_.Reg1(), Instr_.Value22());
                    break;

                /* --- Load/store (offset) instructions --- */
                case Instruction::OpCodes::MemOff::LDB:
                    LDB(Instr_.Reg1(), Instr_.Reg2(), Instr_.SgnValue18());
                    break;
                case Instruction::OpCodes::MemOff::STB:
                    STB(Instr_.Reg1(), Instr_.Reg2(), Instr_.SgnValue18());
                    break;
                case Instruction::OpCodes::MemOff::LDW:
                    LDW(Instr_.Reg1(), Instr_.Reg2(), Instr_.SgnValue18());
                    break;
                case Instruction::OpCodes::MemOff::STW:
                    STW(Instr_.Reg1(), Instr_.Reg2(), Instr_.SgnValue18());
                    break;

                default:
                    return ErrorCodes::UnknownInstruction;
            }

            #if 0//!!!DEBUG!!!
            for (int i = 0; i < 8; ++i)
            {
                if (IReg_[i] < 0)
                {
                    __debugbreak();
                    break;
                }
            }
            #endif

            /* Get next instruction */
            if (!Instr_.IsJumpInstr())
                NextInstr();

            /* Quit program if last instruction has been executed */
            size_t InstrIndex = static_cast<size_t>(RegPC());

            if (InstrIndex >= NumInstructions)
                return StopExecution();

            /* Load new instruction */
            Instr_ = InstrList_->at(InstrIndex);
            
            /* Check for scheduling */
            ++InstrCounter_;
            if (Schedule > 0 && InstrCounter_ > Schedule)
                return ErrorCodes::Schedule;
        }
    }
    catch (const std::string& Err)
    {
        /* Print error message and return with 'exception' error code */
        ConsoleOutput::Error("Exception during execution");
        ConsoleOutput::Error("XVM -- " + Err);

        return ErrorCodes::Exception;
    }

    return ErrorCodes::NoError;
}

VirtualMachine::IntrinsicNameMapType VirtualMachine::EstablishIntrinsicNames()
{
    static const std::string Prefix = "Intr.";

    IntrinsicNameMapType Table;

    #define ADD_INTRINSIC(n) Table[Prefix + #n] = IntrinsicAddresses::n;

    /* Establish all intrinsic names */
    ADD_INTRINSIC( AllocMem )
    ADD_INTRINSIC( FreeMem  )
    ADD_INTRINSIC( CopyMem  )

    ADD_INTRINSIC( SysCall    )
    ADD_INTRINSIC( ClearTerm  )
    ADD_INTRINSIC( Print      )
    ADD_INTRINSIC( PrintLn    )
    ADD_INTRINSIC( PrintInt   )
    ADD_INTRINSIC( PrintFloat )
    ADD_INTRINSIC( InputInt   )
    ADD_INTRINSIC( InputFloat )
    ADD_INTRINSIC( Sleep      )

    ADD_INTRINSIC( CmpE     )
    ADD_INTRINSIC( CmpNE    )
    ADD_INTRINSIC( CmpL     )
    ADD_INTRINSIC( CmpLE    )
    ADD_INTRINSIC( CmpG     )
    ADD_INTRINSIC( CmpGE    )
    ADD_INTRINSIC( LogicOr  )
    ADD_INTRINSIC( LogicAnd )
    ADD_INTRINSIC( LogicNot )

    ADD_INTRINSIC( CreateFile )
    ADD_INTRINSIC( DeleteFile )
    ADD_INTRINSIC( OpenFile   )
    ADD_INTRINSIC( CloseFile  )
    ADD_INTRINSIC( FileSize   )
    ADD_INTRINSIC( FileSetPos )
    ADD_INTRINSIC( FileGetPos )
    ADD_INTRINSIC( FileEOF    )
    ADD_INTRINSIC( WriteByte  )
    ADD_INTRINSIC( WriteWord  )
    ADD_INTRINSIC( ReadByte   )
    ADD_INTRINSIC( ReadWord   )

    ADD_INTRINSIC( SinF  )
    ADD_INTRINSIC( SinD  )
    ADD_INTRINSIC( CosF  )
    ADD_INTRINSIC( CosD  )
    ADD_INTRINSIC( TanF  )
    ADD_INTRINSIC( TanD  )
    ADD_INTRINSIC( ASinF )
    ADD_INTRINSIC( ASinD )
    ADD_INTRINSIC( ACosF )
    ADD_INTRINSIC( ACosD )
    ADD_INTRINSIC( ATanF )
    ADD_INTRINSIC( ATanD )
    ADD_INTRINSIC( PowF  )
    ADD_INTRINSIC( PowD  )
    ADD_INTRINSIC( SqrtF )
    ADD_INTRINSIC( SqrtD )

    #undef ADD_INTRINSIC

    return Table;
}


/*
 * ======= Private: =======
 */

VirtualMachine::err_t VirtualMachine::StopExecution()
{
    Heap_.ClearLeaks();
    return ErrorCodes::NoError;
}

void VirtualMachine::ResetRegisters()
{
    /* Reset all 16 registers */
    memset(IReg_, 0, sizeof(regi_t)*16);
}

void VirtualMachine::CallIntrinsic(const int Addr)
{
    switch (Addr)
    {
        case IntrinsicAddresses::AllocMem:
        {
            auto Ptr = Heap_.AllocMem(StackFetchLB(-1));
            PUSHC(Ptr);
            RET(1, 1);
        }
        break;

        case IntrinsicAddresses::FreeMem:
            Heap_.FreeMem(StackFetchLB(-1));
            RET(0, 1);
            break;

        case IntrinsicAddresses::SysCall:
            system(GetStrPtrFromProcParam(-1));
            RET(0, 1);
            break;
            
        case IntrinsicAddresses::ClearTerm:
            #if 1//!temporary solution! -> make this OS specific
            if (system("cls"))
                system("clear");
            #endif
            RET(0, 0);
            break;
            
        case IntrinsicAddresses::Print:
            std::cout << GetStrPtrFromProcParam(-1);
            RET(0, 1);
            break;
            
        case IntrinsicAddresses::PrintLn:
            std::cout << GetStrPtrFromProcParam(-1) << std::endl;
            RET(0, 1);
            break;
            
        case IntrinsicAddresses::PrintInt:
            std::cout << StackFetchLB(-1);
            RET(0, 1);
            break;
            
        case IntrinsicAddresses::PrintFloat:
            std::cout << *reinterpret_cast<float*>(&(StackFetchLB(-1)));
            RET(0, 1);
            break;

        case IntrinsicAddresses::Sleep:
        {
            auto duration = StackFetchLB(-1);
            std::chrono::milliseconds sleepTime(duration);
            std::this_thread::sleep_for(sleepTime);
            RET(0, 1);
        }
        break;

        case IntrinsicAddresses::InputInt:
        {
            int value = 0;
            std::cin >> value;
            PUSHC(value);
            RET(1, 0);
        }
        break;
            
        case IntrinsicAddresses::CmpE:
            PUSHC((StackFetchLB(-1) == StackFetchLB(-2)) ? 1 : 0);
            RET(1, 2);
            break;
            
        case IntrinsicAddresses::CmpNE:
            PUSHC((StackFetchLB(-1) != StackFetchLB(-2)) ? 1 : 0);
            RET(1, 2);
            break;

        case IntrinsicAddresses::CmpL:
            PUSHC((StackFetchLB(-1) < StackFetchLB(-2)) ? 1 : 0);
            RET(1, 2);
            break;

        case IntrinsicAddresses::CmpLE:
            PUSHC((StackFetchLB(-1) <= StackFetchLB(-2)) ? 1 : 0);
            RET(1, 2);
            break;

        case IntrinsicAddresses::CmpG:
            PUSHC((StackFetchLB(-1) > StackFetchLB(-2)) ? 1 : 0);
            RET(1, 2);
            break;

        case IntrinsicAddresses::CmpGE:
            PUSHC((StackFetchLB(-1) >= StackFetchLB(-2)) ? 1 : 0);
            RET(1, 2);
            break;

        case IntrinsicAddresses::LogicOr:
            PUSHC((StackFetchLB(-1) != 0 || StackFetchLB(-2) != 0) ? 1 : 0);
            RET(1, 2);
            break;

        case IntrinsicAddresses::LogicAnd:
            PUSHC((StackFetchLB(-1) != 0 && StackFetchLB(-2) != 0) ? 1 : 0);
            RET(1, 2);
            break;

        case IntrinsicAddresses::LogicNot:
            PUSHC((StackFetchLB(-1) != 0) ? 0 : 1);
            RET(1, 1);
            break;

        //to be continued ...
        
        default:
        {
            MachineException("Unknown intrinsic procedure called");
        }
        break;
    }
}

void VirtualMachine::MachineException(const std::string& Message)
{
    /* Throw machine exception with information about the occurance position */
    throw (Message + " at instruction 0x" + xxNumToHex(RegPC()) + " after " + xxStr(InstrCounter_) + " cycles");
}

const void* VirtualMachine::GetPtrFromDataField(const unsigned int Addr) const
{
    return reinterpret_cast<const void*>(&InstrList_->at(Addr));
}

const char* VirtualMachine::GetStrPtrFromProcParam(const int StackOffset) const
{
    /* Get 'const char' pointer from stack with specified offset */
    auto ArgStringAddr = StackFetchLB(StackOffset);

    if (ArgStringAddr < VirtualMachine::MAX_INLINE_CODE_ADDR)
    {
        /* Return string address from data field */
        const void* RawPtr = GetPtrFromDataField(ArgStringAddr);
        auto StrPtr = reinterpret_cast<const char*>(RawPtr);
        return StrPtr;
    }
    else
    {
        /* Return string address from heap memory */

        //todo ...

    }

    return 0;
}

void VirtualMachine::PushDynamicLink()
{
    /*
    Store current local-base pointer (LB) on the stack,
    then set LB to the current stack-pointer.
    */
    const auto LB = RegLB();            // <-- Store dynamic link (current LB register value).
    RegLB() = RegSP();                  // <-- Set new stack base pointer (LB).
    PUSHC(LB);                          // <-- Push dynamic link (stored LB register value).

    /* Store current program-counter (PC) on the stack */
    PUSH(Instruction::Registers::pc);   // <-- Push return address (current PC register value).
}

void VirtualMachine::PopDynamicLink()
{
    POP(Instruction::Registers::pc);    // <-- Pop return address (previous PC register value).
    POP(Instruction::Registers::lb);    // <-- Pop dynamic link (previous LB register value).
}

/* --- 2 register instructions --- */

void VirtualMachine::MOV(const reg_index_t& Dst, const reg_index_t& Src)
{
    if (Dst < Instruction::Registers::f0)
    {
        if (Src < Instruction::Registers::f0)
            IReg_[Dst] = IReg_[Src];
        else
            IReg_[Dst] = *reinterpret_cast<const regi_t*>(&FReg_[Src]);
    }
    else
    {
        if (Src < Instruction::Registers::f0)
            FReg_[Dst] = *reinterpret_cast<const regf_t*>(&IReg_[Src]);
        else
            FReg_[Dst] = FReg_[Src];
    }
}

void VirtualMachine::NOT(const reg_index_t& Dst, const reg_index_t& Src)
{
    IReg_[Dst] = ~IReg_[Src];
}

void VirtualMachine::AND(const reg_index_t& Dst, const reg_index_t& Src)
{
    IReg_[Dst] &= IReg_[Src];
}

void VirtualMachine::OR(const reg_index_t& Dst, const reg_index_t& Src)
{
    IReg_[Dst] |= IReg_[Src];
}

void VirtualMachine::XOR(const reg_index_t& Dst, const reg_index_t& Src)
{
    IReg_[Dst] ^= IReg_[Src];
}

void VirtualMachine::ADD(const reg_index_t& Dst, const reg_index_t& Src)
{
    if (Dst < Instruction::Registers::f0)
        IReg_[Dst] += IReg_[Src];
    else
        FReg_[Dst] += FReg_[Src];
}

void VirtualMachine::SUB(const reg_index_t& Dst, const reg_index_t& Src)
{
    if (Dst < Instruction::Registers::f0)
        IReg_[Dst] -= IReg_[Src];
    else
        FReg_[Dst] -= FReg_[Src];
}

void VirtualMachine::MUL(const reg_index_t& Dst, const reg_index_t& Src)
{
    if (Dst < Instruction::Registers::f0)
        IReg_[Dst] *= IReg_[Src];
    else
        FReg_[Dst] *= FReg_[Src];
}

void VirtualMachine::DIV(const reg_index_t& Dst, const reg_index_t& Src)
{
    if (Dst < Instruction::Registers::f0)
    {
        /* Check for exception */
        if (IReg_[Src] == 0)
            MachineException("Division by zero");

        IReg_[Dst] /= IReg_[Src];
    }
    else
        FReg_[Dst] += FReg_[Src];
}

void VirtualMachine::MOD(const reg_index_t& Dst, const reg_index_t& Src)
{
    if (Dst < Instruction::Registers::f0)
    {
        /* Check for exception */
        if (IReg_[Src] == 0)
            MachineException("Division by zero");

        IReg_[Dst] %= IReg_[Src];
    }
}

void VirtualMachine::SLL(const reg_index_t& Dst, const reg_index_t& Src)
{
    IReg_[Dst] <<= IReg_[Src];
}

void VirtualMachine::SLR(const reg_index_t& Dst, const reg_index_t& Src)
{
    IReg_[Dst] >>= IReg_[Src];
}

void VirtualMachine::CMP(const reg_index_t& X, const reg_index_t& Y)
{
    //todo -> floats ... !!!
    ICmpFlags_ = (IReg_[X] - IReg_[Y]);
}

void VirtualMachine::FTI(const reg_index_t& Dst, const reg_index_t& Src, bool HasLongFlag)
{
    if (HasLongFlag)
    {
        /* Type-cast from 64-bit floating-pointer to 64-bit integer */
        double Flt = *reinterpret_cast<const double*>(&FReg_[Src]);
        *reinterpret_cast<int64_t*>(&IReg_[Dst]) = static_cast<int64_t>(Flt);
    }
    else
    {
        /* Type-cast from 32-bit floating-pointer to 32-bit integer */
        IReg_[Dst] = static_cast<regi_t>(FReg_[Src]);
    }
}

void VirtualMachine::ITF(const reg_index_t& Dst, const reg_index_t& Src, bool HasLongFlag)
{
    if (HasLongFlag)
    {
        /* Type-cast from 64-bit integer to 64-bit floating-pointer */
        int64_t Int = *reinterpret_cast<const int64_t*>(&IReg_[Src]);
        *reinterpret_cast<double*>(&FReg_[Dst]) = static_cast<double>(Int);
    }
    else
    {
        /* Type-cast from 32-bit integer to 32-bit floating-pointer */
        FReg_[Dst] = static_cast<regf_t>(IReg_[Src]);
    }
}

/* --- 1 register instructions --- */

void VirtualMachine::MOVC(const reg_index_t& Dst, const int Value)
{
    if (Dst < Instruction::Registers::f0)
        IReg_[Dst] = Value;
    else
        FReg_[Dst] = *reinterpret_cast<const float*>(GetPtrFromDataField(static_cast<unsigned int>(Value)));
}

void VirtualMachine::ANDC(const reg_index_t& Dst, const int Value)
{
    IReg_[Dst] &= Value;
}

void VirtualMachine::ORC(const reg_index_t& Dst, const int Value)
{
    IReg_[Dst] |= Value;
}

void VirtualMachine::XORC(const reg_index_t& Dst, const int Value)
{
    IReg_[Dst] ^= Value;
}

void VirtualMachine::ADDC(const reg_index_t& Dst, const int Value)
{
    IReg_[Dst] += Value;
}

void VirtualMachine::SUBC(const reg_index_t& Dst, const int Value)
{
    IReg_[Dst] -= Value;
}

void VirtualMachine::MULC(const reg_index_t& Dst, const int Value)
{
    IReg_[Dst] *= Value;
}

void VirtualMachine::DIVC(const reg_index_t& Dst, const int Value)
{
    /* Check for exception */
    if (Value == 0)
        MachineException("Division by zero");

    IReg_[Dst] /= Value;
}

void VirtualMachine::MODC(const reg_index_t& Dst, const int Value)
{
    /* Check for exception */
    if (Value == 0)
        MachineException("Division by zero");

    IReg_[Dst] %= Value;
}

void VirtualMachine::SLLC(const reg_index_t& Dst, const int Value)
{
    IReg_[Dst] <<= Value;
}

void VirtualMachine::SLRC(const reg_index_t& Dst, const int Value)
{
    IReg_[Dst] >>= Value;
}

void VirtualMachine::PUSH(const reg_index_t& Src)
{
    /* Check for exception */
    if (RegSP() > VirtualMachine::StackSize)
        MachineException("Stack overflow");

    /* Push value onto stack */
    if (Src < Instruction::Registers::f0)
        StackTop() = IReg_[Src];
    else
        StackTop() = *reinterpret_cast<const regi_t*>(&FReg_[Src]);

    /* Increment stack pointer */
    ++RegSP();
}

void VirtualMachine::POP(const reg_index_t& Dst)
{
    POP();

    /* Pop value from stack */
    if (Dst < Instruction::Registers::f0)
        IReg_[Dst] = StackTop();
    else
        FReg_[Dst] = *reinterpret_cast<const regf_t*>(&StackTop());
}

void VirtualMachine::POP()
{
    /* Check for exception */
    if (RegSP() <= 0)
        MachineException("Stack underflow");

    /* Decrement stack pointer */
    --RegSP();
}

void VirtualMachine::INC(const reg_index_t& Dst)
{
    if (Dst < Instruction::Registers::f0)
        ++IReg_[Dst];
    else
        ++FReg_[Dst];
}

void VirtualMachine::DEC(const reg_index_t& Dst)
{
    if (Dst < Instruction::Registers::f0)
        --IReg_[Dst];
    else
        --FReg_[Dst];
}

/* --- Jump instructions --- */

void VirtualMachine::JMP(const reg_index_t& Src, const int Offset)
{
    /* Setup program counter */
    RegPC() = IReg_[Src] + Offset;
}

void VirtualMachine::JE(const reg_index_t& Src, const int Offset)
{
    if (ICmpFlags_ == 0)
        RegPC() = IReg_[Src] + Offset;
    else
        NextInstr();
}

void VirtualMachine::JNE(const reg_index_t& Src, const int Offset)
{
    if (ICmpFlags_ != 0)
        RegPC() = IReg_[Src] + Offset;
    else
        NextInstr();
}

void VirtualMachine::JG(const reg_index_t& Src, const int Offset)
{
    if (ICmpFlags_ > 0)
        RegPC() = IReg_[Src] + Offset;
    else
        NextInstr();
}

void VirtualMachine::JL(const reg_index_t& Src, const int Offset)
{
    if (ICmpFlags_ < 0)
        RegPC() = IReg_[Src] + Offset;
    else
        NextInstr();
}

void VirtualMachine::JGE(const reg_index_t& Src, const int Offset)
{
    if (ICmpFlags_ >= 0)
        RegPC() = IReg_[Src] + Offset;
    else
        NextInstr();
}

void VirtualMachine::JLE(const reg_index_t& Src, const int Offset)
{
    if (ICmpFlags_ <= 0)
        RegPC() = IReg_[Src] + Offset;
    else
        NextInstr();
}

void VirtualMachine::CALL(const reg_index_t& Src, const int Offset)
{
    RegPC() = IReg_[Src] + Offset;
}

/* --- Special instructions --- */

void VirtualMachine::RET(const unsigned int ResultSize, const unsigned int ArgSize)
{
    /* Buffer and pop procedure result from the stack */
    if (ResultSize > 0)
    {
        if (RegSP() < static_cast<int>(ResultSize))
            MachineException("Stack underflow");

        for (unsigned int i = 0; i < ResultSize; ++i)
            TempStack_[i] = Stack_[RegSP() - i - 1];
    }

    /* Pop current stack frame */
    RegSP() = RegLB() + 2;

    /* Restore dynamic link */
    PopDynamicLink();

    /* Pop procedure argument from the stack */
    RegSP() -= ArgSize;

    /* Push procedure result back onto the stack */
    if (ResultSize > 0)
    {
        RegSP() += ResultSize;

        if (RegSP() > VirtualMachine::StackSize)
            MachineException("Stack overflow");

        for (unsigned int i = 0; i < ResultSize; ++i)
            Stack_[RegSP() - i - 1] = TempStack_[i];
    }
}

void VirtualMachine::PUSHC(const int Value)
{
    /* Check for exception */
    if (RegSP() == (VirtualMachine::StackSize - 1))
        MachineException("Stack overflow");

    /* Push value onto stack */
    StackTop() = Value;

    /* Increment stack pointer */
    ++RegSP();
}

/* --- Memory load/store instructions --- */

void VirtualMachine::LDB(const reg_index_t& Dst, const unsigned int Addr)
{
    //todo ...
}

void VirtualMachine::STB(const reg_index_t& Src, const unsigned int Addr)
{
    //todo ...
}

void VirtualMachine::LDW(const reg_index_t& Dst, const unsigned int Addr)
{
    //todo ...
}

void VirtualMachine::STW(const reg_index_t& Src, const unsigned int Addr)
{
    //todo ...
}

/* --- Memory load/store (offset) instructions --- */

void VirtualMachine::LDB(const reg_index_t& Dst, const reg_index_t& Addr, const int Offset)
{
    const char* StackAddr = nullptr;

    //!!!
    if (Addr == Instruction::Registers::lb)
        StackAddr = reinterpret_cast<const char*>( &(Stack_[RegLB()]) );
    else
        StackAddr = reinterpret_cast<const char*>(IReg_[Addr]);

    StackAddr += Offset;
    IReg_[Dst] = static_cast<regi_t>(*StackAddr);
}

void VirtualMachine::STB(const reg_index_t& Src, const reg_index_t& Addr, const int Offset)
{
    char* StackAddr = nullptr;

    //!!!
    if (Addr == Instruction::Registers::lb)
        StackAddr = reinterpret_cast<char*>( &(Stack_[RegLB()]) );
    else
        StackAddr = reinterpret_cast<char*>(IReg_[Addr]);

    StackAddr += Offset;
    *StackAddr = static_cast<char>(IReg_[Src]);
}

void VirtualMachine::LDW(const reg_index_t& Dst, const reg_index_t& Addr, const int Offset)
{
    const char* StackAddr = nullptr;

    //!!!
    if (Addr == Instruction::Registers::lb)
        StackAddr = reinterpret_cast<const char*>( &(Stack_[RegLB()]) );
    else
        StackAddr = reinterpret_cast<const char*>(IReg_[Addr]);

    StackAddr += Offset;

    auto DWordStackAddr = reinterpret_cast<const int*>(StackAddr);
    IReg_[Dst] = *DWordStackAddr;
}

void VirtualMachine::STW(const reg_index_t& Src, const reg_index_t& Addr, const int Offset)
{
    char* StackAddr = nullptr;

    //!!!
    if (Addr == Instruction::Registers::lb)
        StackAddr = reinterpret_cast<char*>( &(Stack_[RegLB()]) );
    else
        StackAddr = reinterpret_cast<char*>(IReg_[Addr]);

    StackAddr += Offset;

    auto DWordStackAddr = reinterpret_cast<int*>(StackAddr);
    *DWordStackAddr = IReg_[Src];
}


} // /namespace XVM



// ================================================================================