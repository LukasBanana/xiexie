/*
 * Virtual machine header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_VIRTUALMACHINE_H__
#define __XX_VIRTUALMACHINE_H__


#include "ByteCode.h"
#include "HeapManager.h"

#include <stack>
#include <map>


namespace XVM
{


/**
VirtualMachine (for "XieXie-VirtualMachine" or short "XVM") is the runtime environment for the XieXie byte code (XBC).
\see VirtualProgram
*/
class VirtualMachine
{
    
    public:
        
        /* === Enumerations === */

        typedef int err_t;

        //! Maximal address for inlined data fields inside the XieXie byte-code.
        static const unsigned MAX_INLINE_CODE_ADDR = 0x003fffff;

        /* === Structures === */

        //! Error codes of byte-code execution.
        struct ErrorCodes
        {
            enum : err_t
            {
                NoError             = 0,    //!< No error happend.
                Schedule            = 1,    //!< This is not directly an error. But the program has been stoped for scheduling. Continue execution manually.
                NullProgram         = -1,   //!< Program has no instructions.
                UnknownInstruction  = -2,   //!< Unknown instruction found in byte-code.
                Exception           = -3,   //!< An exception has been thrown.
            };
        };

        //! Reserved primitive procedure call addresses (Used for 'CALL' instruction).
        struct IntrinsicAddresses
        {
            typedef int addr_t;

            enum : addr_t
            {
                Reserved    = 0x001fff00, //!< Start address of reserved primitive procedures.

                AllocMem    = 0x001fff00, //!< void* AllocMem(uint sizeInBytes)
                FreeMem     = 0x001fff01, //!< void FreeMem(void* memoryAddress)
                CopyMem     = 0x001fff02, //!< void CopyMem(const void* srcMemAddr, void* dstMemAddr, uint sizeInBytes)

                SysCall     = 0x001fff20, //!< void SysCall(const byte* stringAddress)
                ClearTerm   = 0x001fff21, //!< void ClearTerm()
                Print       = 0x001fff22, //!< void Print(const byte* stringAddress)
                PrintLn     = 0x001fff23, //!< void PrintLn(const byte* stringAddress)
                PrintInt    = 0x001fff24, //!< void PrintInt(int value)
                PrintFloat  = 0x001fff25, //!< void PrintFloat(float value)
                InputInt    = 0x001fff26, //!< int InputInt()
                InputFloat  = 0x001fff27, //!< float InputFloat()
                Sleep       = 0x001fff28, //!< void Sleep(uint duration)

                CmpE        = 0x001fff30,
                CmpNE       = 0x001fff31,
                CmpL        = 0x001fff32,
                CmpLE       = 0x001fff33,
                CmpG        = 0x001fff34,
                CmpGE       = 0x001fff35,
                LogicOr     = 0x001fff36,
                LogicAnd    = 0x001fff37,
                LogicNot    = 0x001fff38,

                CreateFile  = 0x001fff40, //!< int CreateFile(const byte* stringAddress)
                DeleteFile  = 0x001fff41, //!< int DeleteFile(const byte* stringAddress)
                OpenFile    = 0x001fff42, //!< void* OpenFile(const byte* stringAddress)
                CloseFile   = 0x001fff43, //!< void CloseFile(void* fileHandle)
                FileSize    = 0x001fff44, //!< uint FileSize(const void* fileHandle)
                FileSetPos  = 0x001fff45, //!< void FileSetPos(const void* fileHandle, uint pos)
                FileGetPos  = 0x001fff46, //!< uint FileGetPos(const void* fileHandle)
                FileEOF     = 0x001fff47, //!< int FileEOF(const void* fileHandle)
                WriteByte   = 0x001fff48, //!< void WriteByte(const void* fileHandle, const void* memoryAddress)
                WriteWord   = 0x001fff49, //!< void WriteWord(const void* fileHandle, const void* memoryAddress)
                ReadByte    = 0x001fff4a, //!< void ReadByte(const void* fileHandle, void* memoryAddress)
                ReadWord    = 0x001fff4b, //!< void ReadWord(const void* fileHandle, void* memoryAddress)

                SinF        = 0x001fff80, //!< float Sin(float x)
                SinD        = 0x001fff81, //!< double Sin(double x)
                CosF        = 0x001fff82, //!< float Cos(float x)
                CosD        = 0x001fff83, //!< double Cos(double x)
                TanF        = 0x001fff84, //!< float Tan(float x)
                TanD        = 0x001fff85, //!< double Tan(double x)
                ASinF       = 0x001fff86, //!< float ASin(float x)
                ASinD       = 0x001fff87, //!< double ASin(double x)
                ACosF       = 0x001fff88, //!< float ACos(float x)
                ACosD       = 0x001fff89, //!< double ACos(double x)
                ATanF       = 0x001fff8a, //!< float ATan(float x)
                ATanD       = 0x001fff8b, //!< double ATan(double x)
                PowF        = 0x001fff8c, //!< float Pow(float base, float exp)
                PowD        = 0x001fff8d, //!< double Pow(double base, double exp)
                SqrtF       = 0x001fff8e, //!< float Sqrt(float x)
                SqrtD       = 0x001fff8f, //!< double Sqrt(double x)
            };
        };

        typedef std::map<std::string, IntrinsicAddresses::addr_t> IntrinsicNameMapType;

        /* === Constructors & destructor === */

        VirtualMachine();
        ~VirtualMachine();

        /* === Functions === */

        /**
        Executes the specified virtual program. The code must be provided in the XByteCode format.
        \param[in] Program Specifies the program byte code.
        \param[in] Schedule Specifies the execution scheuling. Use 0 to prevent scheduling.
        In this case the program will be interpreted in one (maybe large) step.
        Otherwise only 'Schedule'-number of instructions will be executed.
        \param[in] Reset Specifies whether the program-counter is to be reset or to be
        continued (from the last point of execution). Should be fasle, if scheduling is used
        and the program has already been started, i.e. only set this to true when you start
        a program from the beginning. By default false.
        \note If this virtual-machine runs a program the first time,
        the 'reset' parameter will be set to 'true' automatically.
        \see ByteCode
        \see ErrorCodes
        \return 0 on success otherwise an error number.
        */
        err_t Execute(const ByteCode& Program, unsigned int Schedule = 0, bool Reset = false);

        /* === Static functions === */

        static IntrinsicNameMapType EstablishIntrinsicNames();

        /* === Inline functions === */

        //! Returns the number of cycles since the last program execution.
        inline unsigned int NumCycles() const
        {
            return InstrCounter_;
        }

    private:
        
        /* === Types === */

        typedef Instruction::Registers::reg_t reg_index_t;

        typedef int regi_t;
        typedef float regf_t;

        static const unsigned int NumRegisters = 16;
        static const regi_t StackSize = 4096;
        static const regi_t TempStackSize = 255;

        /* === Functions === */

        err_t StopExecution();

        void ResetRegisters();

        void CallIntrinsic(const int Addr);

        void MachineException(const std::string& Message);

        const void* GetPtrFromDataField(const unsigned int Addr) const;
        const char* GetStrPtrFromProcParam(const int StackOffset) const;

        void PushDynamicLink();
        void PopDynamicLink();

        /* --- 2 register instructions --- */

        void MOV    (const reg_index_t& Dst, const reg_index_t& Src);

        void NOT    (const reg_index_t& Dst, const reg_index_t& Src);
        void AND    (const reg_index_t& Dst, const reg_index_t& Src);
        void OR     (const reg_index_t& Dst, const reg_index_t& Src);
        void XOR    (const reg_index_t& Dst, const reg_index_t& Src);

        void ADD    (const reg_index_t& Dst, const reg_index_t& Src);
        void SUB    (const reg_index_t& Dst, const reg_index_t& Src);
        void MUL    (const reg_index_t& Dst, const reg_index_t& Src);
        void DIV    (const reg_index_t& Dst, const reg_index_t& Src);
        void MOD    (const reg_index_t& Dst, const reg_index_t& Src);

        void SLL    (const reg_index_t& Dst, const reg_index_t& Src);
        void SLR    (const reg_index_t& Dst, const reg_index_t& Src);

        void CMP    (const reg_index_t& X, const reg_index_t& Y);

        void FTI    (const reg_index_t& Dst, const reg_index_t& Src, bool HasLongFlag);
        void ITF    (const reg_index_t& Dst, const reg_index_t& Src, bool HasLongFlag);

        /* --- 1 register instructions (with constant value, e.g. "MOVC" for "Move-Constant") --- */

        void MOVC   (const reg_index_t& Dst, const int Value);

        void ANDC   (const reg_index_t& Dst, const int Value);
        void ORC    (const reg_index_t& Dst, const int Value);
        void XORC   (const reg_index_t& Dst, const int Value);

        void ADDC   (const reg_index_t& Dst, const int Value);
        void SUBC   (const reg_index_t& Dst, const int Value);
        void MULC   (const reg_index_t& Dst, const int Value);
        void DIVC   (const reg_index_t& Dst, const int Value);
        void MODC   (const reg_index_t& Dst, const int Value);

        void SLLC   (const reg_index_t& Dst, const int Value);
        void SLRC   (const reg_index_t& Dst, const int Value);

        void PUSH   (const reg_index_t& Src);
        void POP    (const reg_index_t& Dst);
        void POP    ();

        void INC    (const reg_index_t& Dst);
        void DEC    (const reg_index_t& Dst);

        /* --- Jump instructions --- */

        void JMP    (const reg_index_t& Src, const int Offset);
        void JE     (const reg_index_t& Src, const int Offset);
        void JNE    (const reg_index_t& Src, const int Offset);
        void JG     (const reg_index_t& Src, const int Offset);
        void JL     (const reg_index_t& Src, const int Offset);
        void JGE    (const reg_index_t& Src, const int Offset);
        void JLE    (const reg_index_t& Src, const int Offset);
        void CALL   (const reg_index_t& Src, const int Offset);

        /* --- Basic instructions --- */

        void RET    (const unsigned int ResultSize, const unsigned int ArgSize);
        void PUSHC  (const int Value);

        /* --- Memory load/store instructions --- */

        void LDB    (const reg_index_t& Dst, const unsigned int Addr);
        void STB    (const reg_index_t& Src, const unsigned int Addr);
        void LDW    (const reg_index_t& Dst, const unsigned int Addr);
        void STW    (const reg_index_t& Src, const unsigned int Addr);
        
        /* --- Memory load/store (offset) instructions --- */

        void LDB    (const reg_index_t& Dst, const reg_index_t& Addr, const int Offset);
        void STB    (const reg_index_t& Src, const reg_index_t& Addr, const int Offset);
        void LDW    (const reg_index_t& Dst, const reg_index_t& Addr, const int Offset);
        void STW    (const reg_index_t& Src, const reg_index_t& Addr, const int Offset);
        
        /* === Inline functions === */
        
        //! Conditional flags register.
        inline regi_t& RegCF()
        {
            return IReg_[Instruction::Registers::cf];
        }
        //! Conditional flags register.
        inline const regi_t& RegCF() const
        {
            return IReg_[Instruction::Registers::cf];
        }

        //! Local base pointer register.
        inline regi_t& RegLB()
        {
            return IReg_[Instruction::Registers::lb];
        }
        //! Local base pointer register.
        inline const regi_t& RegLB() const
        {
            return IReg_[Instruction::Registers::lb];
        }

        //! Stack pointer register.
        inline regi_t& RegSP()
        {
            return IReg_[Instruction::Registers::sp];
        }
        //! Stack pointer register.
        inline const regi_t& RegSP() const
        {
            return IReg_[Instruction::Registers::sp];
        }

        //! Program counter register.
        inline regi_t& RegPC()
        {
            return IReg_[Instruction::Registers::pc];
        }
        //! Program counter register.
        inline const regi_t& RegPC() const
        {
            return IReg_[Instruction::Registers::pc];
        }

        inline regi_t& StackTop()
        {
            return Stack_[RegSP()];
        }
        inline const regi_t& StackTop() const
        {
            return Stack_[RegSP()];
        }

        //! Fetches an entry from the stack with the local-base pointer (LB) plus the specified offset (in 32-bit intervals).
        inline regi_t& StackFetchLB(int Offset)
        {
            return Stack_[RegLB() + Offset];
        }
        inline const regi_t& StackFetchLB(int Offset) const
        {
            return Stack_[RegLB() + Offset];
        }
        
        //! Get next instruction by incrementing the program-counter register.
        inline void NextInstr()
        {
            /* Increment program counter */
            ++RegPC();
        }

        /* === Members === */
        
        //! Comparision falgs.
        //!TODO! -> change this to a real 'flag' member.
        regi_t ICmpFlags_;

        //! Runtime registers.
        union
        {
            regi_t IReg_[NumRegisters];
            regf_t FReg_[NumRegisters];
        };

        Instruction Instr_;
        const std::vector<Instruction>* InstrList_;

        unsigned int InstrCounter_;

        regi_t Stack_[StackSize];           //!< Global stack.
        regi_t TempStack_[TempStackSize];   //!< Temporary stack (used for RET instruction).

        HeapManager Heap_;                  //!< Global heap memory manager.

};


} // /namespace XVM


#endif



// ================================================================================