/*
 * Byte code header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_BYTECODE_H__
#define __XX_BYTECODE_H__


#include "Instruction.h"

#include <vector>
#include <memory>
#include <string>


namespace XVM
{


class ByteCode
{
    
    public:
        
        /* === Enumerations === */

        //! Modes for generating disassembled XASM code.
        enum class DisassembleModes
        {
            NoLineNumbers,  //!< Do not add lines numbers.
            DecLineNumbers, //!< Add decimal line numbers.
            HexLineNumbers, //!< Add hexa-decimal line numbers.
        };

        /* === Constructors & destructor === */

        ByteCode();
        ~ByteCode();

        /* === Functions === */

        void AddInstruction(const Instruction& Instr);
        void Clear();

        bool ReadFromFile(const std::string& Filename);
        bool WriteToFile(const std::string& Filename);

        /**
        Disassembles this byte-code program and writes the output into the specified file.
        \see DisassembleModes
        */
        bool Disassemble(const std::string& Filename, const DisassembleModes Mode = DisassembleModes::NoLineNumbers);

        /* === Static functions === */

        //! Returns the magic number for the byte-code file format.
        static unsigned int MagicNumber();

        /* === Inline functions === */

        inline const std::vector<Instruction>& Instructions() const
        {
            return InstrList_;
        }
        inline std::vector<Instruction>& Instructions()
        {
            return InstrList_;
        }

        inline size_t NumInstructions() const
        {
            return InstrList_.size();
        }

    private:
        
        /* === Members === */
        
        std::vector<Instruction> InstrList_;

};

typedef std::shared_ptr<ByteCode> ByteCodePtr;


} // /namespace XVM


#endif



// ================================================================================