/*
 * Byte code file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ByteCode.h"
#include "ConsoleOutput.h"
#include "StringMod.h"

#include <fstream>


namespace XVM
{


/*
 * Internal functions
 */

static inline unsigned int ReadUInt(std::fstream& Stream)
{
    unsigned int Value;
    Stream.read(reinterpret_cast<char*>(&Value), sizeof(Value));
    return Value;
}

static inline void WriteUInt(std::fstream& Stream, unsigned int Value)
{
    Stream.write(reinterpret_cast<const char*>(&Value), sizeof(Value));
}


/*
 * ByteCode class
 */

ByteCode::ByteCode()
{
}
ByteCode::~ByteCode()
{
}

void ByteCode::AddInstruction(const Instruction& Instr)
{
    InstrList_.push_back(Instr);
}

void ByteCode::Clear()
{
    InstrList_.clear();
}

bool ByteCode::ReadFromFile(const std::string& Filename)
{
    /* Information output */
    ConsoleOutput::Message("Reading byte-code from file \"" + Filename + "\"");
    ScopedIndent Unused;

    /* Open file for reading */
    std::fstream File(Filename.c_str(), std::ios_base::in | std::ios_base::binary);

    if (!File.good())
    {
        ConsoleOutput::Error("Could not read file");
        return false;
    }

    /* Read magic number and format version */
    auto Magic = ReadUInt(File);

    if (Magic != ByteCode::MagicNumber())
    {
        ConsoleOutput::Error("Wrong magic number");
        return false;
    }

    auto Version = ReadUInt(File);

    if (Version != Instruction::FormatVersion())
    {
        ConsoleOutput::Error("Unsupported format version");
        return false;
    }

    /* Read instructions */
    auto Num = static_cast<size_t>(ReadUInt(File));

    if (Num == 0)
    {
        InstrList_.clear();
        return true;
    }

    InstrList_.resize(Num);

    for (size_t i = 0; i < Num; ++i)
    {
        auto Code = ReadUInt(File);
        InstrList_[i] = Instruction(Code);
    }

    return true;
}

bool ByteCode::WriteToFile(const std::string& Filename)
{
    /* Information output */
    ConsoleOutput::Message("Writing byte-code to file \"" + Filename + "\"");
    ScopedIndent Unused;

    /* Create file for writing */
    std::fstream File(Filename.c_str(), std::ios_base::out | std::ios_base::binary);

    if (!File.good())
    {
        ConsoleOutput::Error("Could not create file");
        return false;
    }
    
    /* Write magic number and format version */
    WriteUInt(File, ByteCode::MagicNumber());
    WriteUInt(File, Instruction::FormatVersion());

    /* Write instructions */
    unsigned int Num = static_cast<unsigned int>(InstrList_.size());
    WriteUInt(File, Num);

    for (const Instruction& Instr : InstrList_)
        WriteUInt(File, Instr.Code());

    return true;
}

bool ByteCode::Disassemble(const std::string& Filename, const DisassembleModes Mode)
{
    /* Information output */
    ConsoleOutput::Message("Disassemble byte-code to file \"" + Filename + "\"");
    ScopedIndent Unused;

    /* Create file for writing */
    std::fstream File(Filename.c_str(), std::ios_base::out);

    if (!File.good())
    {
        ConsoleOutput::Error("Could not create file");
        return false;
    }

    File << "; Disassembled file (Data fields can not be reproduced!)" << std::endl;
    
    /* Write disassembled instructions to file */
    size_t i = 0;
    const size_t Num = InstrList_.size();

    for (const auto& Instr : InstrList_)
    {
        if (Mode == DisassembleModes::DecLineNumbers)
            File << xxNumberOffset(i++, Num, '0') << "  ";
        else if (Mode == DisassembleModes::HexLineNumbers)
            File << "0x" << xxNumberOffset(i++, Num, '0', 16) << "  ";

        File << Instr.Disassemble() << std::endl;
    }

    return true;
}

unsigned int ByteCode::MagicNumber()
{
    return *reinterpret_cast<const unsigned int*>("XBCF");
}


} // /namespace XVM



// ================================================================================