/*
 * Counter header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_COUNTER_H__
#define __XX_COUNTER_H__


//! Simple counter class. Initializes a counter 
struct Counter
{
    typedef unsigned int DataType;

    Counter() :
        Value(0)
    {
    }

    inline operator DataType () const
    {
        return Value;
    }
    inline DataType operator ++ ()
    {
        return ++Value;
    }
    inline DataType operator ++ (int)
    {
        return Value++;
    }

    /* Members */
    DataType Value;
};


#endif



// ================================================================================