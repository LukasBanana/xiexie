/*
 * Long number file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "LongNumber.h"


LongNumber::LongNumber() :
    IsReal_     (false  ),
    Integral_   (0      ),
    Real_       (0.0    )
{
}
LongNumber::LongNumber(const integral_t& Value) :
    IsReal_     (false                      ),
    Integral_   (Value                      ),
    Real_       (static_cast<real_t>(Value) )
{
}
LongNumber::LongNumber(const real_t& Value) :
    IsReal_     (true                           ),
    Integral_   (static_cast<integral_t>(Value) ),
    Real_       (Value                          )
{
}
LongNumber::LongNumber(const LongNumber& Other) :
    IsReal_     (Other.IsReal_  ),
    Integral_   (Other.Integral_),
    Real_       (Other.Real_    )
{
}
LongNumber::~LongNumber()
{
}

LongNumber& LongNumber::ToReal()
{
    if (!IsReal_)
    {
        IsReal_ = true;
        Real_ = static_cast<real_t>(Integral_);
    }
    return *this;
}

LongNumber& LongNumber::operator += (LongNumber Other)
{
    Update(Other, IsReal_ || Other.IsReal_);

    if (IsReal_)
        Real_ += Other.Real_;
    else
        Integral_ += Other.Integral_;

    return *this;
}

LongNumber& LongNumber::operator -= (LongNumber Other)
{
    Update(Other, IsReal_ || Other.IsReal_);

    if (IsReal_)
        Real_ -= Other.Real_;
    else
        Integral_ -= Other.Integral_;

    return *this;
}

LongNumber& LongNumber::operator *= (LongNumber Other)
{
    Update(Other, IsReal_ || Other.IsReal_);

    if (IsReal_)
        Real_ *= Other.Real_;
    else
        Integral_ *= Other.Integral_;

    return *this;
}

LongNumber& LongNumber::operator /= (LongNumber Other)
{
    Update(Other, true);
    Real_ /= Other.Real_;
    return *this;
}

void LongNumber::Update(LongNumber& Other, bool SetToReal)
{
    if (SetToReal)
    {
        if (!IsReal_)
        {
            IsReal_ = true;
            Real_ = static_cast<real_t>(Integral_);
        }
        if (!Other.IsReal_)
        {
            Other.IsReal_ = true;
            Other.Real_ = static_cast<real_t>(Other.Integral_);
        }
    }
}

LongNumber operator + (const LongNumber& Left, const LongNumber& Right)
{
    LongNumber Result = Left;
    Result += Right;
    return Result;
}

LongNumber operator - (const LongNumber& Left, const LongNumber& Right)
{
    LongNumber Result = Left;
    Result -= Right;
    return Result;
}

LongNumber operator * (const LongNumber& Left, const LongNumber& Right)
{
    LongNumber Result = Left;
    Result *= Right;
    return Result;
}

LongNumber operator / (const LongNumber& Left, const LongNumber& Right)
{
    LongNumber Result = Left;
    Result /= Right;
    return Result;
}



// ================================================================================