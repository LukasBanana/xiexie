/*
 * Version header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_VERSION_H__
#define __XX_VERSION_H__


#include <string>


/* === Version macros === */

#define __XIEXIE_COMPILER__

#define XIEXIE_VERSION_MAJOR    0
#define XIEXIE_VERSION_MINOR    1
#define XIEXIE_VERSION_REVISION 4
#define XIEXIE_VERSION_STATUS   "Alpha"


/* === Functions === */

std::string xxGetVersion();


#endif



// ================================================================================