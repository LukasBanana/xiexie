/*
 * Console output file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ConsoleOutput.h"
#include "Version.h"
#include "StringMod.h"
#include "ErrorReporter.h"
#include "Frame.h"
#include "ConsoleManip.h"
#include "CompilerOptions.h"

#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>


namespace ConsoleOutput
{


using namespace Platform;
using namespace ConsoleManip;

/*
 * Internal objects
 */

static const std::string INDENT_MODIFIFER = "  ";

struct ConsoleState
{
    std::string Indent;                 //!< Current indentation string.
    std::shared_ptr<Frame> AppFrame;    //!< Optional window frame.
    std::string QueuedLine;             //!< Temporary queued line string.

    inline bool HasFrame() const
    {
        return AppFrame != nullptr;
    }
};

static ConsoleState State;


/*
 * Functions
 */

void OpenWindow()
{
    if (!State.HasFrame())
    {
        try
        {
            State.AppFrame = std::make_shared<Frame>("XieXie Debug Output");
        }
        catch (const std::string& Err)
        {
            State.AppFrame = nullptr;
            Error(Err);
        }
    }
}

void CloseWindow()
{
    if (State.HasFrame())
        State.AppFrame = nullptr;
}

void UpdateWindow()
{
    while (State.HasFrame() && State.AppFrame->IsOpen())
        State.AppFrame->UpdateEvents();
}

bool CheckForDefaultParameter(const std::string& Param)
{
    if (Param == "-v" || Param == "--version")
        PrintVersion();
    else if (Param == "-h" || Param == "--help")
        PrintHelp();
    else
        return false;
    return true;
}

void PrintInfo()
{
    PrintVersion();
    std::cout << INDENT_MODIFIFER << "This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'."   << std::endl;
    std::cout << INDENT_MODIFIFER << "This is free software, and you are welcome to redistribute it"                << std::endl;
    std::cout << INDENT_MODIFIFER << "under certain conditions; type `show c' for details."                         << std::endl;
    std::cout << std::endl;
}

void PrintVersion()
{
    std::cout << std::endl;
    std::cout << INDENT_MODIFIFER << "XieXie-Compiler " << xxGetVersion() << std::endl;
    std::cout << INDENT_MODIFIFER << "Copyright (C) 2013 Lukas Hermanns" << std::endl;
    std::cout << std::endl;
}

void PrintHelp()
{
    struct HelpEntry
    {
        std::string Cmd;
        std::string Docu;
    };
    
    std::vector<HelpEntry> Entries;
    size_t MaxLineLen = 0;

    /* Define all lambdas as temporary command line function */
    auto HeadLine = [&](const std::string& Desc)
    {
        HelpEntry Entry = { Desc, "" };
        Entries.push_back(Entry);
    };

    auto NL = [&](const std::string& Cmd, const std::string& Docu)
    {
        HelpEntry Entry = { Cmd, Docu };
        
        Entries.push_back(Entry);

        if (MaxLineLen < Cmd.size())
            MaxLineLen = Cmd.size();
    };

    auto Blank = [&]()
    {
        Entries.push_back(HelpEntry());
    };

    auto Space = [&](const std::string& Str) -> std::string
    {
        return " " + std::string(MaxLineLen - Str.size() + 3, '.') + " ";
    };

    auto PrintCmdHelp = [&](const HelpEntry& Entry)
    {
        if (Entry.Cmd.empty())
            std::cout << std::endl;
        else if (Entry.Docu.empty())
            std::cout << INDENT_MODIFIFER << Entry.Cmd << std::endl;
        else
            std::cout << INDENT_MODIFIFER << Entry.Cmd << Space(Entry.Cmd) << Entry.Docu << std::endl;
    };

    /* Setup command line help output */
    Blank();
    HeadLine("Usage: xxc [options] [-f FILE]");
    NL("  -f FILE, --file FILE",                "Compile specified XieXie source file."                             );
    Blank();

    HeadLine("Usage: xxc [options] [-asm FILE]");
    NL("  -asm FILE, --assemble FILE",          "Assemble specified XASM (XieXie Assembler) source file."           );
    Blank();

    HeadLine("Usage: xxc [options] [-dasm FILE]");
    NL("  -dasm FILE, --disassemble FILE",      "Disassemble specified XBC (XieXie Byte-Code) file."                );
    Blank();

    HeadLine("Usage: xxc [options] [-xvm FILE]");
    NL("  -xvm FILE, --virtual-machine FILE",   "Executes specified XieXie byte-code in the XieXie virtual machine.");
    Blank();

    HeadLine("Usage: xxc [options] [-run FILE]");
    NL("  -run FILE",                           "Compile, Assemble and Execute source file."                        );
    Blank();

    HeadLine("Usage: xxc [options] [-e [EXPR]]");
    NL("  -e [EXPR], --expr [EXPR]",            "Evaluate specified XieXie expression or enter expression."         );
    Blank();

    HeadLine("Compiler Info:");
    NL("  -v, --version",                       "Get XieXie-Compiler version."                                      );
    NL("  -i, --info",                          "Show info text."                                                   );
    NL("  -h, --help",                          "Show this help text."                                              );
    NL("  -k, --keywords",                      "Show all reserved keywords."                                       );
    Blank();

    HeadLine("Config:");
    NL("  -p, --pause",                         "Pause this application after execution."                           );
    NL("  -se, --simp-expr",                    "Simplified expressions, e.g. \"---3\" is allowed."                 );
    NL("  -c98",                                "Disables C++11 code-generation and uses C++98."                    );
    NL("  -Wall, --warn-all",                   "Enables all warning compilation options."                          );
    NL("  -gen-ASM",                            "Generates code for assembler (XASM) instead of C++."               );
    NL("  -O, --optimize",                      "Enables optimization."                                             );
    NL("  -D, --debug",                         "Inserts lot of debug information into output code."                );
    NL("  -imm",                                "Immediate output for evaluated expressions."                       );
    Blank();

    HeadLine("Extended Code Generation:");
    NL("  --no-expr-bloat",                     "Disables expression bloating (e.g. \"a+b\" instead of \"a + b\")." );
    NL("  --no-comments",                       "Disables generation of commentaries."                              );
    NL("  --no-blanks",                         "Disables generation of blank lines."                               );
    NL("  --no-indent",                         "Disables code indentation."                                        );
    NL("  --no-classic-guard",                  "Disables classic header guards (\"#pragma once\" is used instead).");
    NL("  --use-guard-index",                   "Uses indices for the header guard instead of names."               );
    NL("  --no-ext-math-expr",                  "Disables extended math expressions (e.g. \"a<b<c\" is forbidden)." );
    NL("  --obfuscate",                         "Enables code obfuscation."                                         );
    Blank();

    HeadLine("Expert Modes (You should seriously know what you are doing):");
    NL("  --auto-cref",                         "Enables automatic constant reference generation for parameters."   );
    Blank();

    HeadLine("Debug Output:");
    NL("  -w, --window",                        "Opens a window frame for debug output."                            );
    NL("  -ast",                                "Show AST as debug output."                                         );
    NL("  -nv-table",                           "Show name-visibility table as debug output."                       );
    Blank();

    /* Print final command line help output */
    for (auto Entry : Entries)
        PrintCmdHelp(Entry);
}

void PrintKeywords()
{
    /* Static string of all keywords */
    static const std::string Keywords =
        "import include return if else and or not switch case default do while for until forever foreach next class pattern "
        "public protect package static stop break this null true false abstract virtual extends new try catch "
        "throw operator init release enum flags sync copy readonly decl valid alias use lambda assert private ";

    static const std::string TDenoterKeywords =
        "auto void const bool byte ubyte short ushort int uint long ulong float double string wstring proc ";

    static const std::string ExtraKeywords =
        "cpp once pushcfg popcfg ";

    /* Lambda function to print the specified keyword list */
    auto PrintList = [](const std::string& Str)
    {
        /* Sort keywords list alphabetic */
        std::string::size_type Pos = 0, PrevPos = 0;
        std::vector<std::string> List;

        while ( ( Pos = Str.find(' ', PrevPos) ) != std::string::npos)
        {
            List.push_back(Str.substr(PrevPos, Pos - PrevPos));
            PrevPos = Pos + 1;
        }

        std::sort(List.begin(), List.end());

        /* Print sorted keyword list */
        for (const auto& KWrd : List)
            std::cout << "  " << KWrd << std::endl;
    };


    /* Print information about all reserved keywords */
    std::cout << "Reserved Keywords:" << std::endl;
    PrintList(Keywords);

    {
        std::cout << std::endl << "Type Denoters:" << std::endl;
        ScopedColor Unused(Colors::Cyan);
        PrintList(TDenoterKeywords);
    }

    {
        std::cout << std::endl << "Extra Keywords:" << std::endl;
        ScopedColor Unused(Colors::Yellow);
        PrintList(ExtraKeywords);
    }
}

void PrintErrorReport(ErrorReporter &Reporter)
{
    if (Reporter.HasMessages())
    {
        /* Print information about the number of errors and warnings */
        size_t NumErrors = 0, NumWarnings = 0;
        Reporter.CountCategories(NumErrors, NumWarnings);

        const auto ReportHead = xxStr(NumErrors) + " error(s), " + xxStr(NumWarnings) + " warning(s) occured:";

        Message(ReportHead);
        Message(std::string(ReportHead.size(), '-'));

        /* Print error- and warning messages */
        CompilerMessage Msg;
        while (Reporter.Pop(Msg))
            Message(Msg);
    }
}

void StartNL()
{
    if (State.HasFrame())
        State.QueuedLine += State.Indent;
    else
        std::cout << State.Indent;
}

void Append(const std::string& Msg)
{
    if (State.HasFrame())
        State.QueuedLine += Msg;
    else
        std::cout << Msg;
}

void EndNL()
{
    if (State.HasFrame())
    {
        State.AppFrame->AddEntry(State.QueuedLine);
        State.QueuedLine.clear();
    }
    else
        std::cout << std::endl;
}

void Message(const std::string &Msg)
{
    StartNL();
    Append(Msg);
    EndNL();
}

void Warning(const std::string &Msg, bool AppendPrefix)
{
    ScopedColor Unused(Colors::Yellow);

    if (AppendPrefix)
        Message("WARNING >> " + Msg);
    else
        Message(Msg);
}

void Error(const std::string &Msg, bool Fatal, bool AppendPrefix)
{
    PushAttrib();

    if (Fatal)
        SetColor(Colors::Black, Colors::Red | Colors::Intens);
    else
        SetColor(Colors::Red | Colors::Intens);

    if (AppendPrefix)
        Message("ERROR >> " + Msg);
    else
        Message(Msg);

    PopAttrib();
}

void Success(const std::string &Msg)
{
    ScopedColor Unused(Colors::Green | Colors::Intens);
    Message(Msg);
}

void Message(const CompilerMessage& Msg)
{
    switch (Msg.Category())
    {
        case CompilerMessage::Categories::Message:
            Message(Msg.Message());
            break;

        case CompilerMessage::Categories::Warning:
            Warning(Msg.Message(), false);
            break;

        default:
            Error(Msg.Message(), true);

            if (Msg.HasLineWithMark())
            {
                Error(Msg.Line());
                Error(Msg.Mark());
            }
            break;
    }
}

void UpperIndent()
{
    if (!CompilerOptions::Settings.ImmediateOutput)
        State.Indent += INDENT_MODIFIFER;
}

void LowerIndent()
{
    if (!CompilerOptions::Settings.ImmediateOutput)
    {
        if (State.Indent.size() > INDENT_MODIFIFER.size())
            State.Indent.resize(State.Indent.size() - INDENT_MODIFIFER.size());
        else
            State.Indent.clear();
    }
}

void Wait()
{
    //todo -> make this OS specific!
    system("pause");
}


} // /namespace ConsoleOutput



// ================================================================================