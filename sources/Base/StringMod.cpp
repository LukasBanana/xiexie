/*
 * String modification file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "StringMod.h"

#include <sstream>
#include <algorithm>
#include <limits>


/* === Templates === */

template <typename T> inline std::string xxStrTmpl(const T &Val)
{
    std::stringstream SStr;
    SStr << Val;
    return SStr.str();
}

template <typename T> inline std::string xxStrTmplPrec(const T &Val)
{
    std::stringstream SStr;
    SStr.precision(std::numeric_limits<T>::digits10);
    SStr << Val;
    return SStr.str();
}


/* === Functions === */

std::string xxStr(char Val)
{
    return std::string(1, Val);
}

std::string xxStr(short Val)
{
    return xxStrTmpl(Val);
}
std::string xxStr(unsigned short Val)
{
    return xxStrTmpl(Val);
}

std::string xxStr(int Val)
{
    return xxStrTmpl(Val);
}
std::string xxStr(unsigned int Val)
{
    return xxStrTmpl(Val);
}

std::string xxStr(long long int Val)
{
    return xxStrTmpl(Val);
}
std::string xxStr(unsigned long long int Val)
{
    return xxStrTmpl(Val);
}

std::string xxStr(float Val)
{
    return xxStrTmplPrec(Val);
}
std::string xxStr(double Val)
{
    return xxStrTmplPrec(Val);
}
std::string xxStr(long double Val)
{
    return xxStrTmplPrec(Val);
}

std::string xxFilename(const std::string& Filename)
{
    /* Find position with filename only */
    size_t Pos = Filename.find_last_of("/\\");

    if (Pos == std::string::npos)
        return Filename;

    /* Return filename only */
    return Filename.substr(Pos + 1);
}

std::string xxFilePath(const std::string& Filename)
{
    /* Return file path only */
    const auto pos = Filename.find_last_of("\\/");
    return pos != std::string::npos ? Filename.substr(0, pos) : ".";
}

std::string xxFileExt(const std::string& Filename)
{
    /* Return file extension only */
    auto Pos = Filename.find_last_of('.');

    if (Pos == std::string::npos)
        return Filename;

    return Filename.substr(Pos + 1, Filename.size() - Pos - 1);
}

std::string xxReplaceString(
    std::string Subject, const std::string& Search, const std::string& Replace)
{
    size_t Pos = 0;

    while ( ( Pos = Subject.find(Search, Pos) ) != std::string::npos )
    {
        Subject.replace(Pos, Search.size(), Replace);
        Pos += Replace.size();
    }

    return Subject;
}

std::string xxRemoveWhiteSpaces(std::string Str)
{
    auto it = std::remove_if(
        Str.begin(), Str.end(),
        [](char Chr) { return Chr == ' ' || Chr == '\t'; }
    );
        
    Str.erase(it, Str.end());

    return Str;
}

std::string xxNumberOffset(
    size_t Num, size_t MaxNum, const char FillChar, const size_t Base)
{
    if (Num > MaxNum)
        return xxStr(Num);

    const size_t NumOrig = Num;

    /* Find number of numerics */
    size_t MaxNumerics = 0, Numerics = 0;

    while (MaxNum >= Base)
    {
        MaxNum /= Base;
        ++MaxNumerics;
    }

    while (Num >= Base)
    {
        Num /= Base;
        ++Numerics;
    }

    /* Return string with offset and number */
    return
        std::string(MaxNumerics - Numerics, FillChar) +
        (Base > 10 ? xxNumToHex(NumOrig, false) : xxStr(NumOrig));
}

std::string xxLower(std::string Str)
{
    for (char& Chr : Str)
    {
        if (Chr >= 'A' && Chr <= 'Z')
            Chr += 'a' - 'A';
    }
    return Str;
}

std::string xxUpper(std::string Str)
{
    for (char& Chr : Str)
    {
        if (Chr >= 'a' && Chr <= 'z')
            Chr -= 'a' - 'A';
    }
    return Str;
}



// ================================================================================