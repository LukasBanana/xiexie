/*
 * Console output header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_CONSOLE_OUTPUT_H__
#define __XX_CONSOLE_OUTPUT_H__


#include "CompilerMessage.h"

#include <string>


class ErrorReporter;

namespace ConsoleOutput
{


/* === Functions === */

void OpenWindow();
void CloseWindow();
void UpdateWindow();

bool CheckForDefaultParameter(const std::string& Param);

void PrintInfo();
void PrintVersion();
void PrintHelp();
void PrintKeywords();

void PrintErrorReport(ErrorReporter &Reporter);

void StartNL();
void Append(const std::string& Msg);
void EndNL();

void Message(const std::string &Msg);
void Warning(const std::string &Msg, bool AppendPrefix = true);
void Error(const std::string &Msg, bool Fatal = false, bool AppendPrefix = true);
void Success(const std::string &Msg);

void Message(const CompilerMessage& Msg);

void UpperIndent();
void LowerIndent();

void Wait();


} // /namespace ConsoleOutput


/* === Classes === */

struct ScopedIndent
{
    ScopedIndent()
    {
        ConsoleOutput::UpperIndent();
    }
    ~ScopedIndent()
    {
        ConsoleOutput::LowerIndent();
    }
};


#endif



// ================================================================================