/*
 * Stack wrapper header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_STACK_WRAPPER_H__
#define __XX_STACK_WRAPPER_H__


#include <stack>


//! Simple stack wrapper class.
template <typename T> class StackWrapper
{
    
    public:
        
        StackWrapper()
        {
        }
        ~StackWrapper()
        {
        }

        inline void Push(const T& Entry)
        {
            Container.push(Entry);
        }

        inline T Pop()
        {
            if (!Container.empty())
            {
                T Entry = Container.top();
                Container.pop();
                return Entry;
            }
            return T(0);
        }

        inline T Top() const
        {
            return Container.empty() ? T(0) : Container.top();
        }

        inline bool Empty() const
        {
            return Container.empty();
        }

        class ScopedPush
        {
            
            public:
                
                ScopedPush(StackWrapper<T>& Ref, const T& Entry) :
                    Ref_(Ref)
                {
                    Ref_.Push(Entry);
                }
                ~ScopedPush()
                {
                    Ref_.Pop();
                }

            private:
                
                StackWrapper<T>& Ref_;

        };

        std::stack<T> Container;

};


#endif



// ================================================================================