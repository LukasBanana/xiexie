/*
 * String modification header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_STRING_MOD_H__
#define __XX_STRING_MOD_H__


#include <string>
#include <sstream>


/* === Inline functions === */

inline std::string xxStr(const std::string &Str)
{
    return Str;
}

/* === Functions === */

std::string xxStr(char Val);

std::string xxStr(short Val);
std::string xxStr(unsigned short Val);

std::string xxStr(int Val);
std::string xxStr(unsigned int Val);

std::string xxStr(long long int Val);
std::string xxStr(unsigned long long int Val);

std::string xxStr(float Val);
std::string xxStr(double Val);
std::string xxStr(long double Val);

//! Returns the filename of the specified full path (e.g. from "C:/Program Files/Programming/App.xx" to "App.xx").
std::string xxFilename(const std::string& Filename);
//! Returns the full path of the specified filename (e.g. from "C:/Program Files/Programming/App.xx" to "C:/Program Files/Programming").
std::string xxFilePath(const std::string& Filename);
//! Returns the file extension part (e.g. from "C:/Program Files/Programming/App.xx" to "xx").
std::string xxFileExt(const std::string& Filename);

std::string xxReplaceString(
    std::string Subject, const std::string& Search, const std::string& Replace
);

std::string xxRemoveWhiteSpaces(std::string Str);

std::string xxNumberOffset(
    size_t Num, size_t MaxNum, const char FillChar = ' ', const size_t Base = 10
);

std::string xxLower(std::string Str);
std::string xxUpper(std::string Str);

/* === Templates === */

template <typename T> T xxNumber(const std::string& Str)
{
    T Val = T(0);
    std::istringstream SStr(Str);
    SStr >> Val;
    return Val;
}

template <typename T> std::string xxNumToHex(T Number, const size_t Size, const bool Prefix = true)
{
    static const char* HexAlphabet = "0123456789abcdef";

    std::string Str;
    
    if (Prefix)
    {
        for (int i = 2*Size - 1; i >= 0; --i)
        {
            Str += xxStr(
                HexAlphabet[(Number >> i*4) & 0xF]
            );
        }
    }
    else
    {
        do
        {
            Str = xxStr(HexAlphabet[Number & 0xF]) + Str;
            Number >>= 4;
        }
        while (Number > 0);
    }

    return Str;
}

template <typename T> std::string xxNumToHex(const T& Number, const bool Prefix = true)
{
    return xxNumToHex(Number, sizeof(T), Prefix);
}

template <typename T> std::string xxNumToOct(T Number)
{
    std::string Str;
    
    do
    {
        Str = xxStr("01234567"[Number & 0x7]) + Str;
        Number >>= 3;
    }
    while (Number > 0);
    
    return Str;
}

template <typename T> std::string xxNumToBin(T Number)
{
    std::string Str;
    
    do
    {
        Str = ((Number & 0x1) != 0 ? "1" : "0") + Str;
        Number >>= 1;
    }
    while (Number > 0);
    
    return Str;
}

template <typename T> T xxHexToNum(const std::string& Str)
{
    T Num = T(0);
    std::stringstream SStr;
    
    SStr << std::hex << Str;
    SStr >> Num;

    return Num;
}

template <typename T> T xxOctToNum(const std::string& Str)
{
    T Num = T(0);

    auto it = Str.begin();

    if (Str.size() > 2 && Str[0] == '0' && Str[1] == 'c')
        it += 2;

    for (; it != Str.end(); ++it)
    {
        Num <<= 3;
        Num += ((*it) - '0') & 0x7;
    }

    return Num;
}

template <typename T> T xxBinToNum(const std::string& Str)
{
    T Num = T(0);

    auto it = Str.begin();

    if (Str.size() > 2 && Str[0] == '0' && Str[1] == 'b')
        it += 2;

    for (; it != Str.end(); ++it)
    {
        Num <<= 1;
        if (*it != '0')
            ++Num;
    }

    return Num;
}


#endif



// ================================================================================