/*
 * Long number header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_LONG_NUMBER_H__
#define __XX_LONG_NUMBER_H__


//! Long number stores an 64-bit integral- and 128-bit real number.
class LongNumber
{
    
    public:
        
        typedef long long int integral_t;
        typedef long double real_t;

        LongNumber();
        LongNumber(const integral_t& Value);
        LongNumber(const real_t& Value);
        LongNumber(const LongNumber& Other);
        ~LongNumber();

        /* === Functions === */

        LongNumber& ToReal();

        /* === Operators === */

        LongNumber& operator += (LongNumber Other);
        LongNumber& operator -= (LongNumber Other);
        LongNumber& operator *= (LongNumber Other);
        LongNumber& operator /= (LongNumber Other);

        /* === Functions === */

        inline bool IsReal() const
        {
            return IsReal_;
        }

        inline const integral_t& Integral() const
        {
            return Integral_;
        }
        inline const real_t& Real() const
        {
            return Real_;
        }

    private:
        
        /* === Functions === */

        void Update(LongNumber& Other, bool SetToReal);

        /* === Members === */

        bool IsReal_;
        integral_t Integral_;
        real_t Real_;

};


LongNumber operator + (const LongNumber& Left, const LongNumber& Right);
LongNumber operator - (const LongNumber& Left, const LongNumber& Right);
LongNumber operator * (const LongNumber& Left, const LongNumber& Right);
LongNumber operator / (const LongNumber& Left, const LongNumber& Right);


#endif



// ================================================================================