/*
 * Version file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Version.h"
#include "StringMod.h"


std::string xxGetVersion()
{
    return
          xxStr(XIEXIE_VERSION_MAJOR)
        + xxStr(".")
        + xxStr(XIEXIE_VERSION_MINOR)
        #if XIEXIE_VERSION_REVISION != 0
        + xxStr(" Rev. ")
        + xxStr(XIEXIE_VERSION_REVISION)
        #endif
        #ifdef XIEXIE_VERSION_STATUS
        + xxStr(" ")
        + xxStr(XIEXIE_VERSION_STATUS)
        #endif
    ;
}



// ================================================================================