/*
 * Expression evaluator file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ExpressionEvaluator.h"
#include "ASTIncludes.h"
#include "CompilerMessage.h"
#include "StringMod.h"

#include <cmath>


const LongNumber::real_t ExpressionEvaluator::StdVariables::PI = 3.141592653589793238462643383279502884197169399375;
const LongNumber::real_t ExpressionEvaluator::StdVariables::E = 2.718281828459045235;

static const std::string NotAllowedExpr = " are not allowed in arithmetic expressions";

ExpressionEvaluator::ExpressionEvaluator()
{
    EstablishStdFunctions();
}
ExpressionEvaluator::~ExpressionEvaluator()
{
}

bool ExpressionEvaluator::EvaluateExpr(ExpressionPtr ExprAST, LongNumber& Result)
{
    if (ExprAST == nullptr)
        return false;

    try
    {
        ExprAST->VISIT;

        Result = Pop();
    }
    catch (const std::exception& Err)
    {
        ConsoleOutput::Error(Err.what());
        return false;
    }

    return true;
}

/* === Expressions === */

DefVisitProc(ExpressionEvaluator, BracketExpression)
{
    AST->Expr->VISIT;
}

DefVisitProc(ExpressionEvaluator, BinaryExpression)
{
    /* First evaluate sub-expressions */
    AST->Expr1->VISIT;
    AST->Expr2->VISIT;

    /* Compute arithmetic operation in operator AST node */
    AST->Op->VISIT;
}

DefVisitProc(ExpressionEvaluator, UnaryExpression)
{
    /* First evaluate sub-expression */
    AST->Expr->VISIT;

    /* Compute arithmetic operation in operator AST node */
    AST->Op->VISIT;
}

DefVisitProc(ExpressionEvaluator, AssignExpression)
{
    AST->Op->VISIT;
    AST->Expr->VISIT;
}

DefVisitProc(ExpressionEvaluator, LiteralExpression)
{
    AST->Literal->VISIT;
}

DefVisitProc(ExpressionEvaluator, CallExpression)
{
    auto FuncCall = AST->Call;
    const auto& Spell = xxLower(FuncCall->FName->Ident->Spell);

    /* Evaluate argument expressions */
    auto Param = FuncCall->ArgList;
    std::vector<LongNumber> Val;

    while (Param != nullptr)
    {
        Param->Expr->VISIT;
        Param = Param->Next;

        Val.push_back(Pop());
    }

    /* Temporary value check function */
    auto CheckParam = [this, &Val, &AST](const std::string& Name, size_t Num)
    {
        if (Val.size() < Num)
            Error("Too few arguments for function \"" + Name + "\" (Must be " + xxStr(Num) + ")", AST);
        else if (Val.size() > Num)
            Error("Too many arguments for function \"" + Name + "\" (Must be " + xxStr(Num) + ")", AST);
    };

    /* Check for standard function */
    auto it = StdFunctions_.find(Spell);

    if (it != StdFunctions_.end())
    {
        CheckParam(Spell, it->second.NumParams);
        Push(it->second.Func(Val));
    }
    else
        Error("Unknown function \"" + Spell + "\"", AST);
}

DefVisitProc(ExpressionEvaluator, CastExpression)
{
    AST->Expr->VISIT;
}

DefVisitProc(ExpressionEvaluator, ObjectExpression)
{
    const auto& Spell = xxLower(AST->FName->Ident->Spell);

    /* Check for standard variable */
    if (Spell == "pi")
        Push(StdVariables::PI);
    else if (Spell == "e")
        Push(StdVariables::E);
    else
        Error("Unknown constant \"" + Spell + "\"", AST);
}

/* === Literals === */

DefVisitProc(ExpressionEvaluator, BoolLiteral)
{
    Error("Boolean literals" + NotAllowedExpr, AST);
}

DefVisitProc(ExpressionEvaluator, FloatLiteral)
{
    Push(xxNumber<LongNumber::real_t>(AST->Spell));
}

DefVisitProc(ExpressionEvaluator, IntegerLiteral)
{
    Push(xxNumber<LongNumber::integral_t>(AST->Spell));
}

DefVisitProc(ExpressionEvaluator, StringLiteral)
{
    Error("Strings" + NotAllowedExpr, AST);
}

/* === Objects === */

DefVisitProc(ExpressionEvaluator, ClassObject)
{
}

DefVisitProc(ExpressionEvaluator, EnumObject)
{
}

DefVisitProc(ExpressionEvaluator, FlagsObject)
{
}

DefVisitProc(ExpressionEvaluator, FunctionObject)
{
}

DefVisitProc(ExpressionEvaluator, TypeObject)
{
}

DefVisitProc(ExpressionEvaluator, VariableObject)
{
}

/* === Operators === */

DefVisitProc(ExpressionEvaluator, AssignOperator)
{
}

DefVisitProc(ExpressionEvaluator, BinaryOperator)
{
    /* Get sub-expression result */
    auto R2 = Pop();
    auto R1 = Pop();

    /* Temporary function */
    auto IsIntegralType = [=](const std::string& OpName)
    {
        if (R1.IsReal() || R2.IsReal())
            Error(OpName + " operator can only be used for integral numbers");
    };
    
    /* Compute arithmetic binary operation */
    const auto& Spell = AST->Spell;

         if (Spell == "+") Push(R1 + R2);
    else if (Spell == "-") Push(R1 - R2);
    else if (Spell == "*") Push(R1 * R2);
    else if (Spell == "/") Push(R1 / R2);
    else if (Spell == "%")
    {
        IsIntegralType("Modulo");
        Push(R1.Integral() % R2.Integral());
    }
    else if (Spell == "<<")
    {
        IsIntegralType("Left-shift");
        Push(R1.Integral() << R2.Integral());
    }
    else if (Spell == ">>")
    {
        IsIntegralType("Right-shift");
        Push(R1.Integral() >> R2.Integral());
    }
    else
        Error("Unsupported arithmetic operator \"" + Spell + "\"");
}

DefVisitProc(ExpressionEvaluator, UnaryOperator)
{
    /* Get sub-expression result */
    auto R = Pop();

    /* Compute arithmetic unary operation */
    const auto& Spell = AST->Spell;

    if (Spell == "-")
    {
        if (R.IsReal())
            Push(-R.Real());
        else
            Push(-R.Integral());
    }
    else
        Error("Unsupported arithmetic operator \"" + Spell + "\"");
}


/*
 * ======= Private: =======
 */

void ExpressionEvaluator::EstablishStdFunctions()
{
    #define REAL(i) Val[i].ToReal().Real()
    #define ImplementFunc(s, n, f) StdFunctions_[s] = StdFunction( n, [](std::vector<LongNumber> Val) { f } );
    #define ImplementFuncRef(s, n, f) StdFunctions_[s] = StdFunction( n, [this](std::vector<LongNumber> Val) { f } );

    /* Establish all standard functions */
    ImplementFunc( "sin",   1, return std::sin  (REAL(0)); )
    ImplementFunc( "cos",   1, return std::cos  (REAL(0)); )
    ImplementFunc( "tan",   1, return std::tan  (REAL(0)); )

    ImplementFunc( "asin",  1, return std::asin (REAL(0)); )
    ImplementFunc( "acos",  1, return std::acos (REAL(0)); )
    ImplementFunc( "atan",  1, return std::atan (REAL(0)); )

    ImplementFunc( "sinh",  1, return std::sinh (REAL(0)); )
    ImplementFunc( "cosh",  1, return std::cosh (REAL(0)); )
    ImplementFunc( "tanh",  1, return std::tanh (REAL(0)); )

    ImplementFunc( "exp",   1, return std::exp  (REAL(0)); )
    ImplementFunc( "sqrt",  1, return std::sqrt (REAL(0)); )
    ImplementFunc( "log",   1, return std::log10(REAL(0)); )
    ImplementFunc( "ln",    1, return std::log  (REAL(0)); )

    ImplementFunc( "logb",  2, return std::log  (REAL(0)) / std::log(REAL(1));  )
    ImplementFunc( "pow",   2, return std::pow  (REAL(0), REAL(1));             )

    ImplementFunc( "circle",    1, return StdVariables::PI * REAL(0) * REAL(0); )
    ImplementFunc( "ring",      1, return StdVariables::PI * REAL(0) * 2.0L; )
    ImplementFunc( "rect",      2, return REAL(0) * REAL(1); )
    ImplementFunc( "sphere",    1, return StdVariables::PI * REAL(0) * REAL(0) * REAL(0) * 4.0L / 3.0L; )
    ImplementFunc( "box",       3, return REAL(0) * REAL(1) * REAL(2); )

    ImplementFunc(
        "abs", 1,
        return Val[0].IsReal() ?
            LongNumber(std::abs(Val[0].Real())) :
            LongNumber(std::abs(Val[0].Integral()));
    )
    
    ImplementFuncRef(
        "fac", 1,
        if (Val[0].IsReal() || Val[0].Integral() < 0)
        {
            Error("Faculty can only be used for positive integral numbers");
        }
        return ExpressionEvaluator::Faculty(Val[0].Integral());
    )

    #undef REAL
    #undef ImplementFunc
    #undef ImplementFuncRef
}

void ExpressionEvaluator::Error(const std::string &Msg, ASTPtr AST)
{
    if (AST)
        throw ContextError(AST->Pos(), Msg);
    else
        throw ContextError(SourcePosition::Ignore, Msg);
}

void ExpressionEvaluator::Push(const LongNumber& Value)
{
    NumberStack_.push(Value);
}

LongNumber ExpressionEvaluator::Pop()
{
    if (!NumberStack_.empty())
    {
        LongNumber Value = NumberStack_.top();
        NumberStack_.pop();
        return Value;
    }
    return LongNumber();
}

LongNumber::integral_t ExpressionEvaluator::Faculty(LongNumber::integral_t Num)
{
    LongNumber::integral_t Result = 1;

    for (; Num > 0; --Num)
        Result *= Num;

    return Result;
}



// ================================================================================
