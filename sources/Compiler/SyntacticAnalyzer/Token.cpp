/*
 * Token file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Token.h"


namespace SyntacticAnalyzer
{


/*
 * Internal functions
 */

static std::map<std::string, Token::Types> EstablishTable()
{
    std::map<std::string, Token::Types> Table;
    
    // Identifiers
    Table["<identifier>"        ] = Token::Types::Identifier;

    // Literals
    Table["<boolean-literal>"   ] = Token::Types::BoolLiteral;
    Table["<integer-literal>"   ] = Token::Types::IntLiteral;
    Table["<float-literal>"     ] = Token::Types::FloatLiteral;
    Table["<string-literal>"    ] = Token::Types::StringLiteral;
    Table["<hex-literal>"       ] = Token::Types::HexLiteral;
    Table["<oct-literal>"       ] = Token::Types::OctLiteral;
    Table["<bin-literal>"       ] = Token::Types::BinLiteral;
    Table["<pointer-literal>"   ] = Token::Types::PointerLiteral;

    // Type denoter
    Table["<void-type>"     ] = Token::Types::VoidTypeDenoter;
    Table["<const-type>"    ] = Token::Types::ConstTypeDenoter;
    Table["<boolean-type>"  ] = Token::Types::BoolTypeDenoter;
    Table["<integer-type>"  ] = Token::Types::IntTypeDenoter;
    Table["<float-type>"    ] = Token::Types::FloatTypeDenoter;
    Table["<string-type>"   ] = Token::Types::StringTypeDenoter;
    Table["<procedure-type>"] = Token::Types::ProcTypeDenoter;

    // Operators
    Table["<bitwise-OR>"        ] = Token::Types::BitwiseOrOp;
    Table["<bitwise-XOR>"       ] = Token::Types::BitwiseXorOp;
    Table["<bitwise-AND>"       ] = Token::Types::BitwiseAndOp;
    Table["<bitwise-NOT>"       ] = Token::Types::BitwiseNotOp;
    Table["<shift-operator>"    ] = Token::Types::ShiftOp;
    Table["<add-operator>"      ] = Token::Types::AddOp;
    Table["<sub-operator>"      ] = Token::Types::SubOp;
    Table["<mul-operator>"      ] = Token::Types::MulOp;
    Table["<div-operator>"      ] = Token::Types::DivOp;

    Table["<equality>"          ] = Token::Types::EqualityOp;
    Table["<relation>"          ] = Token::Types::RelationOp;
    Table["or"                  ] = Token::Types::LogicOrOp;
    Table["and"                 ] = Token::Types::LogicAndOp;
    Table["not"                 ] = Token::Types::LogicNotOp;

    Table["<assignment>"        ] = Token::Types::AssignOp;
    Table["<unary-assignment>"  ] = Token::Types::UnaryAssignOp;

    // Punctuation
    Table["."   ] = Token::Types::Dot;
    Table[":"   ] = Token::Types::Colon;
    Table[";"   ] = Token::Types::Semicolon;
    Table[","   ] = Token::Types::Comma;
    Table[".."  ] = Token::Types::RangeSep;
    Table["->"  ] = Token::Types::Arrow;
    
    // Brackets
    Table["("   ] = Token::Types::LBracket;
    Table[")"   ] = Token::Types::RBracket;
    Table["{"   ] = Token::Types::LCurly;
    Table["}"   ] = Token::Types::RCurly;
    Table["["   ] = Token::Types::LParen;
    Table["]"   ] = Token::Types::RParen;
    Table["[["  ] = Token::Types::LDParen;
    Table["]]"  ] = Token::Types::RDParen;

    // Special characters
    Table["@"] = Token::Types::At;
    Table["#"] = Token::Types::Hash;

    // Reservered words
    Table["do"      ] = Token::Types::Do;
    Table["while"   ] = Token::Types::While;
    Table["until"   ] = Token::Types::Until;
    Table["for"     ] = Token::Types::For;
    Table["forever" ] = Token::Types::Forever;
    Table["foreach" ] = Token::Types::Foreach;

    Table["if"      ] = Token::Types::If;
    Table["else"    ] = Token::Types::Else;

    Table["switch"  ] = Token::Types::Switch;
    Table["case"    ] = Token::Types::Case;
    Table["default" ] = Token::Types::Default;
    Table["next"    ] = Token::Types::Next;
    Table["break"   ] = Token::Types::Break;

    Table["return"  ] = Token::Types::Return;
    Table["using"   ] = Token::Types::Using;
    Table["import"  ] = Token::Types::Import;
    Table["include" ] = Token::Types::Include;
    Table["package" ] = Token::Types::Package;
    Table["stop"    ] = Token::Types::Stop;

    Table["try"     ] = Token::Types::Try;
    Table["catch"   ] = Token::Types::Catch;
    Table["throw"   ] = Token::Types::Throw;

    Table["static"  ] = Token::Types::Static;
    Table["decl"    ] = Token::Types::Decl;
    Table["readonly"] = Token::Types::Readonly;

    Table["pattern" ] = Token::Types::Pattern;
    Table["use"     ] = Token::Types::Use;
    Table["alias"   ] = Token::Types::Alias;

    Table["class"   ] = Token::Types::Class;
    Table["public"  ] = Token::Types::Public;
    Table["protect" ] = Token::Types::Protect;
    Table["private" ] = Token::Types::Private;
    Table["extends" ] = Token::Types::Extends;
    Table["init"    ] = Token::Types::Init;
    Table["release" ] = Token::Types::Release;
    Table["virtual" ] = Token::Types::Virtual;
    Table["abstract"] = Token::Types::Abstract;

    Table["enum"    ] = Token::Types::Enum;
    Table["flags"   ] = Token::Types::Flags;

    Table["new"     ] = Token::Types::New;
    Table["copy"    ] = Token::Types::Copy;
    Table["valid"   ] = Token::Types::Valid;

    Table["lambda"  ] = Token::Types::Lambda;

    Table["assert"  ] = Token::Types::Assert;

    // Compiler specific keywords
    Table["once"    ] = Token::Types::Once;
    Table["cpp"     ] = Token::Types::Cpp;
    Table["pushcfg" ] = Token::Types::PushConfig;
    Table["popcfg"  ] = Token::Types::PopConfig;

    // Special tokens
    Table["<eof>"] = Token::Types::EndOfFile;

    Table["<register>"] = Token::Types::Register;

    return Table;
}

static std::map<std::string, Token::Types> EstablishTypeDenoterTable()
{
    std::map<std::string, Token::Types> Table;
    
    Table["void"    ] = Token::Types::VoidTypeDenoter;
    Table["const"   ] = Token::Types::ConstTypeDenoter;

    Table["bool"    ] = Token::Types::BoolTypeDenoter;

    Table["byte"    ] = Token::Types::IntTypeDenoter;
    Table["ubyte"   ] = Token::Types::IntTypeDenoter;
    Table["short"   ] = Token::Types::IntTypeDenoter;
    Table["ushort"  ] = Token::Types::IntTypeDenoter;
    Table["int"     ] = Token::Types::IntTypeDenoter;
    Table["uint"    ] = Token::Types::IntTypeDenoter;
    Table["long"    ] = Token::Types::IntTypeDenoter;
    Table["ulong"   ] = Token::Types::IntTypeDenoter;

    Table["float"   ] = Token::Types::FloatTypeDenoter;
    Table["double"  ] = Token::Types::FloatTypeDenoter;

    Table["string"  ] = Token::Types::StringTypeDenoter;
    Table["wstring" ] = Token::Types::StringTypeDenoter;
    
    Table["proc"    ] = Token::Types::ProcTypeDenoter;
    
    return Table;
}


/*
 * Token class
 */

std::map<std::string, Token::Types> Token::Table_ = EstablishTable();
std::map<std::string, Token::Types> Token::TypeDenoterTable_ = EstablishTypeDenoterTable();

Token::Token(Token&& Other) :
    Pos_    (Other.Pos_     ),
    Type_   (Other.Type_    ),
    Spell_  (Other.Spell_   )
{
}
Token::Token(const SourcePosition &Pos, const Types &Type) :
    Pos_    (Pos                ),
    Type_   (Type               ),
    Spell_  (Token::Spell(Type) )
{
}
Token::Token(const SourcePosition &Pos, const Types &Type, const std::string &Spell) :
    Pos_    (Pos    ),
    Type_   (Type   ),
    Spell_  (Spell  )
{
}
Token::~Token()
{
}

bool Token::IsTypeDenoter() const
{
    switch (Type())
    {
        case Types::VoidTypeDenoter:
        case Types::ConstTypeDenoter:
        case Types::BoolTypeDenoter:
        case Types::IntTypeDenoter:
        case Types::FloatTypeDenoter:
        case Types::StringTypeDenoter:
            return true;
        default:
            break;
    }
    return false;
}

bool Token::IsPrivacy() const
{
    switch (Type())
    {
        case Types::Public:
        case Types::Protect:
        case Types::Private:
            return true;
        default:
            break;
    }
    return false;
}

bool Token::IsSetAssign() const
{
    return Type() == Types::AssignOp && Spell() == ":=";
}

bool Token::IsBinaryOperator() const
{
    switch (Type())
    {
        case Token::Types::EqualityOp:
        case Token::Types::RelationOp:
        case Token::Types::LogicOrOp:
        case Token::Types::LogicAndOp:
        case Token::Types::BitwiseOrOp:
        case Token::Types::BitwiseXorOp:
        case Token::Types::BitwiseAndOp:
        case Token::Types::ShiftOp:
        case Token::Types::AddOp:
        case Token::Types::SubOp:
        case Token::Types::MulOp:
        case Token::Types::DivOp:
            return true;
        default:
            break;
    }
    return false;
}

bool Token::IsUnaryOperator() const
{
    switch (Type())
    {
        case Token::Types::SubOp:
        case Token::Types::MulOp:
        case Token::Types::BitwiseAndOp:
        case Token::Types::BitwiseNotOp:
        case Token::Types::LogicNotOp:
            return true;
        default:
            break;
    }
    return false;
}

bool Token::IsAssignOperator() const
{
    switch (Type())
    {
        case Token::Types::AssignOp:
        case Token::Types::UnaryAssignOp:
            return true;
        default:
            break;
    }
    return false;
}

std::string Token::Spell(const Types Type)
{
    for (auto& Entry : Table())
    {
        if (Entry.second == Type)
            return Entry.first;
    }
    return "";
}

TokenPtr Token::Make(const SourcePosition &Pos, const Types &Type)
{
    return std::make_shared<Token>(Pos, Type);
}
TokenPtr Token::Make(const SourcePosition &Pos, const Types &Type, const std::string &Spell)
{
    return std::make_shared<Token>(Pos, Type, Spell);
}

bool Token::IsArithmeticBinaryOperator(const std::string& Spell)
{
    static const char* OpList[] =
    {
        "+", "-", "*", "/", "%", "|", "&", "^", "<<", ">>"
    };

    for (const char* TypeOp : OpList)
    {
        if (Spell == TypeOp)
            return true;
    }
    
    return false;
}

bool Token::IsBooleanRelationBinaryOperator(const std::string& Spell)
{
    return Spell == "<" || Spell == ">" || Spell == "<=" || Spell == ">=";
}

bool Token::IsBooleanEqualityBinaryOperator(const std::string& Spell)
{
    return Spell == "=" || Spell == "!=";
}

bool Token::IsBinaryLink(const std::string& Spell)
{
    return Spell == "||" || Spell == "&&";
}

bool Token::IsBitwiseOperator(const std::string& Spell)
{
    static const char* OpList[] =
    {
        "|", "&", "^", "<<", ">>", "|=", "&=", "^=", "<<=", ">>="
    };

    for (const char* TypeOp : OpList)
    {
        if (Spell == TypeOp)
            return true;
    }
    
    return false;
}


/*
 * ======= Protected: =======
 */

void Token::Modify(const Types& Type, const std::string& Spell)
{
    Type_ = Type;
    Spell_ = Spell;
}


} // /namespace SyntacticAnalyzer



// ================================================================================