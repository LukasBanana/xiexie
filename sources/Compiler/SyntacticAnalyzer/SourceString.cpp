/*
 * Source string file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "SourceString.h"


namespace SyntacticAnalyzer
{


SourceString::SourceString()
{
}
SourceString::~SourceString()
{
}

void SourceString::ReadString(const std::string& Str)
{
    /* Store string as line */
    Line_ = Str + "\n";
}

char SourceString::Next()
{
    /* Check if reader is at end-of-line */
    if (Pos_.Column() >= Line_.size())
        return 0;

    /* Increment column and return current character */
    char Chr = Line_[Pos_.Column()];
    Pos_.IncColumn();

    return Chr;
}


} // /namespace SyntacticAnalyzer



// ================================================================================