/*
 * Source file file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "SourceFile.h"
#include "ConsoleOutput.h"


namespace SyntacticAnalyzer
{


SourceFile::SourceFile()
{
}
SourceFile::~SourceFile()
{
}

bool SourceFile::ReadFile(const std::string& Filename)
{
    /* Open file and store filename */
    Filename_ = Filename;
    Stream_.open(Filename, std::ios_base::in);
    return Stream_.good();
}

char SourceFile::Next()
{
    if (!Stream_.is_open())
        return 0;

    /* Check if reader is at end-of-line */
    while (Pos_.Column() >= Line_.size())
    {
        /* Read new line in source file */
        std::getline(Stream_, Line_);
        Line_ += '\n';
        Pos_.IncRow();

        /* Check if end-of-file is reached */
        if (Stream_.eof())
            return 0;
    }

    /* Increment column and return current character */
    char Chr = Line_[Pos_.Column()];
    Pos_.IncColumn();

    return Chr;
}

SourceFilePtr SourceFile::Open(const std::string& Filename)
{
    /* Create new source-file object and start reading */
    auto SrcFile = std::make_shared<SourceFile>();
    
    if (!SrcFile->ReadFile(Filename))
    {
        ConsoleOutput::Error("Reading file failed");
        return nullptr;
    }

    return SrcFile;
}


} // /namespace SyntacticAnalyzer



// ================================================================================