/*
 * Parser header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_PARSER_H__
#define __XX_PARSER_H__


#include "XieXieScanner.h"
#include "SourceFile.h"
#include "AST.h"
#include "RangeForLoopCommand.h"
#include "FNameIdent.h"
#include "Expression.h"
#include "UnaryExpression.h"
#include "FunctionObject.h"

#include <list>
#include <stack>
#include <vector>
#include <functional>


namespace SyntacticAnalyzer
{


using namespace AbstractSyntaxTrees;


class Parser
{
    
    public:
        
        Parser();
        ~Parser();

        /* === Functions === */

        /**
        Parses the whole program from the given source file.
        \param[in] Filename Specifies the path of the file which is to be parsed.
        \return Smart pointer to the resulting program AST (abstract syntax tree) or nullptr if an error occured.
        \see Program
        */
        ProgramPtr ParseProgram(std::string Filename);

        /*
        Parses a single string line for expressions. This can be used for immediate expression evaluation.
        \param[in] Str Specifies the string which is to be parsed.
        \return Smart pointer to the resulting expression AST (abstract syntax tree) or nullptr if an error occured.
        \see Expression
        */
        ExpressionPtr ParseExpression(const std::string& Str);

        /* === Inline functions === */

        const SourceFilePtr GetSourceFile() const
        {
            return SourceFile_;
        }

    private:
        
        /* === Structures === */

        struct ParsingState
        {
            ParsingState() :
                ScopeLevel  (0),
                ProcDepth   (0)
            {
            }

            /* Functions */
            inline void Init()
            {
                ScopeLevel  = 0;
                ProcDepth   = 0;
            }
            inline bool IsGlobalScope() const
            {
                return ScopeLevel == 0;
            }
            inline bool IsInsideProc() const
            {
                return ProcDepth > 0;
            }

            /* Members */
            unsigned int ScopeLevel;    //!< Common scope level.
            unsigned int ProcDepth;     //!< Procedure scope level.
        };
        
        typedef std::list<TokenPtr> TokenStorage;
        typedef std::shared_ptr<TokenStorage> TokenStoragePtr;

        /* === Functions === */

        /* --- Token scanner functions --- */

        TokenPtr Accept(const Token::Types &Type);
        TokenPtr Accept(const Token::Types &Type, const std::string& Spell);
        TokenPtr AcceptIt();

        void PushScannerPos();
        void PopScannerPosAndRestore();
        void PopScannerPosAndIgnore();

        TokenPtr PopTokenFromQueue();

        void PushToken(TokenPtr Tkn);
        void PushTokenIntoStorage(TokenPtr Tkn);

        /* --- Error handling functions --- */

        void Error(const std::string &Msg, bool AppendLine = true, TokenPtr Tkn = nullptr);
        void ErrorUnexpected(const Token::Types &Type);
        void ErrorUnexpected(const std::string &Desc, bool AppendExpectedStr = true);
        void ErrorUnexpected();
        void ErrorEOF();

        std::string GetTokenErrorMarker(const std::string& Line, const Token& Tkn) const;
        std::string GetTabTruncatedLine(const std::string& Line) const;

        ExpressionPtr BuildBinExprTree(
            std::vector<ExpressionPtr> ExprList, std::vector<BinaryOperatorPtr> OpList
        );

        void SplitToken(
            const Token::Types& T1, const std::string& S1,
            const Token::Types& T2, const std::string& S2
        );

        void CheckCommandParseFlagsError(const Command::ParseFlags::DataType& Flags);
        void CheckCommandParseFlags(const Command::ParseFlags::DataType& Flags);

        /* --- Includind file functions --- */

        bool OpenSourceFile(const std::string& Filename);

        void PushSourceFile(const std::string& Filename);
        bool PopSourceFile();

        std::string GetAbsolutePath(const std::string& Filename) const;

        /* ------- Command parse functions ------- */

        CommandPtr              ParseCommand                (const Command::ParseFlags::DataType& Flags = 0);
        CommandPtr              ParseCallOrAssignOrDef      (const Command::ParseFlags::DataType& Flags = 0);
        CommandPtr              ParseAttributeDecl          ();

        //!TODO! these two functions' names are ambiguous!!!
        CommandPtr              ParseFuncOrVarDef           ();
        CommandPtr              ParseVariableOrProcedureDef (TypeDenoterPtr TDenoterAST);

        ImportCommandPtr        ParseImportCommand          ();
        PackageCommandPtr       ParsePackageCommand         ();
        BlockCommandPtr         ParseBlockCommand           (const Command::ParseFlags::DataType& Flags = 0);
        BlockCommandPtr         ParseDeclBlock              ();
        CallCommandPtr          ParseCallCommand            (FNameIdentPtr ObjectNameAST = nullptr);
        AssignCommandPtr        ParseAssignCommand          (FNameIdentPtr ObjectNameAST = nullptr);
        ReturnCommandPtr        ParseReturnCommand          ();
        IfCommandPtr            ParseIfCommand              ();
        InlineLangCommandPtr    ParseInlineLangCommand      ();
        PrimitiveCommandPtr     ParsePrimitiveCommand       ();
        ClassDeclCommandPtr     ParseClassDeclCommand       (AttributeListPtr AttribListAST = nullptr);
        ClassBlockCommandPtr    ParseClassBlockCommand      ();
        InitCommandPtr          ParseInitCommand            ();
        ReleaseCommandPtr       ParseReleaseCommand         ();
        TryCatchCommandPtr      ParseTryCatchCommand        ();
        ThrowCommandPtr         ParseThrowCommand           ();
        EnumDeclCommandPtr      ParseEnumDeclCommand        ();
        FlagsDeclCommandPtr     ParseFlagsDeclCommand       ();
        SwitchCommandPtr        ParseSwitchCommand          ();
        TypeDefCommandPtr       ParseTypeDefCommand         ();
        AssertCommandPtr        ParseAssertCommand          ();

        InfiniteLoopCommandPtr  ParseInfiniteLoopCommand    ();
        WhileLoopCommandPtr     ParseWhileLoopCommand       ();
        RangeForLoopCommandPtr  ParseRangeForLoopCommand    (AttributeListPtr AttribListAST = nullptr);

        FunctionDefCommandPtr   ParseFunctionDef            (
            TypeDenoterPtr TDenoterAST = nullptr, FNameIdentPtr FNameAST = nullptr,
            FunctionObject::Flags::DataType DefFlags = 0
        );
        FunctionDefCommandPtr   ParseFunctionDecl           ();

        VariableDefCommandPtr   ParseVariableDef            (
            TypeDenoterPtr TDenoterAST = nullptr, IdentifierPtr IdentAST = nullptr,
            AttributeListPtr AttribListAST = nullptr, bool IsStatic = false
        );

        /* ------- Expression parsing helper functions ------- */

        #define DeclBinaryExprProc(n) ExpressionPtr Parse##n##Expr(const Expression::ParseFlags::DataType Flags = 0);

        typedef std::function<ExpressionPtr(const Expression::ParseFlags::DataType Flags)> ExprParseProc;

        ExpressionPtr ParseBinaryExpr(
            const ExprParseProc& First, const ExprParseProc& Next,
            const Token::Types TknType,
            const Expression::ParseFlags::DataType Flags
        );

        /* ------- Common expression parse functions ------- */

        //! Main entry point for parsing expressions.
        ExpressionPtr           ParseExpression             (const Expression::ParseFlags::DataType Flags = 0);
        ExpressionPtr           ParseArithmeticExpr         (const Expression::ParseFlags::DataType Flags = 0);

        DeclBinaryExprProc(LogicOr      )
        DeclBinaryExprProc(LogicAnd     )
        DeclBinaryExprProc(BitwiseOr    )
        DeclBinaryExprProc(BitwiseXor   )
        DeclBinaryExprProc(BitwiseAnd   )
        DeclBinaryExprProc(Equality     )
        DeclBinaryExprProc(Relation     )
        DeclBinaryExprProc(Shift        )
        DeclBinaryExprProc(Add          )
        DeclBinaryExprProc(Sub          )
        DeclBinaryExprProc(Mul          )
        DeclBinaryExprProc(Div          )

        ExpressionPtr           ParseValueExpr              (const Expression::ParseFlags::DataType Flags = 0);
        UnaryExpressionPtr      ParseUnaryExpr              (const Expression::ParseFlags::DataType Flags = 0);
        UnaryExpressionPtr      ParseUnaryAssignExpr        (const UnaryExpression::Notations& Notation);

        ExpressionPtr           ParseObjectOrCallExpr       ();
        ObjectExpressionPtr     ParseObjectExpr             (FNameIdentPtr ObjectNameAST = nullptr);
        CallExpressionPtr       ParseCallExpr               (FNameIdentPtr ObjectNameAST = nullptr);
        CastExpressionPtr       ParseCastExpr               (bool IsOptional = false);
        ExpressionPtr           ParseBracketOrCastExpr      ();
        BracketExpressionPtr    ParseBracketExpr            ();
        AssignExpressionPtr     ParseAssignExpr             ();
        ValidationExpressionPtr ParseValidationExpr         ();
        InitListExpressionPtr   ParseInitListExpr           ();
        LambdaExpressionPtr     ParseLambdaExpr             ();

        /* ------- String expression parse functions ------- */

        ExpressionPtr           ParseStringExpr             ();
        ExpressionPtr           ParseStringSumExpr          ();
        ExpressionPtr           ParseStringValueExpr        ();

        /* ------- Other expression parse functions ------- */
        
        LiteralExpressionPtr    ParseLiteralExpr            ();

        AllocExpressionPtr      ParseAllocExpr              ();
        CopyExpressionPtr       ParseCopyExpr               ();

        OperatorPtr             ParseOperator               ();
        OperatorPtr             ParseOperator               (const Token::Types& OpType);
        AssignOperatorPtr       ParseAssignOperator         ();
        BinaryOperatorPtr       ParseBinaryOperator         ();
        UnaryOperatorPtr        ParseUnaryOperator          ();
        UnaryOperatorPtr        ParseUnaryAssignOperator    ();
        UnaryOperatorPtr        ParseUnaryOperator          (const Token::Types& OpType);

        /* ------- Literal parse functions ------- */

        LiteralPtr              ParseLiteral                ();
        LiteralPtr              ParseArithmeticLiteral      ();

        BoolLiteralPtr          ParseBoolLiteral            ();
        IntegerLiteralPtr       ParseIntegerLiteral         (bool AllowNegativeValue = false);
        FloatLiteralPtr         ParseFloatLiteral           ();
        StringLiteralPtr        ParseStringLiteral          ();
        PointerLiteralPtr       ParsePointerLiteral         ();

        /* ------- Type-denoter parse functions ------- */

        TypeDenoterPtr          ParseTypeDenoter            (bool IsOptional = false, bool AllowFurtherParents = true);
        TypeDenoterPtr          ParseSingleTypeDenoter      (bool IsOptional = false);
        TypeDenoterPtr          ParseParentTypeDenoter      (TypeDenoterPtr TDenoter, bool AllowFurtherParents = true);

        ConstTypeDenoterPtr     ParseConstTypeDenoter       ();
        TypeDenoterPtr          ParseCustomTypeDenoter      (FNameIdentPtr FNameIdentAST = nullptr);

        ProcTypeDenoterPtr      ParseProcTypeDenoter        ();

        TypeDenoterPtr          ParseRawPtrTypeDenoter      (TypeDenoterPtr TDenoter, bool AllowFurtherParents = true);
        TypeDenoterPtr          ParseManagedPtrTypeDenoter  (TypeDenoterPtr TDenoter, bool AllowFurtherParents = true);
        TypeDenoterPtr          ParseConstChildTypeDenoter  (TypeDenoterPtr TDenoter, bool AllowFurtherParents = true);
        TypeDenoterPtr          ParseArrayTypeDenoter       (TypeDenoterPtr TDenoter, bool AllowFurtherParents = true);
        TypeDenoterPtr          ParseRefTypeDenoter         (TypeDenoterPtr TDenoter);

        /* ------- Other parse functions ------- */

        IdentifierPtr           ParseIdentifier             ();
        FNameIdentPtr           ParseFNameIdent             (const FNameIdent::InFlags::DataType& InFlags = 0, FNameIdent::OutFlags::DataType* OutFlags = nullptr);
        EnumEntryPtr            ParseEnumEntry              ();
        CaseBlockPtr            ParseCaseBlock              ();

        VariableObjectPtr       ParseVariableObject         (
            TypeDenoterPtr TDenoterAST = nullptr, IdentifierPtr IdentAST = nullptr,
            const Expression::ParseFlags::DataType ExprFlags = 0
        );

        PatternObjectPtr        ParsePatternObject          ();

        /* ------- List parse functions ------- */

        void                    ParseDenominator            (bool IsDenomRequired, IdentifierPtr& DenomIdent, ExpressionPtr& Expr, const TokenPtr& TempTkn);

        ParameterListPtr        ParseParameterList          ();
        ParameterListPtr        ParseSingleParameter        ();
        
        ArgumentListPtr         ParseArgumentList           ();
        ArgumentListPtr         ParseSingleArgument         (bool IsDenomRequired = false);

        TemplateParamListPtr    ParseTemplateParameterList  ();
        TemplateParamListPtr    ParseSingleTemplateParameter();

        TemplateArgListPtr      ParseTemplateArgumentList   ();
        TemplateArgListPtr      ParseSingleTemplateArgument ();

        InitializerListPtr      ParseInitializerList        ();
        InitializerListPtr      ParseSingleInitializer      (bool IsDenomRequired = false);

        AttributeListPtr        ParseAttributeList          ();
        AttributeListPtr        ParseSingleAttribute        ();

        ArrayIndexListPtr       ParseArrayIndexList         (bool IsOptional = false);
        ArrayIndexListPtr       ParseSingleArrayIndex       ();

        /* ------- Exceptional parse functions ------- */

        void                            ParseIncludeDirective   ();
        RangeForLoopCommand::index_t    ParseIterationIndex     ();

        #undef DeclBinaryExprProc

        /* === Inline functions === */

        inline Token::Types TokenType() const
        {
            return Tkn_->Type();
        }

        /* === Members === */

        XieXieScanner Scanner_;                             //!< Token scanner for the XieXie langauge.
        TokenPtr Tkn_;                                      //!< Aktive token.

        std::string MainSourcePath_;
        SourceFilePtr SourceFile_;                          //!< Current source file.
        std::stack<SourceFilePtr> SourceFileStack_;         //!< Source file stack for include directive parsing.
        std::map<std::string, size_t> FileIncludeTracker_;  //!< Keeps track of number of times a file is included. Used for files declared with the "once" directive.

        std::list<TokenPtr> TokenQueue_;
        std::/*stack*/list<TokenStoragePtr> TokenStorageStack_;     //!< Token storage stack.
        TokenStoragePtr TokenStorage_;                      //!< Active token storage list.

        ParsingState State_;                                //!< Current parsing state.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================