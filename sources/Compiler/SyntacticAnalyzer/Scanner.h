/*
 * Scanner header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_SCANNER_H__
#define __XX_SCANNER_H__


#include "SourceCode.h"
#include "Token.h"

#include <stack>


namespace SyntacticAnalyzer
{


class Scanner
{
    
    public:
        
        virtual ~Scanner();

        /* === Structures === */

        struct Configuration
        {
            Configuration();
            ~Configuration();

            /* Members */
            bool AllowShiftOp;  //!< Allow scanning shift operators ('<<', '<<=', '>>' and '>>='). By default true.
            bool AllowRDParent; //!< Allow right-double-paren brackets (']]'). By default false.
        };

        /* === Functions === */

        /**
        Starts scanning the specified source code.
        \param[in] Source Specifies the source code which is to be scaned.
        \param[in] InitScan Specifies whether scanning is to be initialized
        (if the source code has just opened) or continued. By default true.
        \see SourceFile
        \see SourceString
        */
        bool ScanSource(SourceCodePtr Source, bool InitScan = true);

        /**
        Scans the next token and returns the previous token.
        \see Token
        */
        virtual TokenPtr Next() = 0;

        /**
        Returns the current source position.
        \see SourcePosition
        */
        SourcePosition Pos() const;

        //! Pushs the current character onto the internal stack.
        void Push();
        //! Pops the current characters from the internal stack.
        void Pop();

        /* === Inline functions === */

        inline const SourceCodePtr GetSource() const
        {
            return Source_;
        }

        //! Returns a reference to the configuration state.
        inline Configuration& Config()
        {
            return Config_;
        }

    protected:
        
        Scanner();

        /* === Functions === */

        char Take(char Chr);
        char TakeIt();

        void Error(const std::string &Msg);
        void ErrorUnexpected(char Chr);
        void ErrorUnexpected();
        void ErrorEOF();

        void IgnoreWhiteSpaces();

        TokenPtr MakeToken(const Token::Types &Type, bool TakeChr = false);
        TokenPtr MakeToken(const Token::Types &Type, const std::string &Spell, bool TakeChr = false);

        //! Returns true if the current character is a letter (upper and lower case included).
        bool IsLetter() const;
        //! Returns true if the current character is a decimal number.
        bool IsNumber() const;
        //! Returns true if the current character is a hex number (but no decimal numbers are included).
        bool IsHexNumber() const;
        //! Returns true if the current character is a blank (or rather a white-space character).
        bool IsBlank() const;
        //! Returns true if the current character is an escape character (e.g. '\t', '\0' etc.).
        bool IsEscapeChar() const;

        TokenPtr ScanNumber(bool ConvertToDecimal = false);

        /* === Members === */

        Configuration Config_;

        char Chr_;

    private:
        
        /* === Members === */

        SourceCodePtr Source_;

        std::stack<char> ChrStack_;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================