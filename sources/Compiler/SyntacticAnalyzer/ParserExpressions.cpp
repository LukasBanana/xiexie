/*
 * Parser expressions file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Parser.h"
#include "StringMod.h"
#include "ConsoleOutput.h"
#include "ASTIncludes.h"
#include "CompilerOptions.h"

#include <algorithm>


namespace SyntacticAnalyzer
{


/*
 * Macros
 */

/*
This macro is used to define the implementations of the
default binary expression parser functions.
*/
#define DefBinaryExprProc(n, s)                                 \
    ExpressionPtr Parser::Parse##n##Expr(                       \
        const Expression::ParseFlags::DataType Flags)           \
    {                                                           \
        return ParseBinaryExpr(                                 \
            std::bind(&Parser::Parse##s##Expr, this, Flags),    \
            std::bind(&Parser::Parse##n##Expr, this, Flags),    \
            Token::Types::n##Op,                                \
            Flags                                               \
        );                                                      \
    }


/*
 * Internal functions
 */

static inline BinaryExpressionPtr MakeBinExpr(ExpressionPtr E1, BinaryOperatorPtr Op, ExpressionPtr E2)
{
    return std::make_shared<BinaryExpression>(E1, Op, E2);
}

//!TO BE REMOVED!
static inline StringSumExpressionPtr MakeStrSumExpr(ExpressionPtr E1, ExpressionPtr E2)
{
    return std::make_shared<StringSumExpression>(E1, E2);
}


/* ------- Common expression parse functions ------- */

ExpressionPtr Parser::ParseBinaryExpr(
    const ExprParseProc& First, const ExprParseProc& Next,
    const Token::Types TknType, const Expression::ParseFlags::DataType Flags)
{
    /* Parse first expression */
    auto Expr1 = First(Flags);

    if (TokenType() == TknType)
    {
        /* Parse operator and 2nd expression */
        auto Op = ParseBinaryOperator();
        auto Expr2 = Next(Flags);

        return MakeBinExpr(Expr1, Op, Expr2);
    }

    return Expr1;
}

ExpressionPtr Parser::ParseExpression(const Expression::ParseFlags::DataType Flags)
{
    /* Check for initializer list expression */
    switch (TokenType())
    {
        case Token::Types::LCurly:
            return ParseInitListExpr();
        case Token::Types::Lambda:
            return ParseLambdaExpr();
        default:
            break;
    }

    /* Otherwise parse arithmetic expression */
    return ParseArithmeticExpr(Flags);
}

ExpressionPtr Parser::ParseArithmeticExpr(const Expression::ParseFlags::DataType Flags)
{
    return ParseLogicOrExpr(Flags);
}

/*
The following macros define the default binary expression parser functions.
They always parse the expressions in the same way, but with different
successor parser functions and token types.
The "ParseBinaryExpr" function above implements the actual procedure.
*/
DefBinaryExprProc(LogicOr,      LogicAnd    )
DefBinaryExprProc(LogicAnd,     BitwiseOr   )
DefBinaryExprProc(BitwiseOr,    BitwiseXor  )
DefBinaryExprProc(BitwiseXor,   BitwiseAnd  )
DefBinaryExprProc(BitwiseAnd,   Equality    )
DefBinaryExprProc(Equality,     Relation    )

ExpressionPtr Parser::ParseRelationExpr(const Expression::ParseFlags::DataType Flags)
{
    /* Parse first expression */
    auto Expr1 = ParseShiftExpr(Flags);

    if ((Flags & Expression::ParseFlags::IgnoreRelationOp) == 0 && TokenType() == Token::Types::RelationOp)
    {
        /* Parse operator and 2nd expression */
        auto Op = ParseBinaryOperator();
        auto Expr2 = ParseRelationExpr(Flags);

        return MakeBinExpr(Expr1, Op, Expr2);
    }

    return Expr1;
}

DefBinaryExprProc(Shift,        Add         )
DefBinaryExprProc(Add,          Sub         )

/*
This function is parsing non-recursively to
create the AST nodes from right-to-left.
*/
ExpressionPtr Parser::ParseSubExpr(const Expression::ParseFlags::DataType Flags)
{
    std::vector<ExpressionPtr> ExprList;
    std::vector<BinaryOperatorPtr> OpList;

    /* Parse expressions and binary opertors iterative */
    while (true)
    {
        ExprList.push_back(ParseMulExpr(Flags));

        if (TokenType() == Token::Types::SubOp)
            OpList.push_back(ParseBinaryOperator());
        else
            break;
    }

    return BuildBinExprTree(ExprList, OpList);
}

DefBinaryExprProc(Mul, Div)

/*
This function is parsing non-recursively to
create the AST nodes from right-to-left.
*/
ExpressionPtr Parser::ParseDivExpr(const Expression::ParseFlags::DataType Flags)
{
    std::vector<ExpressionPtr> ExprList;
    std::vector<BinaryOperatorPtr> OpList;

    /* Parse expressions and binary opertors iterative */
    while (true)
    {
        ExprList.push_back(ParseValueExpr(Flags));

        if (TokenType() == Token::Types::DivOp)
            OpList.push_back(ParseBinaryOperator());
        else
            break;
    }

    return BuildBinExprTree(ExprList, OpList);
}

ExpressionPtr Parser::ParseValueExpr(const Expression::ParseFlags::DataType Flags)
{
    switch (TokenType())
    {
        case Token::Types::HexLiteral:
        case Token::Types::OctLiteral:
        case Token::Types::BinLiteral:
        case Token::Types::IntLiteral:
        case Token::Types::BoolLiteral:
        case Token::Types::FloatLiteral:
        case Token::Types::StringLiteral:
        case Token::Types::PointerLiteral:
            return ParseLiteralExpr();

        case Token::Types::Identifier:
            return ParseObjectOrCallExpr();

        case Token::Types::New:
            return ParseAllocExpr();
        case Token::Types::Copy:
            return ParseCopyExpr();
        case Token::Types::Valid:
            return ParseValidationExpr();

        case Token::Types::SubOp:
        case Token::Types::MulOp:
        case Token::Types::BitwiseAndOp:
        case Token::Types::BitwiseNotOp:
        case Token::Types::LogicNotOp:
            return ParseUnaryExpr(Flags);

        case Token::Types::UnaryAssignOp:
            return ParseUnaryAssignExpr(UnaryExpression::Notations::Prefix);

        case Token::Types::LBracket:
            return ParseBracketOrCastExpr();
        
        default:
            break;
    }

    if ((Flags & Expression::ParseFlags::IsOptional) == 0)
        ErrorUnexpected("literal-, bracket-, cast-, bitwise-NOT-, memory-, call, or allocation expression");

    return nullptr;
}

UnaryExpressionPtr Parser::ParseUnaryExpr(const Expression::ParseFlags::DataType Flags)
{
    /* Parse unary operator and value expression */
    auto Op = ParseUnaryOperator();
    auto Expr = ParseValueExpr(Flags);

    /* Create unary expression AST node */
    return std::make_shared<UnaryExpression>(Op, Expr);
}

UnaryExpressionPtr Parser::ParseUnaryAssignExpr(const UnaryExpression::Notations& Notation)
{
    /* Parse unary assignment operator and object expression */
    UnaryOperatorPtr Op;

    if (Notation == UnaryExpression::Notations::Prefix)
        Op = ParseUnaryAssignOperator();

    auto Expr = ParseObjectExpr();

    if (Notation == UnaryExpression::Notations::Postfix)
        Op = ParseUnaryAssignOperator();

    /* Create unary expression AST node */
    return std::make_shared<UnaryExpression>(Op, Expr);
}

ExpressionPtr Parser::ParseBracketOrCastExpr()
{
    /* Store information where we are currently reading in file */
    PushScannerPos();

    /* Try to parse a cast expression ('true' means it is not required) */
    auto CastExpr = ParseCastExpr(true);

    if (CastExpr == nullptr)
    {
        /*
        Restore position we were reading before
        trying to find a cast expression
        */
        PopScannerPosAndRestore();

        /* Now try to parse bracket expression. */
        return ParseBracketExpr();
    }
    else
    {
        /*
        Pop stored position from the scanner's stack and
        continue reading by ignoring the poped data
        */
        PopScannerPosAndIgnore();

        return CastExpr;
    }
}

CastExpressionPtr Parser::ParseCastExpr(bool IsOptional)
{
    /* Parse opening '(' character */
    if (TokenType() == Token::Types::LBracket)
        AcceptIt();
    else
    {
        if (!IsOptional)
            ErrorUnexpected();
        return nullptr;
    }

    /* Parse type denoter to which the value expression is to be casted */
    auto TDenoter = ParseTypeDenoter(IsOptional);
    if (TDenoter == nullptr)
        return nullptr;

    /* Parse closing ')' character */
    if (TokenType() == Token::Types::RBracket)
        AcceptIt();
    else
    {
        if (!IsOptional)
            ErrorUnexpected();
        return nullptr;
    }

    /* Parse the value expression which is to be casted */
    auto Expr = ParseValueExpr();

    /* Create cast-expression AST node */
    return std::make_shared<CastExpression>(TDenoter, Expr);
}

BracketExpressionPtr Parser::ParseBracketExpr()
{
    /* Check for special scanner configuration */
    const bool CfgAllowShiftOp = Scanner_.Config().AllowShiftOp;
    Scanner_.Config().AllowShiftOp = true;

    /* Parse brackets and inner arithmetic expression */
    Accept(Token::Types::LBracket);
    
    auto Expr = ParseArithmeticExpr();
    auto BracketExpr = std::make_shared<BracketExpression>(Expr);
    
    /* Reset scanner configuration */
    Scanner_.Config().AllowShiftOp = CfgAllowShiftOp;

    Accept(Token::Types::RBracket);

    return BracketExpr;
}

AssignExpressionPtr Parser::ParseAssignExpr()
{
    /* Parse assign operator and expression */
    auto Op = ParseAssignOperator();
    auto Expr = ParseExpression();
    
    /* Create assign-expression AST node */
    return std::make_shared<AssignExpression>(Op, Expr);
}

ValidationExpressionPtr Parser::ParseValidationExpr()
{
    /* Parse validation keyword and object expression */
    Accept(Token::Types::Valid);

    auto ObjExpr = ParseObjectExpr();

    /* Create validation-expression AST node */
    return std::make_shared<ValidationExpression>(ObjExpr);
}

InitListExpressionPtr Parser::ParseInitListExpr()
{
    /* Parse initializer list */
    auto InitListAST = ParseInitializerList();

    /* Create initializer list expression AST node */
    return std::make_shared<InitListExpression>(InitListAST);
}

LambdaExpressionPtr Parser::ParseLambdaExpr()
{
    /* Parse lambda expression */
    Accept(Token::Types::Lambda);

    /* Parse procedure header (type denoter and argument list */
    auto TDenoterAST = ParseTypeDenoter();
    auto ParamListAST = ParseParameterList();

    /* Create procedure type denoter (for lambda expression) */
    auto ProcTDenoterAST = std::make_shared<ProcTypeDenoter>(TDenoterAST, ParamListAST);

    /* Parse block command */
    auto BlockCmdAST = ParseBlockCommand();

    /* Create lambda expression AST node */
    return std::make_shared<LambdaExpression>(ProcTDenoterAST, BlockCmdAST);
}

/* ------- String expression parse functions ------- */

ExpressionPtr Parser::ParseStringExpr()
{
    return ParseStringSumExpr();
}

ExpressionPtr Parser::ParseStringSumExpr()
{
    /* Parse first expression */
    auto Expr1 = ParseStringValueExpr();

    if (TokenType() == Token::Types::AddOp)
    {
        /* Parse operator and 2nd expression */
        AcceptIt();
        auto Expr2 = ParseStringSumExpr();

        return MakeStrSumExpr(Expr1, Expr2);
    }

    return Expr1;
}

ExpressionPtr Parser::ParseStringValueExpr()
{
    /* Parse string literal */
    if (TokenType() == Token::Types::StringLiteral)
    {
        auto Tkn = AcceptIt();
        return LiteralExpression::Create(Tkn->Spell());
    }

    /* Otherwise parse arithmetic value expression */
    return ParseValueExpr();
}

/* ------- Other expression parse functions ------- */

ExpressionPtr Parser::ParseObjectOrCallExpr()
{
    /* For memory objects and call epxressions we need an object-name AST node */
    auto ObjectNameAST = ParseFNameIdent(FNameIdent::InFlags::AllowTemplateArgs);

    if (TokenType() == Token::Types::LBracket)
    {
        /* If the object name is followed by a '(' character it must be a function call */
        return ParseCallExpr(ObjectNameAST);
    }

    /* Otherwise it must be a memory object */
    return ParseObjectExpr(ObjectNameAST);
}

ObjectExpressionPtr Parser::ParseObjectExpr(FNameIdentPtr ObjectNameAST)
{
    /* Check if object name has already been parsed; if not, do so. */
    if (ObjectNameAST == nullptr)
        ObjectNameAST = ParseFNameIdent(FNameIdent::InFlags::AllowTemplateArgs);

    /* Create object expression AST node */
    auto ObjectExpr = std::make_shared<ObjectExpression>(ObjectNameAST);
    
    /* Check for assignment (:=, +=, -=, etc.) */
    if (TokenType() == Token::Types::AssignOp)
        ObjectExpr->AssignExpr = ParseAssignExpr();

    return ObjectExpr;
}

CallExpressionPtr Parser::ParseCallExpr(FNameIdentPtr ObjectNameAST)
{
    /* Parse function call statement */
    auto CallCmdAST = ParseCallCommand(ObjectNameAST);
    return std::make_shared<CallExpression>(CallCmdAST);
}

LiteralExpressionPtr Parser::ParseLiteralExpr()
{
    /* Parse literal expression */
    auto LiteralAST = ParseLiteral();
    return std::make_shared<LiteralExpression>(LiteralAST);
}

AllocExpressionPtr Parser::ParseAllocExpr()
{
    /* Parse 'new' keyword and type denoter */
    Accept(Token::Types::New);

    /* Check if an object with an anonymous class is about to be created */
    bool IsAnonymous = false;
    if (TokenType() == Token::Types::Class)
    {
        AcceptIt();
        IsAnonymous = true;
    }

    /* Parse type denoter */
    auto TDenoterAST = ParseTypeDenoter();

    /* Check for custom type denoter there's an anonymous class */
    FNameIdentPtr AnonymousBaseFName;

    if (IsAnonymous)
    {
        /* Get custom type denoter */
        if (TDenoterAST->Type() != TypeDenoter::Types::CustomType)
            Error("Invalid type denoter for anonymous class definition (Expected custom type denoter for base class)");
        auto CTDenoter = dynamic_cast<CustomTypeDenoter*>(TDenoterAST.get());

        /* Store FName identifier of base class */
        AnonymousBaseFName = CTDenoter->FName;
    }

    /* Create allow expression AST node */
    auto AllocExprAST = std::make_shared<AllocExpression>(TDenoterAST);

    /* Parse optinoal initialization parameter list */
    if (TokenType() == Token::Types::LBracket)
        AllocExprAST->ArgList = ParseArgumentList();
    
    /* Parse optional anonymous class definition */
    if (IsAnonymous)
    {
        /* Parse class block command */
        auto ClassBlockAST = ParseClassBlockCommand();

        /* Create anonymous class object AST node */
        AllocExprAST->AnonymousClass = std::make_shared<ClassObject>(ClassBlockAST);
        AllocExprAST->AnonymousClass->Inheritance.push_back(AnonymousBaseFName);
    }

    return AllocExprAST;
}

CopyExpressionPtr Parser::ParseCopyExpr()
{
    /* Parse 'copy' keyword and object identifier */
    Accept(Token::Types::Copy);
    auto FNameIdentAST = ParseFNameIdent();

    /* Create copy expression AST node */
    return std::make_shared<CopyExpression>(FNameIdentAST);
}

OperatorPtr Parser::ParseOperator()
{
    /* Parse current operator */
    auto OpTkn = AcceptIt();

    if (OpTkn->IsBinaryOperator())
        return std::make_shared<BinaryOperator>(OpTkn);
    if (OpTkn->IsAssignOperator())
        return std::make_shared<AssignOperator>(OpTkn);
    if (OpTkn->IsUnaryOperator())
        return std::make_shared<UnaryOperator>(OpTkn);

    ErrorUnexpected("'<operator>'");

    return nullptr;
}

OperatorPtr Parser::ParseOperator(const Token::Types &OpType)
{
    /* Check if token has correct type */
    if (TokenType() != OpType)
        ErrorUnexpected(Token::Spell(OpType));

    return ParseOperator();
}

AssignOperatorPtr Parser::ParseAssignOperator()
{
    /* Parse binary operator */
    auto OpTkn = Accept(Token::Types::AssignOp);
    return std::make_shared<AssignOperator>(OpTkn);
}

BinaryOperatorPtr Parser::ParseBinaryOperator()
{
    /* Parse binary operator */
    auto OpTkn = AcceptIt();

    if (OpTkn->IsBinaryOperator())
        return std::make_shared<BinaryOperator>(OpTkn);

    ErrorUnexpected("'<binary-operator>'");

    return nullptr;
}

UnaryOperatorPtr Parser::ParseUnaryOperator()
{
    /* Parse unary operator */
    if (!Tkn_->IsUnaryOperator())
        ErrorUnexpected("unary operator");

    /* Create unary operator AST node */
    auto OpTkn = AcceptIt();
    return std::make_shared<UnaryOperator>(OpTkn);
}

UnaryOperatorPtr Parser::ParseUnaryAssignOperator()
{
    /* Create unary operator AST node */
    auto OpTkn = Accept(Token::Types::UnaryAssignOp);
    return std::make_shared<UnaryOperator>(OpTkn);
}

UnaryOperatorPtr Parser::ParseUnaryOperator(const Token::Types& OpType)
{
    //!INFO! -> this code snippet can be used for further token-splitting!!!
    #if 0

    if (AllowException && OpType != TokenType())
    {
        /* Parse unary operator with exception */
        try
        {
            if (TokenType() != Token::Types::UnaryAssignOp)
                throw OpType;

            const auto& Spell = Tkn_->Spell();

            if (OpType == Token::Types::AddOp)
            {
                if (Spell != "++")
                    throw OpType;
                SplitToken(Token::Types::AddOp, "+", Token::Types::AddOp, "+");
            }
            else if (OpType == Token::Types::SubOp)
            {
                if (Spell != "--")
                    throw OpType;
                SplitToken(Token::Types::SubOp, "-", Token::Types::SubOp, "-");
            }
            else
                throw OpType;
        }
        catch (const Token::Types& Err)
        {
            ErrorUnexpected(Err);
        }
    }

    #endif

    /* Parse unary operator straight forward */
    auto OpTkn = Accept(OpType);
    return std::make_shared<UnaryOperator>(OpTkn);
}

#undef DefBinaryExprProc


} // /namespace SyntacticAnalyzer



// ================================================================================