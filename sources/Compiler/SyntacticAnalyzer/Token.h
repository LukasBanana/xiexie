/*
 * Token header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_TOKEN_H__
#define __XX_TOKEN_H__


#include "SourcePosition.h"

#include <memory>
#include <map>


namespace SyntacticAnalyzer
{


class Token;

typedef std::shared_ptr<Token> TokenPtr;

class Token
{
    
    public:
        
        /* === Enumerations === */

        enum class Types
        {
            // Identifiers
            Identifier = 0,     //!< <any identifier>

            // Literals
            BoolLiteral,        //!< true, false
            IntLiteral,         //!< <number>
            FloatLiteral,       //!< <number.number>
            StringLiteral,      //!< <"any string">
            HexLiteral,         //!< <hex-literal>
            OctLiteral,         //!< <oct-literal>
            BinLiteral,         //!< <bin-literal>
            PointerLiteral,     //!< null

            // Type denoters
            VoidTypeDenoter,    //!< void
            ConstTypeDenoter,   //!< const
            BoolTypeDenoter,    //!< bool
            IntTypeDenoter,     //!< byte, ubyte, short, ushort, int, uint, long, ulong
            FloatTypeDenoter,   //!< float, double
            StringTypeDenoter,  //!< string
            ProcTypeDenoter,    //!< proc -> std::function<ret (args ...)>

            // Operators
            BitwiseOrOp,        //!< |
            BitwiseXorOp,       //!< ^
            BitwiseAndOp,       //!< &
            BitwiseNotOp,       //!< ~
            ShiftOp,            //!< <<, >>
            AddOp,              //!< +
            SubOp,              //!< -
            MulOp,              //!< *
            DivOp,              //!< /, %

            EqualityOp,         //!< =, !=
            RelationOp,         //!< <, >, <=, >=
            LogicOrOp,          //!< or
            LogicAndOp,         //!< and
            LogicNotOp,         //!< not

            AssignOp,           //!< :=, +=, -=, *=, /=, %=, <<=, >>=, |=, &=, ^=
            UnaryAssignOp,      //!< ++, --

            // Punctuation
            Dot,                //!< .
            Colon,              //!< :
            Semicolon,          //!< ;
            Comma,              //!< ,
            RangeSep,           //!< .. (Range separator, e.g. "1 .. 10")
            Arrow,              //!< ->

            // Brackets
            LBracket,           //!< (
            RBracket,           //!< )
            LCurly,             //!< {
            RCurly,             //!< }
            LParen,             //!< [
            RParen,             //!< ]
            LDParen,            //!< [[
            RDParen,            //!< ]]

            // Special characters
            At,                 //!< @ (Smart pointer)
            Hash,               //!< # (Attribute declaration, e.g. "#unroll")   <----- DEPRECATED!!!

            // Reservered words
            Do,                 //!< do         -> do ... while- or do ... until loop
            While,              //!< while      -> while loop
            Until,              //!< until      -> until loop
            For,                //!< for        -> for loop
            Forever,            //!< forever    -> infinite loop
            Foreach,            //!< foreach    -> for-each loop

            If,                 //!< if         -> if
            Else,               //!< else       -> else

            Switch,             //!< switch     -> switch
            Case,               //!< case       -> case
            Default,            //!< default    -> default
            Next,               //!< next       -> continue
            Break,              //!< break      -> break

            Return,             //!< return     -> return
            Using,              //!< using      -> using namespace
            Import,             //!< import     -> #include (different meaning than "include")
            Include,            //!< include    -> #include
            Package,            //!< package    -> namespace
            Stop,               //!< stop       -> exit(0)
            
            Try,                //!< try        -> try
            Catch,              //!< catch      -> catch
            Throw,              //!< throw      -> throw
            
            Static,             //!< static     -> static variable or function
            Decl,               //!< decl       -> external function declaration block
            Readonly,           //!< readonly   -> const procedure

            Pattern,            //!< pattern    -> template typename parameter
            Use,                //!< use        -> template typename argument
            Alias,              //!< alias      -> type alias definition

            Class,              //!< class      -> class
            Public,             //!< public     -> public
            Protect,            //!< protect    -> protected
            Private,            //!< private    -> private
            Extends,            //!< extends    -> : public <class-name>
            Init,               //!< init       -> Class constructor
            Release,            //!< release    -> Class destructor
            Virtual,            //!< virtual    -> virtual procedure
            Abstract,           //!< abstract   -> pure virtual procedure

            Enum,               //!< enum       -> enum class
            Flags,              //!< flags      -> enum class for flags

            New,                //!< new        -> std::make_shared<...>(...)
            Copy,               //!< copy       -> __XX__Copy(...)
            Valid,              //!< valid      -> pointer validation expression keyword

            Lambda,             //!< lambda     -> [&](...) -> ReturnType { ... }

            Assert,             //!< assert     -> Debug assertion

            // Compiler specific keywords
            Once,               //!< once       -> #pragma once
            Cpp,                //!< cpp        -> Inline C++ code: "cpp { ... <cpp-code-here> ... }"
            PushConfig,         //!< pushcfg    -> Push compiler configuration: "pushcfg { option1:ON option2:OFF ... }"
            PopConfig,          //!< popcfg     -> Pop compiler configuration: "popcfg"

            // Special tokens
            EndOfFile,          //!< End of file

            // XieXie Assembler tokens
            Register,           //!< XieXie ASM register (i0 - i3, cf, lb, sp, pc, f0 - f7).
        };

        /* === Constructor & destructor === */

        Token(Token&& Other);
        Token(const SourcePosition &Pos, const Types &Type);
        Token(const SourcePosition &Pos, const Types &Type, const std::string &Spell);
        ~Token();

        /* === Functions === */

        bool IsTypeDenoter() const;
        bool IsPrivacy() const;
        bool IsSetAssign() const;

        bool IsBinaryOperator() const;
        bool IsUnaryOperator() const;
        bool IsAssignOperator() const;

        /* === Static functions === */

        static std::string Spell(const Types Type);

        static TokenPtr Make(const SourcePosition &Pos, const Types &Type);
        static TokenPtr Make(const SourcePosition &Pos, const Types &Type, const std::string &Spell);

        //! +, -, *, /, %, |, &, ^, <<, >>
        static bool IsArithmeticBinaryOperator(const std::string& Spell);
        //! <, >, <=, >=
        static bool IsBooleanRelationBinaryOperator(const std::string& Spell);
        //! =, !=, <, >, <=, >=
        static bool IsBooleanEqualityBinaryOperator(const std::string& Spell);
        //! &&, ||
        static bool IsBinaryLink(const std::string& Spell);
        //! |, &, ^, <<, >>, |=, &=, ^=, <<=, >>=
        static bool IsBitwiseOperator(const std::string& Spell);

        /* === Inline functions === */

        inline const SourcePosition& Pos() const
        {
            return Pos_;
        }
        inline const Types& Type() const
        {
            return Type_;
        }
        inline const std::string& Spell() const
        {
            return Spell_;
        }

        static inline const std::map<std::string, Types>& Table()
        {
            return Table_;
        }
        static inline const std::map<std::string, Types>& TypeDenoterTable()
        {
            return TypeDenoterTable_;
        }

    protected:
        
        friend class Parser;

        /* === Functions === */

        //! Exceptions for modifying a scaned token can only be made by the parser in some cases.
        void Modify(const Types& Type, const std::string& Spell);

    private:
        
        /* === Members === */

        SourcePosition Pos_;    //!< Source position.
        Types Type_;            //!< Token type.
        std::string Spell_;     //!< Spelling, e.g. "+" for the 'plus' an operator.

        static std::map<std::string, Types> Table_;
        static std::map<std::string, Types> TypeDenoterTable_;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================