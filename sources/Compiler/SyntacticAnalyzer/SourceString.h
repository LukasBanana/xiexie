/*
 * Source string header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_SOURCE_STRING_H__
#define __XX_SOURCE_STRING_H__


#include "SourceCode.h"


namespace SyntacticAnalyzer
{


class SourceString : public SourceCode
{
    
    public:
        
        SourceString();
        ~SourceString();

        /* === Functions === */

        void ReadString(const std::string& Str);

        char Next();

};

typedef std::shared_ptr<SourceString> SourceStringPtr;


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================