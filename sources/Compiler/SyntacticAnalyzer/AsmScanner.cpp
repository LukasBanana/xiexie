/*
 * Assembler scanner file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "AsmScanner.h"
#include "StringMod.h"
#include "ConsoleOutput.h"


using namespace XVM;

namespace SyntacticAnalyzer
{


AsmScanner::AsmScanner()
{
    EstablishRegisterNames();
    EstablishIntrinsicNames();
}
AsmScanner::~AsmScanner()
{
}

TokenPtr AsmScanner::Next()
{
    try
    {
        while (true)
        {
            IgnoreWhiteSpaces();

            /* Check for end-of-file character */
            if (Chr_ == 0)
                return MakeToken(Token::Types::EndOfFile);

            /* Scan commentaries */
            if (Chr_ == ';')
                ScanCommentLine();
            else
                break;
        }

        /* Scan token */
        return ScanToken();
    }
    catch (const std::exception &Err)
    {
        ConsoleOutput::Error(Err.what());
    }

    return nullptr;
}


/*
 * ======= Private: =======
 */

void AsmScanner::EstablishRegisterNames()
{
    /* Establish all register names */
    RegisterNames_["i0"] = Instruction::Registers::i0;
    RegisterNames_["i1"] = Instruction::Registers::i1;
    RegisterNames_["i2"] = Instruction::Registers::i2;
    RegisterNames_["i3"] = Instruction::Registers::i3;

    RegisterNames_["cf"] = Instruction::Registers::cf;
    RegisterNames_["lb"] = Instruction::Registers::lb;
    RegisterNames_["sp"] = Instruction::Registers::sp;
    RegisterNames_["pc"] = Instruction::Registers::pc;

    RegisterNames_["f0"] = Instruction::Registers::f0;
    RegisterNames_["f1"] = Instruction::Registers::f1;
    RegisterNames_["f2"] = Instruction::Registers::f2;
    RegisterNames_["f3"] = Instruction::Registers::f3;
    RegisterNames_["f4"] = Instruction::Registers::f4;
    RegisterNames_["f5"] = Instruction::Registers::f5;
    RegisterNames_["f6"] = Instruction::Registers::f6;
    RegisterNames_["f7"] = Instruction::Registers::f7;
}

void AsmScanner::EstablishIntrinsicNames()
{
    IntrinsicNames_ = VirtualMachine::EstablishIntrinsicNames();
}

void AsmScanner::ScanCommentLine()
{
    /* Scan comment until end-of-line */
    while (Chr_ != '\n')
        TakeIt();
}

TokenPtr AsmScanner::ScanStringLiteral()
{
    std::string Spell;

    /* Take opening '\"' character */
    Take('\"');

    while (true)
    {
        /* Check for escape character */
        while (Chr_ == '\\')
        {
            TakeIt();

            switch (Chr_)
            {
                case '\\':
                case '\"':
                    Spell += '\\';
                    Spell += Chr_;
                    break;
                case 't':
                    Spell += '\t';
                    break;
                case 'n':
                    Spell += '\n';
                    break;
                default:
                    ErrorUnexpected();
                    break;
            }

            TakeIt();
        }

        if (Chr_ == 0)
            ErrorEOF();
                
        /* Check for closing '\"' character */
        if (Chr_ == '\"')
            break;

        /* Append character to string literal */
        Spell += TakeIt();
    }

    /* Take closing '\"' character */
    TakeIt();

    /* Return final string literal token */
    return MakeToken(Token::Types::StringLiteral, Spell);
}

TokenPtr AsmScanner::ScanToken()
{
    std::string Spell;

    /* Scan string literal */
    if (Chr_ == '\"')
        return ScanStringLiteral();

    /* Scan identifier */
    if (IsLetter() || Chr_ == '.')
        return ScanIdentifier();

    /* Scan number */
    if (IsNumber())
        return ScanNumber(true);

    /* Scan punctuation and brackets */
    switch (Chr_)
    {
        case ',': return MakeToken(Token::Types::Comma,     true); break;
        case ':': return MakeToken(Token::Types::Colon,     true); break;
        case '[': return MakeToken(Token::Types::LParen,    true); break;
        case ']': return MakeToken(Token::Types::RParen,    true); break;
        case '(': return MakeToken(Token::Types::LBracket,  true); break;
        case ')': return MakeToken(Token::Types::RBracket,  true); break;
        case '-': return MakeToken(Token::Types::SubOp,     true); break;
    }

    ErrorUnexpected();

    return nullptr;
}

TokenPtr AsmScanner::ScanIdentifier()
{
    /* Scan identifier string */
    std::string Spell;
    Spell += TakeIt();

    bool PrevChrDot = false;

    while (IsLetter() || IsNumber() || Chr_ == '.')
    {
        /* Check for correct punctuation (e.g. ".ident1.ident2" is allowed but ".ident1..ident2" is not allowed) */
        if (Chr_ == '.')
        {
            if (PrevChrDot)
                Error("Identifier may only contain separated dots");
            PrevChrDot = true;
        }
        else
            PrevChrDot = false;

        /* Append character to spelling */
        Spell += TakeIt();
    }

    /* Search for reserved identifiers (register names) */
    auto itReg = RegisterNames_.find(Spell);
    if (itReg != RegisterNames_.end())
        return MakeToken(Token::Types::Register, xxStr(static_cast<char>(itReg->second)));

    /* Search for intrinsic names */
    auto itIntr = IntrinsicNames_.find(Spell);
    if (itIntr != IntrinsicNames_.end())
        return MakeToken(Token::Types::IntLiteral, xxStr(itIntr->second));

    /* Return identifier token */
    return MakeToken(Token::Types::Identifier, Spell);
}


} // /namespace SyntacticAnalyzer



// ================================================================================