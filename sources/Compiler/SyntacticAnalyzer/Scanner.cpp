/*
 * Scanner file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Scanner.h"
#include "CompilerMessage.h"
#include "StringMod.h"
#include "ConsoleOutput.h"


namespace SyntacticAnalyzer
{


Scanner::Scanner() :
    Chr_(0)
{
}
Scanner::~Scanner()
{
}

bool Scanner::ScanSource(SourceCodePtr Source, bool InitScan)
{
    if (Source != nullptr)
    {
        Source_ = Source;

        if (InitScan)
        {
            /* Get first character */
            TakeIt();
        }

        return true;
    }
    return false;
}

SourcePosition Scanner::Pos() const
{
    return Source_ != nullptr ? Source_->Pos() : SourcePosition::Ignore;
}

void Scanner::Push()
{
    ChrStack_.push(Chr_);
}

void Scanner::Pop()
{
    if (!ChrStack_.empty())
    {
        Chr_ = ChrStack_.top();
        ChrStack_.pop();
    }
}


/*
 * ======= Protected: =======
 */

char Scanner::Take(char Chr)
{
    if (Chr_ == Chr)
        return TakeIt();
    ErrorUnexpected(Chr);
    return 0;
}

char Scanner::TakeIt()
{
    char PrevChar = Chr_;
    Chr_ = Source_->Next();
    return PrevChar;
}

void Scanner::Error(const std::string &Msg)
{
    throw SyntaxError(Pos(), Msg);
}

void Scanner::ErrorUnexpected(char Chr)
{
    Error(
        "Unexpected character '" + xxStr(Chr_) +
        "' (Expected '" + xxStr(Chr) + "')"
    );
}

void Scanner::ErrorUnexpected()
{
    Error("Unexpected character '" + xxStr(Chr_) + "'");
}

void Scanner::ErrorEOF()
{
    Error("Unexpected end-of-file");
}

void Scanner::IgnoreWhiteSpaces()
{
    /* Ignore white spaces and seperators */
    while (Chr_ == ' ' || Chr_ == '\t' || Chr_ == '\n' || Chr_ == '\r')
        TakeIt();
}

TokenPtr Scanner::MakeToken(const Token::Types &Type, bool TakeChr)
{
    if (TakeChr)
        TakeIt();
    return Token::Make(Pos(), Type);
}

TokenPtr Scanner::MakeToken(const Token::Types &Type, const std::string &Spell, bool TakeChr)
{
    if (TakeChr)
        TakeIt();
    return Token::Make(Pos(), Type, Spell);
}

bool Scanner::IsLetter() const
{
    return
        ( Chr_ >= 'a' && Chr_ <= 'z' ) ||
        ( Chr_ >= 'A' && Chr_ <= 'Z' ) ||
        Chr_ == '_';
}

bool Scanner::IsNumber() const
{
    return Chr_ >= '0' && Chr_ <= '9';
}

bool Scanner::IsHexNumber() const
{
    return
        ( Chr_ >= 'a' && Chr_ <= 'f' ) ||
        ( Chr_ >= 'A' && Chr_ <= 'F' );
}

bool Scanner::IsBlank() const
{
    return Chr_ == ' ' || Chr_ == '\t' || Chr_ == '\n';
}

bool Scanner::IsEscapeChar() const
{
    return
        Chr_ == '\\' || Chr_ == '\"' || Chr_ == '\'' ||
        Chr_ == '\0' || Chr_ == '?' || Chr_ == 'a' ||
        Chr_ == 'b' || Chr_ == 'f' || Chr_ == 'n' ||
        Chr_ == 'r' || Chr_ == 't' || Chr_ == 'v' ||
        Chr_ == 'x' || Chr_ == 'u' || Chr_ == 'U' ||
        IsNumber();
}

TokenPtr Scanner::ScanNumber(bool ConvertToDecimal)
{
    if (!IsNumber())
        Error("Expected number literal (float, integer or hex number)");
    
    /* Take first number (literals like ".0" are not allowed) */
    std::string Spell;

    const auto StartChr = TakeIt();
    Spell += StartChr;

    Token::Types Type = Token::Types::IntLiteral;

    if (StartChr == '0')
    {
        switch (Chr_)
        {
            case 'x':
                Type = Token::Types::HexLiteral;
                Spell += TakeIt();
                break;
            case 'o':
                Type = Token::Types::OctLiteral;
                Spell += TakeIt();
                break;
            case 'b':
                Type = Token::Types::BinLiteral;
                Spell += TakeIt();
                break;
        }
    }

    while (IsNumber() || IsHexNumber() || Chr_ == '.')
    {
        /* Check for hex numbers */
        if (IsHexNumber())
        {
            if (Type != Token::Types::HexLiteral)
                Error("Hexadecimal literals must begin with \"0x\"");

            /* Convert hex numbers to lower case */
            if (Chr_ >= 'A' && Chr_ <= 'F')
                Chr_ += 'a' - 'A';
        }
        /* Check for floating-pointer number */
        else if (Chr_ == '.')
        {
            switch (Type)
            {
                case Token::Types::FloatLiteral:
                    Error("Multiple dots in number");
                    break;
                case Token::Types::IntLiteral:
                    Type = Token::Types::FloatLiteral;
                    break;
                default:
                    Error("Hex-, octal- and binary numbers must not contain punctuation");
                    break;
            }
        }
        else if (Type == Token::Types::OctLiteral && (Chr_ > '7'))
            Error("Digit '" + xxStr(Chr_) + "' is not allowed within an octal number");
        else if (Type == Token::Types::BinLiteral && (Chr_ != '0' && Chr_ != '1'))
            Error("Digit '" + xxStr(Chr_) + "' is not allowed within a binary number");

        /* Append current character */
        Spell += TakeIt();

        if (!IsHexNumber() && IsLetter())
            Error("Letter '" + xxStr(Chr_) + "' is not allowed within a number");
    }
    
    /* Check if conversion to decimal number is required */
    if (ConvertToDecimal)
    {
        switch (Type)
        {
            case Token::Types::HexLiteral:
                return MakeToken(Token::Types::IntLiteral, xxStr(xxHexToNum<long long int>(Spell)));
            case Token::Types::OctLiteral:
                return MakeToken(Token::Types::IntLiteral, xxStr(xxOctToNum<long long int>(Spell)));
            case Token::Types::BinLiteral:
                return MakeToken(Token::Types::IntLiteral, xxStr(xxBinToNum<long long int>(Spell)));
            default:
                break;
        }
    }

    /* Create number token */
    return MakeToken(Type, Spell);
}


/*
 * Configuration structure
 */

Scanner::Configuration::Configuration() :
    AllowShiftOp    (true   ),
    AllowRDParent   (false  )
{
}
Scanner::Configuration::~Configuration()
{
}


} // /namespace SyntacticAnalyzer



// ================================================================================