/*
 * Source position header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_SOURCE_POSITION_H__
#define __XX_SOURCE_POSITION_H__


#include <string>


namespace SyntacticAnalyzer
{


/*
 * Clases
 */

class SourcePosition
{
    
    public:
        
        SourcePosition();
        SourcePosition(unsigned int Row, unsigned int Column);
        ~SourcePosition();

        /* === Constant member === */

        static const SourcePosition Ignore;

        /* === Functions === */

        //! Returns the source position as string, e.g. "(75:10)".
        std::string GetString() const;

        void IncRow();
        void IncColumn();

        //! Returns ture if this is a valid source position. False if row and column are 0.
        bool Valid() const;

        /* === Inline functions === */

        //! Returns the row of the source position, beginning with 1.
        inline unsigned int Row() const
        {
            return Row_;
        }
        //! Returns the colummn of the source position, beginning with 1.
        inline unsigned int Column() const
        {
            return Column_;
        }

    private:
        
        /* === Members === */

        unsigned int Row_, Column_;

};


/*
 * Operators
 */

inline bool operator == (const SourcePosition& Left, const SourcePosition& Right)
{
    return Left.Row() == Right.Row() && Left.Column() == Right.Column();
}

inline bool operator != (const SourcePosition& Left, const SourcePosition& Right)
{
    return !(Left == Right);
}

inline bool operator < (const SourcePosition& Left, const SourcePosition& Right)
{
    return Left.Row() < Right.Row() || ( Left.Row() == Right.Row() && Left.Column() < Right.Column() );
}

inline bool operator > (const SourcePosition& Left, const SourcePosition& Right)
{
    return Left.Row() > Right.Row() || ( Left.Row() == Right.Row() && Left.Column() > Right.Column() );
}

inline bool operator <= (const SourcePosition& Left, const SourcePosition& Right)
{
    return Left < Right || Left == Right;
}

inline bool operator >= (const SourcePosition& Left, const SourcePosition& Right)
{
    return Left > Right || Left == Right;
}



} // /namespace SyntacticAnalyzer


#endif



// ================================================================================