/*
 * String modification file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "SourcePosition.h"
#include "StringMod.h"


namespace SyntacticAnalyzer
{


const SourcePosition SourcePosition::Ignore;

SourcePosition::SourcePosition() :
    Row_    (0),
    Column_ (0)
{
}
SourcePosition::SourcePosition(unsigned int Row, unsigned int Column) :
    Row_    (Row    ),
    Column_ (Column )
{
}
SourcePosition::~SourcePosition()
{
}

std::string SourcePosition::GetString() const
{
    return "(" + xxStr(Row_) + ":" + xxStr(Column_) + ")";
}

void SourcePosition::IncRow()
{
    ++Row_;
    Column_ = 0;
}
void SourcePosition::IncColumn()
{
    ++Column_;
}

bool SourcePosition::Valid() const
{
    return Row_ > 0 && Column_ > 0;
}


} // /namespace SyntacticAnalyzer



// ================================================================================