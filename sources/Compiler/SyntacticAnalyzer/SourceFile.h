/*
 * Source file header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_SOURCE_FILE_H__
#define __XX_SOURCE_FILE_H__


#include "SourceCode.h"

#include <fstream>


namespace SyntacticAnalyzer
{


class SourceFile;

typedef std::shared_ptr<SourceFile> SourceFilePtr;

class SourceFile : public SourceCode
{
    
    public:
        
        SourceFile();
        ~SourceFile();

        /* === Functions === */

        bool ReadFile(const std::string& Filename);

        char Next();

        /* === Static functinos === */

        static SourceFilePtr Open(const std::string& Filename);

        /* === Inline functions === */

        //! Returns the filename of this file stream.
        inline const std::string& Filename() const
        {
            return Filename_;
        }

    private:
        
        /* === Members === */
        
        std::ifstream Stream_;  //!< File stream.
        std::string Filename_;  //!< Source file name.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================