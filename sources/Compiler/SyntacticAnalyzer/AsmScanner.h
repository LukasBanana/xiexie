/*
 * Assembler scanner header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_SCANNER_ASM_H__
#define __XX_SCANNER_ASM_H__


#include "Scanner.h"
#include "Instruction.h"
#include "VirtualMachine.h"


namespace SyntacticAnalyzer
{


class AsmScanner : public Scanner
{
    
    public:
        
        AsmScanner();
        ~AsmScanner();

        /* === Functions === */

        TokenPtr Next();

    private:
        
        /* === Functions === */

        void EstablishRegisterNames();
        void EstablishIntrinsicNames();

        void ScanCommentLine();

        TokenPtr ScanToken();

        TokenPtr ScanStringLiteral();
        TokenPtr ScanIdentifier();

        /* === Members === */

        std::map<std::string, XVM::Instruction::Registers::reg_t> RegisterNames_;
        XVM::VirtualMachine::IntrinsicNameMapType IntrinsicNames_;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================