/*
 * Source code header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_SOURCE_CODE_H__
#define __XX_SOURCE_CODE_H__


#include "SourcePosition.h"

#include <string>
#include <memory>


namespace SyntacticAnalyzer
{


class SourceCode
{
    
    public:
        
        virtual ~SourceCode()
        {
        }

        /* === Functions === */

        //! Returns the next character from the source code or 0 if the end is reached.
        virtual char Next() = 0;

        /* === Inline functions === */

        /**
        Returns the current source position.
        \see SourcePosition
        */
        inline const SourcePosition& Pos() const
        {
            return Pos_;
        }
        //! Returns the current line which has just been read from file.
        inline const std::string& Line() const
        {
            return Line_;
        }

    protected:

        SourceCode()
        {
        }

        /* === Members === */
        
        std::string Line_;      //!< Current line.
        SourcePosition Pos_;    //!< Current source position.

};

typedef std::shared_ptr<SourceCode> SourceCodePtr;


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================