/*
 * Parser file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Parser.h"
#include "CompilerMessage.h"
#include "StringMod.h"
#include "ConsoleOutput.h"
#include "ASTIncludes.h"
#include "SourceString.h"
#include "CompilerOptions.h"

#include <algorithm>


namespace SyntacticAnalyzer
{


Parser::Parser()
{
}
Parser::~Parser()
{
}

ProgramPtr Parser::ParseProgram(std::string Filename)
{
    /* Open file for reading */
    if (!OpenSourceFile(Filename))
        return nullptr;

    ScopedIndent Unused;

    /* Store filename and source path */
    MainSourcePath_ = xxFilePath(GetSourceFile()->Filename());
    Filename = xxFilename(GetSourceFile()->Filename());

    auto ProgramAST = std::make_shared<Program>();
    ProgramAST->SourceFilename = Filename;

    /* Initialize states */
    State_.Init();

    try
    {
        /* Begin with parsing */
        AcceptIt();

        /* Parse commands until <end-of-file> token */
        while (true)
        {
            /* Check if the end has reached */
            if (TokenType() == Token::Types::EndOfFile)
            {
                if (!PopSourceFile())
                {
                    /* Stop parsing if there was no further parent-included file */
                    break;
                }
            }

            /* Parse next command */
            auto CmdAST = ParseCommand(Command::ParseFlags::GlobalScope);
            if (CmdAST != nullptr)
                ProgramAST->Commands.push_back(CmdAST);
        }
    }
    catch (const SyntaxError &Err)
    {
        /* Show detailed error message */
        ConsoleOutput::Message(Err);
        return nullptr;
    }
    catch (const std::exception& Err)
    {
        /* Show simple error message */
        ConsoleOutput::Error(Err.what(), true);
        return nullptr;
    }

    /* Check for errors */
    /*if (ErrorReporter::Global.HasErrors())
    {
        ConsoleOutput::PrintErrorReport(ErrorReporter::Global);
        return nullptr;
    }*/

    return ProgramAST;
}

ExpressionPtr Parser::ParseExpression(const std::string& Str)
{
    if (!CompilerOptions::Settings.ImmediateOutput)
        ConsoleOutput::Message("Parsing expression \"" + Str + "\" ...");
    ScopedIndent Unused;

    /* Open file for reading */
    auto SourceStr = std::make_shared<SourceString>();
    SourceStr->ReadString(Str);

    /* Start scanning source */
    if (!Scanner_.ScanSource(SourceStr))
    {
        ConsoleOutput::Error("Scanning source failed");
        return nullptr;
    }
    
    ExpressionPtr ExprAST;

    /* Initialize states */
    State_.Init();

    try
    {
        /* Begin with parsing */
        AcceptIt();

        /* Parse expression */
        ExprAST = ParseExpression();

        if (TokenType() != Token::Types::EndOfFile)
            ErrorUnexpected(Token::Types::EndOfFile);
    }
    catch (const SyntaxError &Err)
    {
        /* Show detailed error message */
        ConsoleOutput::Message(Err);
        return nullptr;
    }
    catch (const std::exception& Err)
    {
        /* Show simple error message */
        ConsoleOutput::Error(Err.what(), true);
        return nullptr;
    }

    return ExprAST;
}


/*
 * ======= Private: =======
 */

TokenPtr Parser::Accept(const Token::Types &Type)
{
    /* Accept token on type match */
    if (Tkn_ && TokenType() == Type)
        return AcceptIt();
    
    /* Exit with error */
    ErrorUnexpected(Type);

    return nullptr;
}

TokenPtr Parser::Accept(const Token::Types &Type, const std::string& Spell)
{
    /* Accept token on type- and spelling match */
    if (Tkn_ && TokenType() == Type && Tkn_->Spell() == Spell)
        return AcceptIt();
    
    /* Exit with error */
    ErrorUnexpected("'" + Token::Spell(Type) + "' with spelling '" + Spell + "'");

    return nullptr;
}

//#define _DEB_TOKEN_STACK_

TokenPtr Parser::AcceptIt()
{
    /* Return previous token */
    auto PrevTkn = Tkn_;

    if (!TokenQueue_.empty())
    {
        /* Pop next token from queue */
        Tkn_ = PopTokenFromQueue();
    }
    else
    {
        /* Scan next token and check for error */
        Tkn_ = Scanner_.Next();

        /* Check if end-of-file has been reached */
        if (Tkn_ == nullptr)
        {
            /* Continue reading from previous file, if there is one */
            if (!PopSourceFile())
            {
                /* Otherwise stop with error */
                Error("Scanning token failed");
            }
        }

        #ifdef _DEB_TOKEN_STACK_//!!!DEBUG!!!
        if (Tkn_ != nullptr)
            ConsoleOutput::Message("Parsed Token " + Tkn_->Pos().GetString() + ": \"" + Tkn_->Spell() + "\"");
        #endif
    }

    /* Record tokens if a current storage is used */
    PushTokenIntoStorage(Tkn_);

    return PrevTkn;
}

TokenPtr Parser::PopTokenFromQueue()
{
    /* Pop next token from queue */
    auto Tkn = TokenQueue_.front();

    #ifdef _DEB_TOKEN_STACK_//!!!DEBUG!!!
    std::string Queue;
    for (auto Tkn2 : TokenQueue_)
        Queue += Tkn2->Spell() + ", ";
    ConsoleOutput::Message(
        "POPED Token " + Tkn->Pos().GetString() + ": \"" +
        Tkn->Spell() + "\" from QUEUE: " + Queue
    );
    #endif

    TokenQueue_.pop_front();

    return Tkn;
}

void Parser::PushScannerPos()
{
    /* Push new storage list onto stack */
    TokenStorage_ = std::make_shared<TokenStorage>();
    TokenStorageStack_.push_back(TokenStorage_);

    #ifdef _DEB_TOKEN_STACK_//!!!DEBUG!!!
    ConsoleOutput::Message("Open [ Storage: " + xxStr(TokenStorageStack_.size()) + " ]");
    ConsoleOutput::UpperIndent();
    #endif

    /* Store current token in storage and start recording further tokens */
    PushTokenIntoStorage(Tkn_);
}

void Parser::PopScannerPosAndRestore()
{
    TokenStorage_ = nullptr;

    if (TokenStorageStack_.empty())
        return;

    #ifdef _DEB_TOKEN_STACK_//!!!DEBUG!!!
    ConsoleOutput::LowerIndent();
    ConsoleOutput::Message("Close And RESTORE [ Storage: " + xxStr(TokenStorageStack_.size()) + " ]");
    #endif

    /* Insert tokens into queue */
    auto TknStorage = TokenStorageStack_.back();

    TokenQueue_.insert(
        TokenQueue_.begin(),
        TknStorage->begin(), TknStorage->end()
    );

    TokenQueue_.sort(
        [](const TokenPtr& Left, const TokenPtr& Right)
        {
            return Left->Pos() < Right->Pos();
        }
    );
    TokenQueue_.unique(
        [](const TokenPtr& Left, const TokenPtr& Right)
        {
            return Left->Pos() == Right->Pos();
        }
    );

    /* Pop storage list from stack */
    TokenStorageStack_.pop_back();

    if (!TokenStorageStack_.empty())
        TokenStorage_ = TokenStorageStack_.back();

    /* Pop first token from queue */
    Tkn_ = PopTokenFromQueue();
}

void Parser::PopScannerPosAndIgnore()
{
    TokenStorage_= nullptr;

    if (TokenStorageStack_.empty())
        return;
    
    #ifdef _DEB_TOKEN_STACK_//!!!DEBUG!!!
    ConsoleOutput::LowerIndent();
    ConsoleOutput::Message("Close And IGNORE [ Storage: " + xxStr(TokenStorageStack_.size()) + " ]");
    #endif

    /* Pop and discard storage list from stack */
    TokenStorageStack_.pop_back();

    if (!TokenStorageStack_.empty())
        TokenStorage_ = TokenStorageStack_.back();
}

void Parser::PushToken(TokenPtr Tkn)
{
    if (Tkn != nullptr)
    {
        /* Push new storage list on stack */
        TokenStorage_ = std::make_shared<TokenStorage>();
        TokenStorageStack_.push_back(TokenStorage_);

        /* Store specified token in storage but don't start recording */
        TokenStorage_->push_back(Tkn);
    }
}

void Parser::PushTokenIntoStorage(TokenPtr Tkn)
{
    if (!TokenStorageStack_.empty() && Tkn != nullptr && Tkn->Type() != Token::Types::EndOfFile)
    {
        /* Store token in all token storage lists */
        for (auto TknStorage : TokenStorageStack_)
        {
            if (TknStorage->empty() || Tkn->Pos() > TknStorage->back()->Pos())
                TknStorage->push_back(Tkn);
        }

        #ifdef _DEB_TOKEN_STACK_//!!!DEBUG!!!
        if (TokenStorage_ != nullptr)
        {
            ConsoleOutput::Message(
                "Pushed [ Storage: " + xxStr(TokenStorageStack_.size()) + ", Size: " +
                xxStr(TokenStorage_->size()) + " ] Token " + Tkn->Pos().GetString() +
                ": \"" + Tkn->Spell() + "\""
            );
        }
        #endif
    }
}

void Parser::Error(const std::string &Msg, bool AppendLine, TokenPtr Tkn)
{
    if (Tkn == nullptr)
        Tkn = Tkn_;

    if (AppendLine && Tkn != nullptr && TokenType() != Token::Types::EndOfFile)
    {
        /* Setup error message and marker */
        const std::string& Line = Scanner_.GetSource()->Line();
        const std::string Mark = GetTokenErrorMarker(Line, *Tkn);

        /* Throw the syntax error */
        throw SyntaxError(Scanner_.Pos(), Msg, GetTabTruncatedLine(Line), Mark);
    }
    else
    {
        /* Throw the syntax error */
        throw SyntaxError(Scanner_.Pos(), Msg);
    }
}

void Parser::ErrorUnexpected(const std::string &Desc, bool AppendExpectedStr)
{
    /* Setup error message */
    std::string Msg = "Unexpected token '" + xxStr(Tkn_ ? Tkn_->Spell() : "<unknown>");
    Msg += (AppendExpectedStr ? "' (Expected " : "' (") + Desc + ")";

    Error(Msg, true);
}

void Parser::ErrorUnexpected(const Token::Types &Type)
{
    ErrorUnexpected("'" + Token::Spell(Type) + "'");
}

void Parser::ErrorUnexpected()
{
    /* Setup error message and marker */
    const std::string& Line = Scanner_.GetSource()->Line();
    const std::string Mark = GetTokenErrorMarker(Line, *Tkn_);

    const std::string Msg =
        xxStr("Unexpected token '") +
        xxStr(Tkn_ ? Tkn_->Spell() : "<unknown>") + xxStr("'");

    /* Throw the syntax error */
    throw SyntaxError(Scanner_.Pos(), Msg, GetTabTruncatedLine(Line), Mark);
}

void Parser::ErrorEOF()
{
    Error("Unexpected <end-of-file> token");
}

std::string Parser::GetTokenErrorMarker(const std::string& Line, const Token& Tkn) const
{
    /* Setup error marker */
    size_t From = 0, To = 0;

    To = Tkn.Pos().Column();
    if (To > 0)
        --To;

    const size_t Len = Tkn.Spell().size();

    if (To > Len)
        From = To - Len;

    /* Convert tabs to spaces */
    const size_t Num = std::min(From, Line.size());

    for (size_t i = 0; i < Num; ++i)
    {
        if (Line[i] == '\t')
            From += 3;
    }

    /* Return final marker */
    return std::string(From, ' ') + std::string(Len, '~');
}

std::string Parser::GetTabTruncatedLine(const std::string& Line) const
{
    return xxReplaceString(Line, "\t", std::string(4, ' '));
}

ExpressionPtr Parser::BuildBinExprTree(
    std::vector<ExpressionPtr> ExprList, std::vector<BinaryOperatorPtr> OpList)
{
    if (ExprList.size() != OpList.size() + 1)
        Error("Number of operator and sub-expression mismatch");

    if (OpList.empty())
        return ExprList.front();

    /* Create AST nodes right-to-left */
    auto itExpr = ExprList.begin();
    auto Expr1 = *itExpr;
    ++itExpr;

    for (auto Op : OpList)
    {
        auto Expr2 = *itExpr;
        Expr1 = std::make_shared<BinaryExpression>(Expr1, Op, Expr2);
        ++itExpr;
    }

    return Expr1;
}

void Parser::SplitToken(
    const Token::Types& T1, const std::string& S1,
    const Token::Types& T2, const std::string& S2)
{
    /* Modify current token */
    Tkn_->Modify(T1, S1);

    /* Store next split-part in the temporary token storage */
    PushToken(std::make_shared<Token>(Tkn_->Pos(), T2, S2));
}

void Parser::CheckCommandParseFlagsError(const Command::ParseFlags::DataType& Flags)
{
    if ((Flags & Command::ParseFlags::AllowDefinitionsOnly) != 0)
        ErrorUnexpected("Token is not allowed in current command block", false);
    if ((Flags & Command::ParseFlags::GlobalScope) != 0)
        ErrorUnexpected("Token is not allowed in global scope", false);
}

void Parser::CheckCommandParseFlags(const Command::ParseFlags::DataType& Flags)
{
    const auto Tp = TokenType();

    if ((Flags & Command::ParseFlags::AllowDefinitionsOnly) != 0)
    {
        if ( Tp == Token::Types::If         ||
             Tp == Token::Types::Do         ||
             Tp == Token::Types::While      ||
             Tp == Token::Types::Until      ||
             Tp == Token::Types::Forever    ||
             Tp == Token::Types::For        ||
             Tp == Token::Types::Return     ||
             Tp == Token::Types::Import     ||
             Tp == Token::Types::Package    ||
             Tp == Token::Types::Try        ||
             Tp == Token::Types::Throw      ||
             Tp == Token::Types::LCurly     ||
             Tp == Token::Types::Switch     ||
             Tp == Token::Types::Stop       ||
             Tp == Token::Types::Next       ||
             Tp == Token::Types::Break )
        {
            CheckCommandParseFlagsError(Flags);
        }
    }

    if ((Flags & Command::ParseFlags::GlobalScope) != 0)
    {
        if ( Tp == Token::Types::If         ||
             Tp == Token::Types::Do         ||
             Tp == Token::Types::While      ||
             Tp == Token::Types::Until      ||
             Tp == Token::Types::Forever    ||
             Tp == Token::Types::For        ||
             Tp == Token::Types::Return     ||
             Tp == Token::Types::Try        ||
             Tp == Token::Types::Throw      ||
             Tp == Token::Types::LCurly     ||
             Tp == Token::Types::Switch     ||
             Tp == Token::Types::Stop       ||
             Tp == Token::Types::Next       ||
             Tp == Token::Types::Break )
        {
            CheckCommandParseFlagsError(Flags);
        }
    }
}

bool Parser::OpenSourceFile(const std::string& Filename)
{
    ConsoleOutput::Message("Parsing source file \"" + Filename + "\" ...");

    /* Open file for reading */
    SourceFile_ = SourceFile::Open(Filename);
    if (SourceFile_ == nullptr)
        return false;

    /* Start scanning source */
    if (!Scanner_.ScanSource(SourceFile_))
    {
        ConsoleOutput::Error("Scanning source failed");
        return false;
    }

    return true;
}

void Parser::PushSourceFile(const std::string& Filename)
{
    /* Store current file onto stack */
    SourceFileStack_.push(SourceFile_);
    Scanner_.Push();

    /* Start reading the new file */
    if (!OpenSourceFile(Filename))
        throw FileError(Scanner_.Pos(), "Including file \"" + Filename + "\" failed");
    
    /* Begin with parsing */
    AcceptIt();
}

bool Parser::PopSourceFile()
{
    if (SourceFileStack_.empty())
        return false;

    /* Restore previous file from stack */
    SourceFile_ = SourceFileStack_.top();
    SourceFileStack_.pop();

    /* Continue scanning */
    if (!Scanner_.ScanSource(SourceFile_, false))
    {
        ConsoleOutput::Error("Scanning source failed");
        return false;
    }

    Scanner_.Pop();

    /* Continue with parsing (this accepts the previous filename literal token) */
    AcceptIt();

    return true;
}

std::string Parser::GetAbsolutePath(const std::string& Filename) const
{
    return MainSourcePath_ + "/" + Filename;
}

/* ------- Literal parse functions ------- */

LiteralPtr Parser::ParseLiteral()
{
    switch (TokenType())
    {
        case Token::Types::HexLiteral:
        case Token::Types::OctLiteral:
        case Token::Types::BinLiteral:
        case Token::Types::IntLiteral:
            return ParseIntegerLiteral();
        case Token::Types::BoolLiteral:
            return ParseBoolLiteral();
        case Token::Types::FloatLiteral:
            return ParseFloatLiteral();
        case Token::Types::StringLiteral:
            return ParseStringLiteral();
        case Token::Types::PointerLiteral:
            return ParsePointerLiteral();
        default:
            ErrorUnexpected("literal");
            break;
    }
    return nullptr;
}

LiteralPtr Parser::ParseArithmeticLiteral()
{
    switch (TokenType())
    {
        case Token::Types::HexLiteral:
        case Token::Types::OctLiteral:
        case Token::Types::BinLiteral:
        case Token::Types::IntLiteral:
            return ParseIntegerLiteral();
        case Token::Types::FloatLiteral:
            return ParseFloatLiteral();
        default:
            ErrorUnexpected("arithmetic literal");
            break;
    }
    return nullptr;
}

BoolLiteralPtr Parser::ParseBoolLiteral()
{
    /* Parse boolean literal */
    auto LiteralTkn = Accept(Token::Types::BoolLiteral);
    return std::make_shared<BoolLiteral>(LiteralTkn);
}

IntegerLiteralPtr Parser::ParseIntegerLiteral(bool AllowNegativeValue)
{
    /* Parse integer literal */
    IntegerLiteralPtr LiteralAST;

    /* Parse optional negation token */
    bool IsNeg = false;
    if (AllowNegativeValue && TokenType() == Token::Types::SubOp)
    {
        AcceptIt();
        IsNeg = true;
    }

    switch (TokenType())
    {
        case Token::Types::IntLiteral:
            LiteralAST = std::make_shared<IntegerLiteral>(AcceptIt());
            break;

        case Token::Types::HexLiteral:
            /* Parse hex-literal and convert to an integer-literal */
            LiteralAST = std::make_shared<IntegerLiteral>(AcceptIt());
            LiteralAST->Spell = xxStr(xxHexToNum<unsigned long long int>(LiteralAST->Spell));
            break;

        case Token::Types::OctLiteral:
            /* Parse oct-literal and convert to an integer-literal */
            LiteralAST = std::make_shared<IntegerLiteral>(AcceptIt());
            LiteralAST->Spell = xxStr(xxOctToNum<unsigned long long int>(LiteralAST->Spell));
            break;

        case Token::Types::BinLiteral:
            /* Parse bin-literal and convert to an integer-literal */
            LiteralAST = std::make_shared<IntegerLiteral>(AcceptIt());
            LiteralAST->Spell = xxStr(xxBinToNum<unsigned long long int>(LiteralAST->Spell));
            break;

        default:
            ErrorUnexpected("Integer literal (in hex-, decimal-, octal- or binary notation)");
            break;
    }

    /* Negate literal */
    if (IsNeg)
        LiteralAST->Spell = "-" + LiteralAST->Spell;

    return LiteralAST;
}

FloatLiteralPtr Parser::ParseFloatLiteral()
{
    /* Parse float literal */
    auto LiteralTkn = Accept(Token::Types::FloatLiteral);
    return std::make_shared<FloatLiteral>(LiteralTkn);
}

StringLiteralPtr Parser::ParseStringLiteral()
{
    /* Parse string literal */
    auto LiteralTkn = Accept(Token::Types::StringLiteral);
    return std::make_shared<StringLiteral>(LiteralTkn);
}

PointerLiteralPtr Parser::ParsePointerLiteral()
{
    /* Parser pointer literal */
    auto LiteralTkn = Accept(Token::Types::PointerLiteral);
    return std::make_shared<PointerLiteral>(LiteralTkn);
}

/* ------- Type-denoter parse functions ------- */

TypeDenoterPtr Parser::ParseTypeDenoter(bool IsOptional, bool AllowFurtherParents)
{
    /* Parse first single type dentoer */
    auto TDenoter = ParseSingleTypeDenoter(IsOptional);

    /* Parse optional next parent type */
    return AllowFurtherParents ? ParseParentTypeDenoter(TDenoter) : TDenoter;
}

TypeDenoterPtr Parser::ParseSingleTypeDenoter(bool IsOptional)
{
    /* Parse type denoter */
    switch (TokenType())
    {
        case Token::Types::VoidTypeDenoter:
            return std::make_shared<VoidTypeDenoter>(AcceptIt());
        case Token::Types::ConstTypeDenoter:
            return ParseConstTypeDenoter();
        case Token::Types::BoolTypeDenoter:
            return std::make_shared<BoolTypeDenoter>(AcceptIt());
        case Token::Types::IntTypeDenoter:
            return std::make_shared<IntTypeDenoter>(AcceptIt());
        case Token::Types::FloatTypeDenoter:
            return std::make_shared<FloatTypeDenoter>(AcceptIt());
        case Token::Types::StringTypeDenoter:
            return std::make_shared<StringTypeDenoter>(AcceptIt());
        case Token::Types::ProcTypeDenoter:
            return ParseProcTypeDenoter();
        case Token::Types::Identifier:
            return ParseCustomTypeDenoter();
        default:
            if (!IsOptional)
                ErrorUnexpected("'<type denoter>'");
            break;
    }
    return nullptr;
}

/*
What is a PARENT TYPE DENOTER?
  A parent type denoter is described in the parent AST node of a type denoter tree,
  e.g. for "int*" the pointer type '*' is the parent type denoter,
  for "int[10]" the array type '[10]' is the parent type denoter and
  for "int*[10]@" the smart pointer type '@' is the parent type denoter and so on.
*/
TypeDenoterPtr Parser::ParseParentTypeDenoter(TypeDenoterPtr TDenoter, bool AllowFurtherParents)
{
    /* Check for optional pointers */
    switch (TokenType())
    {
        case Token::Types::MulOp:
            return ParseRawPtrTypeDenoter(TDenoter, AllowFurtherParents);
        case Token::Types::At:
            return ParseManagedPtrTypeDenoter(TDenoter, AllowFurtherParents);
        case Token::Types::ConstTypeDenoter:
            return ParseConstChildTypeDenoter(TDenoter, AllowFurtherParents);
        case Token::Types::BitwiseAndOp:
            return ParseRefTypeDenoter(TDenoter);
        case Token::Types::LParen:
            return ParseArrayTypeDenoter(TDenoter, AllowFurtherParents);
        default:
            break;
    }
    return TDenoter;
}

ConstTypeDenoterPtr Parser::ParseConstTypeDenoter()
{
    /* Parse constant type */
    auto ConstTkn = Accept(Token::Types::ConstTypeDenoter);
    auto TDenoter = ParseTypeDenoter(false, false);

    /* Create constant type denoter */
    return std::make_shared<ConstTypeDenoter>(ConstTkn, TDenoter);
}

TypeDenoterPtr Parser::ParseCustomTypeDenoter(FNameIdentPtr FNameIdentAST)
{
    /* Parse custom type name */
    bool HasDefName = (FNameIdentAST != nullptr);

    if (!HasDefName)
        FNameIdentAST = ParseFNameIdent(FNameIdent::InFlags::AllowTemplateArgs);

    /* Create custom type denoter AST node */
    auto TDenoterAST = std::make_shared<CustomTypeDenoter>(FNameIdentAST);

    if (HasDefName)
        return ParseParentTypeDenoter(TDenoterAST);

    return TDenoterAST;
}

ProcTypeDenoterPtr Parser::ParseProcTypeDenoter()
{
    /* Parse type list opening '<' character */
    Accept(Token::Types::ProcTypeDenoter);
    Accept(Token::Types::RelationOp, "<");

    /* Parse return type denoter */
    auto TDenoterAST = ParseTypeDenoter();

    /* Parse parameter list */
    auto ParamListAST = ParseParameterList();

    /* Parse type list closing '>' character */
    Accept(Token::Types::RelationOp, ">");

    /* Create procedure type denoter */
    auto ProcTDenoterAST = std::make_shared<ProcTypeDenoter>(TDenoterAST, ParamListAST);

    return ProcTDenoterAST;
}

TypeDenoterPtr Parser::ParseRawPtrTypeDenoter(TypeDenoterPtr TDenoter, bool AllowFurtherParents)
{
    /* Parse raw-pointer type */
    auto Tkn = Accept(Token::Types::MulOp);
    TDenoter = std::make_shared<RawPtrTypeDenoter>(Tkn, TDenoter);

    /* Parse optional next parent type */
    return AllowFurtherParents ? ParseParentTypeDenoter(TDenoter) : TDenoter;
}

TypeDenoterPtr Parser::ParseManagedPtrTypeDenoter(TypeDenoterPtr TDenoter, bool AllowFurtherParents)
{
    /* Parse raw-pointer type */
    auto Tkn = Accept(Token::Types::At);
    TDenoter = std::make_shared<MngPtrTypeDenoter>(Tkn, TDenoter);

    /* Parse optional next parent type */
    return AllowFurtherParents ? ParseParentTypeDenoter(TDenoter) : TDenoter;
}

TypeDenoterPtr Parser::ParseConstChildTypeDenoter(TypeDenoterPtr TDenoter, bool AllowFurtherParents)
{
    /* Parse constant type denoter */
    auto Tkn = Accept(Token::Types::ConstTypeDenoter);

    /* Parse child type denoter (but use the 'parent' parsing function) */
    auto ChildTDenoter = ParseParentTypeDenoter(TDenoter, false);

    if (ChildTDenoter == TDenoter)
        Error("Missing type denoter after 'const' token");

    TDenoter = std::make_shared<ConstTypeDenoter>(Tkn, ChildTDenoter);

    /* Parse optional next parent type */
    return AllowFurtherParents ? ParseParentTypeDenoter(TDenoter) : TDenoter;
}

TypeDenoterPtr Parser::ParseRefTypeDenoter(TypeDenoterPtr TDenoter)
{
    /* Parse raw-pointer type */
    auto Tkn = Accept(Token::Types::BitwiseAndOp);
    return std::make_shared<RefTypeDenoter>(Tkn, TDenoter);
}

TypeDenoterPtr Parser::ParseArrayTypeDenoter(TypeDenoterPtr TDenoter, bool AllowFurtherParents)
{
    /* Parse array dimension opening '[' character */
    Accept(Token::Types::LParen);

    /* Create array dimension AST node */
    auto ArrayTDenoter = std::make_shared<ArrayTypeDenoter>(TDenoter);
    
    /* Check if this is a static array */
    if (TokenType() != Token::Types::RParen)
    {
        /* Parse primary array dimension value */
        ArrayTDenoter->Literal = ParseIntegerLiteral(true);

        /* Parse optional range dimension */
        if (TokenType() == Token::Types::RangeSep)
        {
            AcceptIt();

            /* Parse secondary array dimension value */
            auto SecDimLiteralAST = ParseIntegerLiteral(true);

            /* Swap integer literals in AST node */
            ArrayTDenoter->OffsetLiteral = SecDimLiteralAST;
            std::swap(ArrayTDenoter->Literal, ArrayTDenoter->OffsetLiteral);

            /* Check for array size */
            int Start = ArrayTDenoter->GetOffset();
            int End = ArrayTDenoter->GetPrimDim();

            if (End < Start)
                Error("End range can not be smaller than start range for arrays");
        }
    }

    /* Parse array dimension closing ']' character */
    Accept(Token::Types::RParen);

    /* Parse optional next parent type */
    return AllowFurtherParents ? ParseParentTypeDenoter(ArrayTDenoter) : ArrayTDenoter;
}

/* ------- Other parse functions ------- */

IdentifierPtr Parser::ParseIdentifier()
{
    /* Parse identifier token */
    auto IdentToken = Accept(Token::Types::Identifier);
    return std::make_shared<Identifier>(IdentToken);
}

FNameIdentPtr Parser::ParseFNameIdent(
    const FNameIdent::InFlags::DataType& InFlags, FNameIdent::OutFlags::DataType* OutFlags)
{
    /* Parse identifier */
    auto IdentAST = ParseIdentifier();

    auto FNameIdentAST = std::make_shared<FNameIdent>(IdentAST);

    /* Parse optional template parameter list */
    if (TokenType() == Token::Types::RelationOp && Tkn_->Spell() == "<")
    {
        try
        {
            /*
            Store scanner position
            -> maybe this is not a template instantiation but a comparision inside an expressions
            */
            PushScannerPos();

            if ((InFlags & FNameIdent::InFlags::AllowTemplateParams) != 0)
                FNameIdentAST->TParamList = ParseTemplateParameterList();
            else if ((InFlags & FNameIdent::InFlags::AllowTemplateArgs) != 0)
                FNameIdentAST->TArgList = ParseTemplateArgumentList();
            /*else if (InFlags has flag 'allow array indexing')
                ... = parse array indexing ... */

            /* Drop previous scanner position */
            PopScannerPosAndIgnore();
        }
        catch (const SyntaxError&)
        {
            /* Ignore error and reset scanner position */
            PopScannerPosAndRestore();
        }
    }
    /* Otherwise try to parse an array index list */
    else if (TokenType() == Token::Types::LParen)
        FNameIdentAST->ArrayList = ParseArrayIndexList();

    /* Parse further optional FName identifiers */
    if (TokenType() == Token::Types::Dot)
    {
        /* Also return information about multi-identifier */
        if (OutFlags != nullptr)
            (*OutFlags) |= FNameIdent::OutFlags::HasMultiIdent;

        /* Parse next FName identifier */
        AcceptIt();
        FNameIdentAST->Next = ParseFNameIdent(InFlags);
    }

    return FNameIdentAST;
}

EnumEntryPtr Parser::ParseEnumEntry()
{
    /* Parse enumeration entry name */
    auto IdentAST = ParseIdentifier();

    auto EnumEntryAST = std::make_shared<EnumEntry>(IdentAST);

    /* Parse optional identification string */
    if (TokenType() == Token::Types::LBracket)
    {
        AcceptIt();
        EnumEntryAST->Id = ParseStringLiteral();
        Accept(Token::Types::RBracket);
    }

    /* Parse optional initialization */
    if (Tkn_->IsSetAssign())
    {
        /* Accept assign operator and parse initialization expression */
        AcceptIt();
        EnumEntryAST->Expr = ParseExpression();
    }

    return EnumEntryAST;
}

CaseBlockPtr Parser::ParseCaseBlock()
{
    /* Parse case or default block of switch statement */
    auto CaseBlockAST = std::make_shared<CaseBlock>(Tkn_->Pos());

    if (TokenType() == Token::Types::Case)
    {
        AcceptIt();
        CaseBlockAST->Expr = ParseExpression();
        Accept(Token::Types::Colon);
    }
    else if (TokenType() == Token::Types::Default)
    {
        AcceptIt();
        Accept(Token::Types::Colon);
    }
    else
        ErrorUnexpected("'case' or 'def' token in switch statement");

    /* Parse commands until end of case block */
    while ( TokenType() != Token::Types::RCurly &&
            TokenType() != Token::Types::Case   &&
            TokenType() != Token::Types::Default )
    {
        /* Parse next command */
        auto CmdAST = ParseCommand();
        if (CmdAST != nullptr)
            CaseBlockAST->Commands.push_back(CmdAST);
    }

    return CaseBlockAST;
}

VariableObjectPtr Parser::ParseVariableObject(
    TypeDenoterPtr TDenoterAST, IdentifierPtr IdentAST, const Expression::ParseFlags::DataType ExprFlags)
{
    /* Parse type denoter and/or identifier if not passed as argument */
    if (TDenoterAST == nullptr)
        TDenoterAST = ParseTypeDenoter();
    if (IdentAST == nullptr)
        IdentAST = ParseIdentifier();

    /* Create variable object AST node */
    auto VarObjAST = std::make_shared<VariableObject>(TDenoterAST, IdentAST);

    /* Parse optional initialization */
    if (Tkn_->IsSetAssign())
    {
        /* Accept assign operator and parse initialization expression */
        AcceptIt();
        VarObjAST->Expr = ParseExpression(ExprFlags);
    }

    return VarObjAST;
}

PatternObjectPtr Parser::ParsePatternObject()
{
    /* Parse pattern name */
    Accept(Token::Types::Pattern);

    auto IdentAST = ParseIdentifier();

    /* Create pattern object AST node */
    auto PatternObjAST = std::make_shared<PatternObject>(IdentAST);

    /* Parse optional default type denoter */
    if (Tkn_->IsSetAssign())
    {
        AcceptIt();
        PatternObjAST->DefTDenoter = ParseTypeDenoter();
    }

    return PatternObjAST;
}

/* ------- List parse functions ------- */

void Parser::ParseDenominator(bool IsDenomRequired, IdentifierPtr& DenomIdent, ExpressionPtr& ExprAST, const TokenPtr& TempTkn)
{
    if (IsDenomRequired || TokenType() == Token::Types::Colon)
    {
        /* Print helpful error message if requirements are not fulfilled */
        if (IsDenomRequired && TokenType() != Token::Types::Colon)
            Error("Parameter denominator is required but not found");

        /* Parse parameter denomination */
        IsDenomRequired = true;
        AcceptIt();

        /*
        Check that previously parsed expression only consists of
        a memory-object (which will be converted to an identifier for the parameter denomination
        */
        if (ExprAST->Type() != Expression::Types::ObjectExpr)
            Error("Parameter denominators may only contain a single identifier", true, TempTkn);

        auto DenomExpr = dynamic_cast<ObjectExpression*>(ExprAST.get());

        if (DenomExpr->FName->Next != nullptr)
            Error("Only a single identifier is allowed for parameter denomination", true, TempTkn);

        DenomIdent = DenomExpr->FName->Ident;

        /* Parse new expression (and drop old expression) */
        ExprAST = ParseExpression();
    }
}

ParameterListPtr Parser::ParseParameterList()
{
    /* Parse parameter list opening '(' character */
    Accept(Token::Types::LBracket);

    /* Parse parameters */
    ParameterListPtr ParamList;

    if (TokenType() != Token::Types::RBracket)
        ParamList = ParseSingleParameter();

    /* Accept parameter list closing ')' character */
    Accept(Token::Types::RBracket);

    return ParamList;
}

ParameterListPtr Parser::ParseSingleParameter()
{
    /* Parse first parameter */
    auto VarObjAST = ParseVariableObject();

    auto ParamList = std::make_shared<ParameterList>(VarObjAST);
    
    /* Check for further parameters */
    if (TokenType() == Token::Types::Comma)
    {
        /* Parse next parameter */
        AcceptIt();
        ParamList->Next = ParseSingleParameter();
    }

    return ParamList;
}

ArgumentListPtr Parser::ParseArgumentList()
{
    /* Parse argument list opening '(' character */
    Accept(Token::Types::LBracket);

    /* Parse arguments */
    ArgumentListPtr ArgList;

    if (TokenType() != Token::Types::RBracket)
        ArgList = ParseSingleArgument();

    /* Accept argument list closing ')' character */
    Accept(Token::Types::RBracket);

    return ArgList;
}

ArgumentListPtr Parser::ParseSingleArgument(bool IsDenomRequired)
{
    auto TempTkn = Tkn_;

    /* Parse first argument */
    auto ExprAST = ParseExpression();

    /* Check if denonimated function call is used */
    IdentifierPtr DenomIdent;
    ParseDenominator(IsDenomRequired, DenomIdent, ExprAST, TempTkn);

    /* Create argument list AST node */
    auto ArgList = std::make_shared<ArgumentList>(ExprAST);

    if (DenomIdent != nullptr)
        ArgList->Denom = DenomIdent;
    
    /* Check for further arguments */
    if (TokenType() == Token::Types::Comma)
    {
        /* Parse next argument */
        AcceptIt();
        ArgList->Next = ParseSingleArgument(IsDenomRequired);
    }

    return ArgList;
}

TemplateParamListPtr Parser::ParseTemplateParameterList()
{
    /* Parse type list opening '<' character */
    Accept(Token::Types::RelationOp, "<");

    /* Parse parameters */
    TemplateParamListPtr TParamList;

    if (TokenType() != Token::Types::RBracket)
        TParamList = ParseSingleTemplateParameter();

    /* Accept type list closing '>' character */
    Accept(Token::Types::RelationOp, ">");

    return TParamList;
}

TemplateParamListPtr Parser::ParseSingleTemplateParameter()
{
    /* Parse first parameter */
    TemplateParamListPtr TParamList;

    ObjectPtr ObjAST;

    if (TokenType() == Token::Types::Pattern)
    {
        /* Parse pattern object as template parameter */
        ObjAST = ParsePatternObject();
    }
    else
    {
        /*
        Parse variable object as template parameter.
        -> Relations are not directly allowed here, because they
           must terminate the template parameter list.
        */
        ObjAST = ParseVariableObject(
            nullptr, nullptr, Expression::ParseFlags::IgnoreRelationOp
        );
    }
    
    /* Create template parameter AST node */
    TParamList = std::make_shared<TemplateParamList>(ObjAST);

    /* Check for further parameters */
    if (TokenType() == Token::Types::Comma)
    {
        /* Parse next parameter */
        AcceptIt();
        TParamList->Next = ParseSingleTemplateParameter();
    }

    return TParamList;
}

TemplateArgListPtr Parser::ParseTemplateArgumentList()
{
    /* Setup temporary scanner configuration */
    const bool CfgAllowShiftOp = Scanner_.Config().AllowShiftOp;
    Scanner_.Config().AllowShiftOp = false;
    
    /* Parse type list opening '<' character */
    Accept(Token::Types::RelationOp, "<");
    
    /* Parse parameters */
    TemplateArgListPtr TArgList;

    if (TokenType() != Token::Types::RBracket)
        TArgList = ParseSingleTemplateArgument();

    /* Accept type list closing '>' character */
    Accept(Token::Types::RelationOp, ">");

    /* Reset scanner configuration */
    Scanner_.Config().AllowShiftOp = CfgAllowShiftOp;

    return TArgList;
}

TemplateArgListPtr Parser::ParseSingleTemplateArgument()
{
    /* Parse first argument */
    TemplateArgListPtr TArgList;
    
    if (TokenType() == Token::Types::Use)
    {
        /* Parse type denoter as template argument */
        AcceptIt();        
        auto TDenoterAST = ParseTypeDenoter();

        /* Create template argument AST node */
        TArgList = std::make_shared<TemplateArgList>(TDenoterAST);
    }
    else
    {
        /* Parse expression as template argument */
        auto ExprAST = ParseExpression(Expression::ParseFlags::IgnoreRelationOp);

        /* Create template argument AST node */
        TArgList = std::make_shared<TemplateArgList>(ExprAST);
    }

    /* Check for further arguments */
    if (TokenType() == Token::Types::Comma)
    {
        /* Parse next argument */
        AcceptIt();
        TArgList->Next = ParseSingleTemplateArgument();
    }

    return TArgList;
}

InitializerListPtr Parser::ParseInitializerList()
{
    /* Parse initializer list opening '{' character */
    Accept(Token::Types::LCurly);

    /* Parse initializers */
    InitializerListPtr InitList;

    if (TokenType() != Token::Types::RCurly)
        InitList = ParseSingleInitializer();

    /* Accept initializer list closing '}' character */
    Accept(Token::Types::RCurly);

    return InitList;
}

InitializerListPtr Parser::ParseSingleInitializer(bool IsDenomRequired)
{
    auto TempTkn = Tkn_;

    /* Parse first initializer */
    auto ExprAST = ParseExpression();

    /* Check if denonimated initialization is used */
    IdentifierPtr DenomIdent;
    ParseDenominator(IsDenomRequired, DenomIdent, ExprAST, TempTkn);

    /* Create initializer list AST node */
    auto InitList = std::make_shared<InitializerList>(ExprAST);

    if (DenomIdent != nullptr)
        InitList->Denom = DenomIdent;
    
    /* Check for further initializers */
    if (TokenType() == Token::Types::Comma)
    {
        /* Parse next initializer */
        AcceptIt();
        InitList->Next = ParseSingleInitializer(IsDenomRequired);
    }

    return InitList;
}

AttributeListPtr Parser::ParseAttributeList()
{
    /* Setup temporary scanner configuration */
    const bool CfgAllowRDParent = Scanner_.Config().AllowRDParent;
    Scanner_.Config().AllowRDParent = true;
    
    /* Parse attribute list opening '[[' token */
    Accept(Token::Types::LDParen);
    
    /* Parse attributes */
    auto AttribList = ParseSingleAttribute();

    /* Parse attribute list closing ']]' token */
    Accept(Token::Types::RDParen);

    /* Reset scanner configuration */
    Scanner_.Config().AllowRDParent = CfgAllowRDParent;

    return AttribList;
}

AttributeListPtr Parser::ParseSingleAttribute()
{
    /* Parse first attribute */
    auto IdentAST = ParseIdentifier();

    /* Create attribute list AST node */
    auto AttribList = std::make_shared<AttributeList>(IdentAST);

    /* Check for further attributes */
    if (TokenType() == Token::Types::Comma)
    {
        /* Parse next attribute */
        AcceptIt();
        AttribList->Next = ParseSingleAttribute();
    }

    return AttribList;
}

ArrayIndexListPtr Parser::ParseArrayIndexList(bool IsOptional)
{
    /* Parse first array index */
    if (TokenType() == Token::Types::LParen)
        return ParseSingleArrayIndex();
    else if (!IsOptional)
        ErrorUnexpected(Token::Types::LParen);
    return nullptr;
}

ArrayIndexListPtr Parser::ParseSingleArrayIndex()
{
    /* Parse initializer list opening '[' character */
    Accept(Token::Types::LParen);

    /* Parse array index expression */
    auto ExprAST = ParseArithmeticExpr();

    auto IndexList = std::make_shared<ArrayIndexList>(ExprAST);

    /* Accept initializer list closing ']' character */
    Accept(Token::Types::RParen);

    /* Parse optional next array index */
    if (TokenType() == Token::Types::LParen)
        IndexList->Next = ParseSingleArrayIndex();

    return IndexList;
}

/* ------- Exceptional parse functions ------- */

void Parser::ParseIncludeDirective()
{
    /* Parse include directive and filename string literal */
    Accept(Token::Types::Include);

    if (TokenType() != Token::Types::StringLiteral)
        Error("Missing string literal after \"include\" directive");
    
    /* Get current file path */
    const auto& CurFilePath = xxFilePath(SourceFile_->Filename());

    /* Start reading new file */
    auto Filename = CurFilePath + "/" + Tkn_->Spell();//GetAbsolutePath(Tkn_->Spell());
    PushSourceFile(Filename);

    /* Check if this file should only be included once */
    if (TokenType() == Token::Types::Once)
    {
        /* Check if this file has already been included */
        auto it = FileIncludeTracker_.find(Filename);

        if (it != FileIncludeTracker_.end() && it->second > 0)
        {
            /* Leave file immediately */
            PopSourceFile();
            return;
        }

        /* -> Don't accept the "once" keyword here, we need it later on */
    }

    /* Increment include file tracker */
    ++FileIncludeTracker_[Filename];
}

RangeForLoopCommand::index_t Parser::ParseIterationIndex()
{
    /* Check for negation */
    bool IsNeg = false;

    if (TokenType() == Token::Types::SubOp)
    {
        AcceptIt();
        IsNeg = true;
    }

    /* Parse integral value */
    auto IntLiteralAST = ParseIntegerLiteral();

    auto Index = xxNumber<RangeForLoopCommand::index_t>(IntLiteralAST->Spell);

    /* Return iteration index value */
    if (IsNeg)
        Index = -Index;

    return Index;
}


} // /namespace SyntacticAnalyzer



// ================================================================================