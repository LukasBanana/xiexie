/*
 * Parser commands file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Parser.h"
#include "StringMod.h"
#include "ConsoleOutput.h"
#include "ASTIncludes.h"

#include <algorithm>


namespace SyntacticAnalyzer
{


/* ------- Command parse functions ------- */

CommandPtr Parser::ParseCommand(const Command::ParseFlags::DataType& Flags)
{
    /* Check flags */
    if (Flags != 0)
        CheckCommandParseFlags(Flags);

    /* Parse current command */
    switch (TokenType())
    {
        case Token::Types::Static:
        case Token::Types::VoidTypeDenoter:
        case Token::Types::ConstTypeDenoter:
        case Token::Types::BoolTypeDenoter:
        case Token::Types::IntTypeDenoter:
        case Token::Types::FloatTypeDenoter:
        case Token::Types::StringTypeDenoter:
        case Token::Types::ProcTypeDenoter:
            return ParseFuncOrVarDef();

        case Token::Types::Virtual:
        case Token::Types::Abstract:
            return ParseFunctionDef();

        case Token::Types::Identifier:
            return ParseCallOrAssignOrDef(Flags);

        case Token::Types::If:
            return ParseIfCommand();

        case Token::Types::Do:
        case Token::Types::While:
        case Token::Types::Until:
            return ParseWhileLoopCommand();
        case Token::Types::Forever:
            return ParseInfiniteLoopCommand();
        case Token::Types::For:
            //!TODO! -> determine which kind of for-loop this is!!!
            return ParseRangeForLoopCommand();

        case Token::Types::Return:
            return ParseReturnCommand();

        case Token::Types::Import:
            return ParseImportCommand();

        case Token::Types::Package:
            return ParsePackageCommand();

        case Token::Types::Cpp:
            return ParseInlineLangCommand();

        case Token::Types::Class:
            return ParseClassDeclCommand();

        case Token::Types::Try:
            return ParseTryCatchCommand();
        case Token::Types::Throw:
            return ParseThrowCommand();

        case Token::Types::Enum:
            return ParseEnumDeclCommand();

        case Token::Types::Flags:
            return ParseFlagsDeclCommand();

        case Token::Types::Alias:
            return ParseTypeDefCommand();

        case Token::Types::Switch:
            return ParseSwitchCommand();

        case Token::Types::Stop:
        case Token::Types::Next:
        case Token::Types::Break:
        case Token::Types::Once:
        case Token::Types::PushConfig:
        case Token::Types::PopConfig:
            return ParsePrimitiveCommand();

        case Token::Types::LCurly:
            return ParseBlockCommand( /* Don't pass given parse flags here! */ );

        case Token::Types::LDParen:
            return ParseAttributeDecl();

        case Token::Types::Decl:
            return ParseDeclBlock();

        case Token::Types::Assert:
            return ParseAssertCommand();

        case Token::Types::Include:
            /* Include directive does not create a command object! */
            ParseIncludeDirective();
            break;

        default:
            /* If end-of-file has been reached, try to continue previous included file */
            if (TokenType() != Token::Types::EndOfFile || !PopSourceFile())
                ErrorUnexpected();
            break;
    }

    return nullptr;
}

CommandPtr Parser::ParseFuncOrVarDef()
{
    /* Parse optional static qualifier */
    bool IsStatic = false;

    if (TokenType() == Token::Types::Static)
    {
        AcceptIt();
        IsStatic = true;
    }

    /* Parse type denoter */
    auto TypeDenoterAST = ParseTypeDenoter();

    /* Parse identifier */
    FNameIdent::OutFlags::DataType OutFlags = 0;
    auto FNameIdentAST = ParseFNameIdent(FNameIdent::InFlags::AllowTemplateParams, &OutFlags);

    /* Check if the identifier is used for a function.
    This occurs if the identifier is a multi-identifier
    or the identifier is followed by an L-bracket. */
    if ( (OutFlags & FNameIdent::OutFlags::HasMultiIdent) != 0 ||
         (OutFlags & FNameIdent::OutFlags::HasTemplateParams) != 0 ||
         TokenType() == Token::Types::LBracket )
    {
        return ParseFunctionDef(TypeDenoterAST, FNameIdentAST);
    }
    
    /* Parse variable definition */
    return ParseVariableDef(TypeDenoterAST, FNameIdentAST->Ident, nullptr, IsStatic);
}

CommandPtr Parser::ParseCallOrAssignOrDef(const Command::ParseFlags::DataType& Flags)
{
    /* Parse identifier string (single identifier or multiple identifier separated with dots) */
    auto FNameIdentAST = ParseFNameIdent(FNameIdent::InFlags::AllowTemplateArgs);

    /* Parse function call, assignment or definition */
    switch (TokenType())
    {
        case Token::Types::LBracket:
            CheckCommandParseFlagsError(Flags);
            return ParseCallCommand(FNameIdentAST);

        case Token::Types::AssignOp:
            CheckCommandParseFlagsError(Flags);
            return ParseAssignCommand(FNameIdentAST);

        /*
        Check for variable definition (var-name, const-pointer,
        raw-pointer, reference type or smart pointer)
        */
        case Token::Types::Identifier:
        case Token::Types::ConstTypeDenoter:
        case Token::Types::MulOp:
        case Token::Types::BitwiseAndOp:
        case Token::Types::At:
            return ParseVariableOrProcedureDef(ParseCustomTypeDenoter(FNameIdentAST));

        default:
            ErrorUnexpected("function call, assignment or definition");
            break;
    }

    return nullptr;
}

CommandPtr Parser::ParseVariableOrProcedureDef(TypeDenoterPtr TDenoterAST)
{
    /* Parse identifier */
    FNameIdent::OutFlags::DataType OutFlags = 0;
    auto FNameAST = ParseFNameIdent(FNameIdent::InFlags::AllowTemplateParams, &OutFlags);

    /* Check if this is a variable or procedure definition */
    if ( (OutFlags & FNameIdent::OutFlags::HasTemplateParams) != 0 ||
         TokenType() == Token::Types::LBracket )
    {
        /* Parse procedure definition */
        return ParseFunctionDef(TDenoterAST, FNameAST);
    }

    /* Parse variable definition */
    return ParseVariableDef(TDenoterAST, FNameAST->Ident);
}

CommandPtr Parser::ParseAttributeDecl()
{
    /* Parse attribute identifer */
    auto AttribListAST = ParseAttributeList();

    switch (TokenType())
    {
        case Token::Types::Class:
            /* Parse class declaration */
            return ParseClassDeclCommand(AttribListAST);
        case Token::Types::For:
            /* Parse class declaration */
            return ParseRangeForLoopCommand(AttribListAST);
        default:
            break;
    }

    /* Parse variable definition as default */
    return ParseVariableDef(nullptr, nullptr, AttribListAST);
}

ImportCommandPtr Parser::ParseImportCommand()
{
    /* Parse import command */
    AcceptIt();
    return std::make_shared<ImportCommand>(ParseFNameIdent());
}

PackageCommandPtr Parser::ParsePackageCommand()
{
    /* Parse package names */
    AcceptIt();
    auto FNameAST = ParseFNameIdent();

    /* Parse block command */
    auto BlockCmdAST = ParseBlockCommand();
    BlockCmdAST->HasCurly = false;

    return std::make_shared<PackageCommand>(FNameAST, BlockCmdAST);
}

BlockCommandPtr Parser::ParseBlockCommand(const Command::ParseFlags::DataType& Flags)
{
    /* Accept block opening '{' character */
    Accept(Token::Types::LCurly);
    ++State_.ScopeLevel;

    auto BlockCmdAST = std::make_shared<BlockCommand>(Tkn_->Pos());

    /* Parse commands until end of block */
    while (TokenType() != Token::Types::RCurly)
    {
        /* Parse next command */
        auto CmdAST = ParseCommand(Flags);
        if (CmdAST != nullptr)
            BlockCmdAST->Commands.push_back(CmdAST);
    }

    /* Accept block closing '}' character */
    AcceptIt();
    --State_.ScopeLevel;

    return BlockCmdAST;
}

/*
Declaration command blocks only allows function declarations.
Although it uses the '{' and '}' brackets, it does not open a new scope!
*/
BlockCommandPtr Parser::ParseDeclBlock()
{
    /* Accept block beginning */
    auto DeclTkn = Accept(Token::Types::Decl);
    Accept(Token::Types::LCurly);

    auto BlockCmdAST = std::make_shared<BlockCommand>(DeclTkn->Pos());
    BlockCmdAST->OpensScope = false;

    /* Parse declaration commands until end of block */
    while (TokenType() != Token::Types::RCurly)
    {
        /* Parse declaration next command */
        auto CmdAST = ParseFunctionDecl();
        if (CmdAST != nullptr)
            BlockCmdAST->Commands.push_back(CmdAST);
    }

    /* Accept block ending */
    AcceptIt();

    return BlockCmdAST;
}

CallCommandPtr Parser::ParseCallCommand(FNameIdentPtr ObjectNameAST)
{
    /* Parse function object name if not already done */
    if (ObjectNameAST == nullptr)
        ObjectNameAST = ParseFNameIdent(FNameIdent::InFlags::AllowTemplateArgs);

    /* Parser parameter list */
    auto ParamListAST = ParseArgumentList();

    /* Create call command AST node */
    return std::make_shared<CallCommand>(ObjectNameAST, ParamListAST);
}

AssignCommandPtr Parser::ParseAssignCommand(FNameIdentPtr ObjectNameAST)
{
    /* Parse assign operator and expression */
    auto OpAST = ParseOperator(Token::Types::AssignOp);
    auto ExprAST = ParseExpression();

    /* Create assign command AST node */
    auto AssignOpAST = std::dynamic_pointer_cast<AssignOperator>(OpAST);

    return std::make_shared<AssignCommand>(ObjectNameAST, AssignOpAST, ExprAST);
}

ReturnCommandPtr Parser::ParseReturnCommand()
{
    /* Check if return commadn is inside function */
    if (!State_.IsInsideProc())
        Error("\"return\" statement outside procedure definition");

    /* Accept return token */
    auto RetToken = Accept(Token::Types::Return);

    auto RetAST = std::make_shared<ReturnCommand>(RetToken->Pos());

    /* Parse optional expression */
    auto ExprAST = ParseExpression(Expression::ParseFlags::IsOptional);

    if (ExprAST)
        RetAST->Expr = ExprAST;

    return RetAST;
}

IfCommandPtr Parser::ParseIfCommand()
{
    AcceptIt();

    /* Parse boolean expression and block command */
    auto ExprAST = ParseExpression();
    auto CmdBlockAST = ParseBlockCommand();

    /* Create <If> command AST node */
    auto IfCmdAST = std::make_shared<IfCommand>(CmdBlockAST);
    IfCmdAST->Expr = ExprAST;

    /* Parse optional <ElseIf> commands */
    if (TokenType() == Token::Types::Else)
    {
        AcceptIt();

        if (TokenType() != Token::Types::If)
        {
            /* Parse <Else> block */
            auto ElseCmdBlockAST = ParseBlockCommand();

            /* Create <Else> command AST node */
            auto ElseCmdAST = std::make_shared<IfCommand>(ElseCmdBlockAST);
        
            IfCmdAST->ElseIfCmd = ElseCmdAST;
        }
        else
            IfCmdAST->ElseIfCmd = ParseIfCommand();
    }

    return IfCmdAST;
}

InlineLangCommandPtr Parser::ParseInlineLangCommand()
{
    /* Parse inline language command */
    auto LangToken = AcceptIt();
    return std::make_shared<InlineLangCommand>(LangToken);
}

PrimitiveCommandPtr Parser::ParsePrimitiveCommand()
{
    /* Parse primitive command */
    auto Tkn = AcceptIt();
    return std::make_shared<PrimitiveCommand>(Tkn);
}

ClassDeclCommandPtr Parser::ParseClassDeclCommand(AttributeListPtr AttribListAST)
{
    /* Parse optional attribute */
    if (AttribListAST == nullptr && TokenType() == Token::Types::LDParen)
        AttribListAST = ParseAttributeList();

    /* Parse class name */
    Accept(Token::Types::Class);

    auto NameIdentAST = ParseIdentifier();

    /* Parse optional template parameter list */
    TemplateParamListPtr TParamList;

    if (TokenType() == Token::Types::RelationOp && Tkn_->Spell() == "<")
        TParamList = ParseTemplateParameterList();

    /* Parse optional inheritance class names */
    std::vector<FNameIdentPtr> Inheritance;

    if (TokenType() == Token::Types::Extends)
    {
        do
        {
            AcceptIt();

            /* Parse next inheritance class name */
            auto InheritClassNameAST = ParseFNameIdent(FNameIdent::InFlags::AllowTemplateArgs);
            Inheritance.push_back(InheritClassNameAST);
        }
        while (TokenType() == Token::Types::Comma);
    }

    /* Parse class block command */
    auto ClassBlockAST = ParseClassBlockCommand();

    /* Create class object AST node */
    auto ClassObjAST = std::make_shared<ClassObject>(NameIdentAST, ClassBlockAST);

    ClassObjAST->TParamList = TParamList;
    ClassObjAST->AttribList = AttribListAST;

    if (!Inheritance.empty())
        ClassObjAST->Inheritance = Inheritance;

    /* Create class declaration AST node */
    auto ClassDeclAST = std::make_shared<ClassDeclCommand>(ClassObjAST);

    return ClassDeclAST;
}

ClassBlockCommandPtr Parser::ParseClassBlockCommand()
{
    /* Accept block opening '{' character */
    Accept(Token::Types::LCurly);

    auto ClassBlockCmdAST = std::make_shared<ClassBlockCommand>(Tkn_->Pos());

    ClassBlockCommand::Privacy::DataType CmdPrivacy = ClassBlockCommand::Privacy::Public;

    /* Parse commands until end of block */
    while (TokenType() != Token::Types::RCurly)
    {
        /* Check if privacy has changed */
        if (Tkn_->IsPrivacy())
        {
            /* Setup new privacy */
            switch (TokenType())
            {
                case Token::Types::Private:
                    CmdPrivacy = ClassBlockCommand::Privacy::Private;
                    break;
                case Token::Types::Protect:
                    CmdPrivacy = ClassBlockCommand::Privacy::Protect;
                    break;
                case Token::Types::Public:
                default:
                    CmdPrivacy = ClassBlockCommand::Privacy::Public;
                    break;
            }

            /* Accept privacy token followed by a colon */
            AcceptIt();
            Accept(Token::Types::Colon);

            continue;
        }

        /* Parse next command */
        CommandPtr CmdAST;

        switch (TokenType())
        {
            case Token::Types::Init:
                CmdAST = ParseInitCommand();
                break;

            case Token::Types::Release:
                CmdAST = ParseReleaseCommand();
                break;

            default:
                CmdAST = ParseCommand(Command::ParseFlags::AllowDefinitionsOnly);
                break;
        }

        if (CmdAST != nullptr)
            ClassBlockCmdAST->Commands[static_cast<unsigned int>(CmdPrivacy)].push_back(CmdAST);
    }

    /* Accept block closing '}' character */
    AcceptIt();

    return ClassBlockCmdAST;
}

InitCommandPtr Parser::ParseInitCommand()
{
    Accept(Token::Types::Init);

    /* Parse parameter list and block command */
    auto ParamListAST = ParseParameterList();
    auto BlockCmdAST = ParseBlockCommand();

    auto InitCmdAST = std::make_shared<InitCommand>(BlockCmdAST);

    if (ParamListAST != nullptr)
        InitCmdAST->ParamList = ParamListAST;

    return InitCmdAST;
}

ReleaseCommandPtr Parser::ParseReleaseCommand()
{
    Accept(Token::Types::Release);

    /* Parse block command */
    auto BlockCmdAST = ParseBlockCommand();

    return std::make_shared<ReleaseCommand>(BlockCmdAST);
}

TryCatchCommandPtr Parser::ParseTryCatchCommand()
{
    Accept(Token::Types::Try);

    /* Parse try block */
    auto TryBlockAST = ParseBlockCommand();

    Accept(Token::Types::Catch);

    /* Parse optional exception variable */
    ParameterListPtr ExceptParamAST;

    if (TokenType() != Token::Types::LCurly)
        ExceptParamAST = ParseSingleParameter();

    /* Parse catch block */
    auto CatchBlockAST = ParseBlockCommand();

    /* Create try-catch command AST node */
    auto TryCatchAST = std::make_shared<TryCatchCommand>(TryBlockAST, CatchBlockAST);

    TryCatchAST->ExceptParam = ExceptParamAST;

    return TryCatchAST;
}

ThrowCommandPtr Parser::ParseThrowCommand()
{
    Accept(Token::Types::Throw);

    /* Parse throw expression */
    auto ExprAST = ParseExpression();

    return std::make_shared<ThrowCommand>(ExprAST);
}

EnumDeclCommandPtr Parser::ParseEnumDeclCommand()
{
    Accept(Token::Types::Enum);

    /* Parse enumeration name */
    auto IdentAST = ParseIdentifier();

    auto EnumObjAST = std::make_shared<EnumObject>(IdentAST);

    Accept(Token::Types::LCurly);

    while (TokenType() != Token::Types::RCurly)
    {
        /* Parse next enum entry */
        auto EnumEntryAST = ParseEnumEntry();
        EnumObjAST->Entries.push_back(EnumEntryAST);
    }

    AcceptIt();

    /* Create enumeration declaration AST node */
    return std::make_shared<EnumDeclCommand>(EnumObjAST);
}

FlagsDeclCommandPtr Parser::ParseFlagsDeclCommand()
{
    Accept(Token::Types::Flags);

    /* Parse flags name */
    auto IdentAST = ParseIdentifier();

    /* Parse optional inheritance flags names */
    std::vector<FNameIdentPtr> Inheritance;

    if (TokenType() == Token::Types::Extends)
    {
        do
        {
            AcceptIt();

            /* Parse next inheritance flags name */
            auto InheritFlagsNameAST = ParseFNameIdent();
            Inheritance.push_back(InheritFlagsNameAST);
        }
        while (TokenType() == Token::Types::Comma);
    }

    /* Create flags object AST node */
    auto FlagsObjAST = std::make_shared<FlagsObject>(IdentAST);

    FlagsObjAST->Inheritance = Inheritance;

    /* Parse flags entries */
    Accept(Token::Types::LCurly);

    while (TokenType() != Token::Types::RCurly)
        FlagsObjAST->Flags.push_back(ParseIdentifier());

    AcceptIt();

    /* Create flags declaration AST node */
    return std::make_shared<FlagsDeclCommand>(FlagsObjAST);
}

SwitchCommandPtr Parser::ParseSwitchCommand()
{
    /* Parse switch command with expression */
    Accept(Token::Types::Switch);
    auto Expr = ParseExpression();

    auto SwitchCmdAST = std::make_shared<SwitchCommand>(Expr);

    /* Accept opening '{' token */
    Accept(Token::Types::LCurly);

    /* Parse case blocks */
    while (TokenType() != Token::Types::RCurly)
    {
        if (TokenType() == Token::Types::Case)
        {
            auto CaseBlockAST = ParseCaseBlock();
            SwitchCmdAST->CaseBlocks.push_back(CaseBlockAST);
        }
        else if (TokenType() == Token::Types::Default)
        {
            if (SwitchCmdAST->DefBlock != nullptr)
                Error("Multiple default blocks in switch statement");
            else
                SwitchCmdAST->DefBlock = ParseCaseBlock();
        }
        else
            ErrorUnexpected("'case' or 'def' token in switch statement");
    }

    /* Accept closing '}' token */
    AcceptIt();

    return SwitchCmdAST;
}

TypeDefCommandPtr Parser::ParseTypeDefCommand()
{
    /* Parse type definition and identifier */
    Accept(Token::Types::Alias);

    auto IdentAST = ParseIdentifier();

    /* Parse assignment operator */
    auto AssignOp = ParseAssignOperator();

    if (AssignOp->Spell != ":=")
        Error("Wrong assignment operator for type definition (Expected ':=' operator)");

    /* Parse the type denoter which is about to get an alias */
    auto TDenoterAST = ParseTypeDenoter();

    /* Create type definition AST node */
    auto TypeObjAST = std::make_shared<TypeObject>(IdentAST, TDenoterAST);
    auto TypeDefCmdAST = std::make_shared<TypeDefCommand>(TypeObjAST);

    return TypeDefCmdAST;
}

AssertCommandPtr Parser::ParseAssertCommand()
{
    /* Parse assertion expression */
    Accept(Token::Types::Assert);

    auto ExprAST = ParseExpression();

    /* Parse optional information (string) expression */
    ExpressionPtr InfoExprAST;

    if (TokenType() == Token::Types::Colon)
    {
        AcceptIt();
        InfoExprAST = ParseExpression();
    }

    /* Create assert statement AST node */
    return std::make_shared<AssertCommand>(ExprAST, InfoExprAST);
}

InfiniteLoopCommandPtr Parser::ParseInfiniteLoopCommand()
{
    /* Parse the loop's block command */
    Accept(Token::Types::Forever);
    auto BlockCmdAST = ParseBlockCommand();

    /* Create infinite loop command AST node */
    return std::make_shared<InfiniteLoopCommand>(BlockCmdAST);
}

WhileLoopCommandPtr Parser::ParseWhileLoopCommand()
{
    /* Parse loop type (while or do-while loop) */
    ExpressionPtr ExprAST;
    WhileLoopCommand::Flags::DataType DefFlags = 0;

    switch (TokenType())
    {
        case Token::Types::Do:
            AcceptIt();
            DefFlags |= WhileLoopCommand::Flags::DoLoop;
            break;
        case Token::Types::While:
            AcceptIt();
            break;
        case Token::Types::Until:
            AcceptIt();
            DefFlags |= WhileLoopCommand::Flags::UntilLoop;
            break;
        default:
            ErrorUnexpected("'do', 'while' or 'until' token");
            break;
    }

    /* Parse expression on while loop */
    const bool IsDoLoop = ((DefFlags & WhileLoopCommand::Flags::DoLoop) != 0);

    if (!IsDoLoop)
        ExprAST = ParseExpression();

    /* Parse loop's block command */
    auto BlockCmdAST = ParseBlockCommand();

    if (IsDoLoop)
    {
        /* Parse expression for do-loop */
        if (TokenType() == Token::Types::Until)
        {
            AcceptIt();
            DefFlags |= WhileLoopCommand::Flags::UntilLoop;
        }
        else if (TokenType() == Token::Types::While)
            AcceptIt();
        else
            ErrorUnexpected("'while' or 'until' token");

        ExprAST = ParseExpression();
    }

    /* Create while loop command AST node */
    return std::make_shared<WhileLoopCommand>(ExprAST, BlockCmdAST, DefFlags);
}

RangeForLoopCommandPtr Parser::ParseRangeForLoopCommand(AttributeListPtr AttribListAST)
{
    /* Check for attribute */
    if (TokenType() == Token::Types::LDParen)
        AttribListAST = ParseAttributeList();

    /* Parse loop iterator identifier */
    Accept(Token::Types::For);

    IdentifierPtr IdentAST;

    if (TokenType() == Token::Types::Identifier)
    {
        /* Parse identifier with assignment to range */
        IdentAST = ParseIdentifier();
        Accept(Token::Types::Colon);
    }

    /* Parse iteration range */
    auto Start = ParseIterationIndex();
    auto End = Start;

    if (TokenType() == Token::Types::RangeSep)
    {
        /* Parse range end */
        AcceptIt();
        End = ParseIterationIndex();
    }
    else
    {
        /* Use default iteration */
        End = Start;
        Start = 1;
    }

    /* Parse optional step */
    RangeForLoopCommand::index_t Step = 1;

    if (TokenType() == Token::Types::Arrow)
    {
        AcceptIt();
        Step = ParseIterationIndex();

        if (Step <= 0)
            Error("Interval steps of range-based 'for-loops' must be greater than zero", false);
    }

    /* Parse command block */
    auto CmdBlockAST = ParseBlockCommand();

    /* Create range-based for loop AST node */
    auto ForLoopCmdAST = std::make_shared<RangeForLoopCommand>(IdentAST, CmdBlockAST, Start, End);

    ForLoopCmdAST->Step = Step;
    ForLoopCmdAST->AttribList = AttribListAST;

    return ForLoopCmdAST;
}

FunctionDefCommandPtr Parser::ParseFunctionDef(
    TypeDenoterPtr TDenoterAST, FNameIdentPtr FNameAST, FunctionObject::Flags::DataType DefFlags)
{
    /* Parse optional virtual properties */
    if (TDenoterAST == nullptr && FNameAST == nullptr && DefFlags == 0)
    {
        if (TokenType() == Token::Types::Virtual)
        {
            AcceptIt();
            DefFlags |= FunctionObject::Flags::IsVirtual;
        }
        else if (TokenType() == Token::Types::Abstract)
        {
            AcceptIt();
            DefFlags |= FunctionObject::Flags::IsAbstract;
        }
    }

    /* Parse type denoter and/or identifier is not passed as argument */
    if (TDenoterAST == nullptr)
    {
        TDenoterAST = ParseTypeDenoter();
        if (FNameAST == nullptr)
            FNameAST = ParseFNameIdent(FNameIdent::InFlags::AllowTemplateParams);
    }
    else if (FNameAST == nullptr)
        Error("Passed type denoter but missing identifier for procedure definition", false);

    /* Parse parameter list */
    auto ParamListAST = ParseParameterList();

    /* Parse optional property */
    if (TokenType() == Token::Types::Readonly)
    {
        AcceptIt();
        DefFlags |= FunctionObject::Flags::IsReadonly;
    }

    /* Parse function body */
    BlockCommandPtr BlockCmdAST;

    if ((DefFlags & FunctionObject::Flags::IsAbstract) == 0)
    {
        ++State_.ProcDepth;
        BlockCmdAST = ParseBlockCommand();
        --State_.ProcDepth;
    }

    /* Create function object AST node */
    auto FuncObjAST = std::make_shared<FunctionObject>(TDenoterAST, FNameAST->Ident, BlockCmdAST);

    FuncObjAST->DefFlags    = DefFlags;
    FuncObjAST->FName       = FNameAST;
    FuncObjAST->ParamList   = ParamListAST;

    /* Decorate last FName AST node with this function object */
    FuncObjAST->FName->GetLastIdent()->Link = FuncObjAST.get();

    /* Create function definition command AST node */
    return std::make_shared<FunctionDefCommand>(FuncObjAST);
}

/*
This function parses a function declaration and creates a dummy function.
This function object AST node get a flag that no code must be generated for this function.
*/
FunctionDefCommandPtr Parser::ParseFunctionDecl()
{
    /* Parse type denoter and/or identifier is not passed as argument */
    auto TDenoterAST = ParseTypeDenoter();
    auto FNameAST = ParseFNameIdent(FNameIdent::InFlags::AllowTemplateParams);

    /* Parse parameter list */
    auto ParamListAST = ParseParameterList();

    /* Create function object AST node */
    auto FuncObjAST = std::make_shared<FunctionObject>(TDenoterAST, FNameAST->Ident);

    FuncObjAST->FName = FNameAST;
    FuncObjAST->ParamList = ParamListAST;

    /* Decorate last FName AST node with this function object */
    FuncObjAST->FName->GetLastIdent()->Link = FuncObjAST.get();

    /* Create function definition command AST node */
    return std::make_shared<FunctionDefCommand>(FuncObjAST);
}

VariableDefCommandPtr Parser::ParseVariableDef(
    TypeDenoterPtr TDenoterAST, IdentifierPtr IdentAST, AttributeListPtr AttribListAST, bool IsStatic)
{
    /* Parse optional attribute */
    if (IdentAST == nullptr && TokenType() == Token::Types::LDParen)
        AttribListAST = ParseAttributeList();

    /* Parse variable object */
    auto VarObjAST = ParseVariableObject(TDenoterAST, IdentAST);

    if (IsStatic)
        VarObjAST->DefFlags |= VariableObject::Flags::IsStatic;

    /* Create variable definition AST node */
    auto VarDefAST = std::make_shared<VariableDefCommand>(VarObjAST);

    VarDefAST->AttribList = AttribListAST;

    /* Parse further optional variable declarations (in the same context) */
    if (TokenType() == Token::Types::Comma)
    {
        /* Parse next variable definition */
        AcceptIt();
        VarDefAST->Next = ParseVariableDef(VarObjAST->TDenoter, nullptr, AttribListAST, IsStatic);
    }

    return VarDefAST;
}


} // /namespace SyntacticAnalyzer



// ================================================================================