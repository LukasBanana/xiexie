/*
 * XieXie scanner header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_SCANNER_XIEXIE_H__
#define __XX_SCANNER_XIEXIE_H__


#include "Scanner.h"


namespace SyntacticAnalyzer
{


class XieXieScanner : public Scanner
{
    
    public:
        
        XieXieScanner();
        ~XieXieScanner();

        /* === Functions === */

        TokenPtr Next();

    private:
        
        /* === Functions === */

        void ScanCommentLine();
        void ScanCommentBlock();

        TokenPtr ScanToken();

        TokenPtr ScanStringLiteral();
        TokenPtr ScanVerbatimStringLiteral();
        TokenPtr ScanIdentifier();
        TokenPtr ScanInlineCpp(const std::string& LangStr);
        TokenPtr ScanConfig();
        TokenPtr ScanAssignShiftRelationOp(const char Chr);
        TokenPtr ScanPlusOp();
        TokenPtr ScanMinusOp();

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================