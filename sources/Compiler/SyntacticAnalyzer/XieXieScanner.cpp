/*
 * XieXie scanner file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "XieXieScanner.h"
#include "StringMod.h"
#include "ConsoleOutput.h"
#include "CompilerOptions.h"


namespace SyntacticAnalyzer
{


XieXieScanner::XieXieScanner()
{
}
XieXieScanner::~XieXieScanner()
{
}

TokenPtr XieXieScanner::Next()
{
    try
    {
        bool NoCommentary = false;

        do
        {
            IgnoreWhiteSpaces();

            /* Check for end-of-file character */
            if (Chr_ == 0)
                return MakeToken(Token::Types::EndOfFile);

            /* Scan commentaries */
            switch (Chr_)
            {
                case '/':
                {
                    char PrevChar = TakeIt();

                    if (Chr_ == '/')
                        ScanCommentLine();
                    else if (Chr_ == '*')
                        ScanCommentBlock();
                    else
                    {
                        std::string Spell;
                        Spell += PrevChar;

                        if (Chr_ == '=')
                        {
                            Spell += TakeIt();
                            return MakeToken(Token::Types::AssignOp, Spell);
                        }

                        return MakeToken(Token::Types::DivOp, Spell);
                    }
                }
                break;

                default:
                {
                    NoCommentary = true;
                }
                break;
            }
        }
        while (!NoCommentary);

        /* Scan token */
        return ScanToken();
    }
    catch (const std::exception &Err)
    {
        ConsoleOutput::Error(Err.what());
    }

    return nullptr;
}


/*
 * ======= Private: =======
 */

void XieXieScanner::ScanCommentLine()
{
    /* Scan comment until end-of-line */
    while (Chr_ != '\n')
        TakeIt();
}

void XieXieScanner::ScanCommentBlock()
{
    while (true)
    {
        if (Chr_ == 0)
            return;

        /* Scan comment block ending */
        if (Chr_ == '*')
        {
            TakeIt();
            if (Chr_ == '/')
            {
                TakeIt();
                return;
            }
        }
        TakeIt();
    }
}

TokenPtr XieXieScanner::ScanToken()
{
    std::string Spell;

    /* Scan string literal */
    if (Chr_ == '\"')
        return ScanStringLiteral();

    /* Scan identifier */
    if (IsLetter())
        return ScanIdentifier();

    /* Scan number */
    if (IsNumber())
        return ScanNumber();

    /* Scan special character and verbatim string literals */
    if (Chr_ == '@')
    {
        TakeIt();

        /* Scan verbatim string literal */
        if (Chr_ == '\"')
            return ScanVerbatimStringLiteral();

        return MakeToken(Token::Types::At);
    }

    /* Scan operators */
    if (Chr_ == ':')
    {
        Spell += TakeIt();

        if (Chr_ == '=')
        {
            Spell += TakeIt();
            return MakeToken(Token::Types::AssignOp, Spell);
        }

        return MakeToken(Token::Types::Colon, Spell);
    }

    if (Chr_ == '=')
        return MakeToken(Token::Types::EqualityOp, "=", true);

    if (Chr_ == '~')
        return MakeToken(Token::Types::BitwiseNotOp, "~", true);

    if (Chr_ == '!')
    {
        Spell += TakeIt();

        if (Chr_ == '=')
        {
            Spell += TakeIt();
            return MakeToken(Token::Types::EqualityOp, Spell);
        }

        ErrorUnexpected();
    }

    if (Chr_ == '%')
    {
        Spell += TakeIt();

        if (Chr_ == '=')
        {
            Spell += TakeIt();
            return MakeToken(Token::Types::AssignOp, Spell);
        }

        return MakeToken(Token::Types::DivOp, Spell);
    }

    if (Chr_ == '*')
    {
        Spell += TakeIt();

        if (Chr_ == '=')
        {
            Spell += TakeIt();
            return MakeToken(Token::Types::AssignOp, Spell);
        }

        return MakeToken(Token::Types::MulOp, Spell);
    }

    if (Chr_ == '^')
    {
        Spell += TakeIt();

        if (Chr_ == '=')
        {
            Spell += TakeIt();
            return MakeToken(Token::Types::AssignOp, Spell);
        }

        return MakeToken(Token::Types::BitwiseXorOp, Spell);
    }

    if (CompilerOptions::Settings.SimplifiedExpr)
    {
        if (Chr_ == '+')
            return MakeToken(Token::Types::AddOp, Spell += TakeIt());
        if (Chr_ == '-')
            return MakeToken(Token::Types::SubOp, Spell += TakeIt());
    }
    else
    {
        if (Chr_ == '+')
            return ScanPlusOp();
        if (Chr_ == '-')
            return ScanMinusOp();
    }

    if (Chr_ == '<')
        return ScanAssignShiftRelationOp('<');
    if (Chr_ == '>')
        return ScanAssignShiftRelationOp('>');

    if (Chr_ == '&')
    {
        Spell += TakeIt();

        if (Chr_ == '&')
        {
            Spell += TakeIt();
            return MakeToken(Token::Types::LogicAndOp, Spell);
        }
        else if (Chr_ == '=')
        {
            Spell += TakeIt();
            return MakeToken(Token::Types::AssignOp, Spell);
        }

        return MakeToken(Token::Types::BitwiseAndOp, Spell);
    }

    if (Chr_ == '|')
    {
        Spell += TakeIt();

        if (Chr_ == '|')
        {
            Spell += TakeIt();
            return MakeToken(Token::Types::LogicOrOp, Spell);
        }
        else if (Chr_ == '=')
        {
            Spell += TakeIt();
            return MakeToken(Token::Types::AssignOp, Spell);
        }

        return MakeToken(Token::Types::BitwiseOrOp, Spell);
    }

    /* Scan special cases */
    if (Chr_ == '[')
    {
        TakeIt();
        if (Chr_ == '[')
            return MakeToken(Token::Types::LDParen, true);
        return MakeToken(Token::Types::LParen);
    }

    if (Chr_ == ']')
    {
        TakeIt();
        if (Config_.AllowRDParent && Chr_ == ']')
            return MakeToken(Token::Types::RDParen, true);
        return MakeToken(Token::Types::RParen);
    }

    /* Scan punctuation, special characters and brackets */
    if (Chr_ == '.')
    {
        TakeIt();
        if (Chr_ == '.')
            return MakeToken(Token::Types::RangeSep, true);
        return MakeToken(Token::Types::Dot);
    }

    switch (Chr_)
    {
        case '#': return MakeToken(Token::Types::Hash,      true); break;
        case ';': return MakeToken(Token::Types::Semicolon, true); break;
        case ',': return MakeToken(Token::Types::Comma,     true); break;
        case '(': return MakeToken(Token::Types::LBracket,  true); break;
        case ')': return MakeToken(Token::Types::RBracket,  true); break;
        case '{': return MakeToken(Token::Types::LCurly,    true); break;
        case '}': return MakeToken(Token::Types::RCurly,    true); break;
    }

    ErrorUnexpected();

    return nullptr;
}

TokenPtr XieXieScanner::ScanStringLiteral()
{
    std::string Spell;

    while (true)
    {
        /* Take opening '\"' character */
        Take('\"');

        while (true)
        {
            /* Check for escape characters */
            while (Chr_ == '\\')
            {
                TakeIt();

                if (IsEscapeChar())
                {
                    Spell += '\\';
                    Spell += Chr_;
                }
                else
                    ErrorUnexpected();

                TakeIt();
            }

            if (Chr_ == 0)
                ErrorEOF();
                
            /* Check for closing '\"' character */
            if (Chr_ == '\"')
                break;

            /* Check for new-line character */
            if (Chr_ == '\n')
                Spell += '\\';

            /* Append character to string literal */
            Spell += TakeIt();
        }

        /* Take closing '\"' character */
        TakeIt();

        /* Search for next string literal (which will be appended to this token) */
        while (IsBlank())
            TakeIt();

        if (Chr_ != '\"')
            break;

        Spell += "\\n";
    }

    /* Return final string literal token */
    return MakeToken(Token::Types::StringLiteral, Spell);
}

TokenPtr XieXieScanner::ScanVerbatimStringLiteral()
{
    std::string Spell;

    while (true)
    {
        /* Take opening '\"' character */
        Take('\"');

        while (true)
        {
            /* Check for escape characters */
            while (Chr_ == '\\')
            {
                TakeIt();
                Spell += "\\\\";
            }

            if (Chr_ == 0)
                ErrorEOF();
                
            /* Check for closing '\"' character */
            if (Chr_ == '\"')
            {
                TakeIt();

                /* Check for double quotes */
                if (Chr_ == '\"')
                    Spell += "\\";
                else
                    break;
            }

            /* Check for new-line character */
            if (Chr_ == '\n')
                Spell += '\\';

            /* Append character to string literal */
            Spell += TakeIt();
        }

        /* Search for next string literal (which will be appended to this token) */
        while (IsBlank())
            TakeIt();

        if (Chr_ != '\"')
            break;

        Spell += "\\n";
    }

    /* Return final string literal token */
    return MakeToken(Token::Types::StringLiteral, Spell);
}

TokenPtr XieXieScanner::ScanIdentifier()
{
    /* Scan identifier string */
    std::string Spell;
    Spell += TakeIt();

    while (IsLetter() || IsNumber())
        Spell += TakeIt();

    auto Tkn = MakeToken(Token::Types::Identifier, Spell);

    /* Check for reserved internal names */
    if (Spell.compare(0, 6, "__XX__") == 0)
        Error("Reserved prefix \"__XX__\" used in identifier \"" + Spell + "\"");

    /* Scan reserved literal identifiers */
    if (Spell == "true" || Spell == "false")
        return MakeToken(Token::Types::BoolLiteral, Spell);
    else if (Spell == "null")
        return MakeToken(Token::Types::PointerLiteral, Spell);

    /* Scan reserved words */
    auto it = Token::Table().find(Spell);

    if (it != Token::Table().end())
    {
        /* Check for compiler specific keywords */
        switch (it->second)
        {
            case Token::Types::Cpp:
                return ScanInlineCpp(Spell);
            case Token::Types::PushConfig:
                return ScanConfig();
            default:
                break;
        }

        /* Return keyword token */
        return MakeToken(it->second);
    }

    /* Scan data types */
    it = Token::TypeDenoterTable().find(Spell);

    if (it != Token::TypeDenoterTable().end())
        return MakeToken(it->second, Spell);

    return Tkn;
}

TokenPtr XieXieScanner::ScanInlineCpp(const std::string& LangStr)
{
    IgnoreWhiteSpaces();

    /* Scan inline C++ block */
    Take('{');
    unsigned int CurylBracketStack = 1;

    std::string Spell;

    do
    {
        if (Chr_ != '}' || CurylBracketStack > 1)
            Spell += Chr_;

        if (Chr_ == '{')
            ++CurylBracketStack;
        else if (Chr_ == '}')
            --CurylBracketStack;
        
        TakeIt();
    }
    while (CurylBracketStack > 0);

    /* Encode language information into token spelling */
    const std::string LangInfo = LangStr + '@';

    return MakeToken(Token::Types::Cpp, LangInfo + Spell);
}

TokenPtr XieXieScanner::ScanConfig()
{
    IgnoreWhiteSpaces();

    /* Scan configuration block */
    std::string Spell;

    Take('{');
    {
        while (Chr_ != '}')
        {
            Spell += Chr_;
            TakeIt();
        }
    }
    Take('}');

    return MakeToken(Token::Types::PushConfig, Spell);
}

TokenPtr XieXieScanner::ScanAssignShiftRelationOp(const char Chr)
{
    std::string Spell;
    Spell += TakeIt();

    if (Config_.AllowShiftOp && Chr_ == Chr)
    {
        Spell += TakeIt();

        if (Chr_ == '=')
        {
            Spell += TakeIt();
            return MakeToken(Token::Types::AssignOp, Spell);
        }

        return MakeToken(Token::Types::ShiftOp, Spell);
    }

    if (Chr_ == '=')
        Spell += TakeIt();

    return MakeToken(Token::Types::RelationOp, Spell);
}

TokenPtr XieXieScanner::ScanPlusOp()
{
    std::string Spell;
    Spell += TakeIt();
    
    if (Chr_ == '+')
    {
        Spell += TakeIt();
        return MakeToken(Token::Types::UnaryAssignOp, Spell);
    }
    else if (Chr_ == '=')
    {
        Spell += TakeIt();
        return MakeToken(Token::Types::AssignOp, Spell);
    }

    return MakeToken(Token::Types::AddOp, Spell);
}

TokenPtr XieXieScanner::ScanMinusOp()
{
    std::string Spell;
    Spell += TakeIt();

    if (Chr_ == '-')
    {
        Spell += TakeIt();
        return MakeToken(Token::Types::UnaryAssignOp, Spell);
    }
    else if (Chr_ == '=')
    {
        Spell += TakeIt();
        return MakeToken(Token::Types::AssignOp, Spell);
    }
    else if (Chr_ == '>')
    {
        Spell += TakeIt();
        return MakeToken(Token::Types::Arrow, Spell);
    }

    return MakeToken(Token::Types::SubOp, Spell);
}


} // /namespace SyntacticAnalyzer



// ================================================================================