/*
 * Compiler message file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "CompilerMessage.h"


CompilerMessage::CompilerMessage(const Categories& Category) :
    Category_(Category)
{
}
CompilerMessage::CompilerMessage(const CompilerMessage& Other) :
    Category_   (Other.Category_),
    Pos_        (Other.Pos_     ),
    Msg_        (Other.Msg_     ),
    Line_       (Other.Line_    ),
    Mark_       (Other.Mark_    )
{
}
CompilerMessage::CompilerMessage(const std::string &Msg, const Categories& Category) :
    Category_   (Category   ),
    Msg_        (Msg        )
{
}
CompilerMessage::CompilerMessage(
    const SourcePosition &Pos, const std::string &Msg, const Categories& Category) :
        Category_   (Category                                               ),
        Pos_        (Pos                                                    ),
        Msg_        (CompilerMessage::ConstructMessage(Category, Pos, Msg)  )
{
}
CompilerMessage::CompilerMessage(
    const SourcePosition &Pos, const std::string &Msg,
    const std::string& Line, const std::string& Mark,
    const Categories& Category) :
        Category_   (Category                                               ),
        Pos_        (Pos                                                    ),
        Msg_        (CompilerMessage::ConstructMessage(Category, Pos, Msg)  ),
        Line_       (Line                                                   ),
        Mark_       (Mark                                                   )
{
    /* Remove new-line character from source line */
    if (Line_.size() > 0 && Line_.back() == '\n')
        Line_.resize(Line_.size() - 1);
}
CompilerMessage::~CompilerMessage()
{
}

const char* CompilerMessage::what() const throw()
{
    return Msg_.c_str();
}

std::string CompilerMessage::ConstructMessage(
    const Categories Category, const SourcePosition& Pos, const std::string& Msg)
{
    return GetCategoryString(Category) + " " + Pos.GetString() + " -- " + Msg;
}

std::string CompilerMessage::GetCategoryString(const Categories Category)
{
    switch (Category)
    {
        case Categories::Message:
            break;
        case Categories::Warning:
            return "Warning";
        case Categories::SyntaxError:
            return "Syntax Error";
        case Categories::ContextError:
            return "Context Error";
        case Categories::CodeGenError:
            return "Code Generation Error";
        case Categories::StateError:
            return "State Error";
        case Categories::FileError:
            return "File Error";
    }
    return "";
}



// ================================================================================