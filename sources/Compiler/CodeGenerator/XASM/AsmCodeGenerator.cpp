/*
 * Assembler code generator file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "AsmCodeGenerator.h"
#include "CompilerOptions.h"
#include "ConsoleOutput.h"
#include "StringMod.h"
#include "ASTIncludes.h"
#include "Instruction.h"

#include <algorithm>


using namespace XVM;


/*
 * AsmCodeGenerator class
 */

const unsigned int AsmCodeGenerator::REG_CF = Instruction::Registers::cf;
const unsigned int AsmCodeGenerator::REG_LB = Instruction::Registers::lb;
const unsigned int AsmCodeGenerator::REG_SP = Instruction::Registers::sp;
const unsigned int AsmCodeGenerator::REG_PC = Instruction::Registers::pc;

AsmCodeGenerator::AsmCodeGenerator() :
    CodeGenerator(),
    LocalVarMngr_(
        std::bind(&AsmCodeGenerator::IncStackSize, this, std::placeholders::_1),
        std::bind(&AsmCodeGenerator::DecStackSize, this, std::placeholders::_1)
    )
{
}
AsmCodeGenerator::~AsmCodeGenerator()
{
}

bool AsmCodeGenerator::GenerateCode(const std::string &SourcePath, ProgramPtr ProgramAST)
{
    ConsoleOutput::Message("Code generation [Assembler] ...");
    ScopedIndent Unused;

    if (!ProgramAST)
    {
        ConsoleOutput::Error("Invalid program AST root node");
        return false;
    }

    /* Create output file */
    std::string Filename = SourcePath;
    Filename += (ProgramAST->SourceFilename + ".xasm");

    GenerateFile(Filename);

    /* Visit program AST root node for code generation */
    ProgramAST->VISIT;

    return true;
}

/* === Commands === */

//!TODO! -> memory management is incomplete!!!
DefVisitProc(AsmCodeGenerator, AssignCommand)
{
    const auto VarName = AST->FName->FullName();
    Comment("Variable Assignment \"" + VarName + "\"");

    /* Calculate arithmetic expression */
    ExprArg Arg(1);
    AST->Expr->VISIT_ARG(&Arg);
    if (!Arg.Confirmed)
        POP(Reg(1));

    /* Get local variable */
    auto Obj = AST->FName->GetLastIdent()->Link;

    if (Obj == nullptr || Obj->Type() != Object::Types::VarObj)
        throw std::string("\"" + VarName + "\" does not name a variable");

    auto VarObj = dynamic_cast<VariableObject*>(Obj);

    const auto Offset = Fetch(VarObj->Ident->Spell);
    const auto TypeSize = VarObj->TDenoter->GetTypeSize();

    /* Add additional offset for array access */
    bool UseSrcReg2 = false;

    if (AST->FName->ArrayList != nullptr)
    {
        PUSH(Reg(1));

        /* Evaluate array expression */
        UpperIndent();
        Comment("Array Index Evaluation \"" + AST->FName->Ident->Spell + "\"");
        AST->FName->ArrayList->VISIT;
        LowerIndent();

        POP(Reg(2));
        if (TypeSize > 4)
            MUL(Reg(2), xxStr(TypeSize / 4));
        ADD(Reg(2), Reg(REG_LB));

        UseSrcReg2 = true;

        POP(Reg(1));
    }

    /* Load previous variable value */
    //!!!
    if (UseSrcReg2)
    {
        PUSH(Reg(REG_LB));
        MOV(Reg(REG_LB), Reg(2));
    }

    //!!!
    if (TypeSize == 4)
        LDW(Reg(0), Reg(REG_LB), Offset);
    else if (TypeSize == 1)
        LDB(Reg(0), Reg(REG_LB), Offset);

    if (UseSrcReg2)
        POP(Reg(REG_LB));

    /* Store result in memory */
    const std::string& Spell = AST->Op->Spell;

    //:=, +=, -=, *=, /=, %=, <<=, >>=, |=, &=, ^=

    /* --- Arithmetic operations --- */
    if (Spell == ":=")
        MOV(Reg(0), Reg(1));
    else if (Spell == "+=")
        ADD(Reg(0), Reg(1));
    else if (Spell == "-=")
        SUB(Reg(0), Reg(1));
    else if (Spell == "*=")
        MUL(Reg(0), Reg(1));
    else if (Spell == "/=")
        DIV(Reg(0), Reg(1));
    else if (Spell == "%=")
        MOD(Reg(0), Reg(1));

    /* --- Bitwise operations --- */
    else if (Spell == "&=")
        AND(Reg(0), Reg(1));
    else if (Spell == "|=")
        OR(Reg(0), Reg(1));
    else if (Spell == "^=")
        XOR(Reg(0), Reg(1));

    /* --- Shift operations --- */
    else if (Spell == "<<=")
        SLL(Reg(0), Reg(1));
    else if (Spell == ">>=")
        SLR(Reg(0), Reg(1));

    /* Store new variable value */
    //!!!
    if (UseSrcReg2)
    {
        PUSH(Reg(REG_LB));
        MOV(Reg(REG_LB), Reg(2));
    }

    //!!!
    if (TypeSize == 4)
        STW(Reg(0), Reg(REG_LB), Offset);
    else if (TypeSize == 1)
        STB(Reg(0), Reg(REG_LB), Offset);

    if (UseSrcReg2)
        POP(Reg(REG_LB));

    Blank();
}

DefVisitProc(AsmCodeGenerator, BlockCommand)
{
    if (AST->HasCurly)
    {
        LocalVarMngr_.PushScope();
        UpperIndent();
    }

    for (auto Cmd : AST->Commands)
        Cmd->VISIT;

    if (AST->HasCurly)
    {
        LocalVarMngr_.PopScope();
        LowerIndent();
    }
}

DefVisitProc(AsmCodeGenerator, CallCommand)
{
    const std::string& FullName = AST->FName->FullName();

    if (AST->ArgList != nullptr)
        AST->ArgList->VISIT;

    CALL(FullName);
}

DefVisitProc(AsmCodeGenerator, FunctionDefCommand)
{
    auto Obj = AST->Obj;

    if (Obj->IsDecl())
    {
        /* Don't generate code for this function if it's just a declaration */
        return;
    }

    /* Check for special function names */
    const std::string FullName = Obj->FName->FullName();
    const bool IsMainProc = (FullName == "Main");

    /* Documentation */
    Comment("--- Procedure Definition \"" + FullName + "\"  ---");

    Label(FullName);

    LocalVarMngr_.PushScope();
    UpperIndent();
    
    /* Code for stack reservation of parameters */
    if (AST->Obj->ParamList != nullptr)
        AST->Obj->ParamList->VISIT;

    StackWrapper<unsigned int>::ScopedPush Unused(
        LocalParamSizeStack_, LocalVarMngr_.ScopeParamSize()
    );

    /* Code for function body */
    Obj->CmdBlock->HasCurly = false;
    Obj->CmdBlock->VISIT;

    LocalVarMngr_.PopScope();

    /* Code for return instruction */
    if (!IsMainProc)
    {
        /*
        Code for return instruction if there is no return
        command at the end of the function body
        */
        if (Obj->CmdBlock->Commands.empty() || Obj->CmdBlock->Commands.back()->Type() != Command::Types::RetCmd)
            RET(0, LocalParamSizeStack_.Top(), true);
    }
    else
        RET(0, 0);

    LowerIndent();
    Blank();
}

DefVisitProc(AsmCodeGenerator, ImportCommand)
{
}

DefVisitProc(AsmCodeGenerator, PackageCommand)
{
}

DefVisitProc(AsmCodeGenerator, ReturnCommand)
{
    /* Code for return expression */
    unsigned int ExprSize = 0;

    if (AST->Expr != nullptr)
    {
        AST->Expr->VISIT;

        if (AST->Expr->CommonTDenoter != nullptr)
            ExprSize = AST->Expr->CommonTDenoter->GetTypeSize();
    }

    /* Code for return instruction */
    auto LocalParamSize = LocalParamSizeStack_.Top();

    RET(ExprSize, LocalParamSize, true);
}

//!TODO! -> memory management is incomplete!!!
DefVisitProc(AsmCodeGenerator, VariableDefCommand)
{
    auto Obj = AST->Obj;
    const auto& VarName = Obj->Ident->Spell;

    Comment("Variable Initialization \"" + VarName + "\"");

    auto LocalVar = AddLocalVariable(*Obj);

    const auto Offset = Fetch(VarName);

    if (Obj->Expr != nullptr)
    {
        /* Code for initialization expression */
        ExprArg Arg(0);
        Obj->Expr->VISIT_ARG(&Arg);
        if (!Arg.Confirmed)
            POP(Reg(0));
    }
    else
        XOR(Reg(0), Reg(0));

    /* Store initialized value */
    if (LocalVar.Size == 4)
        STW(Reg(0), Reg(REG_LB), Offset);
    else if (LocalVar.Size == 1)
        STB(Reg(0), Reg(REG_LB), Offset);
    else
    {
        /* Initialize values in a loop */
        const auto LoopBeginLabel = GenForLoopLabel();
        const auto LoopEndLabel = LoopBeginLabel + "_end";

        Blank();

        Comment("Array Initialization Loop");
        Comment("(i1 = Loop Index, i2 = Loop Length, i3 = Storage Pointer)");
        
        PUSH(Reg(REG_LB));//!!!

        XOR(Reg(1), Reg(1));
        MOV(Reg(2), xxStr(LocalVar.Size / 4));

        Label(LoopBeginLabel);
        CMP(Reg(1), Reg(2));
        JGE(LoopEndLabel);
        {
            UpperIndent();
            
            STW(Reg(0), Reg(REG_LB), Offset);
            INC(Reg(REG_LB));
            INC(Reg(1));

            LowerIndent();
        }
        JMP(LoopBeginLabel);
        Label(LoopEndLabel);

        POP(Reg(REG_LB));//!!!
    }

    Blank();

    if (AST->Next != nullptr)
        AST->Next->VISIT;
}

DefVisitProc(AsmCodeGenerator, IfCommand)
{
    const auto ElseLabel = GenElseLabel();
    const auto EndIfLabel = GenEndIfLabel();

    /* Code for IF-statement header */
    if (AST->Expr != nullptr)
    {
        Comment("If Statement");
        AST->Expr->VISIT;

        /* Code for conditional branch */
        POP(Reg(0));
        XOR(Reg(1), Reg(1));
        CMP(Reg(0), Reg(1));
        JE(ElseLabel);
    }
    else
        Comment("Else Statement");

    /* Code for IF-block body */
    AST->CmdBlock->VISIT;

    JMP(EndIfLabel);
    Label(ElseLabel);
    
    /* Generate code for <Else> blocks */
    if (AST->ElseIfCmd != nullptr)
    {
        Blank();
        AST->ElseIfCmd->VISIT;
    }

    Label(EndIfLabel);
    Blank();
}

DefVisitProc(AsmCodeGenerator, InfiniteLoopCommand)
{
    const auto LoopBeginLabel = GenInfLoopLabel();
    const auto LoopBreakLabel = LoopBeginLabel + "_break";

    Comment("Infinite Loop");
    Label(LoopBeginLabel);

    AST->CmdBlock->VISIT;

    JMP(LoopBeginLabel);
    Label(LoopBreakLabel);

    Blank();
}

DefVisitProc(AsmCodeGenerator, WhileLoopCommand)
{
    const auto LoopBeginLabel = GenWhileLoopLabel();
    const auto LoopEndLabel = LoopBeginLabel + "_end";

    /* Code for loop condition */
    Comment("While Loop");
    
    Label(LoopBeginLabel);

    AST->Expr->VISIT;

    POP(Reg(0));
    XOR(Reg(1), Reg(1));
    CMP(Reg(0), Reg(1));
    JE(LoopEndLabel);

    AST->CmdBlock->VISIT;

    JMP(LoopBeginLabel);
    Label(LoopEndLabel);

    Blank();
}

DefVisitProc(AsmCodeGenerator, RangeForLoopCommand)
{
    LocalVarMngr_.PushScope();

    const auto LoopBeginLabel = GenForLoopLabel();
    const auto LoopEndLabel = LoopBeginLabel + "_end";

    /* Get meta data from AST node */
    RangeForLoopCommand::MetaData MetaData;
    AST->FillMetaData(MetaData);

    /* Code for loop initialization */
    Comment("Range-Based For Loop");

    LocalVarMngr_.AddVariable(AST->Ident->Spell, 4);
    MOV(Reg(0), xxStr(MetaData.Start));

    Label(LoopBeginLabel);

    /* Code for loop condition */
    Comment("Loop Condition");

    MOV(Reg(1), xxStr(MetaData.End));

    CMP(Reg(0), Reg(1));
    if (MetaData.Forwards)
        JG(LoopEndLabel);
    else
        JL(LoopEndLabel);

    /* Code for loop body */
    const auto ItVarOffset = Fetch(AST->Ident->Spell);
    STW(Reg(0), Reg(REG_LB), ItVarOffset);
    {
        AST->CmdBlock->VISIT;
    }
    LDW(Reg(0), Reg(REG_LB), ItVarOffset);

    /* Code for loop iteration */
    if (MetaData.Step > 1)
    {
        if (MetaData.Forwards)
            ADD(Reg(0), xxStr(MetaData.Step));
        else
            SUB(Reg(0), xxStr(MetaData.Step));
    }
    else
    {
        if (MetaData.Forwards)
            INC(Reg(0));
        else
            DEC(Reg(0));
    }

    JMP(LoopBeginLabel);
    Label(LoopEndLabel);

    Blank();

    LocalVarMngr_.PopScope();
}

DefVisitProc(AsmCodeGenerator, InlineLangCommand)
{
}

DefVisitProc(AsmCodeGenerator, PrimitiveCommand)
{
    if (AST->PrimType == Token::Types::Stop)
        STOP();
}

DefVisitProc(AsmCodeGenerator, EnumDeclCommand)
{
}

DefVisitProc(AsmCodeGenerator, FlagsDeclCommand)
{
}

DefVisitProc(AsmCodeGenerator, ClassDeclCommand)
{
}

DefVisitProc(AsmCodeGenerator, ClassBlockCommand)
{
}

DefVisitProc(AsmCodeGenerator, InitCommand)
{
}

DefVisitProc(AsmCodeGenerator, ReleaseCommand)
{
}

DefVisitProc(AsmCodeGenerator, TryCatchCommand)
{
}

DefVisitProc(AsmCodeGenerator, ThrowCommand)
{
}

/* === Expressions === */

DefVisitProc(AsmCodeGenerator, BracketExpression)
{
    AST->Expr->VISIT_ARG(Args);
}

DefVisitProc(AsmCodeGenerator, BinaryExpression)
{
    if (AST->Expr2->Type() == Expression::Types::LiteralExpr)
    {
        if (AST->Expr1->Type() == Expression::Types::LiteralExpr)
        {
            /* Directly store literal value from 2nd expression into register */
            auto E1 = std::dynamic_pointer_cast<LiteralExpression>(AST->Expr1);

            if (E1->Literal->Type() == Literal::Types::Float)
                MOV(Reg(Instruction::Registers::f0), GenLiteralExprValue(E1->Literal));
            else
                MOV(Reg(0), GenLiteralExprValue(E1->Literal));
        }
        else
        {
            /* Store sub-expression result on the stack */
            ExprArg Arg(0);
            AST->Expr1->VISIT_ARG(&Arg);
            if (!Arg.Confirmed)
                POP(Reg(0));
        }

        /* Directly store literal value from 2nd expression into register */
        auto E2 = std::dynamic_pointer_cast<LiteralExpression>(AST->Expr2);
        MOV(Reg(1), GenLiteralExprValue(E2->Literal));
    }
    else
    {
        /* Store sub-expression results on the stack */
        if (AST->Expr1->Type() != Expression::Types::LiteralExpr)
            AST->Expr1->VISIT;
        
        ExprArg Arg1(1);
        AST->Expr2->VISIT_ARG(&Arg1);
        if (!Arg1.Confirmed)
            POP(Reg(1));
        
        if (AST->Expr1->Type() == Expression::Types::LiteralExpr)
        {
            /* Directly store literal value from 2nd expression into register */
            auto E1 = std::dynamic_pointer_cast<LiteralExpression>(AST->Expr1);
            MOV(Reg(0), GenLiteralExprValue(E1->Literal));
        }
        else
            POP(Reg(0));
    }
    
    /* Calcualte current binary expression */
    const std::string& Spell = AST->Op->Spell;

    /* --- Arithmetic operations --- */
    if (Spell == "+")
        ADD(Reg(0), Reg(1));
    else if (Spell == "-")
        SUB(Reg(0), Reg(1));
    else if (Spell == "*")
        MUL(Reg(0), Reg(1));
    else if (Spell == "/")
        DIV(Reg(0), Reg(1));
    else if (Spell == "%")
        MOD(Reg(0), Reg(1));

    /* --- Bitwise operations --- */
    else if (Spell == "&")
        AND(Reg(0), Reg(1));
    else if (Spell == "|")
        OR(Reg(0), Reg(1));
    else if (Spell == "^")
        XOR(Reg(0), Reg(1));

    /* --- Shift operations --- */
    else if (Spell == "<<")
        SLL(Reg(0), Reg(1));
    else if (Spell == ">>")
        SLR(Reg(0), Reg(1));

    /* --- Relation operators --- */
    #if 1//!!!INCOMPLETE!!!
    
    else// if (AST->Op->IsBoolean())
    {
        #if 0

        CMP(Reg(0), Reg(1));

        if (Spell == "=")
            JNE(GenCondJumpLabel());
        else if (Spell == "!=")
            JE(GenCondJumpLabel());
        else if (Spell == "<")
            JGE(GenCondJumpLabel());
        else if (Spell == "<=")
            JG(GenCondJumpLabel());
        else if (Spell == ">")
            JLE(GenCondJumpLabel());
        else if (Spell == ">=")
            JL(GenCondJumpLabel());

        #else

        PUSH(Reg(1));
        PUSH(Reg(0));

        if (Spell == "=")
            CALL("Intr.CmpE");
        else if (Spell == "!=")
            CALL("Intr.CmpNE");
        else if (Spell == "<")
            CALL("Intr.CmpL");
        else if (Spell == "<=")
            CALL("Intr.CmpLE");
        else if (Spell == ">")
            CALL("Intr.CmpG");
        else if (Spell == ">=")
            CALL("Intr.CmpGE");
        else if (Spell == "or")
            CALL("Intr.LogicOr");
        else if (Spell == "and")
            CALL("Intr.LogicAnd");

        return;// <-- ignore expr. arg. confirmation

        #endif
    }

    #endif

    /* Store result on the stack */
    if (Args != nullptr)
    {
        auto EArg = ExprArg::Get(Args);
        if (EArg->Reg != 0)
            MOV(Reg(EArg->Reg), Reg(0));
        EArg->Confirm();
    }
    else
        PUSH(Reg(0));
}

DefVisitProc(AsmCodeGenerator, UnaryExpression)
{
    AST->Op->VISIT;
    AST->Expr->VISIT;
}

DefVisitProc(AsmCodeGenerator, AssignExpression)
{
    AST->Op->VISIT;
    AST->Expr->VISIT;
}

DefVisitProc(AsmCodeGenerator, LiteralExpression)
{
    if (Args != nullptr)
    {
        auto EArg = ExprArg::Get(Args);
        MOV(Reg(EArg->Reg), GenLiteralExprValue(AST->Literal));
        EArg->Confirm();
    }
    else if (AST->Literal->Type() == Literal::Types::Float)
    {
        const auto reg0 = Reg(Instruction::Registers::f0);
        MOV(reg0, GenLiteralExprValue(AST->Literal));
        PUSH(reg0);
    }
    else
        PUSH(GenLiteralExprValue(AST->Literal));
}

DefVisitProc(AsmCodeGenerator, CallExpression)
{
    AST->Call->VISIT;
}

DefVisitProc(AsmCodeGenerator, CastExpression)
{
    AST->TDenoter->VISIT;
    AST->Expr->VISIT;
}

//!TODO! -> memory management is incomplete!!!
DefVisitProc(AsmCodeGenerator, ObjectExpression)
{
    //Comment("<Object Expression \"" + AST->FName->FullName() + "\">");

    /* Get local variable */
    auto Obj = AST->FName->GetLastIdent()->Link;

    if (Obj == nullptr || Obj->Type() != Object::Types::VarObj)
        throw std::string("\"" + AST->FName->FullName() + "\" does not name a variable");

    auto VarObj = dynamic_cast<VariableObject*>(Obj);

    const auto Offset = Fetch(VarObj->Ident->Spell);
    const auto TypeSize = VarObj->TDenoter->GetTypeSize();

    /* Add additional offset for array access */
    bool UseSrcReg2 = false;

    if (AST->FName->ArrayList != nullptr)
    {
        /* Evaluate array expression */
        UpperIndent();
        Comment("Array Index Evaluation \"" + AST->FName->Ident->Spell + "\"");
        AST->FName->ArrayList->VISIT;
        LowerIndent();

        POP(Reg(2));
        if (TypeSize > 4)
            MUL(Reg(2), xxStr(TypeSize / 4));
        ADD(Reg(2), Reg(REG_LB));

        UseSrcReg2 = true;
    }

    /* Load previous variable value */
    unsigned int RegIndex = 0;

    if (Args != nullptr)
    {
        auto EArg = ExprArg::Get(Args);
        RegIndex = EArg->Reg;
        EArg->Confirm();
    }

    //!!!
    if (UseSrcReg2)
    {
        PUSH(Reg(REG_LB));
        MOV(Reg(REG_LB), Reg(2));
    }

    if (TypeSize == 4)
        LDW(Reg(RegIndex), Reg(REG_LB), Offset);
    else if (TypeSize == 1)
        LDB(Reg(RegIndex), Reg(REG_LB), Offset);

    if (UseSrcReg2)
        POP(Reg(REG_LB));

    if (Args == nullptr)
        PUSH(Reg(0));

    //AST->FName->VISIT;

    //if (AST->AssignExpr != nullptr)
    //    AST->AssignExpr->VISIT;

    //Comment("</Object Expression \"" + AST->FName->FullName() + "\">");
}

/* === Others === */

DefVisitProc(AsmCodeGenerator, Program)
{
    CALL("Main");
    STOP();

    for (auto& Cmd : AST->Commands)
        Cmd->VISIT;

    GenerateDataFields();
}

/* === Lists === */

DefVisitProc(AsmCodeGenerator, ArgumentList)
{
    /* Traverse procedure arguments in right-to-left order */
    if (AST->Next != nullptr)
        AST->Next->VISIT;

    AST->Expr->VISIT;
}

DefVisitProc(AsmCodeGenerator, ParameterList)
{
    AddLocalParameter(*AST->Obj);

    if (AST->Next != nullptr)
        AST->Next->VISIT;
}

DefVisitProc(AsmCodeGenerator, ArrayIndexList)
{
    AST->Expr->VISIT;

    if (AST->Next != nullptr)
        AST->Next->VISIT;
}


/*
 * ======= Private: =======
 */

void AsmCodeGenerator::Comment(const std::string& Text, bool Symetric)
{
    if (CompilerOptions::Settings.EnableComments)
        NL("; " + (Symetric ? Text + " ;" : Text));
}

const char* AsmCodeGenerator::Reg(unsigned int Index)
{
    const char* Str = XVM::Instruction::Registers::Name(Index);

    if (Str == nullptr)
        Error("Register index out of bounds");

    return Str;
}

std::string AsmCodeGenerator::Fetch(const std::string& VarName) const
{
    return xxStr(LocalVarMngr_.Fetch(VarName).Offset);
}

std::string AsmCodeGenerator::GetLineCommentStr() const
{
    return ";";
}

std::string AsmCodeGenerator::GetNextEndIfLabel() const
{
    return ".endif_" + xxStr(Counters_.EndIf);
}

/* --- Generation functions --- */

void AsmCodeGenerator::GenerateDataFields()
{
    Comment("--- Data Field Section ---");

    auto GenDataField = [&](const DataFieldAddresses::DataField& Data, const DataFieldAddresses::Types& Type, const std::string& InstrPrefix)
    {
        for (auto it : Data.Addresses)
        {
            Label(it.second);

            StartNL();
            {
                Append("DATA." + InstrPrefix + " ");

                if (Type == DataFieldAddresses::Types::String)
                    Append("\"");
            
                Append(it.first);

                if (Type == DataFieldAddresses::Types::String)
                    Append("\"");
            }
            EndNL();
        }
    };

    GenDataField(DataFieldAddr_.String, DataFieldAddresses::Types::String,  "string");
    GenDataField(DataFieldAddr_.Float,  DataFieldAddresses::Types::Float,   "float" );
    GenDataField(DataFieldAddr_.Double, DataFieldAddresses::Types::Double,  "double");
    GenDataField(DataFieldAddr_.Int,    DataFieldAddresses::Types::Int,     "int"   );
    GenDataField(DataFieldAddr_.Long,   DataFieldAddresses::Types::Long,    "long"  );
}

std::string AsmCodeGenerator::GenLiteralExprValue(const LiteralPtr& AST)
{
    /* Check for literal type which can not be used directly in the XieXie assembler code */
    switch (AST->Type())
    {
        case Literal::Types::Boolean:
            /* Return binary number for boolean literals */
            return AST->Spell == "false" ? "0" : "1";

        case Literal::Types::String:
            /* Return address of string literal data field */
            return GenStringLiteralAddr(AST->Spell);

        case Literal::Types::Float:
            /* Return address of floating-point literal data field */
            return GenFloatLiteralAddr(AST->Spell);

        case Literal::Types::Pointer:
            /* Return null pointer as integer literal */
            return "0";

        default:
            break;
    }

    /* Otherwise return simple literal spelling */
    return AST->Spell;
}

std::string AsmCodeGenerator::GenStringLiteralAddr(const std::string& Spell)
{
    return DataFieldAddr_.String.Gen("str_lit_", Spell);
}
std::string AsmCodeGenerator::GenFloatLiteralAddr(const std::string& Spell)
{
    return DataFieldAddr_.Float.Gen("flt_lit_", Spell);
}
std::string AsmCodeGenerator::GenDoubleLiteralAddr(const std::string& Spell)
{
    return DataFieldAddr_.Double.Gen("dbl_lit_", Spell);
}
std::string AsmCodeGenerator::GenIntLiteralAddr(const std::string& Spell)
{
    return DataFieldAddr_.Int.Gen("int_lit_", Spell);
}
std::string AsmCodeGenerator::GenLongLiteralAddr(const std::string& Spell)
{
    return DataFieldAddr_.Long.Gen("lng_lit_", Spell);
}

std::string AsmCodeGenerator::GenBranchLabel()
{
    return ".branch_" + xxStr(Counters_.Branch++);
}

std::string AsmCodeGenerator::GenInfLoopLabel()
{
    return ".inf_loop_" + xxStr(Counters_.InfLoop++);
}

std::string AsmCodeGenerator::GenWhileLoopLabel()
{
    return ".while_loop_" + xxStr(Counters_.WhileLoop++);
}

std::string AsmCodeGenerator::GenDoWhileLoopLabel()
{
    return ".do_while_loop_" + xxStr(Counters_.DoWhileLoop++);
}

std::string AsmCodeGenerator::GenForLoopLabel()
{
    return ".for_loop_" + xxStr(Counters_.ForLoop++);
}

std::string AsmCodeGenerator::GenElseLabel()
{
    return ".else_" + xxStr(Counters_.Else++);
}

std::string AsmCodeGenerator::GenEndIfLabel()
{
    const auto Label = GetNextEndIfLabel();
    ++Counters_.EndIf;
    return Label;
}

std::string AsmCodeGenerator::GenCondJumpLabel()
{
    return ".cond_jmp_" + xxStr(Counters_.CondJump++);
}

/* --- Other helper functions --- */

void AsmCodeGenerator::IncStackSize(unsigned int Size)
{
    if (Size > 1)
        ADD(Reg(Instruction::Registers::sp), xxStr(Size));
    else if (Size == 1)
        INC(Reg(Instruction::Registers::sp));
}

void AsmCodeGenerator::DecStackSize(unsigned int Size)
{
    if (Size > 1)
        SUB(Reg(Instruction::Registers::sp), xxStr(Size));
    else if (Size == 1)
        DEC(Reg(Instruction::Registers::sp));
}

AsmLocalVariableManager::LocalVariable AsmCodeGenerator::AddLocalVariable(const VariableObject& VarObj, bool ImmdiateResize)
{
    /* Add variable to local scope */
    const auto TypeSize = VarObj.TDenoter->GetTypeSize() * VarObj.TDenoter->GetArrayLength();
    return LocalVarMngr_.AddVariable(VarObj.Ident->Spell, TypeSize, ImmdiateResize);
}

AsmLocalVariableManager::LocalVariable AsmCodeGenerator::AddLocalParameter(const VariableObject& VarObj)
{
    /* Add parameter to local scope */
    const auto TypeSize = VarObj.TDenoter->GetTypeSize();
    return LocalVarMngr_.AddParameter(VarObj.Ident->Spell, TypeSize);
}


/*
 * DataField structure
 */

AsmCodeGenerator::DataFieldAddresses::DataField::DataField() :
    Counter(0u)
{
}

const std::string& AsmCodeGenerator::DataFieldAddresses::DataField::Gen(
    const std::string& Prefix, const std::string& Spell)
{
    /* Check if this spelling already exists */
    auto it = Addresses.find(Spell);
    if (it != Addresses.end())
        return it->second;

    /* Otherwise generate a new name */
    const std::string Name = Prefix + xxStr(Counter++);
    return (Addresses[Spell] = Name);
}



// ================================================================================