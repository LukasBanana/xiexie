/*
 * Assembler code generator header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_CODEGENERATOR_ASM_H__
#define __XX_CODEGENERATOR_ASM_H__


#include "CodeGenerator.h"
#include "AsmLocalVariableManager.h"
#include "Counter.h"
#include "StackWrapper.h"

#include <map>


class AsmCodeGenerator;

typedef std::shared_ptr<AsmCodeGenerator> AsmCodeGeneratorPtr;

//! XASM (XieXie Assembler) code generator.
class AsmCodeGenerator : public CodeGenerator
{
    
    public:
        
        AsmCodeGenerator();
        ~AsmCodeGenerator();

        /* === Functions === */

        bool GenerateCode(const std::string& SourcePath, ProgramPtr ProgramAST);

        /* === Commands === */

        DeclVisitProc(AssignCommand         )
        DeclVisitProc(BlockCommand          )
        DeclVisitProc(CallCommand           )
        DeclVisitProc(FunctionDefCommand    )
        DeclVisitProc(ImportCommand         )
        DeclVisitProc(PackageCommand        )
        DeclVisitProc(ReturnCommand         )
        DeclVisitProc(VariableDefCommand    )
        DeclVisitProc(IfCommand             )
        DeclVisitProc(InfiniteLoopCommand   )
        DeclVisitProc(WhileLoopCommand      )
        DeclVisitProc(RangeForLoopCommand   )
        DeclVisitProc(InlineLangCommand     )
        DeclVisitProc(PrimitiveCommand      )
        DeclVisitProc(EnumDeclCommand       )
        DeclVisitProc(FlagsDeclCommand      )
        DeclVisitProc(ClassDeclCommand      )
        DeclVisitProc(ClassBlockCommand     )
        DeclVisitProc(InitCommand           )
        DeclVisitProc(ReleaseCommand        )
        DeclVisitProc(TryCatchCommand       )
        DeclVisitProc(ThrowCommand          )

        /* === Expressions === */

        DeclVisitProc(BracketExpression     )
        DeclVisitProc(BinaryExpression      )
        DeclVisitProc(UnaryExpression       )
        DeclVisitProc(AssignExpression      )
        DeclVisitProc(LiteralExpression     )
        DeclVisitProc(CallExpression        )
        DeclVisitProc(CastExpression        )
        DeclVisitProc(ObjectExpression      )

        /* === Lists === */

        DeclVisitProc(ParameterList         )
        DeclVisitProc(ArgumentList          )
        DeclVisitProc(ArrayIndexList        )

        /* === Others === */

        DeclVisitProc(Program               )

    private:
        
        /* === Constant members === */

        static const unsigned int REG_CF;
        static const unsigned int REG_LB;
        static const unsigned int REG_SP;
        static const unsigned int REG_PC;

        /* === Structures == */

        struct LabelCounters
        {
            Counter Branch;
            Counter InfLoop;
            Counter WhileLoop;
            Counter DoWhileLoop;
            Counter ForLoop;
            Counter Else;
            Counter CondJump;
            Counter EndIf;
        };

        //! Structure for all data field types (used for string-, integer- or float literals in XASM code).
        struct DataFieldAddresses
        {
            enum class Types
            {
                String,
                Float,
                Double,
                Int,
                Long
            };

            struct DataField
            {
                public:
                    DataField();

                    const std::string& Gen(const std::string& Prefix, const std::string& Spell);

                    std::map<std::string, std::string> Addresses;

                private:
                    unsigned int Counter;
            };

            /* Members */
            DataField String;
            DataField Float;
            DataField Double;
            DataField Int;
            DataField Long;
        };

        struct ExprArg
        {
            ExprArg(unsigned int Index) :
                Reg         (Index),
                Confirmed   (false)
            {
            }

            inline void Confirm()
            {
                Confirmed = true;
            }
            static inline ExprArg* Get(void* Args)
            {
                return reinterpret_cast<ExprArg*>(Args);
            }

            /* Members */
            unsigned int Reg; //!< Register index in which the expression is to be moved.
            bool Confirmed;
        };

        /* === Functions === */

        /**
        Appends a commentary (but only if commentaries are enabled in the compiler options).
        \param[in] Text Specifies the commentary text.
        \param[in] Symetric Also appends the ";" character at the end of the line for symetry.
        \see CompilerOptions::ConfigSettings::EnableComments
        */
        void Comment(const std::string& Text, bool Symetric = false);

        const char* Reg(unsigned int Index);

        //! Returns the offset of the specified local variable as string.
        std::string Fetch(const std::string& VarName) const;

        std::string GetLineCommentStr() const;

        std::string GetNextEndIfLabel() const;

        /* --- Generation functions --- */

        void GenerateDataFields();

        std::string GenLiteralExprValue(const LiteralPtr& AST);

        std::string GenStringLiteralAddr(const std::string& Spell);
        std::string GenFloatLiteralAddr (const std::string& Spell);
        std::string GenDoubleLiteralAddr(const std::string& Spell);
        std::string GenIntLiteralAddr   (const std::string& Spell);
        std::string GenLongLiteralAddr  (const std::string& Spell);

        std::string GenBranchLabel();
        std::string GenInfLoopLabel();
        std::string GenWhileLoopLabel();
        std::string GenDoWhileLoopLabel();
        std::string GenForLoopLabel();
        std::string GenElseLabel();
        std::string GenEndIfLabel();
        std::string GenCondJumpLabel();

        /* --- Other helper functions --- */

        void IncStackSize(unsigned int Size);
        void DecStackSize(unsigned int Size);

        AsmLocalVariableManager::LocalVariable AddLocalVariable(const VariableObject& VarObj, bool ImmdiateResize = true);
        AsmLocalVariableManager::LocalVariable AddLocalParameter(const VariableObject& VarObj);

        /* --- Code generation functions --- */

        void Label(const std::string& Name);

        void PUSH(const std::string& Src);
        void POP(const std::string& Dst);

        void AND(const std::string& Dst, const std::string& Src);
        void OR(const std::string& Dst, const std::string& Src);
        void XOR(const std::string& Dst, const std::string& Src);
        void NOT(const std::string& Dst);

        void SLL(const std::string& Dst, const std::string& Src);
        void SLR(const std::string& Dst, const std::string& Src);

        void ADD(const std::string& Dst, const std::string& Src);
        void SUB(const std::string& Dst, const std::string& Src);
        void MUL(const std::string& Dst, const std::string& Src);
        void DIV(const std::string& Dst, const std::string& Src);
        void MOD(const std::string& Dst, const std::string& Src);

        void MOV(const std::string& Dst, const std::string& Src);

        void INC(const std::string& Src);
        void DEC(const std::string& Src);

        void CMP(const std::string& Op1, const std::string& Op2);

        void JMP(const std::string& Label);
        void JE(const std::string& Label);
        void JNE(const std::string& Label);
        void JG(const std::string& Label);
        void JL(const std::string& Label);
        void JGE(const std::string& Label);
        void JLE(const std::string& Label);
        void CALL(const std::string& Label);

        void STOP();
        void RET(unsigned int ResultSize, unsigned int ArgSize, bool ConvertToWordAligned = false);

        void LDB(const std::string& Dst, const std::string& Addr);
        void STB(const std::string& Src, const std::string& Addr);
        void LDW(const std::string& Dst, const std::string& Addr);
        void STW(const std::string& Src, const std::string& Addr);

        void LDB(const std::string& Dst, const std::string& Addr, const std::string& Offset);
        void STB(const std::string& Src, const std::string& Addr, const std::string& Offset);
        void LDW(const std::string& Dst, const std::string& Addr, const std::string& Offset);
        void STW(const std::string& Src, const std::string& Addr, const std::string& Offset);

        /* === Members === */

        LabelCounters Counters_;
        DataFieldAddresses DataFieldAddr_;

        AsmLocalVariableManager LocalVarMngr_;

        StackWrapper<unsigned int> LocalParamSizeStack_;

};


#endif



// ================================================================================