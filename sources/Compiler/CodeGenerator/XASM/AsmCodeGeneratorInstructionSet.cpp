/*
 * Assembler code generator instruction set file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "AsmCodeGenerator.h"
#include "StringMod.h"


/*
 * ======= Private: =======
 */

/* --- Code generation functions --- */

void AsmCodeGenerator::Label(const std::string& Name)
{
    NL(Name + ":");
}

void AsmCodeGenerator::PUSH(const std::string& Src)
{
    NL("push " + Src);
}

void AsmCodeGenerator::POP(const std::string& Dst)
{
    NL("pop " + Dst);
}

void AsmCodeGenerator::AND(const std::string& Dst, const std::string& Src)
{
    NL("and " + Dst + ", " + Src);
}

void AsmCodeGenerator::OR(const std::string& Dst, const std::string& Src)
{
    NL("or " + Dst + ", " + Src);
}

void AsmCodeGenerator::XOR(const std::string& Dst, const std::string& Src)
{
    NL("xor " + Dst + ", " + Src);
}

void AsmCodeGenerator::NOT(const std::string& Dst)
{
    NL("not " + Dst);
}

void AsmCodeGenerator::SLL(const std::string& Dst, const std::string& Src)
{
    NL("sll " + Dst + ", " + Src);
}

void AsmCodeGenerator::SLR(const std::string& Dst, const std::string& Src)
{
    NL("slr " + Dst + ", " + Src);
}

void AsmCodeGenerator::ADD(const std::string& Dst, const std::string& Src)
{
    NL("add " + Dst + ", " + Src);
}

void AsmCodeGenerator::SUB(const std::string& Dst, const std::string& Src)
{
    NL("sub " + Dst + ", " + Src);
}

void AsmCodeGenerator::MUL(const std::string& Dst, const std::string& Src)
{
    NL("mul " + Dst + ", " + Src);
}

void AsmCodeGenerator::DIV(const std::string& Dst, const std::string& Src)
{
    NL("div " + Dst + ", " + Src);
}

void AsmCodeGenerator::MOD(const std::string& Dst, const std::string& Src)
{
    NL("mod " + Dst + ", " + Src);
}

void AsmCodeGenerator::MOV(const std::string& Dst, const std::string& Src)
{
    /* Check if "MOV" instruction can be optimized by "XOR" instruction */
    if (Src == "0")
        NL("xor " + Dst + ", " + Dst);
    else
        NL("mov " + Dst + ", " + Src);
}

void AsmCodeGenerator::INC(const std::string& Src)
{
    NL("inc " + Src);
}

void AsmCodeGenerator::DEC(const std::string& Src)
{
    NL("dec " + Src);
}

void AsmCodeGenerator::CMP(const std::string& Op1, const std::string& Op2)
{
    NL("cmp " + Op1 + ", " + Op2);
}

void AsmCodeGenerator::JMP(const std::string& Label)
{
    NL("jmp " + Label);
}

void AsmCodeGenerator::JE(const std::string& Label)
{
    NL("je " + Label);
}

void AsmCodeGenerator::JNE(const std::string& Label)
{
    NL("jne " + Label);
}

void AsmCodeGenerator::JG(const std::string& Label)
{
    NL("jg " + Label);
}

void AsmCodeGenerator::JL(const std::string& Label)
{
    NL("jl " + Label);
}

void AsmCodeGenerator::JGE(const std::string& Label)
{
    NL("jge " + Label);
}

void AsmCodeGenerator::JLE(const std::string& Label)
{
    NL("jle " + Label);
}

void AsmCodeGenerator::CALL(const std::string& Label)
{
    NL("call " + Label);
}

void AsmCodeGenerator::STOP()
{
    NL("stop");
}

void AsmCodeGenerator::RET(unsigned int ResultSize, unsigned int ArgSize, bool ConvertToWordAligned)
{
    if (ConvertToWordAligned)
    {
        ResultSize  = AsmLocalVariableManager::DWordAlignedSize(ResultSize)/4;
        ArgSize     = AsmLocalVariableManager::DWordAlignedSize(ArgSize   )/4;
    }

    NL("ret (" + xxStr(ResultSize) + ") " + xxStr(ArgSize));
}

void AsmCodeGenerator::LDB(const std::string& Dst, const std::string& Addr)
{
    NL("ldb " + Dst + ", " + Addr);
}

void AsmCodeGenerator::STB(const std::string& Src, const std::string& Addr)
{
    NL("stb " + Src + ", " + Addr);
}

void AsmCodeGenerator::LDW(const std::string& Dst, const std::string& Addr)
{
    NL("ldw " + Dst + ", " + Addr);
}

void AsmCodeGenerator::STW(const std::string& Src, const std::string& Addr)
{
    NL("stw " + Src + ", " + Addr);
}

void AsmCodeGenerator::LDB(const std::string& Dst, const std::string& Addr, const std::string& Offset)
{
    NL("ldb " + Dst + ", (" + Addr + ") " + Offset);
}

void AsmCodeGenerator::STB(const std::string& Src, const std::string& Addr, const std::string& Offset)
{
    NL("stb " + Src + ", (" + Addr + ") " + Offset);
}

void AsmCodeGenerator::LDW(const std::string& Dst, const std::string& Addr, const std::string& Offset)
{
    NL("ldw " + Dst + ", (" + Addr + ") " + Offset);
}

void AsmCodeGenerator::STW(const std::string& Src, const std::string& Addr, const std::string& Offset)
{
    NL("stw " + Src + ", (" + Addr + ") " + Offset);
}



// ================================================================================