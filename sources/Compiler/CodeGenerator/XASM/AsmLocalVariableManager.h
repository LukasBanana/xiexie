/*
 * Assembler local variable manager header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_ASM_LOCAL_VARIABLE_MANAGER_H__
#define __XX_ASM_LOCAL_VARIABLE_MANAGER_H__


#include <map>
#include <vector>
#include <string>
#include <functional>


typedef std::function<void(unsigned int Size)> ResizeStackProc;


class AsmLocalVariableManager
{
    
    public:
        
        struct LocalVariable
        {
            LocalVariable() :
                Offset  (0),
                Size    (0)
            {
            }
            LocalVariable(int VarOffset, unsigned int VarSize) :
                Offset  (VarOffset  ),
                Size    (VarSize    )
            {
            }

            /* Members */
            int Offset;         //!< Offset (in bytes) from local base (LB) register.
            unsigned int Size;  //!< Variable size (in bytes).
        };

        /* === Constructor & destructor === */

        AsmLocalVariableManager(const ResizeStackProc& IncStackProc, const ResizeStackProc& DecStackProc);
        ~AsmLocalVariableManager();

        /* === Functions === */

        void PushScope();
        void PopScope();

        /**
        Adds a new variable to the local scope.
        \throws std::string If the specified name is already used in the current scope.
        \see LocalVariable
        */
        LocalVariable AddVariable(const std::string& Name, unsigned int Size, bool ImmdiateResize = true);

        /**
        Adds a new parameter to the local scope.
        \throws std::string If the specified name is already used in the current scope.
        \see LocalVariable
        */
        LocalVariable AddParameter(const std::string& Name, unsigned int Size);

        /**
        Resize the stack to fit all new added local variables.
        This must be called after new variables has been added.
        */
        void ResizeUpdate();

        /**
        Fetches the specified local variable from the current scope in bottom-up order.
        \throws std::string if the variable could not be found.
        \see LocalVariable
        */
        LocalVariable Fetch(const std::string& Name) const;

        //! Returns the parameter list size of the current scope.
        unsigned int ScopeParamSize() const;

        /* === Static functions === */

        //! Returns the specified size with double-word (4 bytes) alignment.
        static unsigned int DWordAlignedSize(unsigned int Size);

    private:
        
        /* === Structures == */

        struct Scope
        {
            Scope(unsigned int OffsetLB);

            /* Functions */
            LocalVariable Add(const std::string& Name, unsigned int VarSize, bool IsParam);
            bool Fetch(const std::string& Name, LocalVariable& Var) const;

            /* Members */
            unsigned int Offset;                            //!< Offset (in bytes) from local base (LB) register.
            unsigned int Size;                              //!< Scope size (in bytes).
            unsigned int ParamSize;                         //!< Parameter list size (in bytes).

            std::map<std::string, LocalVariable> Variables; //!< Hash-map of all local variables in this scope.
        };

        /* === Inline functions === */

        inline const Scope* TopScope() const
        {
            return ScopeStack_.empty() ? nullptr : &(ScopeStack_.back());
        }
        inline Scope* TopScope()
        {
            return ScopeStack_.empty() ? nullptr : &(ScopeStack_.back());
        }

        /* === Members === */

        std::vector<Scope> ScopeStack_;

        ResizeStackProc IncStackProc_;
        ResizeStackProc DecStackProc_;

        unsigned int ActualStackSize_;

};


#endif



// ================================================================================