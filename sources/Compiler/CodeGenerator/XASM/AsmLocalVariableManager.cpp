/*
 * Assembler local variable manager file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "AsmLocalVariableManager.h"


AsmLocalVariableManager::AsmLocalVariableManager(
    const ResizeStackProc& IncStackProc, const ResizeStackProc& DecStackProc) :
        IncStackProc_   (IncStackProc   ),
        DecStackProc_   (DecStackProc   ),
        ActualStackSize_(0              )
{
}
AsmLocalVariableManager::~AsmLocalVariableManager()
{
}

void AsmLocalVariableManager::PushScope()
{
    /* Add new scope and set offset, so that it is placed after the previous scope */
    const Scope* TpScp = TopScope();

    /* Get previous size and offset and new start offset */
    unsigned int Offset = TpScp != nullptr ? TpScp->Offset + TpScp->Size : 0u;

    ScopeStack_.push_back(Scope(Offset));
}

void AsmLocalVariableManager::PopScope()
{
    /* Pop scope frmo back */
    if (ScopeStack_.empty())
        throw std::string("Local variable scope underflow (In ASM Local Variable Manager)");

    ScopeStack_.pop_back();
    
    /* Check if the actual stack size must be updated */
    const Scope* TpScp = TopScope();

    if (TpScp != nullptr)
    {
        const auto MaxSize = AsmLocalVariableManager::DWordAlignedSize(TpScp->Offset + TpScp->Size);

        if (ActualStackSize_ > MaxSize)
        {
            /* Set new actual stack size with double-word alignment and call push-stack callback */
            const auto Decrease = ActualStackSize_ - MaxSize;

            ActualStackSize_ = MaxSize;
            DecStackProc_(Decrease / 4);
        }
    }
    else if (ActualStackSize_ > 0)
    {
        /* Decrease actual stack size and call pop-stack callback */
        DecStackProc_(ActualStackSize_ / 4);
        ActualStackSize_ = 0;
    }
}

AsmLocalVariableManager::LocalVariable AsmLocalVariableManager::AddVariable(
    const std::string& Name, unsigned int Size, bool ImmdiateResize)
{
    Scope* TpScp = TopScope();

    if (TpScp != nullptr)
    {
        /* Add new local variable */
        auto Var = TpScp->Add(Name, Size, false);
        
        if (ImmdiateResize)
            ResizeUpdate();

        return Var;
    }

    /* Throw error message */
    throw std::string("No local scope to add variables");

    return LocalVariable();
}

AsmLocalVariableManager::LocalVariable AsmLocalVariableManager::AddParameter(const std::string& Name, unsigned int Size)
{
    Scope* TpScp = TopScope();

    /* Add new parameter */
    if (TpScp != nullptr)
        return TpScp->Add(Name, Size, true);

    /* Throw error message */
    throw std::string("No local scope to add parameters");

    return LocalVariable();
}

void AsmLocalVariableManager::ResizeUpdate()
{
    /* Get top-level scope */
    const Scope* TpScp = TopScope();
    if (TpScp == nullptr)
        return;

    /* Check if the actual stack size must be updated */
    const auto MinSize = TpScp->Offset + TpScp->Size;

    if (MinSize > ActualStackSize_)
    {
        /* Set new actual stack size with double-word alignment and call push-stack callback */
        const auto NewSize = AsmLocalVariableManager::DWordAlignedSize(MinSize);
        const auto Increase = NewSize - ActualStackSize_;

        ActualStackSize_ = NewSize;
        IncStackProc_(Increase / 4);
    }
}

AsmLocalVariableManager::LocalVariable AsmLocalVariableManager::Fetch(const std::string& Name) const
{
    /* Search variable in local scopes in bottom-up order */
    for (auto it = ScopeStack_.rbegin(); it != ScopeStack_.rend(); ++it)
    {
        LocalVariable Var;
        if (it->Fetch(Name, Var))
            return Var;
    }

    /* If no variable could be found -> error */
    throw std::string("Local variable \"" + Name + "\" could not be found in scopes");
}

unsigned int AsmLocalVariableManager::ScopeParamSize() const
{
    return TopScope() != nullptr ? TopScope()->ParamSize : 0;
}

unsigned int AsmLocalVariableManager::DWordAlignedSize(unsigned int Size)
{
    return Size + (4 - (Size % 4)) % 4;
}


/*
 * Scope structure
 */

AsmLocalVariableManager::Scope::Scope(unsigned int OffsetLB) :
    Offset      (OffsetLB   ),
    Size        (0          ),
    ParamSize   (0          )
{
}

AsmLocalVariableManager::LocalVariable AsmLocalVariableManager::Scope::Add(
    const std::string& Name, unsigned int VarSize, bool IsParam)
{
    /* Check if variable already exists in this local scope */
    auto it = Variables.find(Name);
    if (it != Variables.end())
        throw std::string("Local variable \"" + Name + "\" already exists in current scope");

    /* Otherwise insert variable into scope and setup offset from local base (LB) register */
    LocalVariable Var(
        IsParam ? -static_cast<int>(ParamSize + VarSize) : static_cast<int>(Offset + Size),
        VarSize
    );

    Variables[Name] = Var;

    if (IsParam)
    {
        /* Increment parameter list size */
        ParamSize += VarSize;
    }
    else
    {
        /* Increment scope size */
        Size += VarSize;
    }

    return Var;
}

bool AsmLocalVariableManager::Scope::Fetch(const std::string& Name, LocalVariable& Var) const
{
    /* Search variable in this local scope */
    auto it = Variables.find(Name);
    
    if (it != Variables.end())
    {
        Var = it->second;

        if (Var.Offset >= 0)
        {
            /* Add offset for link data if this is a local variable (and not a parameter) */
            Var.Offset += 8;
        }

        return true;
    }

    return false;
}



// ================================================================================