/*
 * C++ code generator header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_CODEGENERATOR_CPP_H__
#define __XX_CODEGENERATOR_CPP_H__


#include "CodeGenerator.h"
#include "RangeForLoopCommand.h"
#include "Program.h"
#include "FlagsObject.h"

#include <map>
#include <list>
#include <string>


class CppCodeGenerator;

typedef std::shared_ptr<CppCodeGenerator> CppCodeGeneratorPtr;

class CppCodeGenerator : public CodeGenerator
{
    
    public:
        
        CppCodeGenerator();
        ~CppCodeGenerator();

        /* === Functions === */

        bool GenerateCode(const std::string& SourcePath, ProgramPtr ProgramAST);

        /* === Commands === */

        DeclVisitProc(AssertCommand         )
        DeclVisitProc(AssignCommand         )
        DeclVisitProc(BlockCommand          )
        DeclVisitProc(CallCommand           )
        DeclVisitProc(FunctionDefCommand    )
        DeclVisitProc(ImportCommand         )
        DeclVisitProc(PackageCommand        )
        DeclVisitProc(ReturnCommand         )
        DeclVisitProc(VariableDefCommand    )
        DeclVisitProc(IfCommand             )
        DeclVisitProc(InfiniteLoopCommand   )
        DeclVisitProc(WhileLoopCommand      )
        DeclVisitProc(RangeForLoopCommand   )
        DeclVisitProc(InlineLangCommand     )
        DeclVisitProc(PrimitiveCommand      )
        DeclVisitProc(EnumDeclCommand       )
        DeclVisitProc(FlagsDeclCommand      )
        DeclVisitProc(ClassDeclCommand      )
        DeclVisitProc(ClassBlockCommand     )
        DeclVisitProc(InitCommand           )
        DeclVisitProc(ReleaseCommand        )
        DeclVisitProc(TryCatchCommand       )
        DeclVisitProc(ThrowCommand          )
        DeclVisitProc(SwitchCommand         )
        DeclVisitProc(TypeDefCommand        )

        /* === Expressions === */

        DeclVisitProc(BracketExpression     )
        DeclVisitProc(BinaryExpression      )
        DeclVisitProc(UnaryExpression       )
        DeclVisitProc(StringSumExpression   )
        DeclVisitProc(AssignExpression      )
        DeclVisitProc(LiteralExpression     )
        DeclVisitProc(CallExpression        )
        DeclVisitProc(CastExpression        )
        DeclVisitProc(ObjectExpression      )
        DeclVisitProc(AllocExpression       )
        DeclVisitProc(CopyExpression        )
        DeclVisitProc(ValidationExpression  )
        DeclVisitProc(InitListExpression    )
        DeclVisitProc(LambdaExpression      )

        /* === Type denoters === */

        DeclVisitProc(VoidTypeDenoter       )
        DeclVisitProc(ConstTypeDenoter      )
        DeclVisitProc(BoolTypeDenoter       )
        DeclVisitProc(IntTypeDenoter        )
        DeclVisitProc(FloatTypeDenoter      )
        DeclVisitProc(StringTypeDenoter     )
        DeclVisitProc(CustomTypeDenoter     )
        DeclVisitProc(MngPtrTypeDenoter     )
        DeclVisitProc(RawPtrTypeDenoter     )
        DeclVisitProc(RefTypeDenoter        )
        DeclVisitProc(ProcTypeDenoter       )
        DeclVisitProc(ArrayTypeDenoter      )
        
        /* === Lists === */

        DeclVisitProc(ParameterList         )
        DeclVisitProc(ArgumentList          )
        DeclVisitProc(TemplateParamList     )
        DeclVisitProc(InitializerList       )
        DeclVisitProc(ArrayIndexList        )

        /* === Literals === */

        DeclVisitProc(BoolLiteral           )
        DeclVisitProc(PointerLiteral        )
        DeclVisitProc(IntegerLiteral        )
        DeclVisitProc(FloatLiteral          )
        DeclVisitProc(StringLiteral         )

        /* === Operators === */

        DeclVisitProc(AssignOperator        )
        DeclVisitProc(BinaryOperator        )
        DeclVisitProc(UnaryOperator         )

        /* === Others === */

        DeclVisitProc(Identifier            )
        DeclVisitProc(FNameIdent            )
        DeclVisitProc(Program               )
        DeclVisitProc(EnumEntry             )
        DeclVisitProc(CaseBlock             )

    private:
        
        /* === Constant members === */

        static const std::string PackStructName;
        static const std::string StdEnumName;
        
        static const std::string EnumMemberEntryName;
        static const std::string EnumDefaultValueName;
        
        static const std::string FlagsEnumBitFieldName;
        static const std::string FlagsDataTypeName;
        
        static const std::string McrDefineFlagsFunctions;
        static const std::string McrDefineFlagsOperators;
        static const std::string McrDefineEnumDefMembers;
        static const std::string McrEnableAssertions;

        static const std::string StringWrap;

        /* === Functions === */

        void EstablishTypeDenoterMap();
        void EstablishPrimCommandMap();

        std::string ConvertSpell(OperatorPtr AST) const;

        void AddGlobalInclude(const std::string &Filename);
        void AddLocalInclude(const std::string &Filename);

        /**
        Appends a commentary (but only if commentaries are enabled in the compiler options).
        \param[in] Text Specifies the commentary text.
        \param[in] Symetric Also appends the two "//" characters at the end of the line for symetry.
        \see CompilerOptions::ConfigSettings::EnableComments
        */
        void Comment(const std::string& Text, bool Symetric = false);

        /**
        Appends a documentation commentary.
        \see Comment
        */
        void Docu(const std::string& Text);

        std::string GetLineCommentStr() const;

        /* --- Code generation functions --- */

        void AppendStdOrBoost();

        void GenerateIncludeDirectives(ProgramPtr ProgramAST);
        
        void GenerateHeaderGuardStart(const std::string &Filename);
        void GenerateHeaderGuardEnd();

        void GenerateSharedPtr(ASTPtr AST);
        void GenerateMakeShared(ASTPtr AST);

        void GenerateCopyProcedureDecl(const ClassObject* Obj);
        void GenerateCopyProcedureDef(const ClassObject* Obj);

        void GeneratePackStructPush();
        void GeneratePackStructPop();

        void GenerateParameter(VariableObject* Obj, bool AllowAutoCRef = false);
        void GenerateTemplateParameters(TemplateParamListPtr TParamList);

        void VisitAllCommands(const std::list<CommandPtr>& Commands);

        void AppendResolvedFName(
            const FNameIdentPtr& AST, unsigned int FNameLevel = 0,
            bool ResolvePointers = false, bool SkipLastIdent = false
        );
        void AppendResolvedFNamePointer(const FNameIdentPtr& AST, bool ResolvePointers = false);

        std::string GetRangeIteratorDataType(
            bool IsSigned, const RangeForLoopCommand::index_t& MinRange, const RangeForLoopCommand::index_t& MaxRange
        ) const;

        bool GenerateAnonymousClass(const ClassObject* Obj, const std::string& BaseFilename);
        void GenerateClassInheritance(const ClassObject* Obj);

        void GenerateTemplateParameterInstances(TemplateParamListPtr TParamList);

        void GenerateNullPtr();

        void GenerateStdProcCall(const CallCommandPtr& AST, const std::string& ProcName, const FunctionObject& ProcObj);
        void GenerateStdProcCallString(const CallCommandPtr& AST, const std::string& ProcName, const FunctionObject& ProcObj, bool& UseDefaultArgs);

        std::string GetFlagsDataTypeName(const FlagsObject::DataTypes& Type) const;

        std::string GetObfuscatedName(const std::string& Name);

        /* === Members === */

        std::map<std::string, std::string> TypeDenoterMap_;
        std::map<std::string, std::string> PrimCommandMap_;
        std::map<std::string, std::string> ObfuscationMap_;

        static unsigned int HeaderGuardIndex_;
        static unsigned int ObfuscatedNameIndex_;

};


#endif



// ================================================================================