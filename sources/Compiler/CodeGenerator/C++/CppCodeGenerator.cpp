/*
 * C++ code generator file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "CppCodeGenerator.h"
#include "CompilerOptions.h"
#include "ConsoleOutput.h"
#include "StringMod.h"
#include "ASTIncludes.h"
#include "Program.h"
#include "ErrorReporter.h"
#include "Constant.h"


using namespace ConstantFolding;

const std::string CppCodeGenerator::PackStructName          = CodeGenerator::IdentPrefix + "PACK_STRUCT__";
const std::string CppCodeGenerator::StdEnumName             = CodeGenerator::IdentPrefix + "Enum";

const std::string CppCodeGenerator::EnumMemberEntryName     = CodeGenerator::IdentPrefix + "Entry";
const std::string CppCodeGenerator::EnumDefaultValueName    = CodeGenerator::IdentPrefix + "Uninitialized";

const std::string CppCodeGenerator::FlagsEnumBitFieldName   = CodeGenerator::IdentPrefix + "BitField";
const std::string CppCodeGenerator::FlagsDataTypeName       = CodeGenerator::IdentPrefix + "DataType";

const std::string CppCodeGenerator::McrDefineFlagsFunctions = CodeGenerator::IdentPrefix + "DEFINE_FLAGS_FUNCTIONS__";
const std::string CppCodeGenerator::McrDefineFlagsOperators = CodeGenerator::IdentPrefix + "DEFINE_FLAGS_OPERATORS__";
const std::string CppCodeGenerator::McrDefineEnumDefMembers = CodeGenerator::IdentPrefix + "DEFINE_ENUM_DEFAULT_MEMBERS__";
const std::string CppCodeGenerator::McrEnableAssertions     = CodeGenerator::IdentPrefix + "ENABLE_ASSERTIONS___";

const std::string CppCodeGenerator::StringWrap              = "Lang::" + CodeGenerator::IdentPrefix + "Str(";

unsigned int CppCodeGenerator::HeaderGuardIndex_    = 0;
unsigned int CppCodeGenerator::ObfuscatedNameIndex_ = 0;


CppCodeGenerator::CppCodeGenerator() :
    CodeGenerator()
{
    EstablishTypeDenoterMap();
    EstablishPrimCommandMap();
}
CppCodeGenerator::~CppCodeGenerator()
{
}

bool CppCodeGenerator::GenerateCode(const std::string &SourcePath, ProgramPtr ProgramAST)
{
    ConsoleOutput::Message("Code generation [C++] ...");
    ScopedIndent Unused;

    if (!ProgramAST)
    {
        ConsoleOutput::Error("Invalid program AST root node");
        return false;
    }

    /* Create output file */
    const std::string BaseFilename = SourcePath + ProgramAST->SourceFilename;
    const std::string Filename = BaseFilename + ".cpp";

    GenerateFile(Filename);

    /* Check if this file should be included only once */
    const bool HeaderOnce = ProgramAST->IsHeaderGuardUsed;

    if (HeaderOnce)
        GenerateHeaderGuardStart(Filename);

    /* Generate code for all include directives */
    GenerateIncludeDirectives(ProgramAST);

    /* Visit program AST root node for code generation */
    ProgramAST->VISIT;

    if (ErrorReporter::Instance.HasMessages())
        ConsoleOutput::PrintErrorReport(ErrorReporter::Instance);

    /* Complete header guard */
    if (HeaderOnce)
        GenerateHeaderGuardEnd();

    /* Generate files for anonymous classes */
    for (auto ClassObj : ProgramAST->AnonymousClassObjects)
    {
        if (!GenerateAnonymousClass(ClassObj, BaseFilename))
            return false;
    }

    return true;
}

/* === Commands === */

DefVisitProc(CppCodeGenerator, AssertCommand)
{
    NL("#ifdef " + McrEnableAssertions);

    StartNL();
    {
        if (AST->InfoExpr != nullptr)
        {
            Append("__XX__ASSERT_INFO(");
            AST->Expr->VISIT;
            Append(", ", true);
            AST->InfoExpr->VISIT;
            Append(");");
        }
        else
        {
            Append("__XX__ASSERT(");
            AST->Expr->VISIT;
            Append(");");
        }
    }
    EndNL();

    NL("#endif");
}

DefVisitProc(CppCodeGenerator, AssignCommand)
{
    AST->FName->VISIT;
    Bloat();
    AST->Op->VISIT;
    Bloat();
    AST->Expr->VISIT;
}

DefVisitProc(CppCodeGenerator, BlockCommand)
{
    if (AST->HasCurly)
    {
        NL("{");
        UpperIndent();
    }

    VisitAllCommands(AST->Commands);

    if (AST->HasCurly)
    {
        LowerIndent();
        NL("}");
    }
}

DefVisitProc(CppCodeGenerator, CallCommand)
{
    /* Check if this called procedure is a standard procedure (e.g. part of the 'string' type denoter) */
    bool IsStdProc = false;
    FunctionObject* ProcObj = nullptr;

    auto LastFName = AST->FName->GetLastIdent();
    auto Obj = LastFName->Link;

    if (Obj != nullptr && Obj->Type() == Object::Types::FuncObj)
    {
        ProcObj = dynamic_cast<FunctionObject*>(Obj);
        if (ProcObj->IsStdProc())
            IsStdProc = true;
    }

    /* Code for procedure call */
    if (!IsStdProc)
    {
        AST->FName->VISIT;

        Append("(");
        {
            if (AST->ArgList != nullptr)
                AST->ArgList->VISIT;
        }
        Append(")");
    }
    else
        GenerateStdProcCall(AST, LastFName->Ident->Spell, *ProcObj);
}

DefVisitProc(CppCodeGenerator, FunctionDefCommand)
{
    auto Obj = AST->Obj;

    if (Obj->IsDecl())
    {
        /* Don't generate code for this function if it's just a declaration */
        return;
    }

    const std::string FullName = Obj->FName->FullName();

    /* Store function defintion flags */
    const bool IsVirtual    = (Obj->DefFlags & FunctionObject::Flags::IsVirtual ) != 0;
    const bool IsAbstract   = (Obj->DefFlags & FunctionObject::Flags::IsAbstract) != 0;

    /* Check for special function names */
    static const std::string MPNumArgsIdent = CodeGenerator::IdentPrefix + "NumArgs";
    static const std::string MPArgStringsIdent = CodeGenerator::IdentPrefix + "ArgStrings";

    const bool IsMainProc = (FullName == "Main");

    bool HasMainProcArgs = false;
    bool HasMainProcRet = false;
    std::string MPInputArgIdent;

    if (IsMainProc && Obj->TDenoter->Type() == TypeDenoter::Types::IntType)
        HasMainProcRet = true;

    /* Generate documentation commentaries */
    if (CompilerOptions::Settings.EnableComments)
    {
        std::string DocuProcHeader = FullName;

        if (IsVirtual)
            DocuProcHeader += " virtual";
        else if (IsAbstract)
            DocuProcHeader += " abstract";

        Docu(DocuProcHeader + " procedure.");

        auto ParamList = Obj->ParamList;

        while (ParamList != nullptr)
        {
            auto VarObj = ParamList->Obj;
            const auto& Ident = VarObj->Ident->Spell;
            
            StartNL();
            {
                /* Docu for parameter name */
                Append("//! ");

                //if (is input?)
                    Append("\\param[in] " + Ident + " Input parameter " + Ident + ".");
                /*else if (is input and output?)
                    Append("\\param[in,out] " + Ident + " Input- and output parameter " + Ident + ".");
                else
                    Append("\\param[out] " + Ident + " Output parameter " + Ident + ".");*/

                /* Docu for default parameter expression */
                if (ParamList->Obj->Expr != nullptr)
                {
                    Append(" By default ");
                    ParamList->Obj->Expr->VISIT;
                    Append(".");
                }
            }
            EndNL();

            ParamList = ParamList->Next;
        }

        if (Obj->TDenoter->Type() != TypeDenoter::Types::VoidType)
            Docu("\\return " + Obj->TDenoter->Spell + ".");
    }

    /* Code for function header */
    StartNL();
    
    if (IsAbstract || IsVirtual)
        Append("virtual ");

    GenerateTemplateParameters(Obj->FName->GetLastIdent()->TParamList);

    /* Code for return type */
    if (IsMainProc && !HasMainProcRet)
        Append("int");
    else
        Obj->TDenoter->VISIT;

    Append(" ");

    if (IsMainProc)
        Append("main");
    else
        Obj->FName->VISIT;

    Append("(");
    {
        if (Obj->ParamList != nullptr)
        {
            if (IsMainProc)
            {
                Append("int " + MPNumArgsIdent + ", const char** " + MPArgStringsIdent);

                if (Obj->ParamList->Obj != nullptr)
                {
                    HasMainProcArgs = true;
                    MPInputArgIdent = Obj->ParamList->Obj->Ident->Spell;
                }
            }
            else
                Obj->ParamList->VISIT;
        }
    }
    Append(")");
    
    if ( (Obj->DefFlags & FunctionObject::Flags::IsReadonly) != 0 &&
         (Obj->DefFlags & FunctionObject::Flags::IsClassMember) != 0 )
    {
        Append(" const", true);
    }

    if (IsAbstract)
        Append(" = 0;", true);

    EndNL();

    /* Code for function body */
    if (!IsAbstract)
    {
        auto CmdBlock = Obj->CmdBlock;

        NL("{");
        UpperIndent();

        if (HasMainProcArgs)
        {
            /* Code for filling string array of main-procedure argument strings */
            auto ArgObj = Obj->ParamList->Obj;

            StartNL();
            {
                ArgObj->TDenoter->VISIT;
                Append(" ");
                ArgObj->Ident->VISIT;
                Append(";");
            }
            EndNL();

            NL(MPInputArgIdent + ".resize(" + MPNumArgsIdent + ");");
            NL("for (int i = 0; i < " + MPNumArgsIdent + "; ++i)");
        
            NL("{");
            UpperIndent();
        
            NL(MPInputArgIdent + "[i] = std::string(*(" + MPArgStringsIdent + "++));");

            LowerIndent();
            NL("}");
        }

        /* Code for function statements */
        VisitAllCommands(CmdBlock->Commands);

        if (IsMainProc && !HasMainProcRet)
            NL("return 0;");

        LowerIndent();
        NL("}");
    }

    Blank();
}

DefVisitProc(CppCodeGenerator, ImportCommand)
{
    #if 1
    //!TESTING!
    Append("using namespace " + AST->FName->FullName());
    #endif
}

DefVisitProc(CppCodeGenerator, PackageCommand)
{
    /* Generate code for all namespaces */
    auto FName = AST->FName;

    while (FName != nullptr)
    {
        NL("namespace " + FName->Ident->Spell);
        NL("{");
        FName = FName->Next;
    }

    /* Generate code for the code inside the namespaces */
    AST->BlockCmd->VISIT;

    /* Close the namespace blocks */
    FName = AST->FName;

    while (FName)
    {
        if (CompilerOptions::Settings.EnableComments)
            NL("} // /namespace " + FName->Ident->Spell);
        else
            NL("}");
        FName = FName->Next;

        Blank();
    }
}

DefVisitProc(CppCodeGenerator, ReturnCommand)
{
    Append("return");

    if (AST->Expr)
    {
        Append(" ");
        AST->Expr->VISIT;
    }
}

DefVisitProc(CppCodeGenerator, VariableDefCommand)
{
    /* Check if first (variable-) object in the definition list is a static constant */
    //if (AST->Obj->IsStaticConst() && AST->Obj->Const != nullptr)
    //    return;

    /* Fetch variable flags */
    const bool IsClassMember = ((AST->DefFlags & VariableDefCommand::Flags::IsClassMember) != 0);

    size_t Num = 0;

    while (AST != nullptr)
    {
        /* Get variable object form current definition */
        auto Obj = AST->Obj;

        /* Code for type denoter */
        if (Num == 0)
        {
            if ((Obj->DefFlags & VariableObject::Flags::IsStatic) != 0)
                Append("static ");

            Obj->TDenoter->VISIT;
            Append(" ");
        }

        ++Num;

        /* Variable name */
        Obj->Ident->VISIT;

        /* Code for variable initialization */
        if (!IsClassMember)
        {
            if (Obj->Expr != nullptr)
            {
                Append(" = ", true);
                Obj->Expr->VISIT;
            }
            else if (!Obj->TDenoter->GetDefaultValue().empty())
            {
                /* Code for default initialization */
                Append(" = ", true);

                Append(Obj->TDenoter->GetDefaultValue());

                if (CompilerOptions::Settings.WarnUninitializedVariables)
                {
                    Warning(
                        "Uninitialized variable \"" + Obj->Ident->Spell +
                        "\" at " + AST->Pos().GetString()
                    );
                }
            }
        }

        /* Get next variable definition AST node */
        AST = AST->Next;

        if (AST != nullptr)
            Append(", ", true);
    }
}

DefVisitProc(CppCodeGenerator, IfCommand)
{
    /* Generate code for <If> block */
    if (AST->Expr != nullptr)
    {
        StartNL();
        {
            Append("if (");
            AST->Expr->VISIT;
            Append(")");
        }
        EndNL();
    }

    AST->CmdBlock->VISIT;
    
    /* Generate code for <Else> blocks */
    if (AST->ElseIfCmd != nullptr)
    {
        NL("else");

        AST->ElseIfCmd->VISIT;
    }
}

DefVisitProc(CppCodeGenerator, InfiniteLoopCommand)
{
    Comment("Infinite Loop");

    NL("while (true)");

    AST->CmdBlock->VISIT;

    Blank();
}

DefVisitProc(CppCodeGenerator, WhileLoopCommand)
{
    const bool IsDoLoop     = ((AST->DefFlags & WhileLoopCommand::Flags::DoLoop     ) != 0);
    const bool IsUntilLoop  = ((AST->DefFlags & WhileLoopCommand::Flags::UntilLoop  ) != 0);

    if (IsDoLoop)
    {
        Comment(IsUntilLoop ? "Do/Until Loop" : "Do/While Loop");

        NL("do");
        
        AST->CmdBlock->VISIT;

        StartNL();
        {
            Append("while (");
            
            if (IsUntilLoop)
                Append(" !( ", true);

            AST->Expr->VISIT;

            if (IsUntilLoop)
                Append(" ) ", true);

            Append(");");
        }
        EndNL();
    }
    else
    {
        Comment(IsUntilLoop ? "Until Loop" : "While Loop");

        StartNL();
        {
            Append("while (");

            if (IsUntilLoop)
                Append(" !( ", true);

            AST->Expr->VISIT;

            if (IsUntilLoop)
                Append(" ) ", true);

            Append(")");
        }
        EndNL();

        AST->CmdBlock->VISIT;
    }

    Blank();
}

DefVisitProc(CppCodeGenerator, RangeForLoopCommand)
{
    /* Get meta data from AST node */
    RangeForLoopCommand::MetaData MetaData;
    AST->FillMetaData(MetaData);

    if (MetaData.Unroll)
    {
        Comment("Unrolled Range-Based For Loop");
        
        NL("{");
        UpperIndent();

        /* Code for unrolled loop iterations */
        RangeForLoopCommand::index_t i = MetaData.Start;

        while ( ( MetaData.Forwards && i <= MetaData.End ) || ( !MetaData.Forwards && i >= MetaData.End ) )
        {
            /* Code for current iteration */
            Comment("Iteration " + xxStr(i));

            StartNL();
            {
                if (i == MetaData.Start)
                    Append(GetRangeIteratorDataType(MetaData.IsSigned, MetaData.MinRange, MetaData.MaxRange) + " ");

                AST->Ident->VISIT;

                Append(" = ", true);
                Append(xxStr(i) + ";");
            }
            EndNL();

            /* Code for command block */
            AST->CmdBlock->VISIT;

            Blank();

            /* Next iteration */
            if (MetaData.Forwards)
                i += MetaData.Step;
            else
                i -= MetaData.Step;
        }

        LowerIndent();
        NL("}");
    }
    else
    {
        /* Code for loop header */
        Comment("Range-Based For Loop");
        
        StartNL();
        {
            Append("for (");

            /* Code for iterator data type */
            Append(GetRangeIteratorDataType(MetaData.IsSigned, MetaData.MinRange, MetaData.MaxRange) + " ");

            /* Code for iterator initialization */
            AST->Ident->VISIT;

            Append(" = ", true);
            Append(xxStr(MetaData.Start));
            Append("; ", true);

            /* Code for loop condition */
            AST->Ident->VISIT;
            Append(MetaData.Forwards ? " <= " : " >= ", true);
            Append(xxStr(MetaData.End));

            /* Code for iteration step */
            Append("; ", true);

            if (MetaData.Step == 1)
            {
                Append(MetaData.Forwards ? "++" : "--");
                AST->Ident->VISIT;
            }
            else
            {
                AST->Ident->VISIT;
                Append(MetaData.Forwards ? " += " : " -= ", true);
                Append(xxStr(MetaData.Step));
            }

            Append(")");
        }
        EndNL();

        /* Code for command block */
        AST->CmdBlock->VISIT;

        Blank();
    }
}

DefVisitProc(CppCodeGenerator, InlineLangCommand)
{
    /* Directly insert the token spelling into the output code (for the specific language) */
    if (AST->LangInfo == "cpp")
    {
        Comment("< Inline C++ Code >");
        
        Append(AST->Code);
        NL();

        Comment("< /Inline C++ Code >");
    }
}

DefVisitProc(CppCodeGenerator, PrimitiveCommand)
{
    const auto& Spell = AST->Spell;

    switch (AST->PrimType)
    {
        case Token::Types::PushConfig:
            CompilerOptions::PushConfig(Spell);
            break;
        case Token::Types::PopConfig:
            CompilerOptions::PopConfig();
            break;
        case Token::Types::Once:
            /* Ignore this primitive command here */
            break;
        default:
            Append(PrimCommandMap_[Spell]);
            break;
    }
}

DefVisitProc(CppCodeGenerator, EnumDeclCommand)
{
    const std::string& EnumName = AST->Obj->Ident->Spell;

    Docu(EnumName + " enumeration.");

    /* Code for enumeration name */
    StartNL();
    {
        Append("struct ");
        AST->Obj->Ident->VISIT;
    }
    EndNL();

    /* Code for enumeration structure */
    NL("{");
    UpperIndent();

    /* Code for actual enumeration */
    NL("enum " + CppCodeGenerator::StdEnumName);
    NL("{");
    UpperIndent();

    for (auto Entry : AST->Obj->Entries)
    {
        StartNL();
        {
            /* Code for current enumeration entry */
            Entry->VISIT;
            Append(",");
        }
        EndNL();
    }

    /* Code for auto-generated enumeration entry (for uninitialized values) */
    NL(CppCodeGenerator::EnumDefaultValueName);

    LowerIndent();
    NL("};");

    /* Code for auto-generated "Num" procedure */
    NL("static inline size_t Num()");
    NL("{");
    UpperIndent();
    {
        NL("return " + xxStr(AST->Obj->Entries.size()) + ";");
    }
    LowerIndent();
    NL("}");

    /* Find longest flag name */
    size_t LongestNameLen = 0;

    if (CompilerOptions::Settings.BloatExpressions)
    {
        for (auto Entry : AST->Obj->Entries)
        {
            const auto Len = Entry->Ident->Spell.size();
            if (LongestNameLen < Len)
                LongestNameLen = Len;
        }
        ++LongestNameLen;
    }

    /* Code for auto-generated "Str" procedure */
    NL("std::string Str() const");
    NL("{");
    UpperIndent();
    {
        StartNL();
        Append("switch (__XX__Entry)", true);
        EndNL();

        NL("{");
        UpperIndent();
        
        for (auto Entry : AST->Obj->Entries)
        {
            StartNL();
            {
                const auto& EntryName = Entry->Ident->Spell;

                /* Code for current enumeration entry-to-string conversion */
                Append("case " + EntryName + ":");

                /* Bloat expression for better readability */
                if (CompilerOptions::Settings.BloatExpressions)
                    Append(std::string(LongestNameLen - EntryName.size(), ' '));
                
                /* Code for return string */
                Append("return ", true);
                Append("\"" + (Entry->Id != nullptr ? Entry->Id->Spell : EntryName) + "\";");
            }
            EndNL();
        }
        
        LowerIndent();
        NL("}");

        NL("return \"\";");
    }
    LowerIndent();
    NL("}");

    /* Code for default member definition macro */
    NL(CppCodeGenerator::McrDefineEnumDefMembers + "(" + EnumName + ")");

    LowerIndent();
    NL("};");

    Blank();
}

DefVisitProc(CppCodeGenerator, FlagsDeclCommand)
{
    const std::string& FlagsName = AST->Obj->Ident->Spell;

    Docu(FlagsName + " flags enumeration.");

    /* Get data type name for flags */
    const std::string DataTypedefStr = GetFlagsDataTypeName(AST->Obj->DataType);
    const size_t DataTypeSize = static_cast<size_t>(AST->Obj->DataType);

    /* Code for flag structure name */
    StartNL();
    {
        Append("struct ");
        AST->Obj->Ident->VISIT;
    }
    EndNL();
    
    /* Find longest flag name */
    size_t LongestNameLen = 4;

    if (CompilerOptions::Settings.BloatExpressions)
    {
        for (auto Flag : AST->Obj->Flags)
        {
            const auto Len = Flag->Spell.size();
            if (LongestNameLen < Len)
                LongestNameLen = Len;
        }
        ++LongestNameLen;
    }

    NL("{");
    UpperIndent();

    /* Code for typedef */
    NL("typedef " + DataTypedefStr + " " + CppCodeGenerator::FlagsDataTypeName + ";");

    /* Code for actual flags enumeration */
    StartNL();
    {
        Append("enum " + CppCodeGenerator::StdEnumName);
        if (CompilerOptions::Settings.EnableCpp11)
        {
            Append(" : ", true);
            Append(CppCodeGenerator::FlagsDataTypeName);
        }
    }
    EndNL();

    NL("{");
    UpperIndent();

    /* Code for auto-generated entries */
    auto AutoGenFlag = [&](const std::string& EntryName, const std::string& ValueStr)
    {
        StartNL();
        {
            /* Code for current current flag */
            Append(EntryName);

            /* Bloat expression for better readability */
            if (CompilerOptions::Settings.BloatExpressions)
                Append(std::string(LongestNameLen - EntryName.size(), ' '));

            /* Code for value assignment */
            Append("= ", true);
            Append(ValueStr + ",");
        }
        EndNL();
    };

    AutoGenFlag("None", "0");

    /* Determine bit-mask to cover all flags */
    size_t i = 0;
    const size_t Num = AST->Obj->Flags.size();

    unsigned long long int BitMaskAll = 0;

    for (; i < Num; ++i)
        BitMaskAll |= (1ull << i);

    AutoGenFlag("All", "0x" + xxNumToHex(BitMaskAll, DataTypeSize));

    /* Code for enumeration flags */
    i = 0;

    for (auto Flag : AST->Obj->Flags)
    {
        StartNL();
        {
            /* Code for current current flag */
            Flag->VISIT;

            /* Bloat expression for better readability */
            if (CompilerOptions::Settings.BloatExpressions)
            {
                const size_t Len = Flag->Spell.size();
                Append(std::string(LongestNameLen - Len, ' '));
            }

            /* Code for value assignment */
            Append("= ", true);

            /* Code for flag value */
            Append("(1 << ", true);

            if (CompilerOptions::Settings.BloatExpressions)
            {
                const auto MaxIndex = Num - 1;
                Append(xxNumberOffset(i, MaxIndex));
            }
            else
                Append(xxStr(i));
            ++i;

            Append("),");
        }
        EndNL();
    }

    LowerIndent();
    NL("};");

    /* Code flags functions */
    NL(CppCodeGenerator::McrDefineFlagsFunctions + "(" + FlagsName + ")");

    /* Code for bit-field */
    NL("private:");
    NL(CppCodeGenerator::FlagsDataTypeName + " " + CppCodeGenerator::FlagsEnumBitFieldName + ";");

    LowerIndent();
    NL("};");

    /* Code for global operators */
    NL(CppCodeGenerator::McrDefineFlagsOperators + "(" + FlagsName + ")");

    Blank();
}

DefVisitProc(CppCodeGenerator, ClassDeclCommand)
{
    auto Obj = AST->Obj;

    /* Skip if this class is a pattern */
    if ((Obj->DefFlags & ClassObject::Flags::IsPattern) != 0)
        return;

    /* Generate structure packing alignment (if enabled) */
    const bool IsPack = (Obj->AttribList != nullptr && Obj->AttribList->HasAttrib(AttributeList::Attributes::Pack));
    if (IsPack)
        GeneratePackStructPush();

    /* Generate documentation commentaries */
    if (Obj->TParamList != nullptr)
        Docu(Obj->Ident->Spell + " template class.");
    else
        Docu(Obj->Ident->Spell + " class.");

    for (auto BaseClassAST : Obj->Inheritance)
        Docu("\\see " + BaseClassAST->FullName()); // <-- !TODO! class name must be resolved here, too.

    StartNL();

    /* Code for template and their arguments */
    if ((Obj->DefFlags & ClassObject::Flags::Instantiated) == 0)
        GenerateTemplateParameters(Obj->TParamList);

    /* Code for class name */
    Append("class ");
    Obj->Ident->VISIT;

    /* Code for inheritance list */
    GenerateClassInheritance(Obj.get());

    EndNL();

    /* Code for class block */
    AST->Obj->ClassBlockCmd->VISIT;

    Blank();

    if (IsPack)
        GeneratePackStructPop();
}

DefVisitProc(CppCodeGenerator, ClassBlockCommand)
{
    /* Get parent object information */
    auto Obj = AST->Parent;

    /* Check if class object has pack alignment enabled */
    const bool IsPack = (Obj->AttribList != nullptr && Obj->AttribList->HasAttrib(AttributeList::Attributes::Pack));

    /* Open class block */
    NL("{");
    UpperIndent();

    /* Code for class block */
    static const char* PrivacyCppNames[3] = { "public:", "protected:", "private:" };

    for (unsigned int i = 0; i < 3; ++i)
    {
        if (AST->Commands[i].empty())
            continue;
        
        /* Code for privacy scope */
        NL(PrivacyCppNames[i]);
        UpperIndent();

        /* Check if this is a template instantiated class */
        if (i == 0 && (Obj->DefFlags & ClassObject::Flags::Instantiated) != 0)
            GenerateTemplateParameterInstances(Obj->TParamList);

        /* Code for all statements in class block (for current privacy state) */
        VisitAllCommands(AST->Commands[i]);

        if ((Obj->DefFlags & ClassObject::Flags::CopyProcMarker) != 0)
            GenerateCopyProcedureDef(Obj);

        LowerIndent();
    }

    /* Close class block */
    LowerIndent();
    NL(IsPack ? "} " + CppCodeGenerator::PackStructName + ";" : "};");
}

DefVisitProc(CppCodeGenerator, InitCommand)
{
    //!TODO! -> MSVC 2012 does not support uniform initializers!!!
    const bool UseUniformInit = false;//(CompilerOptions::Settings.EnableCpp11);

    StartNL();
    
    /* Code for constructor declaration */
    Append(AST->ClassName);
    Append("(");
    
    if (AST->ParamList)
        AST->ParamList->VISIT;
    
    Append(")");

    /* Code for initialization list */
    if (!AST->Members.empty())
    {
        Append(" :");
        EndNL();

        UpperIndent();

        for (auto it = AST->Members.begin(); it != AST->Members.end();)
        {
            auto VarDefAST = *it;
            auto VarObjAST = VarDefAST->Obj;

            /* Check if member variable can have a default initialization */
            if (VarObjAST->Expr == nullptr && VarObjAST->TDenoter->GetDefaultValue().empty())
            {
                ++it;
                continue;
            }

            /* Code for member variable initialization */
            StartNL();

            Append(VarObjAST->Ident->Spell);
            Append(UseUniformInit ? "{" : "(");

            /* Code for initialization expression */
            if (VarObjAST->Expr != nullptr)
                VarObjAST->Expr->VISIT;
            else
                Append(VarObjAST->TDenoter->GetDefaultValue());

            Append(UseUniformInit ? "}" : ")");

            /* Get next member variable */
            ++it;
            if (it != AST->Members.end())
                Append(",");

            EndNL();
        }

        LowerIndent();
    }
    else
        EndNL();

    AST->CmdBlock->VISIT;

    Blank();
}

DefVisitProc(CppCodeGenerator, ReleaseCommand)
{
    /* Code for destructor header */
    StartNL();
    {
        if (!AST->VirtualDisabled)
            Append("virtual ");
        Append("~" + AST->ClassName + "()");
    }
    EndNL();
    
    /* Code for destructor body */
    AST->CmdBlock->VISIT;

    Blank();
}

DefVisitProc(CppCodeGenerator, TryCatchCommand)
{
    NL("try");

    AST->TryBlock->VISIT;

    if (AST->ExceptParam != nullptr)
    {
        StartNL();
        {
            Append("catch (");
            AST->ExceptParam->VISIT;
            Append(")");
        }
        EndNL();
    }
    else
        NL("catch (...)");

    AST->CatchBlock->VISIT;

    Blank();
}

DefVisitProc(CppCodeGenerator, ThrowCommand)
{
    Append("throw ");
    AST->Expr->VISIT;
}

DefVisitProc(CppCodeGenerator, SwitchCommand)
{
    Comment("Switch/Case Statement");

    if (AST->CaseBlocks.empty() && AST->DefBlock == nullptr)
        return;

    /* Code for switch keyword */
    if (AST->IfCmds)
    {
        bool FirstBlock = true;

        for (auto Block : AST->CaseBlocks)
        {
            /* Code for condition */
            StartNL();
            {
                if (FirstBlock)
                {
                    Append("if ((");
                    FirstBlock = false;
                }
                else
                    Append("else if ((");

                /* If statement and main expression */
                AST->Expr->VISIT;
                Append(") == (", true);

                /* Comparision expression */
                Block->Expr->VISIT;
                Append("))");
            }
            EndNL();

            /* Code for case block */
            NL("{");
            UpperIndent();
            {
                VisitAllCommands(Block->Commands);
            }
            LowerIndent();
            NL("}");
        }

        /* Code for default block */
        if (AST->DefBlock != nullptr)
        {
            NL("else");
            NL("{");
            UpperIndent();
            {
                VisitAllCommands(AST->DefBlock->Commands);
            }
            LowerIndent();
            NL("}");
        }
    }
    else
    {
        StartNL();
        {
            Append("switch (");
            AST->Expr->VISIT;
            Append(")");
        }
        EndNL();

        /* Code for case and default blocks */
        NL("{");
        UpperIndent();
        {
            for (auto Block : AST->CaseBlocks)
                Block->VISIT;

            if (AST->DefBlock != nullptr)
                AST->DefBlock->VISIT;
        }
        LowerIndent();
        NL("}");
    }

    Blank();
}

DefVisitProc(CppCodeGenerator, TypeDefCommand)
{
    auto Obj = AST->Obj;

    Append("typedef ");
    Obj->TDenoter->VISIT;
    Append(" ");
    Obj->Ident->VISIT;
}

/* === Expressions === */

DefVisitProc(CppCodeGenerator, BracketExpression)
{
    Append("(");
    AST->Expr->VISIT;
    Append(")");
}

DefVisitProc(CppCodeGenerator, BinaryExpression)
{
    /* Check if additional brackets are required for assignment expression */
    bool HasAssign = false;
    if (AST->Expr2->Type() == Expression::Types::ObjectExpr)
    {
        auto ObjectExpr = std::dynamic_pointer_cast<ObjectExpression>(AST->Expr2);
        if (ObjectExpr->AssignExpr != nullptr)
            HasAssign = true;
    }

    const bool IsStringWrap = ((AST->DefFlags & Expression::Flags::StringWrap) != 0) && !HasAssign;
    bool IsStringWrap1 = false, IsStringWrap2 = false;

    if (IsStringWrap)
    {
        IsStringWrap1 = ((AST->Expr1->DefFlags & Expression::Flags::StringWrap) == 0);
        IsStringWrap2 = ((AST->Expr2->DefFlags & Expression::Flags::StringWrap) == 0);
    }

    /* Code for 1st sub-expression */
    if (IsStringWrap1)
        Append(CppCodeGenerator::StringWrap);
    
    AST->Expr1->VISIT;

    if (IsStringWrap1)
        Append(")");

    /* Code for the operator */
    Bloat();
    AST->Op->VISIT;
    Bloat();

    if (CompilerOptions::Settings.AllowExtMathExpr)
    {
        /* Check for special cases of relation-operators */
        BinaryExpression* SubBinExpr = nullptr;
        if (AST->IsRelationWithFollowingRelation(&SubBinExpr))
        {
            /* Append attachment for comfort math expressions (e.g. "0 < num < 10" to "0 < num && num < 10") */
            SubBinExpr->Expr1->VISIT;
            Append(" && ", true);
        }
    }

    /* Code for 2nd sub-expression */
    if (HasAssign)
        Append("(");
    else if (IsStringWrap2)
        Append(CppCodeGenerator::StringWrap);
    
    AST->Expr2->VISIT;

    if (HasAssign || IsStringWrap2)
        Append(")");
}

DefVisitProc(CppCodeGenerator, UnaryExpression)
{
    Append("(");
    
    if (AST->Notation == UnaryExpression::Notations::Prefix)
        AST->Op->VISIT;
    
    AST->Expr->VISIT;
    
    if (AST->Notation == UnaryExpression::Notations::Postfix)
        AST->Op->VISIT;

    Append(")");
}

DefVisitProc(CppCodeGenerator, AssignExpression)
{
    AST->Op->VISIT;
    Bloat();
    AST->Expr->VISIT;
}

DefVisitProc(CppCodeGenerator, StringSumExpression)
{
    bool StrSum1 = AST->Expr1->Type() != Expression::Types::StrSumExpr;
    bool StrSum2 = AST->Expr2->Type() != Expression::Types::StrSumExpr;

    /* Code for first expression */
    if (StrSum1)
        Append(CppCodeGenerator::StringWrap);
    AST->Expr1->VISIT;
    if (StrSum1)
        Append(")");

    /* Code for sumation operator */
    Append(" + ", true);

    /* Code for seconnd expression */
    if (StrSum2)
        Append(CppCodeGenerator::StringWrap);
    AST->Expr2->VISIT;
    if (StrSum2)
        Append(")");
}

DefVisitProc(CppCodeGenerator, LiteralExpression)
{
    AST->Literal->VISIT;
}

DefVisitProc(CppCodeGenerator, CallExpression)
{
    AST->Call->VISIT;
}

DefVisitProc(CppCodeGenerator, CastExpression)
{
    /* Code for the type casting */
    if (AST->IsSmartPointer)
    {
        AppendStdOrBoost();

        switch (AST->Casting)
        {
            case CastExpression::TypeCasts::StaticCast:
                Append("static_pointer_cast");
                break;
            case CastExpression::TypeCasts::ConstCast:
                Append("const_pointer_cast");
                break;
            case CastExpression::TypeCasts::DynamicCast:
                Append("dynamic_pointer_cast");
                break;
            default:
                Error("Reinterpret cast can not be used for smart pointers");
                break;
        }
    }
    else
    {
        switch (AST->Casting)
        {
            case CastExpression::TypeCasts::StaticCast:
                Append("static_cast");
                break;
            case CastExpression::TypeCasts::ConstCast:
                Append("const_cast");
                break;
            case CastExpression::TypeCasts::DynamicCast:
                Append("dynamic_cast");
                break;
            case CastExpression::TypeCasts::ReinterpretCast:
                Append("reinterpret_cast");
                break;
        }
    }

    /* Code for expression which is to be casted */
    Append("< ");

    if (AST->IsSmartPointer && AST->TDenoter->Type() == TypeDenoter::Types::MngPtrType)
    {
        auto PtrTDenoter = std::dynamic_pointer_cast<MngPtrTypeDenoter>(AST->TDenoter);
        PtrTDenoter->TDenoter->VISIT;
    }
    else
        AST->TDenoter->VISIT;

    Append(" >(");
    AST->Expr->VISIT;
    Append(")");
}

DefVisitProc(CppCodeGenerator, ObjectExpression)
{
    /* Check for static constant */
    const Constant* Const = nullptr;

    if (AST->Obj != nullptr && AST->Obj->Type() == Object::Types::VarObj)
    {
        auto VarObj = dynamic_cast<VariableObject*>(AST->Obj);
        if (VarObj->IsStaticConst() && VarObj->Const != nullptr)
            Const = VarObj->Const.get();
    }

    if (Const == nullptr)
    {
        /* Check for special case (e.g. flags object) */
        const bool IsFlagsEntry = ((AST->FName->GetLastIdent()->IdentFlags & FNameIdent::Flags::FlagsEntry) != 0);

        if (IsFlagsEntry)
        {
            AppendResolvedFName(AST->FName, 0u, false, true);
            Append("(");
        }

        /* Code for memory object expression */
        AST->FName->VISIT;

        if (IsFlagsEntry)
            Append(")");

        if (AST->AssignExpr != nullptr)
        {
            Bloat();
            AST->AssignExpr->VISIT;
        }
    }
    else
    {
        /* Just append constant value */
        Append(Const->Str());
    }
}

DefVisitProc(CppCodeGenerator, AllocExpression)
{
    GenerateMakeShared(AST->TDenoter);

    Append("(");
    {
        if (AST->ArgList != nullptr)
            AST->ArgList->VISIT;
    }
    Append(")");
}

DefVisitProc(CppCodeGenerator, CopyExpression)
{
    /* Resolve FName completely */
    AppendResolvedFName(AST->FName, 0, true);

    /* Code for 'copy' procedure */
    Append("." + CodeGenerator::IdentPrefix + "Copy()");
}

DefVisitProc(CppCodeGenerator, ValidationExpression)
{
    /* Collect object information */
    auto Obj = AST->ObjExpr->Obj;
    
    size_t Num = 0;

    if (Obj->Type() == Object::Types::VarObj)
    {
        auto VarObj = dynamic_cast<VariableObject*>(Obj);
        auto TDenoter = VarObj->TDenoter.get();

        while (TDenoter != nullptr)
        {
            /* Get type-denoter for next iteration */
            if (TDenoter->Type() == TypeDenoter::Types::RawPtrType)
                TDenoter = dynamic_cast<RawPtrTypeDenoter*>(TDenoter)->TDenoter.get();
            else if (TDenoter->Type() == TypeDenoter::Types::MngPtrType)
                TDenoter = dynamic_cast<MngPtrTypeDenoter*>(TDenoter)->TDenoter.get();
            else
                break;

            /* Append condition binary expression and pointer dereferencing */
            if (Num > 0)
            {
                Append(" && ", true);
                Append(std::string(Num, '*'));
            }
            else
                Append("(");

            ++Num;

            AST->ObjExpr->FName->VISIT;
            
            /* Append validation condition */
            Append(" != ", true);
            GenerateNullPtr();
        }
    }

    if (Num > 0)
        Append(")");
    else
        Append("true");
}

DefVisitProc(CppCodeGenerator, InitListExpression)
{
    Append("{ ", true);
    AST->InitList->VISIT;
    Append(" }", true);
}

DefVisitProc(CppCodeGenerator, LambdaExpression)
{
    /* Code for lambda header */
    Append("[&](");
    AST->ProcTDenoter->ParamList->VISIT;
    Append(")");

    /* Code for lambda header return type */
    if (AST->ProcTDenoter->TDenoter->Type() != TypeDenoter::Types::VoidType)
    {
        Append(" -> ", true);
        AST->ProcTDenoter->TDenoter->VISIT;
    }

    /* Code for lambda body (reverse line ending here!) */
    EndNL();
    {
        AST->BlockCmd->VISIT;
    }
    StartNL();
}

/* === Type denoters === */

DefVisitProc(CppCodeGenerator, VoidTypeDenoter)
{
    Append("void");
}

DefVisitProc(CppCodeGenerator, ConstTypeDenoter)
{
    AST->TDenoter->VISIT;
    Append(" const");
}

/*DefVisitProc(CppCodeGenerator, VoidTypeDenoter)
{
    
}*/

DefVisitProc(CppCodeGenerator, BoolTypeDenoter)
{
    Append("bool");
}

DefVisitProc(CppCodeGenerator, IntTypeDenoter)
{
    Append(TypeDenoterMap_[AST->Spell]);
}

DefVisitProc(CppCodeGenerator, FloatTypeDenoter)
{
    Append(TypeDenoterMap_[AST->Spell]);
}

DefVisitProc(CppCodeGenerator, StringTypeDenoter)
{
    Append(TypeDenoterMap_[AST->Spell]);
}

DefVisitProc(CppCodeGenerator, CustomTypeDenoter)
{
    AST->FName->VISIT;
}

DefVisitProc(CppCodeGenerator, MngPtrTypeDenoter)
{
    Append(CompilerOptions::Settings.EnableCpp11 ? "std" : "boost");
    Append("::shared_ptr< ");

    AST->TDenoter->VISIT;

    Append(" >");
}

DefVisitProc(CppCodeGenerator, RawPtrTypeDenoter)
{
    AST->TDenoter->VISIT;
    Append("*");
}

DefVisitProc(CppCodeGenerator, RefTypeDenoter)
{
    AST->TDenoter->VISIT;
    Append("&");
}

DefVisitProc(CppCodeGenerator, ProcTypeDenoter)
{
    Append(CompilerOptions::Settings.EnableCpp11 ? "std" : "boost");
    Append("::function< ");

    AST->TDenoter->VISIT;

    Append(" (", true);

    if (AST->ParamList != nullptr)
        AST->ParamList->VISIT;

    Append(") >");
}

DefVisitProc(CppCodeGenerator, ArrayTypeDenoter)
{
    /* Code for array type */
    AppendStdOrBoost();

    Append(AST->IsStatic() ? "array" : "vector");
    Append("< ");

    AST->TDenoter->VISIT;

    /* Code for dimension */
    if (AST->IsStatic())
    {
        Append(", ");

        #if 1//!!!
        Append(xxStr(AST->GetDim()));
        #else
        //!TODO! -> use constant expression
        //Append("(" + xxStr(AST->GetDim()) + ")");
        #endif
    }

    Append(" >");
}

/* === Lists === */

DefVisitProc(CppCodeGenerator, ParameterList)
{
    /* Code for parameter definition */
    GenerateParameter(AST->Obj.get(), CompilerOptions::Settings.AutoConstRef);

    /* Code for optional next parameter */
    if (AST->Next != nullptr)
    {
        Append(", ");
        AST->Next->VISIT;
    }
}

DefVisitProc(CppCodeGenerator, ArgumentList)
{
    AST->Expr->VISIT;

    if (AST->Next != nullptr)
    {
        Append(", ");
        AST->Next->VISIT;
    }
}

DefVisitProc(CppCodeGenerator, TemplateParamList)
{
    auto Obj = AST->Obj.get();

    if (Obj != nullptr)
    {
        if (Obj->Type() == Object::Types::VarObj)
            GenerateParameter(dynamic_cast<VariableObject*>(Obj));
    }

    if (AST->Next != nullptr)
    {
        Append(", ", true);
        AST->Next->VISIT;
    }
}

DefVisitProc(CppCodeGenerator, InitializerList)
{
    AST->Expr->VISIT;

    if (AST->Next != nullptr)
    {
        Append(", ", true);
        AST->Next->VISIT;
    }
}

DefVisitProc(CppCodeGenerator, ArrayIndexList)
{
    Append("[");
    
    /* Code for index expression */
    AST->Expr->VISIT;

    /* Check for index offset */
    if (AST->Link != nullptr && AST->Link->Type() == TypeDenoter::Types::ArrayType)
    {
        auto ArrayTDenoter = dynamic_cast<ArrayTypeDenoter*>(AST->Link);
        if (ArrayTDenoter->HasOffset())
        {
            //!TODO! -> don't subtract when negative offset is used to avoid expression like 'i - -5' -> instead: 'i + 5'.
            /* Subtract index offset */
            Append(" - ", true);
            Append(ArrayTDenoter->OffsetLiteral->Spell);
        }
    }

    Append("]");

    if (AST->Next != nullptr)
        AST->Next->VISIT;
}

/* === Literals === */

DefVisitProc(CppCodeGenerator, BoolLiteral)
{
    Append(AST->Spell);
}

DefVisitProc(CppCodeGenerator, PointerLiteral)
{
    if (AST->Spell == "null")
        GenerateNullPtr();
    else if (AST->Spell == "this")
    {
        //!TODO! -> determine if a shared pointer is used or not
        Append(AST->Spell);
    }
}

DefVisitProc(CppCodeGenerator, IntegerLiteral)
{
    Append(AST->Spell);
}

DefVisitProc(CppCodeGenerator, FloatLiteral)
{
    Append(AST->Spell);
}

DefVisitProc(CppCodeGenerator, StringLiteral)
{
    Append("\"" + AST->Spell + "\"");
}

/* === Operators === */

DefVisitProc(CppCodeGenerator, AssignOperator)
{
    Append(ConvertSpell(AST));
}

DefVisitProc(CppCodeGenerator, BinaryOperator)
{
    Append(ConvertSpell(AST));
}

DefVisitProc(CppCodeGenerator, UnaryOperator)
{
    Append(ConvertSpell(AST));
}

/* === Others === */

DefVisitProc(CppCodeGenerator, Identifier)
{
    if (CompilerOptions::Settings.Obfuscate)
        Append(GetObfuscatedName(AST->Spell));
    else
        Append(AST->Spell);
}

DefVisitProc(CppCodeGenerator, FNameIdent)
{
    AppendResolvedFName(AST);
}

DefVisitProc(CppCodeGenerator, Program)
{
    VisitAllCommands(AST->Commands);
}

DefVisitProc(CppCodeGenerator, EnumEntry)
{
    /* Code for enumeration entry name */
    AST->Ident->VISIT;

    if (AST->Expr != nullptr)
    {
        /* Code for enumeration entry explicit initialization */
        Append(" = ", true);

        AST->Expr->VISIT;
    }
}

DefVisitProc(CppCodeGenerator, CaseBlock)
{
    /* Start for case or default statement */
    if (AST->Expr != nullptr)
    {
        StartNL();
        {
            Append("case ");
            AST->Expr->VISIT;
            Append(":");
        }
        EndNL();
    }
    else
        NL("default:");

    /* Code for case/default block */
    if (!AST->Commands.empty())
    {
        NL("{");
        UpperIndent();
        {
            VisitAllCommands(AST->Commands);
        }
        LowerIndent();
        NL("}");
    }

    if (!AST->SkipBreak)
        NL("break;");

    if (!AST->Commands.empty() || !AST->SkipBreak)
        Blank();
}



// ================================================================================