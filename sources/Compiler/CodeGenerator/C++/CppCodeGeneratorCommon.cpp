/*
 * C++ code generator common file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "CppCodeGenerator.h"
#include "CompilerOptions.h"
#include "StringMod.h"
#include "ASTIncludes.h"
#include "ProcedureFactory.h"

#include <algorithm>
#include <numeric>


/*
 * ======= Private: =======
 */

void CppCodeGenerator::EstablishTypeDenoterMap()
{
    /* Establish all type denoter mappings */
    TypeDenoterMap_["byte"      ] = "char";
    TypeDenoterMap_["ubyte"     ] = "unsigned char";
    TypeDenoterMap_["short"     ] = "short";
    TypeDenoterMap_["ushort"    ] = "unsigned short";
    TypeDenoterMap_["int"       ] = "int";
    TypeDenoterMap_["uint"      ] = "unsigned int";
    TypeDenoterMap_["long"      ] = "long int";
    TypeDenoterMap_["ulong"     ] = "unsigned long int";

    TypeDenoterMap_["float"     ] = "float";
    TypeDenoterMap_["double"    ] = "double";
    TypeDenoterMap_["quad"      ] = "long double";

    TypeDenoterMap_["string"    ] = "std::string";
    TypeDenoterMap_["wstring"   ] = "std::wstring";
}

void CppCodeGenerator::EstablishPrimCommandMap()
{
    /* Establish all primitive command mappings */
    PrimCommandMap_["next"  ] = "continue";
    PrimCommandMap_["break" ] = "break";
    PrimCommandMap_["stop"  ] = "exit(0)";
}

std::string CppCodeGenerator::ConvertSpell(OperatorPtr AST) const
{
    if (AST->Spell == "=")
        return "==";
    else if (AST->Spell == ":=")
        return "=";
    return AST->Spell;
}

void CppCodeGenerator::AddGlobalInclude(const std::string &Filename)
{
    NL("#include <" + Filename + ">");
}

void CppCodeGenerator::AddLocalInclude(const std::string &Filename)
{
    NL("#include \"" + Filename + "\"");
}

void CppCodeGenerator::Comment(const std::string& Text, bool Symetric)
{
    if (CompilerOptions::Settings.EnableComments)
        NL("// " + (Symetric ? Text + " //" : Text));
}

void CppCodeGenerator::Docu(const std::string& Text)
{
    if (CompilerOptions::Settings.EnableComments)
        NL("//! " + Text);
}

std::string CppCodeGenerator::GetLineCommentStr() const
{
    return "//";
}

void CppCodeGenerator::AppendStdOrBoost()
{
    Append(CompilerOptions::Settings.EnableCpp11 ? "std::" : "boost::");
}

void CppCodeGenerator::GenerateIncludeDirectives(ProgramPtr ProgramAST)
{
    if (ProgramAST->IncludeFiles.empty())
        return;

    NL();

    /* Append all include files */
    for (const auto& IFile : ProgramAST->IncludeFiles)
    {
        if (IFile.second)
            AddGlobalInclude(IFile.first);
        else
            AddLocalInclude(IFile.first);
    }

    #if 0//!TESTING!
    AddGlobalInclude("cstdlib");
    AddGlobalInclude("iostream");
    #endif

    NL();
}

void CppCodeGenerator::GenerateHeaderGuardStart(const std::string &Filename)
{
    if (CompilerOptions::Settings.UseClassicHeaderGuard)
    {
        std::string HeaderGuard;
        
        if (CompilerOptions::Settings.UseHeaderGuardName)
        {
            /* Adjust filename for header guard macro */
            HeaderGuard = Filename;

            std::for_each(
                HeaderGuard.begin(), HeaderGuard.end(),
                [](char &Chr)
                {
                    if (Chr >= 'a' && Chr <= 'z')
                        Chr = Chr - 'a' + 'A';
                    else if ( !( ( Chr >= 'A' && Chr <= 'Z' ) || ( Chr >= '0' && Chr <= '9' ) ) )
                        Chr = '_';
                }
            );
        }
        else
        {
            /* Just use a global index for the header guard */
            HeaderGuard = xxStr(++CppCodeGenerator::HeaderGuardIndex_);
        }

        /* Generate header guard */
        HeaderGuard = CodeGenerator::IdentPrefix + HeaderGuard + "__H__";

        NL("#ifndef " + HeaderGuard);
        NL("#define " + HeaderGuard);
    }
    else
        NL("#pragma once");
}

void CppCodeGenerator::GenerateHeaderGuardEnd()
{
    if (CompilerOptions::Settings.UseClassicHeaderGuard)
        NL("#endif");
}

static const std::string CopyProcParamIn    = "src";
static const std::string CopyProcParamOut   = "dest";

void CppCodeGenerator::GenerateSharedPtr(ASTPtr AST)
{
    AppendStdOrBoost();
    Append("shared_ptr< ");
    AST->VISIT;
    Append(" >");
}

void CppCodeGenerator::GenerateMakeShared(ASTPtr AST)
{
    AppendStdOrBoost();
    Append("make_shared< ");
    AST->VISIT;
    Append(" >");
}

void CppCodeGenerator::GenerateCopyProcedureDecl(const ClassObject* Obj)
{
    /* Code for return type and function name */
    GenerateSharedPtr(Obj->Ident);
    Append(CodeGenerator::IdentPrefix + "Copy() const");
}

void CppCodeGenerator::GenerateCopyProcedureDef(const ClassObject* Obj)
{
    /* Code for function header */
    Docu("Automatic generated 'copy' procedure for \"" + Obj->Ident->Spell + "\" class.");
    
    StartNL();
    {
        GenerateCopyProcedureDecl(Obj);
    }
    EndNL();

    /* Code for function body */
    NL("{");
    UpperIndent();
    {
        StartNL();
        {
            /* Code for destination variable and allocation */
            GenerateSharedPtr(Obj->Ident);
            Append(" ");
            Append(CopyProcParamOut + " = ", true);
            GenerateMakeShared(Obj->Ident);
            Append("();");
        }
        EndNL();

        //todo ...
        Comment("TODO");

        /* Code for return statement */
        NL("return " + CopyProcParamOut + ";");
    }
    LowerIndent();
    NL("}");

    Blank();
}

void CppCodeGenerator::GeneratePackStructPush()
{
    Comment("Structure Packing Alignment");

    NL("#if defined(_MSC_VER)");
    NL("#   pragma pack(push, packing)");
    NL("#   pragma pack(1)");
    NL("#   define " + CppCodeGenerator::PackStructName);
    NL("#elif defined(__GNUC__)");
    NL("#   define " + CppCodeGenerator::PackStructName + " __attribute__((packed))");
    NL("#else");
    NL("#   define " + CppCodeGenerator::PackStructName);
    NL("#endif");

    Blank();
}

void CppCodeGenerator::GeneratePackStructPop()
{
    Comment("/Structure Packing Alignment");

    NL("#ifdef _MSC_VER");
    NL("#   pragma pack(pop, packing)");
    NL("#endif");
    NL("#undef " + CppCodeGenerator::PackStructName);

    Blank();
}

void CppCodeGenerator::GenerateParameter(VariableObject* Obj, bool AllowAutoCRef)
{
    /* Code for parameter data type*/
    Obj->TDenoter->VISIT;

    if (AllowAutoCRef && Obj->TDenoter->Type() == TypeDenoter::Types::ConstType)
        Append("&");

    Append(" ");

    /* Code for parameter identifier */
    Obj->Ident->VISIT;

    if (Obj->Expr != nullptr)
    {
        Append(" = ", true);
        Obj->Expr->VISIT;
    }
}

void CppCodeGenerator::GenerateTemplateParameters(TemplateParamListPtr TParamList)
{
    if (TParamList != nullptr)
    {
        Append("template < ");
        TParamList->VISIT;
        Append(" > ");
    }
}

void CppCodeGenerator::VisitAllCommands(const std::list<CommandPtr>& Commands)
{
    for (auto Cmd : Commands)
    {
        const bool IsNotStandAlone = !Cmd->IsStandAloneCmd();

        if (IsNotStandAlone)
            StartNL();
        
        Cmd->VISIT;
        
        if (IsNotStandAlone)
        {
            Append(";");
            EndNL();
        }
    }
}

void CppCodeGenerator::AppendResolvedFName(
    const FNameIdentPtr& AST, unsigned int FNameLevel, bool ResolvePointers, bool SkipLastIdent)
{
    /* Check if last identifier should be skipped */
    if (SkipLastIdent && AST->Next == nullptr)
        return;

    /* Get identifier spelling */
    auto Spell = AST->Ident->Spell;

    if (CompilerOptions::Settings.Obfuscate)
        Spell = GetObfuscatedName(Spell);

    /* Get linked object */
    auto Obj = AST->Link;
    if (Obj == nullptr)
        Error("Unresolved identifier \"" + AST->FullName() + "\" -- " + AST->Pos().GetString());

    /* Get information about the linked object */
    size_t NumPointerRes = 0;
    bool IsMemberField = false;

    if (Spell == "this")
    {
        NumPointerRes = 1;
        IsMemberField = true;
    }
    else if (Obj != nullptr)
    {
        switch (Obj->Type())
        {
            case Object::Types::VarObj:
            {
                IsMemberField = true;

                /* Only dereference, if there's a next sub-member */
                if (AST->Next != nullptr || ResolvePointers)
                {
                    /* Get number of pointer references */
                    auto VarObj = dynamic_cast<VariableObject*>(Obj);
                    NumPointerRes = VarObj->TDenoter->CountPointerRefs();
                }
            }
            break;

            default:
                break;
        }
    }

    /* Append pointer resolution string */
    if (FNameLevel == 0)
        AppendResolvedFNamePointer(AST, ResolvePointers);

    /* Append identifier string */
    Append(Spell);

    /* Close pointer resolution */
    if (NumPointerRes > 0)
        Append(")");

    /* Append optional array index list */
    if (AST->ArrayList != nullptr)
        AST->ArrayList->VISIT;

    /* Append next FName AST node */
    if (AST->Next != nullptr)
    {
        /* Check again if last identifier should be skipped */
        if (SkipLastIdent && AST->Next->Next == nullptr)
            return;

        /* Check for resolution type */
        if (IsMemberField)
            Append(".");
        else
            Append("::");

        /* Continue with next FName AST node */
        AppendResolvedFName(AST->Next, FNameLevel + 1, ResolvePointers, SkipLastIdent);
    }
}

void CppCodeGenerator::AppendResolvedFNamePointer(const FNameIdentPtr& AST, bool ResolvePointers)
{
    /* Append next pointer resolution first */
    if (AST->Next != nullptr)
        AppendResolvedFNamePointer(AST->Next, ResolvePointers);

    /* Get linked object */
    auto Obj = AST->Link;

    size_t NumPointerRes = 0;

    if (AST->Ident->Spell == "this")
        NumPointerRes = 1;
    else if (Obj != nullptr)
    {
        switch (Obj->Type())
        {
            case Object::Types::VarObj:
            {
                /* Only dereference, if there's a next sub-member */
                if (AST->Next != nullptr || ResolvePointers)
                {
                    /* Get number of pointer references */
                    auto VarObj = dynamic_cast<VariableObject*>(Obj);
                    NumPointerRes = VarObj->TDenoter->CountPointerRefs();
                }
            }
            break;

            default:
                break;
        }
    }

    /* Append pointer resolution string */
    if (NumPointerRes > 0)
        Append("(" + std::string(NumPointerRes, '*'));
}

std::string CppCodeGenerator::GetRangeIteratorDataType(
    bool IsSigned, const RangeForLoopCommand::index_t& MinRange, const RangeForLoopCommand::index_t& MaxRange) const
{
    if (IsSigned)
    {
        if (MaxRange > std::numeric_limits<long int>::max() || MinRange < std::numeric_limits<long int>::min())
            return "long long int";
        else if (MaxRange > std::numeric_limits<int>::max() || MinRange < std::numeric_limits<int>::min())
            return "long int";
        else if (MaxRange > std::numeric_limits<short int>::max() || MinRange < std::numeric_limits<short int>::min())
            return "int";
        else if (MaxRange > std::numeric_limits<char>::max() || MinRange < std::numeric_limits<char>::min())
            return "short int";
        else
            return "char";
    }
    else
    {
        if (MaxRange > std::numeric_limits<unsigned long int>::max())
            return "unsigned long long int";
        else if (MaxRange > std::numeric_limits<unsigned int>::max())
            return "unsigned long int";
        else if (MaxRange > std::numeric_limits<unsigned short int>::max())
            return "unsigned int";
        else if (MaxRange > std::numeric_limits<unsigned char>::max())
            return "unsigned short int";
        else
            return "unsigned char";
    }
    return "";
}

bool CppCodeGenerator::GenerateAnonymousClass(const ClassObject* Obj, const std::string& BaseFilename)
{
    /* Get class object ID string */
    if (Obj == nullptr)
        return false;

    const auto& AnonymousId = Obj->AnonymousId;

    /* Create output file */
    const std::string Filename = BaseFilename + "." + AnonymousId + ".h";

    GenerateFile(Filename);

    /* Open header guard */
    GenerateHeaderGuardStart(Filename);

    /* Code for all include directives */
    //todo ... !!!

    /* Generate documentation commentaries */
    Docu(AnonymousId + " class.");

    StartNL();
    {
        /* Code for class name */
        Append("class " + AnonymousId);

        /* Code for inheritance list */
        GenerateClassInheritance(Obj);
    }
    EndNL();

    /* Code for class body */
    Obj->ClassBlockCmd->VISIT;

    Blank();

    /* Close header guard */
    GenerateHeaderGuardEnd();

    return true;
}

void CppCodeGenerator::GenerateClassInheritance(const ClassObject* Obj)
{
    if (!Obj->Inheritance.empty())
    {
        Append(" : ");

        for (auto it = Obj->Inheritance.begin(); it != Obj->Inheritance.end();)
        {
            /* Code for current inheritance class */
            Append("public ");
            (*it)->VISIT;

            /* Code for seperator */
            ++it;
            if (it != Obj->Inheritance.end())
                Append(", ");
        }
    }
}

void CppCodeGenerator::GenerateTemplateParameterInstances(TemplateParamListPtr TParamList)
{
    /* Code for pattern types */
    while (TParamList != nullptr)
    {
        auto Obj = TParamList->Obj.get();

        StartNL();

        if (Obj->Type() == Object::Types::PatternObj)
        {
            auto PatternObj = dynamic_cast<PatternObject*>(Obj);

            /* Define pattern type */
            Append("typedef ");
            PatternObj->TDenoter->VISIT;
            Append(" " + PatternObj->Ident->Spell + ";");
        }
        else if (Obj->Type() == Object::Types::VarObj)
        {
            auto VarObj = dynamic_cast<VariableObject*>(Obj);

            /* Define static constant */
            if (VarObj->Expr != nullptr)
            {
                Append("static const ");
                VarObj->TDenoter->VISIT;
                Append(" " + VarObj->Ident->Spell);
                Append(" = ", true);
                VarObj->Expr->VISIT;
                Append(";");
            }
        }

        EndNL();

        TParamList = TParamList->Next;
    }
}

void CppCodeGenerator::GenerateNullPtr()
{
    if (CompilerOptions::Settings.EnableCpp11)
        Append("nullptr");
    else
        Append("0");
}

void CppCodeGenerator::GenerateStdProcCall(const CallCommandPtr& AST, const std::string& ProcName, const FunctionObject& ProcObj)
{
    bool UseDefaultArgs = true;

    switch (ProcObj.StdProcType)
    {
        case FunctionObject::StdProcs::String:
            GenerateStdProcCallString(AST, ProcName, ProcObj, UseDefaultArgs);
            break;

        default:
            break;
    }

    /* Code for procedure arguments */
    if (UseDefaultArgs)
    {
        Append("(");
        {
            if (AST->ArgList != nullptr)
                AST->ArgList->VISIT;
        }
        Append(")");
    }
}

void CppCodeGenerator::GenerateStdProcCallString(
    const CallCommandPtr& AST, const std::string& ProcName, const FunctionObject& ProcObj, bool& UseDefaultArgs)
{
    if (ProcName == "Lower")
    {
        Append("Lang::__XX__StrLower(");
        AppendResolvedFName(AST->FName, 0, false, true);
        Append(")");

        UseDefaultArgs = false;
    }
    else if (ProcName == "Upper")
    {
        Append("Lang::__XX__StrUpper(");
        AppendResolvedFName(AST->FName, 0, false, true);
        Append(")");

        UseDefaultArgs = false;
    }
    else
    {
        /* Code for string variable */
        AppendResolvedFName(AST->FName, 0, false, true);
        Append(".");

        if (ProcName == "Empty" || ProcName == "Size" || ProcName == "Clear" || ProcName == "Resize" || ProcName == "Find")
            Append(xxLower(ProcName));
        else if (ProcName == "Sub")
            Append("substr");
        else if (ProcName == "Left")
        {
            Append("substr(0, ", true);
            if (AST->ArgList != nullptr)
                AST->ArgList->VISIT;
            Append(")");

            UseDefaultArgs = false;
        }
        else if (ProcName == "Right")
        {
            Append("substr(", true);

            AppendResolvedFName(AST->FName, 0, false, true);
            Append(".size() - ");

            if (AST->ArgList != nullptr)
            {
                AST->ArgList->VISIT;
                Append(", ", true);
                AST->ArgList->VISIT;
            }
            else
                Append("0, 0)", true);

            Append(")");

            UseDefaultArgs = false;
        }
        else
            Append(ProcName);
    }
}

std::string CppCodeGenerator::GetFlagsDataTypeName(const FlagsObject::DataTypes& Type) const
{
    switch (Type)
    {
        case FlagsObject::DataTypes::UInt8: return "unsigned char";
        case FlagsObject::DataTypes::UInt16: return "unsigned short int";
        case FlagsObject::DataTypes::UInt32: return "unsigned long int";
        case FlagsObject::DataTypes::UInt64: return "unsigned long long int";
    }
    return "";
}

std::string CppCodeGenerator::GetObfuscatedName(const std::string& Name)
{
    /* Search for possible already existing name */
    auto it = ObfuscationMap_.find(Name);

    if (it != ObfuscationMap_.end())
        return it->second;

    /* Generate new obfuscated name */
    std::string ObfName = "_" + xxStr(CppCodeGenerator::ObfuscatedNameIndex_++);

    ObfuscationMap_[Name] = ObfName;

    return ObfName;
}



// ================================================================================