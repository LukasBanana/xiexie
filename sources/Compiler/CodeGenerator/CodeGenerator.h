/*
 * Code generator header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_CODEGENERATOR_H__
#define __XX_CODEGENERATOR_H__


#include "Visitor.h"

#include <fstream>


using namespace AbstractSyntaxTrees;


class_visitor(CodeGenerator)
{
    
    public:
        
        virtual ~CodeGenerator();

        /* === Functions === */

        /**
        Generates the given program as output source files.
        \param[in] SourcePath Specifies the destination source path.
        \param[in] ProgramAST Specifies the program's AST root node.
        \return True on success, otherwise false.
        */
        virtual bool GenerateCode(const std::string& SourcePath, ProgramPtr ProgramAST) = 0;

        /**
        Sets up the indentation modifier.
        \param[in] IndentModify Specifies the new indentation modifier. By default four blanks ("    ").
        */
        void SetupIndentation(const std::string& IndentModifier);

    protected:

        CodeGenerator();

        /* === Static members === */

        static const std::string IdentPrefix;

        /* === Functions === */

        //! Adds a new output source file.
        bool GenerateFile(const std::string &Filename);

        void Warning(const std::string &Message);
        void Error(const std::string &Message);

        //! Adds a line with new-line seperator ('\n') in the output source file.
        void NL(const std::string &Line);
        void NL();

        void StartNL();
        void Append(const std::string &Str, bool IsBloated = false);
        void EndNL();

        /**
        Appends a blank line (but only if blanks are allowed in the compiler options).
        \see CompilerOptions::ConfigSettings::AllowBlanks
        */
        void Blank();

        void UpperIndent();
        void LowerIndent();

        //! Appends a blank if bloating if enabled in compiler options.
        void Bloat();

        virtual std::string GetLineCommentStr() const = 0;

    private:
        
        /* === Functions === */

        void UpdateHeadAndFootPrint(const std::string &Filename);

        void AppendHead();
        void AppendFootPrint();

        void Close();

        std::string GetDate() const;

        /* === Members === */
        
        std::ofstream Stream_;

        std::string Head_, FootPrint_;
        
        std::string IndentModifier_;
        std::string Indent_;

};


#endif



// ================================================================================