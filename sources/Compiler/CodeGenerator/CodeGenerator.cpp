/*
 * Code generator file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "CodeGenerator.h"
#include "ConsoleOutput.h"
#include "CompilerOptions.h"
#include "StringMod.h"
#include "ErrorReporter.h"

#include <ctime>
#include <chrono>


const std::string CodeGenerator::IdentPrefix = "__XX__";

CodeGenerator::CodeGenerator() :
    Visitor         (                   ),
    IndentModifier_ (std::string(4, ' '))
{
}
CodeGenerator::~CodeGenerator()
{
    Close();
}

void CodeGenerator::SetupIndentation(const std::string& IndentModifier)
{
    IndentModifier_ = IndentModifier;
}


/* 
 * ======= Protected: =======
 */

bool CodeGenerator::GenerateFile(const std::string& Filename)
{
    /* Close previous file */
    Close();

    /* Open new file */
    Stream_.open(Filename, std::ofstream::out);

    if (Stream_.good())
    {
        ConsoleOutput::Message("Generate output source file \"" + xxFilename(Filename) + "\"");

        /* Append head after opening file */
        UpdateHeadAndFootPrint(Filename);
        AppendHead();
    }
    else
    {
        /* Return with error message */
        ConsoleOutput::Error("Could not create file \"" + Filename + "\"");
        return false;
    }

    return true;
}

void CodeGenerator::Warning(const std::string &Message)
{
    ErrorReporter::Instance.Push(CompilerWarning(Message));
}

void CodeGenerator::Error(const std::string &Message)
{
    ErrorReporter::Instance.Push(CodeGenError(Message));
}

void CodeGenerator::NL(const std::string &Line)
{
    StartNL();
    Append(Line);
    EndNL();
}

void CodeGenerator::NL()
{
    EndNL();
}

void CodeGenerator::StartNL()
{
    /* Append indentation string */
    if (CompilerOptions::Settings.Indentation && !Indent_.empty())
        Stream_ << Indent_.c_str();
}

void CodeGenerator::Append(const std::string &Str, bool IsBloated)
{
    if (IsBloated && !CompilerOptions::Settings.BloatExpressions)
    {
        /* Remove white spaces */
        std::string NewStr = xxRemoveWhiteSpaces(Str);

        /* Append new line string */
        Stream_ << NewStr.c_str();
    }
    else
    {
        /* Append new line string */
        Stream_ << Str.c_str();
    }
}

void CodeGenerator::EndNL()
{
    /* Complete the line with a new-line character ('\n') */
    Stream_ << std::endl;
}

void CodeGenerator::Blank()
{
    if (CompilerOptions::Settings.AllowBlanks)
    {
        StartNL();
        EndNL();
    }
}

void CodeGenerator::UpperIndent()
{
    Indent_ += IndentModifier_;
}

void CodeGenerator::LowerIndent()
{
    if (Indent_.size() > IndentModifier_.size())
        Indent_.resize(Indent_.size() - IndentModifier_.size());
    else
        Indent_.clear();
}

void CodeGenerator::Bloat()
{
    if (CompilerOptions::Settings.BloatExpressions)
        Append(" ");
}


/*
 * ======= Private: =======
 */

void CodeGenerator::UpdateHeadAndFootPrint(const std::string &Filename)
{
    const std::string CommentStr = GetLineCommentStr();

    Head_ = (
        CommentStr + " " + Filename + "\n" +
        CommentStr + " XieXie generated source file\n" +
        CommentStr + " " + GetDate()
    );

    FootPrint_ = CommentStr + " " + std::string(16, '=');
}

void CodeGenerator::AppendHead()
{
    if (!Head_.empty())
        NL(Head_);
}

void CodeGenerator::AppendFootPrint()
{
    if (!FootPrint_.empty())
        NL(FootPrint_);
}

void CodeGenerator::Close()
{
    if (Stream_.is_open())
    {
        /* Append foot print before closing file */
        AppendFootPrint();
        Stream_.close();
    }
}

std::string CodeGenerator::GetDate() const
{
    /* Get current time as string */
    std::time_t CurrTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    return std::ctime(&CurrTime);
}



// ================================================================================