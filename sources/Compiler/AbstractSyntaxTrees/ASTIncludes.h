/*
 * AST includes header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_INCLUDES_H__
#define __XX_AST_INCLUDES_H__


#include "Program.h"
#include "ParameterList.h"
#include "ArgumentList.h"
#include "TypeDenoter.h"
#include "EnumEntry.h"

#include "AssertCommand.h"
#include "ImportCommand.h"
#include "PackageCommand.h"
#include "BlockCommand.h"
#include "AssignCommand.h"
#include "CallCommand.h"
#include "FunctionDefCommand.h"
#include "VariableDefCommand.h"
#include "TypeDefCommand.h"
#include "ReturnCommand.h"
#include "IfCommand.h"
#include "InfiniteLoopCommand.h"
#include "WhileLoopCommand.h"
#include "RangeForLoopCommand.h"
#include "InlineLangCommand.h"
#include "PrimitiveCommand.h"
#include "EnumDeclCommand.h"
#include "FlagsDeclCommand.h"
#include "ClassDeclCommand.h"
#include "InitCommand.h"
#include "ReleaseCommand.h"
#include "TryCatchCommand.h"
#include "ThrowCommand.h"
#include "SwitchCommand.h"

#include "BracketExpression.h"
#include "BinaryExpression.h"
#include "UnaryExpression.h"
#include "StringSumExpression.h"
#include "AssignExpression.h"
#include "LiteralExpression.h"
#include "CallExpression.h"
#include "CastExpression.h"
#include "ObjectExpression.h"
#include "AllocExpression.h"
#include "CopyExpression.h"
#include "ValidationExpression.h"
#include "InitListExpression.h"
#include "LambdaExpression.h"

#include "ClassObject.h"
#include "EnumObject.h"
#include "FlagsObject.h"
#include "FunctionObject.h"
#include "TypeObject.h"
#include "VariableObject.h"
#include "PatternObject.h"

#include "AssignOperator.h"
#include "BinaryOperator.h"
#include "UnaryOperator.h"

#include "ArgumentList.h"
#include "AttributeList.h"
#include "TemplateArgumentList.h"
#include "TemplateParameterList.h"
#include "InitializerList.h"
#include "ArrayIndexList.h"

#include "PointerLiteral.h"
#include "BoolLiteral.h"
#include "IntegerLiteral.h"
#include "FloatLiteral.h"
#include "StringLiteral.h"

#include "BoolTypeDenoter.h"
#include "VoidTypeDenoter.h"
#include "ConstTypeDenoter.h"
#include "AutoTypeDenoter.h"
#include "IntTypeDenoter.h"
#include "FloatTypeDenoter.h"
#include "StringTypeDenoter.h"
#include "CustomTypeDenoter.h"
#include "RawPtrTypeDenoter.h"
#include "ManagedPtrTypeDenoter.h"
#include "ReferenceTypeDenoter.h"
#include "ProcTypeDenoter.h"
#include "ArrayTypeDenoter.h"


#endif



// ================================================================================