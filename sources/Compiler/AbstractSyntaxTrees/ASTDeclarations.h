/*
 * AST pointer declarations header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_PTRDECL_H__
#define __XX_AST_PTRDECL_H__


#include <memory>


namespace AbstractSyntaxTrees
{


#define DeclPtr(n)                      \
    class n;                            \
    typedef std::shared_ptr<n> n##Ptr;


DeclPtr(Visitor             )
DeclPtr(AST                 )

DeclPtr(Command             )
DeclPtr(AssertCommand       )
DeclPtr(AssignCommand       )
DeclPtr(BlockCommand        )
DeclPtr(CallCommand         )
DeclPtr(FunctionDefCommand  )
DeclPtr(ImportCommand       )
DeclPtr(PackageCommand      )
DeclPtr(ReturnCommand       )
DeclPtr(VariableDefCommand  )
DeclPtr(TypeDefCommand      )
DeclPtr(IfCommand           )
DeclPtr(InfiniteLoopCommand )
DeclPtr(WhileLoopCommand    )
DeclPtr(RangeForLoopCommand )
DeclPtr(InlineLangCommand   )
DeclPtr(PrimitiveCommand    )
DeclPtr(EnumDeclCommand     )
DeclPtr(FlagsDeclCommand    )
DeclPtr(ClassDeclCommand    )
DeclPtr(ClassBlockCommand   )
DeclPtr(InitCommand         )
DeclPtr(ReleaseCommand      )
DeclPtr(TryCatchCommand     )
DeclPtr(ThrowCommand        )
DeclPtr(SwitchCommand       )

DeclPtr(Expression          )
DeclPtr(BracketExpression   )
DeclPtr(BinaryExpression    )
DeclPtr(UnaryExpression     )
DeclPtr(StringSumExpression )
DeclPtr(AssignExpression    )
DeclPtr(LiteralExpression   )
DeclPtr(CallExpression      )
DeclPtr(CastExpression      )
DeclPtr(ObjectExpression    )
DeclPtr(AllocExpression     )
DeclPtr(CopyExpression      )
DeclPtr(ValidationExpression)
DeclPtr(InitListExpression  )
DeclPtr(LambdaExpression    )

DeclPtr(TypeDenoter         )
DeclPtr(VoidTypeDenoter     )
DeclPtr(ConstTypeDenoter    )
DeclPtr(AutoTypeDenoter     )
DeclPtr(BoolTypeDenoter     )
DeclPtr(IntTypeDenoter      )
DeclPtr(FloatTypeDenoter    )
DeclPtr(StringTypeDenoter   )
DeclPtr(CustomTypeDenoter   )
DeclPtr(RawPtrTypeDenoter   )
DeclPtr(MngPtrTypeDenoter   )
DeclPtr(RefTypeDenoter      )
DeclPtr(ProcTypeDenoter     )
DeclPtr(ArrayTypeDenoter    )

DeclPtr(ArgumentList        )
DeclPtr(ParameterList       )
DeclPtr(TemplateParamList   )
DeclPtr(TemplateArgList     )
DeclPtr(InitializerList     )
DeclPtr(AttributeList       )
DeclPtr(ArrayIndexList      )

DeclPtr(Literal             )
DeclPtr(BoolLiteral         )
DeclPtr(PointerLiteral      )
DeclPtr(IntegerLiteral      )
DeclPtr(FloatLiteral        )
DeclPtr(StringLiteral       )

DeclPtr(Object              )
DeclPtr(ClassObject         )
DeclPtr(EnumObject          )
DeclPtr(FlagsObject         )
DeclPtr(FunctionObject      )
DeclPtr(TypeObject          )
DeclPtr(VariableObject      )
DeclPtr(PatternObject       )

DeclPtr(Operator            )
DeclPtr(AssignOperator      )
DeclPtr(BinaryOperator      )
DeclPtr(UnaryOperator       )

DeclPtr(Identifier          )
DeclPtr(FNameIdent          )
DeclPtr(Program             )
DeclPtr(Terminal            )
DeclPtr(EnumEntry           )
DeclPtr(CaseBlock           )


#undef DeclPtr


} // /namespace AbstractSyntaxTrees


#endif



// ================================================================================