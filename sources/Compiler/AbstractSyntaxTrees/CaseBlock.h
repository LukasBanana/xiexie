/*
 * Case command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_CASEBLOCK_H__
#define __XX_AST_CASEBLOCK_H__


#include "Command.h"
#include "Expression.h"

#include <list>


namespace AbstractSyntaxTrees
{


class CaseBlock : public AST
{
    
    public:
        
        CaseBlock(const SourcePosition &Pos) :
            AST         (Pos    ),
            SkipBreak   (false  )
        {
        }
        ~CaseBlock()
        {
        }

        DefineASTVisitProc(CaseBlock)

        void PrintAST()
        {
            if (Expr != nullptr)
                Deb("Case Block");
            else
                Deb("Default Block");

            ScopedIndent Unused;

            if (Expr != nullptr)
                Expr->PrintAST();

            for (CommandPtr Cmd : Commands)
                Cmd->PrintAST();
        }

        CaseBlockPtr Copy() const
        {
            auto CopyObj = std::make_shared<CaseBlock>(Pos());

            CopyObj->Expr = Expr->Copy();
            CopyObj->SkipBreak = SkipBreak;

            for (auto Cmd : CopyObj->Commands)
                CopyObj->Commands.push_back(Cmd->Copy());

            return CopyObj;
        }

        ExpressionPtr Expr;
        std::list<CommandPtr> Commands;

        bool SkipBreak;                 //!< Information for decorated AST.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================