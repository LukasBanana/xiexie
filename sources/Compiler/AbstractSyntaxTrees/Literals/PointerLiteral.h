/*
 * Pointer literal AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LITERAL_POINTER_H__
#define __XX_AST_LITERAL_POINTER_H__


#include "Literal.h"


namespace AbstractSyntaxTrees
{


class PointerLiteral : public Literal
{
    
    public:
        
        PointerLiteral(TokenPtr Tkn) :
            Literal(Literal::Types::Pointer, Tkn)
        {
        }
        PointerLiteral(const SourcePosition& Pos, const std::string& Spelling) :
            Literal(Literal::Types::Pointer, Pos, Spelling)
        {
        }
        ~PointerLiteral()
        {
        }

        DefineASTVisitProc(PointerLiteral)

        void PrintAST()
        {
            Deb("Pointer Literal \"" + Spell + "\"");
        }

        LiteralPtr Copy() const
        {
            return std::make_shared<PointerLiteral>(Pos(), Spell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================