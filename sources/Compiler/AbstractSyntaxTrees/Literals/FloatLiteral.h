/*
 * Float literal AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LITERAL_FLOAT_H__
#define __XX_AST_LITERAL_FLOAT_H__


#include "Literal.h"


namespace AbstractSyntaxTrees
{


class FloatLiteral : public Literal
{
    
    public:
        
        FloatLiteral(TokenPtr Tkn) :
            Literal(Literal::Types::Float, Tkn)
        {
        }
        FloatLiteral(const SourcePosition& Pos, const std::string& Spell) :
            Literal(Literal::Types::Float, Pos, Spell)
        {
        }
        ~FloatLiteral()
        {
        }

        DefineASTVisitProc(FloatLiteral)

        void PrintAST()
        {
            Deb("Float Literal \"" + Spell + "\"");
        }

        LiteralPtr Copy() const
        {
            return std::make_shared<FloatLiteral>(Pos(), Spell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================