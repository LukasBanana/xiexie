/*
 * String literal AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LITERAL_STRING_H__
#define __XX_AST_LITERAL_STRING_H__


#include "Literal.h"


namespace AbstractSyntaxTrees
{


class StringLiteral : public Literal
{
    
    public:
        
        StringLiteral(TokenPtr Tkn) :
            Literal(Literal::Types::String, Tkn)
        {
        }
        StringLiteral(const SourcePosition& Pos, const std::string& Spell) :
            Literal(Literal::Types::String, Pos, Spell)
        {
        }
        ~StringLiteral()
        {
        }

        DefineASTVisitProc(StringLiteral)

        void PrintAST()
        {
            Deb("String Literal \"" + Spell + "\"");
        }

        LiteralPtr Copy() const
        {
            return std::make_shared<StringLiteral>(Pos(), Spell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================