/*
 * Boolean literal AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LITERAL_BOOL_H__
#define __XX_AST_LITERAL_BOOL_H__


#include "Literal.h"


namespace AbstractSyntaxTrees
{


class BoolLiteral : public Literal
{
    
    public:
        
        BoolLiteral(TokenPtr Tkn) :
            Literal(Literal::Types::Boolean, Tkn)
        {
        }
        BoolLiteral(const SourcePosition& Pos, const std::string& Spell) :
            Literal(Literal::Types::Boolean, Pos, Spell)
        {
        }
        ~BoolLiteral()
        {
        }

        DefineASTVisitProc(BoolLiteral)

        void PrintAST()
        {
            Deb("Boolean Literal \"" + Spell + "\"");
        }

        LiteralPtr Copy() const
        {
            return std::make_shared<BoolLiteral>(Pos(), Spell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================