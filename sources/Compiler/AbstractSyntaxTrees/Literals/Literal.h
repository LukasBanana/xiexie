/*
 * Literal AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LITERAL_H__
#define __XX_AST_LITERAL_H__


#include "Terminal.h"
#include "Token.h"


namespace AbstractSyntaxTrees
{


class Literal : public Terminal
{
    
    public:
        
        enum class Types
        {
            Boolean,    //!< true, false
            Pointer,    //!< null, this
            Integer,    //!< number
            Float,      //!< number.number
            String,     //!< "string"
        };

        virtual ~Literal()
        {
        }

        inline Types Type() const
        {
            return Type_;
        }

        virtual void PrintAST()
        {
            switch (Type())
            {
                case Types::Boolean:
                    Deb("Boolean Literal \"" + Spell + "\"");
                    break;
                case Types::Pointer:
                    Deb("Pointer Literal \"" + Spell + "\"");
                    break;
                case Types::Integer:
                    Deb("Integer Literal \"" + Spell + "\"");
                    break;
                case Types::Float:
                    Deb("Float Literal \"" + Spell + "\"");
                    break;
                case Types::String:
                    Deb("String Literal \"" + Spell + "\"");
                    break;
            }
        }

        virtual LiteralPtr Copy() const = 0;

    protected:

        Literal(const Types& Type, TokenPtr Tkn) :
            Terminal(Tkn->Pos(), Tkn->Spell()   ),
            Type_   (Type                       )
        {
        }
        Literal(const Types& Type, const SourcePosition& Pos, const std::string& Spell) :
            Terminal(Pos, Spell ),
            Type_   (Type       )
        {
        }

    private:
        
        Types Type_;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================