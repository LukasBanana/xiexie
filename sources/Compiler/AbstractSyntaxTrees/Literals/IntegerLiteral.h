/*
 * Integer literal AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LITERAL_INTEGER_H__
#define __XX_AST_LITERAL_INTEGER_H__


#include "Literal.h"


namespace AbstractSyntaxTrees
{


class IntegerLiteral : public Literal
{
    
    public:
        
        IntegerLiteral(TokenPtr Tkn) :
            Literal(Literal::Types::Integer, Tkn)
        {
        }
        IntegerLiteral(const SourcePosition& Pos, const std::string& Spell) :
            Literal(Literal::Types::Integer, Pos, Spell)
        {
        }
        ~IntegerLiteral()
        {
        }

        DefineASTVisitProc(IntegerLiteral)

        void PrintAST()
        {
            Deb("Integer Literal \"" + Spell + "\"");
        }

        LiteralPtr Copy() const
        {
            return std::make_shared<IntegerLiteral>(Pos(), Spell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================