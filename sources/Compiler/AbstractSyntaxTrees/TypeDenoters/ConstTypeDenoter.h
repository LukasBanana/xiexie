/*
 * Constant type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_CONST_H__
#define __XX_AST_TYPEDENOTER_CONST_H__


#include "TypeDenoter.h"


namespace AbstractSyntaxTrees
{


class ConstTypeDenoter : public TypeDenoter
{
    
    public:
        
        ConstTypeDenoter(TokenPtr TypeToken, TypeDenoterPtr TDenoterAST) :
            TypeDenoter (TypeDenoter::Types::ConstType, TypeToken   ),
            TDenoter    (TDenoterAST                                )
        {
        }
        ConstTypeDenoter(const SourcePosition &Pos, const std::string &Spelling, TypeDenoterPtr TDenoterAST) :
            TypeDenoter (TypeDenoter::Types::ConstType, Pos, Spelling   ),
            TDenoter    (TDenoterAST                                    )
        {
        }
        ~ConstTypeDenoter()
        {
        }

        DefineASTVisitProc(ConstTypeDenoter)

        virtual void PrintAST()
        {
            Deb("Type Denoter \"constant\"");
            ScopedIndent Unused;
            TDenoter->PrintAST();
        }

        std::string GetDefaultValue() const
        {
            return TDenoter->GetDefaultValue();
        }

        int GetTypeSize() const
        {
            return TDenoter->GetTypeSize();
        }

        TypeDenoter* GetResolvedType()
        {
            return TDenoter->GetResolvedType();
        }
        const TypeDenoter* GetResolvedType() const
        {
            return TDenoter->GetResolvedType();
        }

        TypeDenoter* GetNonConstType()
        {
            return TDenoter->GetNonConstType();
        }
        const TypeDenoter* GetNonConstType() const
        {
            return TDenoter->GetNonConstType();
        }

        const ProcTypeDenoter* GetResolvedProcType() const
        {
            return TDenoter->GetResolvedProcType();
        }

        size_t CountPointerRefs() const
        {
            return TDenoter->CountPointerRefs();
        }

        bool IsExact() const
        {
            return TDenoter->IsExact();
        }

        TypeDenoterPtr Copy() const
        {
            return std::make_shared<ConstTypeDenoter>(Pos(), Spell, TDenoter->Copy());
        }

        static ConstTypeDenoterPtr Create(const TypeDenoterPtr& TDenoterAST)
        {
            return std::make_shared<ConstTypeDenoter>(SourcePosition::Ignore, "const", TDenoterAST);
        }

        TypeDenoterPtr TDenoter;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================