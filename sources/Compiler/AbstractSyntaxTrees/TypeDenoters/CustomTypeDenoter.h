/*
 * Custom type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_CUSTOM_H__
#define __XX_AST_TYPEDENOTER_CUSTOM_H__


#include "TypeDenoter.h"
#include "FNameIdent.h"
#include "Object.h"
#include "TypeObject.h"


namespace AbstractSyntaxTrees
{


class CustomTypeDenoter : public TypeDenoter
{
    
    public:
        
        CustomTypeDenoter(FNameIdentPtr FNameAST, ObjectPtr ObjLink = nullptr) :
            TypeDenoter (TypeDenoter::Types::CustomType, FNameAST->Pos(), "<custom>"),
            FName       (FNameAST                                                   ),
            Link        (ObjLink                                                    )
        {
        }
        ~CustomTypeDenoter()
        {
        }

        DefineASTVisitProc(CustomTypeDenoter)

        void PrintAST()
        {
            Deb("Custom Type Denoter");// \"" + FName->FullName() + "\"");
            ScopedIndent Unused;

            FName->PrintAST();

            if (Link != nullptr)
            {
                /* Don't print the object AST (because of recursion) */
                Deb("Def. at " + Link->Pos().GetString());
            }
        }

        bool Compare(const TypeDenoter* Other) const
        {
            if (Other == nullptr || Type() != Other->Type())
                return false;

            /* Get other type as custom type denoter */
            auto OtherCT = dynamic_cast<const CustomTypeDenoter*>(Other);
            if (OtherCT == nullptr)
                return false;

            /* Compare their linked objects */
            if (Link == nullptr || OtherCT->Link == nullptr || Link->Type() != OtherCT->Link->Type())
                return false;

            /* Check for exceptions */
            //...

            /* Compare two type objects */
            return Link == OtherCT->Link;
        }

        std::string GetDefaultValue() const
        {
            return Link != nullptr ? Link->GetDefaultValue() : "";
        }

        const ProcTypeDenoter* GetResolvedProcType() const
        {
            if (Link != nullptr && Link->Type() == Object::Types::TypeObj)
            {
                /* Get resolved procedure type denoter from type-definition */
                auto TypeObj = std::dynamic_pointer_cast<TypeObject>(Link);
                return TypeObj->TDenoter->GetResolvedProcType();
            }
            return nullptr;
        }

        size_t CountPointerRefs() const;

        bool DecorateSub(FNameIdentPtr FName)
        {
            /* Get type-denoter's linked object */
            if (Link == nullptr)
                throw std::string("Unresolved custom type denoter \"" + Spell + "\"");

            /* Decorate sub-member */
            return Link->DecorateSub(FName);
        }

        ObjectPtr FindSub(FNameIdentPtr FName)
        {
            return Link != nullptr ? Link->FindSub(FName) : nullptr;
        }

        TypeDenoterPtr Copy() const
        {
            auto Obj = std::make_shared<CustomTypeDenoter>(FName->Copy());
            if (Link != nullptr)
                Obj->Link = Link->Copy();
            return Obj;
        }

        FNameIdentPtr FName;

        ObjectPtr Link;         //!< Information for decorated AST.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================