/*
 * Procedure type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_PROCEDURE_H__
#define __XX_AST_TYPEDENOTER_PROCEDURE_H__


#include "TypeDenoter.h"
#include "CustomTypeDenoter.h"
#include "ParameterList.h"
#include "FunctionObject.h"


namespace AbstractSyntaxTrees
{


class ProcTypeDenoter : public TypeDenoter
{
    
    public:
        
        struct Flags
        {
            typedef unsigned char DataType;
            static const DataType LambdaHeader = (1 << 0); //!< This proc-type-denoter is used as header for a lambda expression.
        };
        
        ProcTypeDenoter(TypeDenoterPtr TDenoterAST, ParameterListPtr ParamListAST = nullptr) :
            TypeDenoter (TypeDenoter::Types::ProcType, TDenoterAST->Pos(), "<proc>" ),
            TDenoter    (TDenoterAST                                                ),
            ParamList   (ParamListAST                                               ),
            DefFlags    (0                                                          )
        {
        }
        ~ProcTypeDenoter()
        {
        }

        DefineASTVisitProc(ProcTypeDenoter)

        void PrintAST()
        {
            Deb("Procedure Type Denoter");
            ScopedIndent Unused;

            TDenoter->PrintAST();
            if (ParamList != nullptr)
                ParamList->PrintAST();
        }

        bool Compare(const TypeDenoter* Other) const
        {
            /* Get other custom type denoter */
            if (Other == nullptr || Other->Type() != TypeDenoter::Types::CustomType)
                return false;

            auto CTDenoter = dynamic_cast<const CustomTypeDenoter*>(Other);

            /* Get function object from type denoter */
            if (CTDenoter->Link->Type() != Object::Types::FuncObj)
                return false;

            auto ProcObj = std::dynamic_pointer_cast<FunctionObject>(CTDenoter->Link);

            /* Compare function argument list with this argument list */
            try
            {
                ValidProc(ProcObj.get());
            }
            catch (const std::string& /*Err*/)
            {
                //TODO -> make use of this error message!!!
                return false;
            }

            return true;
        }

        /**
        Checks if the specified object is a valid function for this procedure type denoter.
        \throws std::string
        */
        void ValidProc(const FunctionObject* Proc) const
        {
            if (Proc == nullptr)
                throw std::string("Invalid object for procedure type denoter comparision");

            /* Check if return argument fits to this procedure type denoter */
            if (!TDenoter->Compare(Proc->TDenoter.get()))
                throw std::string("Invalid return type for procedure type denoter");

            /* Check if all arguments fit to this procedure type denoter */
            bool HasArgs0 = (ParamList != nullptr);
            bool HasArgs1 = (Proc->ParamList != nullptr);

            if ( HasArgs0 != HasArgs1 || ( HasArgs0 && !ParamList->Compare(Proc->ParamList.get()) ) )
                throw std::string("Invalid argument lists for procedure type denoter");
        }

        //! Returns this type denoter object since this is already a procedure type denoter.
        const ProcTypeDenoter* GetResolvedProcType() const
        {
            return this;
        }

        bool IsExact() const
        {
            return true;
        }

        TypeDenoterPtr Copy() const
        {
            auto CopyObj = std::make_shared<ProcTypeDenoter>(TDenoter->Copy());

            CopyObj->DefFlags = DefFlags;

            if (ParamList != nullptr)
                CopyObj->ParamList = ParamList->Copy();

            return CopyObj;
        }

        TypeDenoterPtr TDenoter;    //!< Return type denoter.
        ParameterListPtr ParamList; //!< Paramter list (optional).

        Flags::DataType DefFlags;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================