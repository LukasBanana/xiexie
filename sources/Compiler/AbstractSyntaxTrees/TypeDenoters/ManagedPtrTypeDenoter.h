/*
 * Managed-pointer type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_MANAGED_POINTER_H__
#define __XX_AST_TYPEDENOTER_MANAGED_POINTER_H__


#include "TypeDenoter.h"


namespace AbstractSyntaxTrees
{


class MngPtrTypeDenoter : public TypeDenoter
{
    
    public:
        
        MngPtrTypeDenoter(TokenPtr TypeToken, TypeDenoterPtr TDenoterAST = nullptr) :
            TypeDenoter (TypeDenoter::Types::MngPtrType, TypeToken  ),
            TDenoter    (TDenoterAST                                )
        {
        }
        MngPtrTypeDenoter(const SourcePosition &Pos, const std::string &Spelling, TypeDenoterPtr TDenoterAST = nullptr) :
            TypeDenoter (TypeDenoter::Types::MngPtrType, Pos, Spelling  ),
            TDenoter    (TDenoterAST                                    )
        {
        }
        ~MngPtrTypeDenoter()
        {
        }

        DefineASTVisitProc(MngPtrTypeDenoter)

        void PrintAST()
        {
            Deb("Type Denoter \"managed-pointer\"");
            ScopedIndent Unused;

            if (TDenoter != nullptr)
                TDenoter->PrintAST();
        }

        std::string GetDefaultValue() const
        {
            return "nullptr";
        }

        TypeDenoter* GetResolvedType()
        {
            return TDenoter != nullptr ? TDenoter->GetResolvedType() : this;
        }
        const TypeDenoter* GetResolvedType() const
        {
            return TDenoter != nullptr ? TDenoter->GetResolvedType() : this;
        }

        size_t CountPointerRefs() const
        {
            return TDenoter != nullptr ? TDenoter->CountPointerRefs() + 1 : 1;
        }

        TypeDenoterPtr Copy() const
        {
            auto CopyObj = std::make_shared<MngPtrTypeDenoter>(Pos(), Spell);
            
            if (TDenoter != nullptr)
                CopyObj->TDenoter = TDenoter->Copy();

            return CopyObj;
        }

        TypeDenoterPtr TDenoter; // This type denoter may be optional (for the null-pointer literal)!

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================