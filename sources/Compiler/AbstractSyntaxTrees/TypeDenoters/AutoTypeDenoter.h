/*
 * Auto type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_AUTO_H__
#define __XX_AST_TYPEDENOTER_AUTO_H__


#include "TypeDenoter.h"
#include "FNameIdent.h"


namespace AbstractSyntaxTrees
{


class AutoTypeDenoter : public TypeDenoter
{
    
    public:
        
        AutoTypeDenoter(const SourcePosition& Pos) :
            TypeDenoter(TypeDenoter::Types::AutoType, Pos, "<auto>")
        {
        }
        ~AutoTypeDenoter()
        {
        }

        DefineASTVisitProc(AutoTypeDenoter)

        void PrintAST()
        {
            Deb("Auto Type Denoter");
            if (Link != nullptr)
            {
                ScopedIndent Unused;
                Link->PrintAST();
            }
        }

        size_t CountPointerRefs() const
        {
            return Link != nullptr ? Link->CountPointerRefs() : 0;
        }

        bool IsExact() const
        {
            return Link != nullptr ? Link->IsExact() : false;
        }

        TypeDenoterPtr Copy() const
        {
            auto Obj = std::make_shared<AutoTypeDenoter>(Pos());
            if (Link != nullptr)
                Obj->Link = Link->Copy();
            return Obj;
        }

        TypeDenoterPtr Link;    //!< Information for decorated AST.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================