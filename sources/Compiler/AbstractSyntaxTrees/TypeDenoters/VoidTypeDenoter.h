/*
 * Void type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_VOID_H__
#define __XX_AST_TYPEDENOTER_VOID_H__


#include "TypeDenoter.h"


namespace AbstractSyntaxTrees
{


class VoidTypeDenoter : public TypeDenoter
{
    
    public:
        
        VoidTypeDenoter(TokenPtr TypeToken) :
            TypeDenoter(TypeDenoter::Types::VoidType, TypeToken)
        {
        }
        VoidTypeDenoter(const SourcePosition &Pos, const std::string &Spelling) :
            TypeDenoter(TypeDenoter::Types::VoidType, Pos, Spelling)
        {
        }
        ~VoidTypeDenoter()
        {
        }

        DefineASTVisitProc(VoidTypeDenoter)

        TypeDenoterPtr Copy() const
        {
            return std::make_shared<VoidTypeDenoter>(Pos(), Spell);
        }

        static VoidTypeDenoterPtr Create()
        {
            return std::make_shared<VoidTypeDenoter>(SourcePosition::Ignore, "void");
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================