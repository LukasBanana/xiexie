/*
 * Raw-pointer type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_RAW_POINTER_H__
#define __XX_AST_TYPEDENOTER_RAW_POINTER_H__


#include "TypeDenoter.h"


namespace AbstractSyntaxTrees
{


class RawPtrTypeDenoter : public TypeDenoter
{
    
    public:
        
        RawPtrTypeDenoter(TokenPtr TypeToken, TypeDenoterPtr TDenoterAST) :
            TypeDenoter (TypeDenoter::Types::RawPtrType, TypeToken  ),
            TDenoter    (TDenoterAST                                )
        {
        }
        RawPtrTypeDenoter(const SourcePosition &Pos, const std::string &Spelling, TypeDenoterPtr TDenoterAST) :
            TypeDenoter (TypeDenoter::Types::RawPtrType, Pos, Spelling  ),
            TDenoter    (TDenoterAST                                    )
        {
        }
        ~RawPtrTypeDenoter()
        {
        }

        DefineASTVisitProc(RawPtrTypeDenoter)

        void PrintAST()
        {
            Deb("Type Denoter \"raw-pointer\"");
            ScopedIndent Unused;
            TDenoter->PrintAST();
        }

        std::string GetDefaultValue() const
        {
            return "nullptr";
        }

        int GetTypeSize() const
        {
            /* Size of pointer */
            return 4;
        }

        TypeDenoter* GetResolvedType()
        {
            return TDenoter->GetResolvedType();
        }
        const TypeDenoter* GetResolvedType() const
        {
            return TDenoter->GetResolvedType();
        }

        size_t CountPointerRefs() const
        {
            return TDenoter->CountPointerRefs() + 1;
        }

        TypeDenoterPtr Copy() const
        {
            return std::make_shared<RawPtrTypeDenoter>(Pos(), Spell, TDenoter->Copy());
        }

        static RawPtrTypeDenoterPtr Create(const TypeDenoterPtr& TDenoterAST)
        {
            return std::make_shared<RawPtrTypeDenoter>(SourcePosition::Ignore, "*", TDenoterAST);
        }

        TypeDenoterPtr TDenoter;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================