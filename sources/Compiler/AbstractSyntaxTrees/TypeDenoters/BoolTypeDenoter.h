/*
 * Boolean type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_BOOLEAN_H__
#define __XX_AST_TYPEDENOTER_BOOLEAN_H__


#include "TypeDenoter.h"


namespace AbstractSyntaxTrees
{


class BoolTypeDenoter : public TypeDenoter
{
    
    public:
        
        BoolTypeDenoter(TokenPtr TypeToken) :
            TypeDenoter(TypeDenoter::Types::BoolType, TypeToken)
        {
        }
        BoolTypeDenoter(const SourcePosition &Pos, const std::string &Spelling) :
            TypeDenoter(TypeDenoter::Types::BoolType, Pos, Spelling)
        {
        }
        ~BoolTypeDenoter()
        {
        }

        DefineASTVisitProc(BoolTypeDenoter)

        std::string GetDefaultValue() const
        {
            return "false";
        }

        int GetTypeSize() const
        {
            return 1;
        }

        bool IsExact() const
        {
            return true;
        }

        TypeDenoterPtr Copy() const
        {
            return std::make_shared<BoolTypeDenoter>(Pos(), Spell);
        }

        static BoolTypeDenoterPtr Create()
        {
            return std::make_shared<BoolTypeDenoter>(SourcePosition::Ignore, "bool");
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================