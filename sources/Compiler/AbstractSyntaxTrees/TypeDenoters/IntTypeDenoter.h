/*
 * Integer type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_INTEGER_H__
#define __XX_AST_TYPEDENOTER_INTEGER_H__


#include "TypeDenoter.h"


namespace AbstractSyntaxTrees
{


class IntTypeDenoter : public TypeDenoter
{
    
    public:
        
        IntTypeDenoter(TokenPtr TypeToken) :
            TypeDenoter(TypeDenoter::Types::IntType, TypeToken)
        {
        }
        IntTypeDenoter(const SourcePosition &Pos, const std::string &Spelling) :
            TypeDenoter(TypeDenoter::Types::IntType, Pos, Spelling)
        {
        }
        ~IntTypeDenoter()
        {
        }

        DefineASTVisitProc(IntTypeDenoter)

        std::string GetDefaultValue() const
        {
            return !Spell.empty() && Spell[0] == 'u' ? "0u" : "0";
        }

        int GetTypeSize() const
        {
            if (Spell == "byte" || Spell == "ubyte")
                return 1;
            if (Spell == "short" || Spell == "ushort")
                return 2;
            if (Spell == "int" || Spell == "uint")
                return 4;
            if (Spell == "dint" || Spell == "udint")
                return 8;
            return 0;
        }

        bool IsExact() const
        {
            return true;
        }

        TypeDenoterPtr Copy() const
        {
            return std::make_shared<IntTypeDenoter>(Pos(), Spell);
        }

        static IntTypeDenoterPtr Create(const std::string& Spell = "int")
        {
            return std::make_shared<IntTypeDenoter>(SourcePosition::Ignore, Spell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================