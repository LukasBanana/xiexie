/*
 * Type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_H__
#define __XX_AST_TYPEDENOTER_H__


#include "Terminal.h"
#include "Token.h"


namespace AbstractSyntaxTrees
{


class ProcTypeDenoter;

class TypeDenoter : public Terminal
{
    
    public:
        
        enum class Types
        {
            // Meta types
            VoidType,       //!< Void type (can only be used for pointers or function return types).
            ConstType,      //!< Constant type.
            AutoType,       //!< Automatic deduced type.
            ArrayType,      //!< Array dimension type.

            // Pointer types
            RefType,        //!< Reference type (e.g. "int& x").
            RawPtrType,     //!< Raw pointer type (e.g. "int* x").
            MngPtrType,     //!< Managed pointer type (e.g. "int@ x").

            // Primitive types
            BoolType,       //!< Boolean type.
            IntType,        //!< Integer type.
            FloatType,      //!< Floating-point type.

            // String types
            StringType,     //!< String type.

            // Special types
            CustomType,     //!< Custom type (class or type-definition).
            ProcType,       //!< Procedure type (for function pointers).
        };

        virtual ~TypeDenoter()
        {
        }

        inline Types Type() const
        {
            return Type_;
        }

        virtual void PrintAST()
        {
            Deb("Type Denoter \"" + Spell + "\"");
        }

        virtual std::string GetDefaultValue() const
        {
            return "";
        }

        //! Returns true if this type-denoter is equal to the other type-denoter.
        virtual bool Compare(const TypeDenoter* Other) const
        {
            return
                Other != nullptr &&
                GetNonRefType()->GetNonConstType()->Type() == Other->GetNonRefType()->GetNonConstType()->Type();
        }

        /**
        Returns the size (in bytes) of this type denoter,
        -1 if it has dynamic size (e.g. strings) or 0 if it's unknown.
        */
        virtual int GetTypeSize() const
        {
            return 0;
        }
        //! Returns the array length. If this type denoter is not an array, the return value is 1.
        virtual int GetArrayLength() const
        {
            return 1;
        }

        //! Returns this type as non-constant-, non-pointer- and non-referenced type.
        virtual TypeDenoter* GetResolvedType()
        {
            return this;
        }
        //! Returns this type as non-constant and non-pointer type.
        virtual const TypeDenoter* GetResolvedType() const
        {
            return this;
        }

        //! Returns this type as non-constant type.
        virtual TypeDenoter* GetNonConstType()
        {
            return this;
        }
        //! Returns this type as non-constant type.
        virtual const TypeDenoter* GetNonConstType() const
        {
            return this;
        }

        //! Returns this type as non-reference type.
        virtual TypeDenoter* GetNonRefType()
        {
            return this;
        }
        //! Returns this type as non-reference type.
        virtual const TypeDenoter* GetNonRefType() const
        {
            return this;
        }

        //! Returns the resolved procedure type denoter or nullptr if there it can not be resolved.
        virtual const ProcTypeDenoter* GetResolvedProcType() const
        {
            return nullptr;
        }

        //! Returns the number of pointer references.
        virtual size_t CountPointerRefs() const
        {
            return 0;
        }

        /**
        Returns true if this type denoter has an exact representation,
        e.g. integers have an exact representation but floats have an approximative representation.
        */
        virtual bool IsExact() const
        {
            return false;
        }

        virtual bool DecorateSub(FNameIdentPtr FName)
        {
            /* Per default sub-members are not allowed */
            throw std::string("Type denoter \"" + Spell + "\" does not has any sub-members");
            return false;
        }

        virtual ObjectPtr FindSub(FNameIdentPtr FName)
        {
            /* Per default sub-members are not allowed */
            return nullptr;
        }

        //! Returns true if this is a pointer type denoter (raw- or managed pointer).
        inline bool IsPointer() const
        {
            return Type() == Types::RawPtrType || Type() == Types::MngPtrType;
        }

        virtual TypeDenoterPtr Copy() const = 0;

    protected:

        TypeDenoter(const Types &Type, const TokenPtr TokenType) :
            Terminal(TokenType->Pos(), TokenType->Spell()   ),
            Type_   (Type                                   )
        {
        }
        TypeDenoter(const Types &Type, const SourcePosition &Pos, const std::string &Spelling) :
            Terminal(Pos, Spelling  ),
            Type_   (Type           )
        {
        }
        TypeDenoter(const Types &Type, const Terminal& Term) :
            Terminal(Term),
            Type_   (Type)
        {
        }

    private:
        
        Types Type_;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================