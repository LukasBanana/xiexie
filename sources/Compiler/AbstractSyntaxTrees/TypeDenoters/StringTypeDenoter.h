/*
 * String type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_STRING_H__
#define __XX_AST_TYPEDENOTER_STRING_H__


#include "TypeDenoter.h"
#include "ProcedureFactory.h"
#include "FunctionObject.h"
#include "ReferenceTypeDenoter.h"
#include "ConstTypeDenoter.h"


namespace AbstractSyntaxTrees
{


class StringTypeDenoter : public TypeDenoter
{
    
    public:
        
        StringTypeDenoter(TokenPtr TypeToken) :
            TypeDenoter(TypeDenoter::Types::StringType, TypeToken)
        {
        }
        StringTypeDenoter(const SourcePosition &Pos, const std::string &Spelling) :
            TypeDenoter(TypeDenoter::Types::StringType, Pos, Spelling)
        {
        }
        ~StringTypeDenoter()
        {
        }

        DefineASTVisitProc(StringTypeDenoter)

        int GetTypeSize() const
        {
            return -1;
        }

        bool IsExact() const
        {
            return true;
        }

        bool DecorateSub(FNameIdentPtr FName)
        {
            if (!ProcedureFactory::DecorateWithStdStringProc(FName))
                throw std::string("Unknown standard procedure \"" + FName->Ident->Spell + "\" for 'string' type denoter");
            return true;
        }

        ObjectPtr FindSub(FNameIdentPtr FName)
        {
            return ProcedureFactory::FindStdStringProc(FName);
        }

        TypeDenoterPtr Copy() const
        {
            return std::make_shared<StringTypeDenoter>(Pos(), Spell);
        }

        static StringTypeDenoterPtr Create(bool HasUNICODE = false)
        {
            return std::make_shared<StringTypeDenoter>(SourcePosition::Ignore, HasUNICODE ? "wstring" : "string");
        }
        static RefTypeDenoterPtr CreateConstRef(bool HasUNICODE = false)
        {
            return RefTypeDenoter::Create(ConstTypeDenoter::Create(StringTypeDenoter::Create(HasUNICODE)));
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================