/*
 * Float type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_FLOAT_H__
#define __XX_AST_TYPEDENOTER_FLOAT_H__


#include "TypeDenoter.h"


namespace AbstractSyntaxTrees
{


class FloatTypeDenoter : public TypeDenoter
{
    
    public:
        
        FloatTypeDenoter(TokenPtr TypeToken) :
            TypeDenoter(TypeDenoter::Types::FloatType, TypeToken)
        {
        }
        FloatTypeDenoter(const SourcePosition &Pos, const std::string &Spelling) :
            TypeDenoter(TypeDenoter::Types::FloatType, Pos, Spelling)
        {
        }
        ~FloatTypeDenoter()
        {
        }

        DefineASTVisitProc(FloatTypeDenoter)

        std::string GetDefaultValue() const
        {
            return Spell == "float" ? "0.0f" : "0.0";
        }

        int GetTypeSize() const
        {
            if (Spell == "float")
                return 4;
            if (Spell == "double")
                return 8;
            if (Spell == "quad")
                return 16;
            return 0;
        }

        TypeDenoterPtr Copy() const
        {
            return std::make_shared<FloatTypeDenoter>(Pos(), Spell);
        }

        static FloatTypeDenoterPtr Create(const std::string& Spell = "float")
        {
            return std::make_shared<FloatTypeDenoter>(SourcePosition::Ignore, Spell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================