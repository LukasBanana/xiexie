/*
 * Array type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_ARRAY_H__
#define __XX_AST_TYPEDENOTER_ARRAY_H__


#include "TypeDenoter.h"
#include "IntegerLiteral.h"
#include "StringMod.h"


namespace AbstractSyntaxTrees
{


class ArrayTypeDenoter : public TypeDenoter
{
    
    public:
        
        ArrayTypeDenoter(TypeDenoterPtr TDenoterAST, IntegerLiteralPtr DimLiteral = nullptr) :
            TypeDenoter (TypeDenoter::Types::ArrayType, TDenoterAST->Pos(), "<array>"   ),
            TDenoter    (TDenoterAST                                                    ),
            Literal     (DimLiteral                                                     )
        {
        }
        ~ArrayTypeDenoter()
        {
        }

        DefineASTVisitProc(ArrayTypeDenoter)

        void PrintAST()
        {
            if (Literal != nullptr)
            {
                if (OffsetLiteral != nullptr)
                    Deb("Array Type Denoter [" + OffsetLiteral->Spell + " .. " + Literal->Spell + "]");
                else
                    Deb("Array Type Denoter [" + Literal->Spell + "]");
            }
            else
                Deb("Array Type Denoter (Dynamic)");

            ScopedIndent Unused;
            TDenoter->PrintAST();
        }

        std::string GetDefaultValue() const
        {
            return IsStatic() ? "{ 0 }" : "";
        }

        TypeDenoter* GetResolvedType()
        {
            return TDenoter->GetResolvedType();
        }
        const TypeDenoter* GetResolvedType() const
        {
            return TDenoter->GetResolvedType();
        }

        int GetTypeSize() const
        {
            return TDenoter->GetTypeSize();
        }
        int GetArrayLength() const
        {
            return GetDim();
        }

        int GetDim() const
        {
            return HasOffset() ?
                GetPrimDim() - GetOffset() + 1 :
                GetPrimDim();
        }

        int GetPrimDim() const
        {
            return xxNumber<int>(Literal->Spell);
        }
        int GetOffset() const
        {
            return HasOffset() ? xxNumber<int>(OffsetLiteral->Spell) : 0;
        }

        inline bool IsStatic() const
        {
            return Literal != nullptr;
        }
        inline bool HasOffset() const
        {
            return OffsetLiteral != nullptr;
        }

        TypeDenoterPtr Copy() const
        {
            auto CopyObj = std::make_shared<ArrayTypeDenoter>(
                TDenoter->Copy(),
                Literal != nullptr ?
                    std::dynamic_pointer_cast<IntegerLiteral>(Literal->Copy()) :
                    nullptr
            );

            if (OffsetLiteral != nullptr)
                CopyObj->OffsetLiteral = std::dynamic_pointer_cast<IntegerLiteral>(OffsetLiteral->Copy());

            return CopyObj;
        }

        IntegerLiteralPtr Literal;          //!< Optional primary literal.
        IntegerLiteralPtr OffsetLiteral;    //!< Optional offset literal (Used for range based array dimensioning).

        TypeDenoterPtr TDenoter;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================