/*
 * Reference type denoter AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TYPEDENOTER_REFERENCE_H__
#define __XX_AST_TYPEDENOTER_REFERENCE_H__


#include "TypeDenoter.h"


namespace AbstractSyntaxTrees
{


class RefTypeDenoter : public TypeDenoter
{
    
    public:
        
        RefTypeDenoter(TokenPtr TypeToken, TypeDenoterPtr TDenoterAST) :
            TypeDenoter (TypeDenoter::Types::RefType, TypeToken ),
            TDenoter    (TDenoterAST                            )
        {
        }
        RefTypeDenoter(const SourcePosition &Pos, const std::string &Spelling, TypeDenoterPtr TDenoterAST) :
            TypeDenoter (TypeDenoter::Types::RefType, Pos, Spelling ),
            TDenoter    (TDenoterAST                                )
        {
        }
        ~RefTypeDenoter()
        {
        }

        DefineASTVisitProc(RefTypeDenoter)

        void PrintAST()
        {
            Deb("Type Denoter \"reference\"");
            ScopedIndent Unused;
            TDenoter->PrintAST();
        }

        int GetTypeSize() const
        {
            /* Size of pointer reference */
            return 4;
        }

        TypeDenoter* GetResolvedType()
        {
            return TDenoter->GetResolvedType();
        }
        const TypeDenoter* GetResolvedType() const
        {
            return TDenoter->GetResolvedType();
        }

        TypeDenoter* GetNonRefType()
        {
            return TDenoter.get();
        }
        const TypeDenoter* GetNonRefType() const
        {
            return TDenoter.get();
        }

        size_t CountPointerRefs() const
        {
            return TDenoter->CountPointerRefs();
        }

        TypeDenoterPtr Copy() const
        {
            return std::make_shared<RefTypeDenoter>(Pos(), Spell, TDenoter->Copy());
        }

        static RefTypeDenoterPtr Create(const TypeDenoterPtr& TDenoterAST)
        {
            return std::make_shared<RefTypeDenoter>(SourcePosition::Ignore, "", TDenoterAST);
        }

        TypeDenoterPtr TDenoter;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================