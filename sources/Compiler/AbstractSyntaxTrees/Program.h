/*
 * Program AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_PROGRAM_H__
#define __XX_AST_PROGRAM_H__


#include "AST.h"
#include "Command.h"

#include <list>
#include <map>
#include <set>


namespace AbstractSyntaxTrees
{


//! Set of constant type denoter pointers.
typedef std::set<ClassObject*> ClassObjectSet;
typedef std::shared_ptr<ClassObjectSet> ClassObjectSetPtr;


class Program : public AST
{
    
    public:
        
        Program() :
            AST                 (SourcePosition::Ignore ),
            IsHeaderGuardUsed   (false                  )
        {
        }
        ~Program()
        {
        }

        DefineASTVisitProc(Program)

        void PrintAST()
        {
            Deb("Program");
            ScopedIndent Unused;
            for (auto Cmd : Commands)
                Cmd->PrintAST();
        }

        ProgramPtr Copy() const
        {
            auto Obj = std::make_shared<Program>();

            for (auto Cmd : Commands)
                Obj->Commands.push_back(Cmd->Copy());

            Obj->SourceFilename = SourceFilename;

            return Obj;
        }

        std::list<CommandPtr> Commands;

        std::string SourceFilename;

        std::map<std::string, bool> IncludeFiles;       //!< Information for decorated AST.
        bool IsHeaderGuardUsed;                         //!< Information for decorated AST.

        ClassObjectSetPtr CopyProcObjects;              //!< Information for decorated AST.
        std::list<ClassObject*> AnonymousClassObjects;  //!< Information for decorated AST.

};


} // /namespace AbstractSyntaxTrees


#endif



// ================================================================================