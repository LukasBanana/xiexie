/*
 * Identifier AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_IDENTIFIER_H__
#define __XX_AST_IDENTIFIER_H__


#include "Terminal.h"
#include "Token.h"


namespace AbstractSyntaxTrees
{


//! Simple identifier AST node.
class Identifier : public Terminal
{
    
    public:
        
        Identifier(TokenPtr InitIdent) :
            Terminal(InitIdent->Pos(), InitIdent->Spell())
        {
        }
        Identifier(const SourcePosition& Pos, const std::string& Spelling) :
            Terminal(Pos, Spelling)
        {
        }
        ~Identifier()
        {
        }

        DefineASTVisitProc(Identifier)

        void PrintAST()
        {
            Deb("Identifier \"" + Spell + "\"");
        }

        IdentifierPtr Copy() const
        {
            return std::make_shared<Identifier>(Pos(), Spell);
        }

        static IdentifierPtr Create(const std::string& IdentSpell)
        {
            return std::make_shared<Identifier>(SourcePosition::Ignore, IdentSpell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================