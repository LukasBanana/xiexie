/*
 * Initializer list AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LIST_INITIALIZER_H__
#define __XX_AST_LIST_INITIALIZER_H__


#include "AST.h"
#include "Expression.h"


namespace AbstractSyntaxTrees
{


class InitializerList : public AST
{
    
    public:
        
        InitializerList(ExpressionPtr ParamExpr, InitializerListPtr NextInitList = nullptr) :
            AST     (ParamExpr->Pos()   ),
            Expr    (ParamExpr          ),
            Next    (NextInitList       )
        {
        }
        ~InitializerList()
        {
        }

        DefineASTVisitProc(InitializerList)

        void PrintAST()
        {
            Deb("Initializer List");
            ScopedIndent Unused;

            if (Denom != nullptr)
                Deb("Denominator \"" + Denom->Spell + "\"");

            Expr->PrintAST();
            
            if (Next != nullptr)
                Next->PrintAST();
        }

        InitializerListPtr Copy() const
        {
            auto CopyObj = std::make_shared<InitializerList>(Expr->Copy());

            if (Denom != nullptr)
                CopyObj->Denom = Denom->Copy();
            if (Next != nullptr)
                CopyObj->Next = Next->Copy();

            return CopyObj;
        }

        //! Returns true if a denominator is used in any initializer of this list (with recursive search).
        bool HasDenom() const
        {
            if (Denom != nullptr)
                return true;
            if (Next != nullptr)
                return Next->HasDenom();
            return false;
        }

        IdentifierPtr Denom;        //!< Optional initializer denominator (when used to initialize a class instance).
        ExpressionPtr Expr;         //!< Initializer expression.

        InitializerListPtr Next;    //!< Optional next initializer list.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================