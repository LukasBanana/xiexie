/*
 * Template argument list AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LIST_TEMPLATEARG_H__
#define __XX_AST_LIST_TEMPLATEARG_H__


#include "AST.h"


namespace AbstractSyntaxTrees
{


class TemplateArgList : public AST
{
    
    public:
        
        TemplateArgList(ExpressionPtr ExprAST);
        TemplateArgList(TypeDenoterPtr TDenoterAST);
        ~TemplateArgList()
        {
        }

        DefineASTVisitProc(TemplateArgList)

        void PrintAST();

        TemplateArgListPtr Copy() const;

        ExpressionPtr Expr;         // Expression (only if no type-denoter is used).
        TypeDenoterPtr TDenoter;    // Type denoter (only if no expression is used).

        TemplateArgListPtr Next;    // Optional next template argument.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================