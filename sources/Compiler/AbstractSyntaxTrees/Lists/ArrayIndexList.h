/*
 * Array index list AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LIST_ARRAYINDEX_H__
#define __XX_AST_LIST_ARRAYINDEX_H__


#include "AST.h"
#include "Expression.h"


namespace AbstractSyntaxTrees
{


class ArrayIndexList : public AST
{
    
    public:
        
        ArrayIndexList(ExpressionPtr ExprAST, ArrayIndexListPtr NextArrayList = nullptr) :
            AST (ExprAST->Pos() ),
            Expr(ExprAST        ),
            Link(nullptr        ),
            Next(NextArrayList  )
        {
        }
        ~ArrayIndexList()
        {
        }

        DefineASTVisitProc(ArrayIndexList)

        void PrintAST()
        {
            Deb("Array Index List");
            ScopedIndent Unused;

            Expr->PrintAST();

            if (Link != nullptr)
                Deb("Def. at " + Link->Pos().GetString());
            
            if (Next != nullptr)
                Next->PrintAST();
        }

        ArrayIndexListPtr Copy() const
        {
            auto CopyObj = std::make_shared<ArrayIndexList>(Expr->Copy());

            CopyObj->Link = Link;

            if (Next != nullptr)
                CopyObj->Next = Next->Copy();

            return CopyObj;
        }

        ExpressionPtr Expr;     //!< Array indexing expression.
        TypeDenoter* Link;      //!< Information for the decorated AST (Linked array- or raw-pointer type-denoter).

        ArrayIndexListPtr Next; //!< Optional next array index list.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================