/*
 * Attribute list AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LIST_ATTRIBUTE_H__
#define __XX_AST_LIST_ATTRIBUTE_H__


#include "AST.h"
#include "Identifier.h"

#include <functional>


namespace AbstractSyntaxTrees
{


class AttributeList : public AST
{
    
    public:
        
        enum class Attributes
        {
            Pack,
            Set,
            Get,
            Unroll,
        };

        static const std::string AttribSpellings[];

        AttributeList(IdentifierPtr IdentAST, AttributeListPtr NextAttrib = nullptr) :
            AST     (IdentAST->Pos()),
            Ident   (IdentAST       ),
            Next    (NextAttrib     )
        {
        }
        ~AttributeList()
        {
        }

        DefineASTVisitProc(AttributeList)

        void PrintAST()
        {
            Deb("Attribute List \"" + Ident->Spell + "\"");

            if (Next != nullptr)
            {
                ScopedIndent Unused;
                Next->PrintAST();
            }
        }

        AttributeListPtr Copy() const
        {
            auto CopyObj = std::make_shared<AttributeList>(Ident->Copy());

            if (Next != nullptr)
                CopyObj->Next = Next->Copy();

            return CopyObj;
        }

        /**
        Returns true if the specified attribute is contained in this list.
        \see Attributes
        */
        bool HasAttrib(const Attributes& Attrib, bool RecursiveSearch = true) const
        {
            /* Search for attribute in spelling list */
            if (Ident->Spell == AttribSpellings[static_cast<size_t>(Attrib)])
                return true;

            /* Search in next attribute list */
            if (RecursiveSearch && Next != nullptr)
                return Next->HasAttrib(Attrib, true);

            return false;
        }

        static void ForEach(AttributeListPtr AttribList, const std::function<void(const AttributeList& Attrib)>& Proc)
        {
            while (AttribList != nullptr)
            {
                Proc(*AttribList);
                AttribList = AttribList->Next;
            }
        }

        IdentifierPtr Ident;    //!< Attribute identifier.

        AttributeListPtr Next;  //!< Optional next attribute list.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================