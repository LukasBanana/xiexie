/*
 * Argument list AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LIST_ARGUMENT_H__
#define __XX_AST_LIST_ARGUMENT_H__


#include "AST.h"
#include "Expression.h"


namespace AbstractSyntaxTrees
{


//! Argument lists are used for function calls.
class ArgumentList : public AST
{
    
    public:
        
        ArgumentList(ExpressionPtr ParamExpr, ArgumentListPtr NextParam = nullptr) :
            AST     (ParamExpr->Pos()   ),
            Expr    (ParamExpr          ),
            Next    (NextParam          )
        {
        }
        ~ArgumentList()
        {
        }

        DefineASTVisitProc(ArgumentList)

        void PrintAST()
        {
            Deb("Argument List");
            ScopedIndent Unused;

            if (Denom != nullptr)
                Deb("Denominator \"" + Denom->Spell + "\"");

            Expr->PrintAST();
            
            if (Next != nullptr)
                Next->PrintAST();
        }

        ArgumentListPtr Copy() const
        {
            auto CopyObj = std::make_shared<ArgumentList>(Expr->Copy());

            if (Denom != nullptr)
                CopyObj->Denom = Denom->Copy();
            if (Next != nullptr)
                CopyObj->Next = Next->Copy();

            return CopyObj;
        }

        //! Returns true if a denominator is used in any argument of this list (with recursive search).
        bool HasDenom() const
        {
            if (Denom != nullptr)
                return true;
            if (Next != nullptr)
                return Next->HasDenom();
            return false;
        }

        //! Returns the number of arguments in this list.
        size_t GetArgNum() const
        {
            return Next != nullptr ? Next->GetArgNum() + 1 : 1;
        }

        IdentifierPtr Denom;    //!< Optional parameter denominator.
        ExpressionPtr Expr;     //!< Argument expression.

        ArgumentListPtr Next;   //!< Optional next argument list.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================