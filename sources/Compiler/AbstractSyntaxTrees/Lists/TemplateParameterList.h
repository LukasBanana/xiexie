/*
 * Template parameter list AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LIST_TEMPLATEPARAM_H__
#define __XX_AST_LIST_TEMPLATEPARAM_H__


#include "AST.h"


namespace ContextualAnalyzer
{
    class ScopeManager;
}

namespace AbstractSyntaxTrees
{


class TemplateParamList : public AST
{
    
    public:
        
        TemplateParamList(ObjectPtr ObjAST);
        ~TemplateParamList()
        {
        }

        DefineASTVisitProc(TemplateParamList)

        void PrintAST();

        //! Returns true if this template parameter list has a pattern.
        bool HasPattern() const;

        TemplateParamListPtr Copy() const;

        /**
        Compares this template parameter list with the specified template argument list.
        \throws std::string
        */
        void Compare(const TemplateArgList* ArgList, const ContextualAnalyzer::ScopeManager& ScopeMngr) const;

        ObjectPtr Obj;
        TemplateParamListPtr Next;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================