/*
 * Parameter list AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_LIST_PARAMETER_H__
#define __XX_AST_LIST_PARAMETER_H__


#include "AST.h"
#include "VariableObject.h"
#include "ArgumentList.h"
#include "ScopeManager.h"


namespace AbstractSyntaxTrees
{


//! Parameter lists are used for function definitions.
class ParameterList : public AST
{
    
    public:
        
        struct Flags
        {
            typedef unsigned char DataType;
            static const DataType SkipFromScope = (1 << 0); //!< This parameter will not be added to the scope (used for procedure-type-denoters).
        };

        ParameterList(VariableObjectPtr VarObj, ParameterListPtr InitNext = nullptr) :
            AST     (VarObj->Pos()  ),
            Obj     (VarObj         ),
            Next    (InitNext       ),
            DefFlags(0              )
        {
        }
        ~ParameterList()
        {
        }

        DefineASTVisitProc(ParameterList)

        void PrintAST()
        {
            Deb("Parameter List");
            ScopedIndent Unused;

            Obj->PrintAST();

            if (Next != nullptr)
                Next->PrintAST();
        }

        //! Returns the size of the parameter list (by recursive search).
        size_t GetListSize() const
        {
            return Next != nullptr ? Next->GetListSize() + 1 : 1;
        }

        //! Compares this parameter list with the specified opposit parameter list.
        bool Compare(const ParameterList* Other) const
        {
            if (Other == nullptr)
                return false;

            /* Compare current parameter */
            if (!Obj->TDenoter->Compare(Other->Obj->TDenoter.get()))
                return false;

            /* Check if there are further parameters in the list */
            if (Next != nullptr)
                return Next->Compare(Other->Next.get());
            else if (Other->Next != nullptr)
                return false;
            
            return true;
        }

        /**
        Compares this parameter list with the specified argument list.
        \throws std::string
        */
        void Compare(const ArgumentList* ArgList, const ScopeManager& ScopeMngr) const
        {
            if (ArgList == nullptr)
            {
                /* Check if argument has default value */
                if (Obj->Expr == nullptr)
                    throw std::string("Too few arguments for parameter list");
                return;
            }

            //!TODO! -> refactor type denoter comparision with expressions!!!
            #if 0
            /* Compare current type denoters */
            auto TDenoterAST = ArgList->Expr->GetTypeDenoter(ScopeMngr)->GetNonConstType();

            if (!Obj->TDenoter->GetNonConstType()->Compare(TDenoterAST))
                throw std::string("Incompatible type for argument expression");
            #endif

            /* Check if there are further parameters */
            if (Next != nullptr)
                Next->Compare(ArgList->Next.get(), ScopeMngr);
            else if (ArgList->Next != nullptr)
                throw std::string("Too many arguments for parameter list");
        }

        /**
        Returns true if this parameter list has common default parameters,
        i.e. if a parameter has a default value, all following parameters must have
        default values, too. Or there is no default value inside this parameter list.
        Otherwise it is an 'unordered default parameter list'.
        \param[in] HadDefaultValue Specifies whether there was already a default value.
        This parameter is used internally for resursive calls.
        */
        bool HasCommonDefaultParams(bool HadDefaultValue = false) const
        {
            if (Obj->Expr != nullptr)
                HadDefaultValue = true;
            else if (HadDefaultValue)
                return false;

            if (Next != nullptr)
                return Next->HasCommonDefaultParams(HadDefaultValue);

            return true;
        }

        /**
        Trys to find the index of the specified parameter denominator.
        \returns Found index or -1 if no such denominator exists in this parameter list.
        */
        int FindIndexByDenom(const std::string& Denom, int StartIndex = 0) const
        {
            if (Obj->Ident->Spell == Denom)
                return StartIndex;
            if (Next != nullptr)
                return Next->FindIndexByDenom(Denom, StartIndex + 1);
            return -1;
        }

        /**
        Try to find the default argument expression by the specified denominator.
        \return Expression or null pointer if there is no such default expression.
        */
        ExpressionPtr FindDefaultExprByIndex(int Index) const
        {
            if (Index == 0)
                return Obj->Expr;
            if (Next != nullptr && Index > 0)
                return Next->FindDefaultExprByIndex(Index - 1);
            return nullptr;
        }

        ParameterListPtr Copy() const
        {
            auto CopyObj = std::make_shared<ParameterList>(
                std::dynamic_pointer_cast<VariableObject>(Obj->Copy())
            );

            CopyObj->DefFlags = DefFlags;

            if (Next != nullptr)
                CopyObj->Next = Next->Copy();

            return CopyObj;
        }

        VariableObjectPtr Obj;
        ParameterListPtr Next; // Optional next parameter list.

        Flags::DataType DefFlags;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================