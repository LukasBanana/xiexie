/*
 * Abstract syntax tree header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_H__
#define __XX_AST_H__


#include "SourcePosition.h"
#include "ConsoleOutput.h"
#include "ASTDeclarations.h"
#include "Visitor.h"

#include <memory>
#include <functional>


namespace AbstractSyntaxTrees
{


using namespace SyntacticAnalyzer;


#define DefineASTVisitProc(n)                                                   \
    void Visit(Visitor* Vst, void* Args = nullptr)                              \
    {                                                                           \
        Vst->Visit##n(std::dynamic_pointer_cast<n>(shared_from_this()), Args);  \
    }


/**
AST (Abstract Syntax Tree) node base class.
There are several functions, which must be implemented by each child class.
The "Visit" class will be implemented by using the "DefineASTVisitProc" macro.
The "std::shared_ptr<ASTClassName> Copy() const" function must be implemented implicitly,
this function must copy the whole AST node.
*/
class AST : public std::enable_shared_from_this<AST>
{
    
    public:
        
        virtual ~AST()
        {
        }

        /**
        Virutal visitor function.
        \param[in] Vst Pass a code-generation- or contex-analysis visitor.
        */
        virtual void Visit(Visitor* Vst, void* Args = nullptr)
        {
            // Dummy
        }

        //! Views debugging information with recursive node calls.
        virtual void PrintAST()
        {
            // Dummy
        }

        inline const SourcePosition& Pos() const
        {
            return Pos_;
        }

    protected:

        AST(const SourcePosition &Pos) :
            Pos_(Pos)
        {
        }

        void Deb(const std::string &Str)
        {
            ConsoleOutput::Message(Str);
        }

        template <typename T> inline unsigned int GetFlag(const T& Flag) const
        {
            return static_cast<unsigned int>(Flag);
        }

        template <typename T> inline std::shared_ptr<T> ThisPtr()
        {
            return std::dynamic_pointer_cast<T>(shared_from_this());
        }

    private:
        
        SourcePosition Pos_;    //!< Source position.

};


} // /namespace AbstractSyntaxTrees


#endif



// ================================================================================