/*
 * Range based for loop command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_LOOP_RANGEFOR_H__
#define __XX_AST_COMMAND_LOOP_RANGEFOR_H__


#include "Command.h"
#include "BlockCommand.h"
#include "Identifier.h"
#include "VariableObject.h"
#include "AttributeList.h"
#include "StringMod.h"


namespace AbstractSyntaxTrees
{


class RangeForLoopCommand : public Command
{
    
    public:
        
        typedef long long int index_t;

        struct MetaData
        {
            MetaData() :
                Start   (0      ),
                End     (0      ),
                Step    (1      ),
                MinRange(0      ),
                MaxRange(0      ),
                IsSigned(false  ),
                Forwards(true   ),
                Unroll  (false  )
            {
            }
            ~MetaData()
            {
            }

            /* Members */
            index_t Start, End, Step, MinRange, MaxRange;
            bool IsSigned, Forwards, Unroll;
        };

        RangeForLoopCommand(
            IdentifierPtr IteratorIdent, BlockCommandPtr LoopCmdBlock, const index_t& StartIndex, const index_t& EndIndex) :
                Command (LoopCmdBlock->Pos(), Command::Types::RangeForLoopCmd   ),
                Ident   (IteratorIdent                                          ),
                CmdBlock(LoopCmdBlock                                           ),
                Start   (StartIndex                                             ),
                End     (EndIndex                                               ),
                Step    (1                                                      )
        {
        }
        ~RangeForLoopCommand()
        {
        }

        DefineASTVisitProc(RangeForLoopCommand)

        void PrintAST()
        {
            Deb(
                "Range For Loop" +
                (Ident != nullptr ? " \"" + Ident->Spell + "\" (" : " (") +
                xxStr(Start) + " .. " + xxStr(End) + ")" +
                std::string(Step != 1 ? " -> " + xxStr(Step) : "")
            );
            ScopedIndent Unused;

            if (AttribList != nullptr)
                AttribList->PrintAST();

            CmdBlock->PrintAST();
        }

        void FillMetaData(MetaData& Data) const
        {
            /* Fill meta data from AST node */
            Data.Start      = Start;
            Data.End        = End;
            Data.Step       = std::abs(Step);

            Data.MinRange   = std::min(Start, End);
            Data.MaxRange   = std::max(Start, End);

            Data.IsSigned   = (Start < 0 || End < 0);
            Data.Forwards   = (Start < End);

            Data.Unroll     = (AttribList != nullptr && AttribList->HasAttrib(AttributeList::Attributes::Unroll));
        }

        CommandPtr Copy() const
        {
            auto CopyObj = std::make_shared<RangeForLoopCommand>(
                Ident != nullptr ? Ident->Copy() : nullptr,
                std::dynamic_pointer_cast<BlockCommand>(CmdBlock->Copy()),
                Start, End
            );

            if (AttribList != nullptr)
                CopyObj->AttribList = AttribList->Copy();
            if (Iterator != nullptr)
                CopyObj->Iterator = std::dynamic_pointer_cast<VariableObject>(Iterator->Copy());

            return CopyObj;
        }

        IdentifierPtr Ident;            //!< Iterator identifier.
        BlockCommandPtr CmdBlock;
        AttributeListPtr AttribList;    //!< Optional attribute list.

        index_t Start, End;             //!< Direct range limits.
        index_t Step;                   //!< Iteration interval steps. Must always be greater zero! By default 1.

        VariableObjectPtr Iterator;     //!< Information for the decorated AST.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================