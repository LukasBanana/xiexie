/*
 * Class block command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_CLASSBLOCK_H__
#define __XX_AST_COMMAND_CLASSBLOCK_H__


#include "Command.h"
#include "FNameIdent.h"

#include <list>
#include <vector>


namespace AbstractSyntaxTrees
{


class ClassBlockCommand : public Command
{
    
    public:
        
        ClassBlockCommand(const SourcePosition &Pos) :
            Command (Pos, Command::Types::ClassBlockCmd ),
            Parent  (nullptr                            )
        {
        }
        ~ClassBlockCommand()
        {
        }

        DefineASTVisitProc(ClassBlockCommand)

        struct Privacy
        {
            typedef size_t DataType;
            enum : DataType
            {
                Public = 0,
                Protect,
                Private,
            };
        };

        void PrintAST()
        {
            Deb("Class Block Command");
            ScopedIndent Unused;

            ViewDebugBlock(0, "public");
            ViewDebugBlock(1, "protect");
            ViewDebugBlock(2, "private");
        }

        CommandPtr Copy() const
        {
            auto CopyObj = std::make_shared<ClassBlockCommand>(Pos());

            for (int i = 0; i < 3; ++i)
            {
                for (auto Entry : Commands[i])
                    CopyObj->Commands[i].push_back(Entry->Copy());
            }

            return CopyObj;
        }

        std::list<CommandPtr> Commands[3];
        
        ClassObject* Parent;                //!< Parent AST node.

    private:
        
        void ViewDebugBlock(unsigned int Index, const std::string& Name)
        {
            if (!Commands[Index].empty())
            {
                Deb(Name + ":");
                ScopedIndent Unused;

                for (CommandPtr Cmd : Commands[Index])
                    Cmd->PrintAST();
            }
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================