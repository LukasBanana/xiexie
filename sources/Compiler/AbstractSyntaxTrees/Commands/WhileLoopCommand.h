/*
 * While loop command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_LOOP_WHILE_H__
#define __XX_AST_COMMAND_LOOP_WHILE_H__


#include "Command.h"
#include "BlockCommand.h"
#include "Expression.h"


namespace AbstractSyntaxTrees
{


class WhileLoopCommand : public Command
{
    
    public:
        
        struct Flags
        {
            typedef unsigned int DataType;
            static const DataType DoLoop    = (1 << 0); //!< Is this loop a do-loop?
            static const DataType UntilLoop = (1 << 1); //!< Is this loop an until-loop?
        };

        WhileLoopCommand(ExpressionPtr CondExpr, BlockCommandPtr LoopCmdBlock, Flags::DataType Options = 0) :
            Command         (LoopCmdBlock->Pos(), Command::Types::WhileLoopCmd  ),
            Expr            (CondExpr                                           ),
            CmdBlock        (LoopCmdBlock                                       ),
            DefFlags        (Options                                            )
        {
        }
        ~WhileLoopCommand()
        {
        }

        DefineASTVisitProc(WhileLoopCommand)

        void PrintAST()
        {
            if ((DefFlags & Flags::DoLoop) != 0)
            {
                if ((DefFlags & Flags::UntilLoop) != 0)
                    Deb("Do-Until Loop");
                else
                    Deb("Do-While Loop");
            }
            else
            {
                if ((DefFlags & Flags::UntilLoop) != 0)
                    Deb("Until Loop");
                else
                    Deb("While Loop");
            }

            ScopedIndent Unused;

            Expr->PrintAST();
            CmdBlock->PrintAST();
        }

        CommandPtr Copy() const
        {
            return std::make_shared<WhileLoopCommand>(
                Expr->Copy(), std::dynamic_pointer_cast<BlockCommand>(CmdBlock->Copy()), DefFlags
            );
        }

        ExpressionPtr Expr;
        BlockCommandPtr CmdBlock;

        Flags::DataType DefFlags;   //!< Definition flags.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================