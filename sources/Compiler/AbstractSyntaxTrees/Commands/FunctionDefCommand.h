/*
 * Function definition command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_FUNCDEF_H__
#define __XX_AST_COMMAND_FUNCDEF_H__


#include "Command.h"
#include "FunctionObject.h"


namespace AbstractSyntaxTrees
{


class FunctionDefCommand : public Command
{
    
    public:
        
        FunctionDefCommand(FunctionObjectPtr FuncObj) :
            Command (FuncObj->Pos(), Command::Types::FuncDefCmd ),
            Obj     (FuncObj                                    )
        {
        }
        ~FunctionDefCommand()
        {
        }

        DefineASTVisitProc(FunctionDefCommand)

        void PrintAST()
        {
            Deb("Function Definition");
            ScopedIndent Unused;
            Obj->PrintAST();
        }

        CommandPtr Copy() const
        {
            return std::make_shared<FunctionDefCommand>(
                std::dynamic_pointer_cast<FunctionObject>(Obj->Copy())
            );
        }

        FunctionObjectPtr Obj;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================