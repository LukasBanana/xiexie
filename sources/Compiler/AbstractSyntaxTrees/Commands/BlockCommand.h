/*
 * Block command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_BLOCK_H__
#define __XX_AST_COMMAND_BLOCK_H__


#include "Command.h"
#include "FNameIdent.h"

#include <list>


namespace AbstractSyntaxTrees
{


//!TODO! -> use more kinds of command blocks -> ClassBlockCommand, PackageBlockCommand, FunctionBlockCommand etc.
class BlockCommand : public Command
{
    
    public:
        
        BlockCommand(const SourcePosition &Pos) :
            Command     (Pos, Command::Types::BlockCmd  ),
            HasCurly    (true                           ),
            OpensScope  (true                           )
        {
        }
        ~BlockCommand()
        {
        }

        DefineASTVisitProc(BlockCommand)

        void PrintAST()
        {
            Deb("Block Command");
            ScopedIndent Unused;
            for (CommandPtr Cmd : Commands)
                Cmd->PrintAST();
        }

        CommandPtr Copy() const
        {
            auto CopyObj = std::make_shared<BlockCommand>(Pos());

            CopyObj->HasCurly = HasCurly;
            CopyObj->OpensScope = OpensScope;

            for (auto Entry : Commands)
                CopyObj->Commands.push_back(Entry->Copy());

            return CopyObj;
        }

        static BlockCommandPtr Create()
        {
            return std::make_shared<BlockCommand>(SourcePosition::Ignore);
        }

        std::list<CommandPtr> Commands;

        bool HasCurly;
        bool OpensScope;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================