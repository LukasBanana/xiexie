/*
 * Primitive token command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_PRIMITIVE_H__
#define __XX_AST_COMMAND_PRIMITIVE_H__


#include "Command.h"
#include "BlockCommand.h"
#include "Expression.h"


namespace AbstractSyntaxTrees
{


class PrimitiveCommand : public Command
{
    
    public:
        
        PrimitiveCommand(TokenPtr PrimTkn) :
            Command (PrimTkn->Pos(), Command::Types::PrimCmd),
            PrimType(PrimTkn->Type()                        ),
            Spell   (PrimTkn->Spell()                       )
        {
        }
        PrimitiveCommand(const Token::Types TknType, const SourcePosition& Pos, const std::string& Spelling) :
            Command (Pos, Command::Types::PrimCmd   ),
            PrimType(TknType                        ),
            Spell   (Spelling                       )
        {
        }
        ~PrimitiveCommand()
        {
        }

        DefineASTVisitProc(PrimitiveCommand)

        void PrintAST()
        {
            Deb("Primitive Command \"" + Spell + "\"");
        }

        CommandPtr Copy() const
        {
            return std::make_shared<PrimitiveCommand>(PrimType, Pos(), Spell);
        }

        Token::Types PrimType;
        std::string Spell;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================