/*
 * Return command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_RETURN_H__
#define __XX_AST_COMMAND_RETURN_H__


#include "Command.h"
#include "Expression.h"


namespace AbstractSyntaxTrees
{


class ReturnCommand : public Command
{
    
    public:
        
        ReturnCommand(const SourcePosition &Pos) :
            Command(Pos, Command::Types::RetCmd)
        {
        }
        ~ReturnCommand()
        {
        }

        DefineASTVisitProc(ReturnCommand)

        void PrintAST()
        {
            Deb("Return Statement");
            if (Expr != nullptr)
            {
                ScopedIndent Unused;
                Expr->PrintAST();
            }
        }

        CommandPtr Copy() const
        {
            auto CopyObj = std::make_shared<ReturnCommand>(Pos());

            if (Expr != nullptr)
                CopyObj->Expr = Expr->Copy();

            return CopyObj;
        }

        static ReturnCommandPtr Create(ExpressionPtr ExprAST = nullptr)
        {
            auto CmdAST = std::make_shared<ReturnCommand>(SourcePosition::Ignore);
            if (ExprAST != nullptr)
                CmdAST->Expr = ExprAST;
            return CmdAST;
        }

        ExpressionPtr Expr;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================