/*
 * Enumeration declaration command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_ENUMDECL_H__
#define __XX_AST_COMMAND_ENUMDECL_H__


#include "Command.h"
#include "EnumObject.h"


namespace AbstractSyntaxTrees
{


class EnumDeclCommand : public Command
{
    
    public:
        
        EnumDeclCommand(EnumObjectPtr EnumObj) :
            Command (EnumObj->Pos(), Command::Types::EnumDeclCmd),
            Obj     (EnumObj                                    )
        {
        }
        ~EnumDeclCommand()
        {
        }

        DefineASTVisitProc(EnumDeclCommand)

        void PrintAST()
        {
            Deb("Enumeration Decl Cmd");
            ScopedIndent Unused;
            Obj->PrintAST();
        }

        CommandPtr Copy() const
        {
            return std::make_shared<EnumDeclCommand>(
                std::dynamic_pointer_cast<EnumObject>(Obj->Copy())
            );
        }

        EnumObjectPtr Obj;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================