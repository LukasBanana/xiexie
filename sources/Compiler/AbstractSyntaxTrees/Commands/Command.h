/*
 * Command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_H__
#define __XX_AST_COMMAND_H__


#include "AST.h"


namespace AbstractSyntaxTrees
{


//!TODO! -> rename them all from "Command" to "Statement"!!!
class Command : public AST
{
    
    public:
        
        struct ParseFlags
        {
            typedef unsigned char DataType;
            static const DataType AllowDefinitionsOnly;
            static const DataType GlobalScope;
        };

        //!TODO! -> rename the types from '*Cmd' to '*Stmnt'!!!
        enum class Types
        {
            IfCmd,              //!< If command.
            SwitchCmd,          //!< Switch-case command.
            
            ForLoopCmd,         //!< For loop command.
            ForEachLoopCmd,     //!< For-each loop command.
            RangeForLoopCmd,    //!< Range based for loop command.
            WhileLoopCmd,       //!< While (or Do-While) loop command.
            InfiniteLoopCmd,    //!< Infinite loop command.

            VarDeclCmd,         //!< Variable declaration command.
            FuncDeclCmd,        //!< Function declaration command.
            FuncDefCmd,         //!< Function definition command.
            EnumDeclCmd,        //!< Enumeration declaration command.
            TypeDefCmd,         //!< Type definition command.
            FlagsDeclCmd,       //!< Flags declaration command.
            ClassDeclCmd,       //!< Class declaration command.

            ClassBlockCmd,      //!< Class block command.
            InitCmd,            //!< Init command (class constructor).
            ReleaseCmd,         //!< Release command (class destructor).

            AssignCmd,          //!< Assignment command.
            CallCmd,            //!< Function call command.
            ImportCmd,          //!< Import command, e.g. "import ClassName".
            PackageCmd,         //!< Package command, e.g. "package MyPackage { ... }".
            LangCmd,            //!< Inline language command, e.g. "cpp { ... }".
            BlockCmd,           //!< Block command, "{ ... }".
            RetCmd,             //!< Return command.
            PrimCmd,            //!< Primitive command, e.g. "next", "break" or "stop".
            AssertCmd,          //!< Assertion command.

            TryCatchCmd,        //!< Try-catch command, "try { ... } catch Expr { ... }".
            ThrowCmd,           //!< Throw command, e.g. "throw new ErrorException(...)"
        };

        virtual ~Command()
        {
        }

        inline Types Type() const
        {
            return Type_;
        }

        /**
        Returns true if this is a 'stand-alone command'. This is required
        for code-generation, to avoid useless ';' characters in the output code.
        */
        bool IsStandAloneCmd() const
        {
            switch (Type())
            {
                case Types::IfCmd:
                case Types::SwitchCmd:
                case Types::ForLoopCmd:
                case Types::ForEachLoopCmd:
                case Types::WhileLoopCmd:
                case Types::RangeForLoopCmd:
                case Types::InfiniteLoopCmd:
                case Types::FuncDefCmd:
                case Types::EnumDeclCmd:
                case Types::FlagsDeclCmd:
                case Types::ClassDeclCmd:
                case Types::PackageCmd:
                case Types::LangCmd:
                case Types::BlockCmd:
                case Types::InitCmd:
                case Types::ReleaseCmd:
                case Types::TryCatchCmd:
                case Types::AssertCmd:
                    return true;
                default:
                    break;
            }
            return false;
        }

        virtual CommandPtr Copy() const = 0;

    protected:
        
        Command(const SourcePosition &Pos, const Types &Type) :
            AST     (Pos    ),
            Type_   (Type   )
        {
        }

    private:
        
        Types Type_;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================
