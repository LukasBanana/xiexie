/*
 * Package command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_PACKAGE_H__
#define __XX_AST_COMMAND_PACKAGE_H__


#include "Command.h"
#include "FNameIdent.h"
#include "BlockCommand.h"


namespace AbstractSyntaxTrees
{


class PackageCommand : public Command
{
    
    public:
        
        PackageCommand(const FNameIdentPtr& InitFName, const BlockCommandPtr& InitBlockCmd) :
            Command (InitFName->Pos(), Command::Types::PackageCmd   ),
            FName   (InitFName                                      ),
            BlockCmd(InitBlockCmd)
        {
        }
        ~PackageCommand()
        {
        }

        DefineASTVisitProc(PackageCommand)

        void PrintAST()
        {
            Deb("Package \"" + FName->FullName() + "\"");
            ScopedIndent Unused;
            BlockCmd->PrintAST();
        }

        CommandPtr Copy() const
        {
            return std::make_shared<PackageCommand>(
                FName->Copy(), std::dynamic_pointer_cast<BlockCommand>(BlockCmd->Copy())
            );
        }

        static PackageCommandPtr Create(const FNameIdentPtr& FNameAST, const BlockCommandPtr& BlockCmdAST)
        {
            return std::make_shared<PackageCommand>(FNameAST, BlockCmdAST);
        }

        FNameIdentPtr FName;
        BlockCommandPtr BlockCmd;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================