/*
 * Type definition command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_TYPEDEF_H__
#define __XX_AST_COMMAND_TYPEDEF_H__


#include "Command.h"
#include "TypeObject.h"


namespace AbstractSyntaxTrees
{


class TypeDefCommand : public Command
{
    
    public:
        
        TypeDefCommand(TypeObjectPtr TypeObj) :
            Command (TypeObj->Pos(), Command::Types::TypeDefCmd ),
            Obj     (TypeObj                                    )
        {
        }
        ~TypeDefCommand()
        {
        }

        DefineASTVisitProc(TypeDefCommand)

        void PrintAST()
        {
            Deb("Type Def Cmd");
            ScopedIndent Unused;
            Obj->PrintAST();
        }

        CommandPtr Copy() const
        {
            return std::make_shared<TypeDefCommand>(
                std::dynamic_pointer_cast<TypeObject>(Obj->Copy())
            );
        }

        TypeObjectPtr Obj;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================