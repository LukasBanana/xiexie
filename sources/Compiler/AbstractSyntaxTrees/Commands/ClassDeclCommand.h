/*
 * Class declaration command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_CLASSDECL_H__
#define __XX_AST_COMMAND_CLASSDECL_H__


#include "Command.h"
#include "ClassObject.h"


namespace AbstractSyntaxTrees
{


class ClassDeclCommand : public Command
{
    
    public:
        
        ClassDeclCommand(ClassObjectPtr ClassObj) :
            Command (ClassObj->Pos(), Command::Types::ClassDeclCmd  ),
            Obj     (ClassObj                                       )
        {
        }
        ~ClassDeclCommand()
        {
        }

        DefineASTVisitProc(ClassDeclCommand)

        void PrintAST()
        {
            Deb("Class Declaration");
            ScopedIndent Unused;
            Obj->PrintAST();
        }

        CommandPtr Copy() const
        {
            return std::make_shared<ClassDeclCommand>(
                std::dynamic_pointer_cast<ClassObject>(Obj->Copy())
            );
        }

        ClassObjectPtr Obj;     //!< The class object.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================