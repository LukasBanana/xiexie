/*
 * Inline language command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_INLINELANG_H__
#define __XX_AST_COMMAND_INLINELANG_H__


#include "Command.h"


namespace AbstractSyntaxTrees
{


class InlineLangCommand : public Command
{
    
    public:
        
        InlineLangCommand(TokenPtr Tkn) :
            Command(Tkn->Pos(), Command::Types::LangCmd)
        {
            ParseLangInfoAndCode(Tkn->Spell());
        }
        InlineLangCommand(const SourcePosition &Pos, const std::string& Spelling) :
            Command(Pos, Command::Types::LangCmd)
        {
            ParseLangInfoAndCode(Spelling);
        }
        InlineLangCommand(const InlineLangCommand& Other) :
            Command (Other.Pos(), Command::Types::LangCmd   ),
            LangInfo(Other.LangInfo                         ),
            Code    (Other.Code                             )
        {
        }
        ~InlineLangCommand()
        {
        }

        DefineASTVisitProc(InlineLangCommand)

        void PrintAST()
        {
            Deb("Inline Language \"" + LangInfo + "\"");
            ScopedIndent Unused;
            Deb(Code);
        }

        CommandPtr Copy() const
        {
            return std::make_shared<InlineLangCommand>(*this);
        }

        std::string LangInfo;
        std::string Code;

    private:
        
        void ParseLangInfoAndCode(const std::string &Str)
        {
            size_t Sep = Str.find('@');
            if (Sep != std::string::npos)
            {
                LangInfo = Str.substr(0, Sep);
                Code = Str.substr(Sep + 1);
            }
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================