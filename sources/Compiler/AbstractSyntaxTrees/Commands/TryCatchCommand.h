/*
 * Try-Catch command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_TRYCATCH_H__
#define __XX_AST_COMMAND_TRYCATCH_H__


#include "Command.h"
#include "BlockCommand.h"
#include "ParameterList.h"


namespace AbstractSyntaxTrees
{


class TryCatchCommand : public Command
{
    
    public:
        
        TryCatchCommand(BlockCommandPtr TB, BlockCommandPtr CB) :
            Command     (TB->Pos(), Command::Types::TryCatchCmd ),
            TryBlock    (TB                                     ),
            CatchBlock  (CB                                     )
        {
        }
        ~TryCatchCommand()
        {
        }

        DefineASTVisitProc(TryCatchCommand)

        void PrintAST()
        {
            Deb("Try Statement");
            {
                ScopedIndent Unused;
                TryBlock->PrintAST();
            }

            Deb("Catch Statement");
            {
                ScopedIndent Unused;
                if (ExceptParam != nullptr)
                    ExceptParam->PrintAST();
                CatchBlock->PrintAST();
            }
        }

        CommandPtr Copy() const
        {
            auto CopyObj = std::make_shared<TryCatchCommand>(
                std::dynamic_pointer_cast<BlockCommand>(TryBlock->Copy()),
                std::dynamic_pointer_cast<BlockCommand>(CatchBlock->Copy())
            );

            if (ExceptParam != nullptr)
                CopyObj->ExceptParam = ExceptParam->Copy();

            return CopyObj;
        }

        BlockCommandPtr TryBlock;
        ParameterListPtr ExceptParam;

        //!TODO! -> several catch blocks must be possible!!!
        BlockCommandPtr CatchBlock;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================