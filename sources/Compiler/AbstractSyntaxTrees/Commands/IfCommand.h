/*
 * <If> command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_IF_H__
#define __XX_AST_COMMAND_IF_H__


#include "Command.h"
#include "BlockCommand.h"
#include "Expression.h"


namespace AbstractSyntaxTrees
{


class IfCommand : public Command
{
    
    public:
        
        IfCommand(BlockCommandPtr IfCmdBlock) :
            Command (IfCmdBlock->Pos(), Command::Types::IfCmd   ),
            CmdBlock(IfCmdBlock                                 )
        {
        }
        ~IfCommand()
        {
        }

        DefineASTVisitProc(IfCommand)

        void PrintAST()
        {
            if (Expr)
                Deb("If Statement");
            else
                Deb("Else Statement");

            ScopedIndent Unused;
            if (Expr)
                Expr->PrintAST();
            CmdBlock->PrintAST();

            if (ElseIfCmd)
                ElseIfCmd->PrintAST();
        }

        CommandPtr Copy() const
        {
            auto CopyObj = std::make_shared<IfCommand>(
                std::dynamic_pointer_cast<BlockCommand>(CmdBlock->Copy())
            );

            if (Expr != nullptr)
                CopyObj->Expr = Expr->Copy();
            if (ElseIfCmd != nullptr)
                CopyObj->ElseIfCmd = std::dynamic_pointer_cast<IfCommand>(ElseIfCmd->Copy());

            return CopyObj;
        }

        ExpressionPtr Expr;
        BlockCommandPtr CmdBlock;
        IfCommandPtr ElseIfCmd;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================