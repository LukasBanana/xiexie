/*
 * Assertion command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_ASSERT_H__
#define __XX_AST_COMMAND_ASSERT_H__


#include "Command.h"
#include "Expression.h"


namespace AbstractSyntaxTrees
{


class AssertCommand : public Command
{
    
    public:
        
        AssertCommand(ExpressionPtr ExprAST, ExpressionPtr InfoExprAST = nullptr) :
            Command (ExprAST->Pos(), Command::Types::AssertCmd  ),
            Expr    (ExprAST                                    ),
            InfoExpr(InfoExprAST                                )
        {
        }
        ~AssertCommand()
        {
        }

        DefineASTVisitProc(AssertCommand)

        void PrintAST()
        {
            Deb("Assertion Stmnt");
            ScopedIndent Unused;

            Expr->PrintAST();
            if (InfoExpr != nullptr)
                InfoExpr->PrintAST();
        }

        CommandPtr Copy() const
        {
            return std::make_shared<AssertCommand>(Expr->Copy(), InfoExpr != nullptr ? InfoExpr->Copy() : nullptr);
        }

        ExpressionPtr Expr;
        ExpressionPtr InfoExpr;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================