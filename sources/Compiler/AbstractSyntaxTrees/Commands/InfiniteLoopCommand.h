/*
 * Infintie loop command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_LOOP_INFINITE_H__
#define __XX_AST_COMMAND_LOOP_INFINITE_H__


#include "Command.h"
#include "BlockCommand.h"


namespace AbstractSyntaxTrees
{


class InfiniteLoopCommand : public Command
{
    
    public:
        
        InfiniteLoopCommand(BlockCommandPtr LoopCmdBlock) :
            Command (LoopCmdBlock->Pos(), Command::Types::InfiniteLoopCmd   ),
            CmdBlock(LoopCmdBlock                                           )
        {
        }
        ~InfiniteLoopCommand()
        {
        }

        DefineASTVisitProc(InfiniteLoopCommand)

        void PrintAST()
        {
            Deb("Infinite Loop");
            ScopedIndent Unused;
            CmdBlock->PrintAST();
        }

        CommandPtr Copy() const
        {
            return std::make_shared<InfiniteLoopCommand>(
                std::dynamic_pointer_cast<BlockCommand>(CmdBlock->Copy())
            );
        }

        BlockCommandPtr CmdBlock;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================