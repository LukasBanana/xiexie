/*
 * Import command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_VARDECL_H__
#define __XX_AST_COMMAND_VARDECL_H__


#include "Command.h"
#include "VariableObject.h"
#include "AttributeList.h"


namespace AbstractSyntaxTrees
{


class VariableDefCommand : public Command
{
    
    public:
        
        struct Flags
        {
            typedef unsigned int DataType;
            static const DataType IsClassMember = (1 << 0); //!< This variable is a member of a class.
        };

        VariableDefCommand(VariableObjectPtr VarObj) :
            Command (VarObj->Pos(), Command::Types::VarDeclCmd  ),
            Obj     (VarObj                                     ),
            DefFlags(0                                          )
        {
        }
        ~VariableDefCommand()
        {
        }

        DefineASTVisitProc(VariableDefCommand)

        void PrintAST()
        {
            Deb("Variable Definition Cmd");
            ScopedIndent Unused;

            Obj->PrintAST();

            if (AttribList != nullptr)
                AttribList->PrintAST();
            if (Next != nullptr)
                Next->PrintAST();
        }

        CommandPtr Copy() const
        {
            auto CopyObj = std::make_shared<VariableDefCommand>(
                std::dynamic_pointer_cast<VariableObject>(Obj->Copy())
            );

            CopyObj->DefFlags = DefFlags;

            if (AttribList != nullptr)
                CopyObj->AttribList = AttribList->Copy();
            if (Next != nullptr)
                CopyObj->Next = std::dynamic_pointer_cast<VariableDefCommand>(Next->Copy());

            return CopyObj;
        }

        VariableObjectPtr Obj;          //!< Variable object.
        AttributeListPtr AttribList;    //!< Optional attribute list.
        VariableDefCommandPtr Next;     //!< Optional next variable definition.

        Flags::DataType DefFlags;       //!< Definition flags.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================