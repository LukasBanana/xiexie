/*
 * Init command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_INIT_H__
#define __XX_AST_COMMAND_INIT_H__


#include "Command.h"
#include "ParameterList.h"
#include "BlockCommand.h"
#include "VariableDefCommand.h"


namespace AbstractSyntaxTrees
{


//! The init command represents a class' constructor.
class InitCommand : public Command
{
    
    public:
        
        InitCommand(BlockCommandPtr InitCmdBlock) :
            Command (InitCmdBlock->Pos(), Command::Types::InitCmd   ),
            CmdBlock(InitCmdBlock                                   )
        {
        }
        ~InitCommand()
        {
        }

        DefineASTVisitProc(InitCommand)

        void PrintAST()
        {
            Deb("Init Command");
            ScopedIndent Unused;

            if (ParamList != nullptr)
                ParamList->PrintAST();

            CmdBlock->PrintAST();
        }

        CommandPtr Copy() const
        {
            auto CopyObj = std::make_shared<InitCommand>(
                std::dynamic_pointer_cast<BlockCommand>(CmdBlock->Copy())
            );

            CopyObj->ClassName = ClassName;

            if (ParamList != nullptr)
                CopyObj->ParamList = ParamList->Copy();

            for (auto Entry : Members)
            {
                CopyObj->Members.push_back(
                    std::dynamic_pointer_cast<VariableDefCommand>(Entry->Copy())
                );
            }

            return CopyObj;
        }

        ParameterListPtr ParamList;
        BlockCommandPtr CmdBlock;

        std::string ClassName;                      //!< Information for decorated AST.
        std::vector<VariableDefCommandPtr> Members; //!< Information for decorated AST.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================