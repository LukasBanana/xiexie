/*
 * Assign command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_ASSIGN_H__
#define __XX_AST_COMMAND_ASSIGN_H__


#include "Command.h"
#include "AssignOperator.h"
#include "Expression.h"
#include "FNameIdent.h"


namespace AbstractSyntaxTrees
{


class AssignCommand : public Command
{
    
    public:
        
        AssignCommand(FNameIdentPtr FNameIdent, AssignOperatorPtr AssignOp, ExpressionPtr AssignExpr) :
            Command (FNameIdent->Pos(), Command::Types::AssignCmd   ),
            FName   (FNameIdent                                     ),
            Op      (AssignOp                                       ),
            Expr    (AssignExpr                                     )
        {
        }
        ~AssignCommand()
        {
        }

        DefineASTVisitProc(AssignCommand)

        void PrintAST()
        {
            Deb("Assignment \"" + FName->FullName() + "\"");
            ScopedIndent Unused;

            FName->PrintAST();

            if (Expr != nullptr)
            {
                Op->PrintAST();
                Expr->PrintAST();
            }
        }

        CommandPtr Copy() const
        {
            return std::make_shared<AssignCommand>(
                FName->Copy(), std::dynamic_pointer_cast<AssignOperator>(Op->Copy()), Expr->Copy()
            );
        }

        FNameIdentPtr FName;
        AssignOperatorPtr Op;
        ExpressionPtr Expr;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================