/*
 * <Switch> command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_SWITCH_H__
#define __XX_AST_COMMAND_SWITCH_H__


#include "Command.h"
#include "CaseBlock.h"
#include "Expression.h"

#include <list>


namespace AbstractSyntaxTrees
{


class SwitchCommand : public Command
{
    
    public:
        
        SwitchCommand(ExpressionPtr SwitchExpr) :
            Command (SwitchExpr->Pos(), Command::Types::SwitchCmd   ),
            Expr    (SwitchExpr                                     ),
            IfCmds  (false                                          )
        {
        }
        ~SwitchCommand()
        {
        }

        DefineASTVisitProc(SwitchCommand)

        void PrintAST()
        {
            Deb("Switch Statement");
            ScopedIndent Unused;

            Expr->PrintAST();

            for (auto Block : CaseBlocks)
                Block->PrintAST();
            if (DefBlock != nullptr)
                DefBlock->PrintAST();
        }

        CommandPtr Copy() const
        {
            auto CopyObj = std::make_shared<SwitchCommand>(Expr->Copy());

            CopyObj->IfCmds = IfCmds;

            for (auto Entry : CaseBlocks)
                CopyObj->CaseBlocks.push_back(Entry->Copy());
            if (Expr != nullptr)
                CopyObj->Expr = Expr->Copy();

            return CopyObj;
        }

        ExpressionPtr Expr;
        std::list<CaseBlockPtr> CaseBlocks;
        CaseBlockPtr DefBlock;

        /**
        Information for decorated AST.
        If true, the switch-statement will be converted to if-statements.
        */
        bool IfCmds;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================