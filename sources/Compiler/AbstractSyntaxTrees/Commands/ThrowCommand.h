/*
 * Throw command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_THROW_H__
#define __XX_AST_COMMAND_THROW_H__


#include "Command.h"
#include "Expression.h"


namespace AbstractSyntaxTrees
{


class ThrowCommand : public Command
{
    
    public:
        
        ThrowCommand(ExpressionPtr ThrowExpr) :
            Command (ThrowExpr->Pos(), Command::Types::ThrowCmd ),
            Expr    (ThrowExpr                                  )
        {
        }
        ~ThrowCommand()
        {
        }

        DefineASTVisitProc(ThrowCommand)

        void PrintAST()
        {
            Deb("Throw Statement");
            ScopedIndent Unused;
            Expr->PrintAST();
        }

        CommandPtr Copy() const
        {
            return std::make_shared<ThrowCommand>(Expr->Copy());
        }

        ExpressionPtr Expr;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================