/*
 * Flags declaration command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_FLAGSDECL_H__
#define __XX_AST_COMMAND_FLAGSDECL_H__


#include "Command.h"
#include "FlagsObject.h"


namespace AbstractSyntaxTrees
{


class FlagsDeclCommand : public Command
{
    
    public:
        
        FlagsDeclCommand(FlagsObjectPtr FlagsObj) :
            Command (FlagsObj->Pos(), Command::Types::FlagsDeclCmd  ),
            Obj     (FlagsObj                                       )
        {
        }
        ~FlagsDeclCommand()
        {
        }

        DefineASTVisitProc(FlagsDeclCommand)

        void PrintAST()
        {
            Deb("Flags Declaration");
            ScopedIndent Unused;
            Obj->PrintAST();
        }

        CommandPtr Copy() const
        {
            return std::make_shared<FlagsDeclCommand>(
                std::dynamic_pointer_cast<FlagsObject>(Obj->Copy())
            );
        }

        FlagsObjectPtr Obj;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================