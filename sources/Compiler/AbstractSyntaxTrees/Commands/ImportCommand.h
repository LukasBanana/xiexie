/*
 * Import command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_IMPORT_H__
#define __XX_AST_COMMAND_IMPORT_H__


#include "Command.h"
#include "FNameIdent.h"


namespace AbstractSyntaxTrees
{


class ImportCommand : public Command
{
    
    public:
        
        ImportCommand(FNameIdentPtr InitFName) :
            Command (InitFName->Pos(), Command::Types::ImportCmd),
            FName   (InitFName                                  )
        {
        }
        ~ImportCommand()
        {
        }

        DefineASTVisitProc(ImportCommand)

        void PrintAST()
        {
            Deb("Import \"" + FName->FullName() + "\"");
        }

        CommandPtr Copy() const
        {
            return std::make_shared<ImportCommand>(FName->Copy());
        }

        FNameIdentPtr FName;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================