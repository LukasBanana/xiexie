/*
 * Release command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_RELEASE_H__
#define __XX_AST_COMMAND_RELEASE_H__


#include "Command.h"
#include "BlockCommand.h"


namespace AbstractSyntaxTrees
{


//! The release command represents a class' destructor.
class ReleaseCommand : public Command
{
    
    public:
        
        ReleaseCommand(BlockCommandPtr InitCmdBlock) :
            Command         (InitCmdBlock->Pos(), Command::Types::ReleaseCmd),
            CmdBlock        (InitCmdBlock                                   ),
            VirtualDisabled (false                                          )
        {
        }
        ~ReleaseCommand()
        {
        }

        DefineASTVisitProc(ReleaseCommand)

        void PrintAST()
        {
            Deb("Release Command");
            ScopedIndent Unused;
            CmdBlock->PrintAST();
        }

        CommandPtr Copy() const
        {
            auto CopyObj = std::make_shared<ReleaseCommand>(
                std::dynamic_pointer_cast<BlockCommand>(CmdBlock->Copy())
            );

            CopyObj->ClassName = ClassName;
            CopyObj->VirtualDisabled = VirtualDisabled;

            return CopyObj;
        }

        BlockCommandPtr CmdBlock;

        std::string ClassName;      //!< Information for decorated AST.
        bool VirtualDisabled;       //!< Information for decorated AST.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================