/*
 * Call command AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_COMMAND_CALL_H__
#define __XX_AST_COMMAND_CALL_H__


#include "Command.h"
#include "FNameIdent.h"
#include "ArgumentList.h"


namespace AbstractSyntaxTrees
{


class CallCommand : public Command
{
    
    public:
        
        CallCommand(FNameIdentPtr FNameIdent, ArgumentListPtr ArgListAST) :
            Command (FNameIdent->Pos(), Command::Types::CallCmd ),
            FName   (FNameIdent                                 ),
            ArgList (ArgListAST                                 )
        {
        }
        ~CallCommand()
        {
        }

        DefineASTVisitProc(CallCommand)

        void PrintAST()
        {
            Deb("Function Call \"" + FName->FullName() + "\"");

            if (ArgList != nullptr)
            {
                ScopedIndent Unused;
                ArgList->PrintAST();
            }
        }

        CommandPtr Copy() const;

        FNameIdentPtr FName;
        ArgumentListPtr ArgList;    //!< Argument list (optional).

        FunctionObjectPtr Obj;      //!< Information for decorated AST (not usable for procedure type denoters!).

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================