/*
 * Object expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_OBJECT_H__
#define __XX_AST_EXPRESSION_OBJECT_H__


#include "Expression.h"
#include "FNameIdent.h"
#include "VariableDefCommand.h"


namespace AbstractSyntaxTrees
{


class ObjectExpression : public Expression
{
    
    public:
        
        ObjectExpression(FNameIdentPtr FNameAST) :
            Expression  (FNameAST->Pos(), Expression::Types::ObjectExpr ),
            FName       (FNameAST                                       ),
            Obj         (nullptr                                        )
        {
        }
        ~ObjectExpression()
        {
        }

        DefineASTVisitProc(ObjectExpression)

        void PrintAST()
        {
            Deb("Memory Object Expr \"" + FName->FullName() + "\"");
            ScopedIndent Unused;

            FName->PrintAST();

            if (AssignExpr != nullptr)
            {
                Deb("Assignment");
                ScopedIndent Unused2;
                AssignExpr->PrintAST();
            }
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            if (Obj != nullptr)
                CommonTDenoter = Obj->GetTypeDenoter();
            else
                throw ContextError("Invalid reference in object expression");
        }

        ExpressionPtr Copy() const
        {
            auto CopyObj = std::make_shared<ObjectExpression>(FName->Copy());

            if (AssignExpr != nullptr)
                CopyObj->AssignExpr = AssignExpr->Copy();

            return CopyObj;
        }

        bool IsConst() const
        {
            if (Obj != nullptr && Obj->Type() == Object::Types::VarObj)
            {
                auto VarObj = dynamic_cast<VariableObject*>(Obj);
                return VarObj->IsStaticConst();
            }
            return false;
        }

        static ObjectExpressionPtr Create(FNameIdentPtr FNameAST)
        {
            return std::make_shared<ObjectExpression>(FNameAST);
        }

        FNameIdentPtr FName;
        ExpressionPtr AssignExpr;

        Object* Obj;                //!< Information for the decorated AST (Reference to the object).

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================