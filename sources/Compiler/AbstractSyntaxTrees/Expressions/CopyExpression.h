/*
 * Copy expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_COPY_H__
#define __XX_AST_EXPRESSION_COPY_H__


#include "Expression.h"
#include "FNameIdent.h"


namespace AbstractSyntaxTrees
{


class CopyExpression : public Expression
{
    
    public:
        
        CopyExpression(FNameIdentPtr FNameAST) :
            Expression  (FNameAST->Pos(), Expression::Types::CopyExpr   ),
            FName       (FNameAST                                       )
        {
        }
        ~CopyExpression()
        {
        }

        DefineASTVisitProc(CopyExpression)

        virtual void PrintAST()
        {
            Deb("Copy Expr \"" + FName->FullName() + "\"");
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            auto Obj = FName->GetLastIdent()->Link;
            
            if (Obj == nullptr || Obj->Type() != Object::Types::VarObj)
                throw ContextError("Invalid object for copy expression");
            
            auto VarObj = dynamic_cast<VariableObject*>(Obj);

            //!TODO! -> generate ManagedPtrTypeDenoter here !!!
            CommonTDenoter = VarObj->TDenoter.get();
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<CopyExpression>(FName->Copy());
        }

        FNameIdentPtr FName; //!< Name of the object which is to be copied.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================