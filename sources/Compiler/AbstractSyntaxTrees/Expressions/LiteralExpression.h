/*
 * Literal expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_LITERAL_H__
#define __XX_AST_EXPRESSION_LITERAL_H__


#include "Expression.h"
#include "Operator.h"
#include "Literal.h"
#include "BoolLiteral.h"
#include "IntegerLiteral.h"
#include "FloatLiteral.h"
#include "StringLiteral.h"
#include "StringMod.h"
#include "ScopeManager.h"


namespace AbstractSyntaxTrees
{


class LiteralExpression : public Expression
{
    
    public:
        
        LiteralExpression(LiteralPtr LiteralAST) :
            Expression  (LiteralAST->Pos(), Expression::Types::LiteralExpr  ),
            Literal     (LiteralAST                                         )
        {
        }
        ~LiteralExpression()
        {
        }

        DefineASTVisitProc(LiteralExpression)

        virtual void PrintAST()
        {
            Deb("Literal Expr");
            ScopedIndent Unused;
            Literal->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            switch (Literal->Type())
            {
                case Literal::Types::Boolean:
                    SetupCommonTypeDenoterPrim(ScopeMngr, TypeDenoter::Types::BoolType);
                    break;
                case Literal::Types::Integer:
                    SetupCommonTypeDenoterPrim(ScopeMngr, TypeDenoter::Types::IntType);
                    break;
                case Literal::Types::Float:
                    SetupCommonTypeDenoterPrim(ScopeMngr, TypeDenoter::Types::FloatType);
                    break;
                case Literal::Types::String:
                    SetupCommonTypeDenoterPrim(ScopeMngr, TypeDenoter::Types::StringType);
                    break;
                case Literal::Types::Pointer:
                    SetupCommonTypeDenoterPrim(ScopeMngr, TypeDenoter::Types::MngPtrType);
                    break;
                default:
                    break;
            }
        }

        //! Creates an boolean literal expression.
        static LiteralExpressionPtr Create(bool Value)
        {
            auto Literal = std::make_shared<BoolLiteral>(SourcePosition::Ignore, xxStr(Value));
            return std::make_shared<LiteralExpression>(Literal);
        }

        //! Creates an integer literal expression.
        static LiteralExpressionPtr Create(int Value)
        {
            auto Literal = std::make_shared<IntegerLiteral>(SourcePosition::Ignore, xxStr(Value));
            return std::make_shared<LiteralExpression>(Literal);
        }

        //! Creates an integer literal expression (for 64 bit integers).
        static LiteralExpressionPtr Create(const long long int& Value)
        {
            auto Literal = std::make_shared<IntegerLiteral>(SourcePosition::Ignore, xxStr(Value));
            return std::make_shared<LiteralExpression>(Literal);
        }

        //! Creates an float literal expression.
        static LiteralExpressionPtr Create(float Value)
        {
            auto Literal = std::make_shared<FloatLiteral>(SourcePosition::Ignore, xxStr(Value));
            return std::make_shared<LiteralExpression>(Literal);
        }

        //! Creates an float literal expression (for 128 bit floats).
        static LiteralExpressionPtr Create(const long double& Value)
        {
            auto Literal = std::make_shared<FloatLiteral>(SourcePosition::Ignore, xxStr(Value));
            return std::make_shared<LiteralExpression>(Literal);
        }

        //! Creates an string literal expression.
        static LiteralExpressionPtr Create(const std::string& Value)
        {
            auto Literal = std::make_shared<StringLiteral>(SourcePosition::Ignore, xxStr(Value));
            return std::make_shared<LiteralExpression>(Literal);
        }

        //! Returns true since literals are always constant.
        bool IsConst() const
        {
            return true;
        }

        bool IsString() const
        {
            return Literal->Type() == Literal::Types::String;
        }

        bool IsPointer() const
        {
            return Literal->Type() == Literal::Types::Pointer;
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<LiteralExpression>(Literal->Copy());
        }

        LiteralPtr Literal;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================