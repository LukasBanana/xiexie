/*
 * Assignment expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_ASSIGN_H__
#define __XX_AST_EXPRESSION_ASSIGN_H__


#include "Expression.h"
#include "AssignOperator.h"


namespace AbstractSyntaxTrees
{


class AssignExpression : public Expression
{
    
    public:
        
        AssignExpression(AssignOperatorPtr ExprOp, ExpressionPtr E) :
            Expression  (ExprOp->Pos(), Expression::Types::AssignExpr   ),
            Op          (ExprOp                                         ),
            Expr        (E                                              )
        {
        }
        ~AssignExpression()
        {
        }

        DefineASTVisitProc(AssignExpression)

        virtual void PrintAST()
        {
            Deb("Assign Expr \"" + Op->Spell + "\"");
            ScopedIndent Unused;
            Expr->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            CommonTDenoter = Expr->CommonTDenoter;
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<AssignExpression>(
                std::dynamic_pointer_cast<AssignOperator>(Op->Copy()), Expr->Copy()
            );
        }

        AssignOperatorPtr Op;
        ExpressionPtr Expr;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================