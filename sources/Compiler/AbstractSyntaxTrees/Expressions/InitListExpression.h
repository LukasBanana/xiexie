/*
 * Initializer list expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_INITLIST_H__
#define __XX_AST_EXPRESSION_INITLIST_H__


#include "Expression.h"
#include "InitializerList.h"


namespace AbstractSyntaxTrees
{


class InitListExpression : public Expression
{
    
    public:
        
        InitListExpression(InitializerListPtr List) :
            Expression  (List->Pos(), Expression::Types::InitListExpr   ),
            InitList    (List                                           )
        {
        }
        ~InitListExpression()
        {
        }

        DefineASTVisitProc(InitListExpression)

        void PrintAST()
        {
            Deb("Initializer List Expr");
            ScopedIndent Unused;
            InitList->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            //!TODO! -> generate ArrayTypeDenoter here.
            //CommonTDenoter = ;
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<InitListExpression>(InitList->Copy());
        }

        InitializerListPtr InitList;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================