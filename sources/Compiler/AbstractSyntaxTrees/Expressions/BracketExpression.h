/*
 * Bracket expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_BRACKET_H__
#define __XX_AST_EXPRESSION_BRACKET_H__


#include "Expression.h"
#include "Operator.h"


namespace AbstractSyntaxTrees
{


class BracketExpression : public Expression
{
    
    public:
        
        BracketExpression(ExpressionPtr E) :
            Expression  (E->Pos(), Expression::Types::BracketExpr   ),
            Expr        (E                                          )
        {
        }
        ~BracketExpression()
        {
        }

        DefineASTVisitProc(BracketExpression)

        void PrintAST()
        {
            Deb("Bracket Expr");
            ScopedIndent Unused;
            Expr->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            CommonTDenoter = Expr->CommonTDenoter;
        }

        bool IsConst() const
        {
            return Expr->IsConst();
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<BracketExpression>(Expr->Copy());
        }

        ExpressionPtr Expr;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================