/*
 * Allocation expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_ALLOC_H__
#define __XX_AST_EXPRESSION_ALLOC_H__


#include "Expression.h"
#include "TypeDenoter.h"
#include "ArgumentList.h"
#include "ClassObject.h"


namespace AbstractSyntaxTrees
{


class AllocExpression : public Expression
{
    
    public:
        
        AllocExpression(TypeDenoterPtr TDenoterAST) :
            Expression  (TDenoterAST->Pos(), Expression::Types::AllocExpr   ),
            TDenoter    (TDenoterAST                                        )
        {
        }
        ~AllocExpression()
        {
        }

        DefineASTVisitProc(AllocExpression)

        void PrintAST()
        {
            Deb("Alloc Expr");
            ScopedIndent Unused;

            TDenoter->PrintAST();

            if (ArgList != nullptr)
                ArgList->PrintAST();

            if (AnonymousClass != nullptr)
            {
                Deb("Anonymous Class");
                ScopedIndent Unused2;
                AnonymousClass->PrintAST();
            }
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            CommonTDenoter = TDenoter.get();
        }

        ExpressionPtr Copy() const
        {
            auto CopyObj = std::make_shared<AllocExpression>(TDenoter->Copy());
            
            if (ArgList != nullptr)
                CopyObj->ArgList = ArgList->Copy();
            if (AnonymousClass != nullptr)
                CopyObj->AnonymousClass = std::dynamic_pointer_cast<ClassObject>(AnonymousClass->Copy());

            return CopyObj;
        }

        //!TODO! -> use ManagedPtrTypeDenoter as root type-denoter AST node !!!
        TypeDenoterPtr TDenoter;        //!< Type object which is to be allocated.
        ArgumentListPtr ArgList;        //!< Initialization argument list (optional).
        ClassObjectPtr AnonymousClass;  //!< Anonymous class object (optional).

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================