/*
 * Expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_H__
#define __XX_AST_EXPRESSION_H__


#include "AST.h"
#include "TypeDenoter.h"


namespace ContextualAnalyzer
{
    class ScopeManager;
}

namespace AbstractSyntaxTrees
{

using namespace ContextualAnalyzer;


class Expression : public AST
{
    
    public:
        
        struct Flags
        {
            typedef unsigned char DataType;
            static const DataType StringWrap    = (1 << 0); //!< This expression must be wrapped around a string (i.e. "Lang::__XX__Str(...)").
        };

        struct ParseFlags
        {
            typedef unsigned char DataType;
            static const DataType IsOptional        = (1 << 0);
            static const DataType IgnoreRelationOp  = (1 << 1);
        };

        enum class Types
        {
            // Operator expressions
            AssignExpr,     // :=, +=, -=, *=, /=, %=, <<=, >>=, |=, &=, ^=
            BinaryExpr,     // ||, &&, |, ^, &, =, !=, <, >, <=, >=, <<, >>, +, -, *, /, %
            UnaryExpr,      // !, -, +, ~, ++, --
            StrSumExpr,     // +

            // Other expressions
            AllocExpr,      // new
            CopyExpr,       // copy
            LiteralExpr,
            BracketExpr,
            CallExpr,
            CastExpr,
            ObjectExpr,
            ValidExpr,
            InitListExpr,
            LambdaExpr,
        };

        virtual ~Expression()
        {
        }

        //! \deprecated Use "SetupCommonTypeDenoter"
        const TypeDenoter* GetTypeDenoter(const ScopeManager& ScopeMngr) const
        {
            return CommonTDenoter;
        }

        /**
        Sets up the common type denoter reference.
        \throw SyntaxError if an error occured.
        \throw CompilerWarning if a warning is thrown.
        \see CommonTDenoter
        */
        virtual void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr) = 0;

        //! Returns true if this expression is a (static) constant expression.
        virtual bool IsConst() const
        {
            return false;
        }

        //! Returns true if this is a string expression.
        virtual bool IsString() const
        {
            return false;
        }

        inline Types Type() const
        {
            return Type_;
        }

        virtual ExpressionPtr Copy() const = 0;

        //!TODO! -> must be included inside the "Copy" functions!!! (NOT DONE YET)
        Flags::DataType DefFlags;

        //!TODO! -> must be included inside the "Copy" functions!!! (NOT DONE YET)
        const TypeDenoter* CommonTDenoter;  //!< Information for the decorated AST (Reference to the common expression type-denoter).

    protected:

        Expression(const SourcePosition &Pos, const Types &Type) :
            AST             (Pos    ),
            Type_           (Type   ),
            DefFlags        (0      ),
            CommonTDenoter  (nullptr)
        {
        }

        void SetupCommonTypeDenoterPrim(const ScopeManager& ScopeMngr, const TypeDenoter::Types& Type);

    private:
        
        Types Type_;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================