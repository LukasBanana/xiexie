/*
 * Call expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_CALL_H__
#define __XX_AST_EXPRESSION_CALL_H__


#include "Expression.h"
#include "CallCommand.h"
#include "FunctionObject.h"


namespace AbstractSyntaxTrees
{


class CallExpression : public Expression
{
    
    public:
        
        CallExpression(CallCommandPtr CallCmd) :
            Expression  (CallCmd->Pos(), Expression::Types::CallExpr),
            Call        (CallCmd                                    )
        {
        }
        ~CallExpression()
        {
        }

        DefineASTVisitProc(CallExpression)

        virtual void PrintAST()
        {
            Deb("Call Expr");
            ScopedIndent Unused;
            Call->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            if (Call->Obj != nullptr)
                CommonTDenoter = Call->Obj->TDenoter.get();
            //!TODO! -> does currently not work for standard procedures !!!
            /*else
                throw ContextError("Unknown procedure for call expression");*/
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<CallExpression>(
                std::dynamic_pointer_cast<CallCommand>(Call->Copy())
            );
        }

        CallCommandPtr Call;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================