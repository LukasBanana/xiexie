/*
 * Lambda expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_LAMBDA_H__
#define __XX_AST_EXPRESSION_LAMBDA_H__


#include "Expression.h"
#include "BlockCommand.h"
#include "ProcTypeDenoter.h"


namespace AbstractSyntaxTrees
{


class LambdaExpression : public Expression
{
    
    public:
        
        LambdaExpression(ProcTypeDenoterPtr ProcTDenoterAST, BlockCommandPtr BlockCmdAST) :
            Expression  (ProcTDenoterAST->Pos(), Expression::Types::LambdaExpr  ),
            ProcTDenoter(ProcTDenoterAST                                        ),
            BlockCmd    (BlockCmdAST                                            )
        {
            ProcTDenoter->DefFlags |= ProcTypeDenoter::Flags::LambdaHeader;
        }
        ~LambdaExpression()
        {
        }

        DefineASTVisitProc(LambdaExpression)

        virtual void PrintAST()
        {
            Deb("Lambda Expr");
            ScopedIndent Unused;

            ProcTDenoter->PrintAST();
            BlockCmd->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            CommonTDenoter = ProcTDenoter.get();
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<LambdaExpression>(
                std::dynamic_pointer_cast<ProcTypeDenoter>(ProcTDenoter->Copy()),
                std::dynamic_pointer_cast<BlockCommand>(BlockCmd->Copy())
            );
        }

        ProcTypeDenoterPtr ProcTDenoter;
        BlockCommandPtr BlockCmd;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================