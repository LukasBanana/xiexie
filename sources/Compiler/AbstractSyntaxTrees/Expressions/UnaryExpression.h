/*
 * Unary expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_UNARY_H__
#define __XX_AST_EXPRESSION_UNARY_H__


#include "Expression.h"
#include "UnaryOperator.h"
#include "StringMod.h"


namespace AbstractSyntaxTrees
{


class UnaryExpression : public Expression
{
    
    public:
        
        enum class Notations
        {
            Prefix,
            Postfix
        };

        UnaryExpression(UnaryOperatorPtr ExprOp, ExpressionPtr E, const Notations& OpNotation = Notations::Prefix) :
            Expression  (ExprOp->Pos(), Expression::Types::UnaryExpr),
            Op          (ExprOp                                     ),
            Expr        (E                                          ),
            Notation    (OpNotation                                 )
        {
        }
        ~UnaryExpression()
        {
        }

        DefineASTVisitProc(UnaryExpression)

        void PrintAST()
        {
            Deb("Unary Expr \"" + Op->Spell + "\" (" + xxStr(Notation == Notations::Prefix ? "Prefix" : "Postfix") + ")");
            ScopedIndent Unused;
            Expr->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            CommonTDenoter = Expr->CommonTDenoter;
        }

        bool IsConst() const
        {
            return Expr->IsConst();
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<UnaryExpression>(
                std::dynamic_pointer_cast<UnaryOperator>(Op->Copy()), Expr->Copy(), Notation
            );
        }

        UnaryOperatorPtr Op;
        ExpressionPtr Expr;
        Notations Notation;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================