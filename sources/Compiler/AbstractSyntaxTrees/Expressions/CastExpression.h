/*
 * Cast expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_CAST_H__
#define __XX_AST_EXPRESSION_CAST_H__


#include "Expression.h"
#include "TypeDenoter.h"


namespace AbstractSyntaxTrees
{


class CastExpression : public Expression
{
    
    public:
        
        //! Type casting classifications.
        enum class TypeCasts
        {
            StaticCast,         //!< 'static_cast' or 'std::static_pointer_cast' must be used.
            ConstCast,          //!< 'const_cast' or 'std::const_pointer_cast' must be used.
            DynamicCast,        //!< 'dynamic_cast' or 'std::dynamic_pointer_cast' must be used.
            ReinterpretCast,    //!< 'reinterpret_cast' must be used.
        };

        CastExpression(TypeDenoterPtr T, ExpressionPtr E) :
            Expression      (E->Pos(), Expression::Types::CastExpr  ),
            TDenoter        (T                                      ),
            Expr            (E                                      ),
            Casting         (TypeCasts::StaticCast                  ),
            IsSmartPointer  (false                                  )
        {
        }
        ~CastExpression()
        {
        }

        DefineASTVisitProc(CastExpression)

        virtual void PrintAST()
        {
            Deb("Type Cast Expr");
            ScopedIndent Unused;
            TDenoter->PrintAST();
            Expr->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            CommonTDenoter = TDenoter.get();

            //!TODO! -> check cast compatibility !!!
        }

        bool IsConst() const
        {
            return Expr->IsConst();
        }

        ExpressionPtr Copy() const
        {
            auto CopyObj = std::make_shared<CastExpression>(TDenoter->Copy(), Expr->Copy());

            CopyObj->Casting = Casting; //??? { -> IDE (VisualStudio 2012) marks an error for assignment but compiles without error }
            CopyObj->IsSmartPointer = IsSmartPointer;

            return CopyObj;
        }

        TypeDenoterPtr TDenoter;    //!< Type denoter to which the expression will be casted.
        ExpressionPtr Expr;         //!< Expression which is to be casted.

        TypeCasts Casting;          //!< Information for the decorated AST.
        bool IsSmartPointer;        //!< Information for the decorated AST.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================