/*
 * Validation expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_VALIDATION_H__
#define __XX_AST_EXPRESSION_VALIDATION_H__


#include "Expression.h"
#include "ObjectExpression.h"


namespace AbstractSyntaxTrees
{


class ValidationExpression : public Expression
{
    
    public:
        
        ValidationExpression(ObjectExpressionPtr ObjExprAST) :
            Expression  (ObjExprAST->Pos(), Expression::Types::ValidExpr),
            ObjExpr     (ObjExprAST                                     )
        {
        }
        ~ValidationExpression()
        {
        }

        DefineASTVisitProc(ValidationExpression)

        virtual void PrintAST()
        {
            Deb("Validation Expr");
            ScopedIndent Unused;
            ObjExpr->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            SetupCommonTypeDenoterPrim(ScopeMngr, TypeDenoter::Types::BoolType);
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<ValidationExpression>(
                std::dynamic_pointer_cast<ObjectExpression>(ObjExpr->Copy())
            );
        }

        ObjectExpressionPtr ObjExpr;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================