/*
 * Binary expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_BINARY_H__
#define __XX_AST_EXPRESSION_BINARY_H__


#include "Expression.h"
#include "BinaryOperator.h"
#include "ScopeManager.h"
#include "CompilerMessage.h"


namespace AbstractSyntaxTrees
{


/**
Binary expression order:
-# Logic OR: ||
-# Logic AND: &&
-# Bitwise OR: |
-# Bitwise XOR: ^
-# Bitwise AND: &
-# Equality comparator: =, !=
-# Relational comparator: <, >, <=, >=
-# Bit shift: <<, >>
-# Addition: +
-# Subtraction: -
-# Multiplication: *
-# Division and modulo: /, %
*/
class BinaryExpression : public Expression
{
    
    public:
        
        BinaryExpression(ExpressionPtr E1, BinaryOperatorPtr ExprOp, ExpressionPtr E2) :
            Expression  (E1->Pos(), Expression::Types::BinaryExpr   ),
            Op          (ExprOp                                     ),
            Expr1       (E1                                         ),
            Expr2       (E2                                         )
        {
        }
        ~BinaryExpression()
        {
        }

        DefineASTVisitProc(BinaryExpression)

        void PrintAST()
        {
            Deb("Binary Expr \"" + Op->Spell + "\"");
            ScopedIndent Unused;
            Expr1->PrintAST();
            Expr2->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            /* Get type-denoters and type-IDs */
            auto TDen1 = Expr1->CommonTDenoter;
            auto TDen2 = Expr2->CommonTDenoter;

            /* Check sub-expression types validity */
            if (TDen1 == nullptr || TDen2 == nullptr)
                //throw ContextError("Invalid sub-expression common type-denoters");
                throw CompilerWarning("Invalid sub-expression common type-denoters"); //!!!

            auto T1 = TDen1->GetResolvedType()->Type();
            auto T2 = TDen2->GetResolvedType()->Type();

            /* Check which kind of binary expression this is */
            if (Op->IsEquality())
            {
                /* Setup primitive boolean type denoter */
                SetupCommonTypeDenoterPrim(ScopeMngr, TypeDenoter::Types::BoolType);
            }
            else if (Op->IsRelation())
            {
                /* Check for type compatibility */
                if ( T1 == TypeDenoter::Types::BoolType || ( T2 == TypeDenoter::Types::BoolType && !IsRelationWithFollowingRelation() ) )
                    throw ContextError("Boolean types can not be compared in a <, >, <= or >= relation");
                
                /* Setup primitive boolean type denoter */
                SetupCommonTypeDenoterPrim(ScopeMngr, TypeDenoter::Types::BoolType);
            }
            else
            {
                /* Check for bitwise and modulo operator */
                if (Op->IsBitwise() || Op->Spell == "%")
                {
                    auto CheckTypeForOp = [&](const TypeDenoter* TDen)
                    {
                        if (!BinaryExpression::IsValidModuloAndBitwiseType(TDen->Type()))
                            throw ContextError("Operator '" + Op->Spell + "' not supported for '" + TDen->Spell + "' type-denoter");
                    };

                    CheckTypeForOp(TDen1);
                    CheckTypeForOp(TDen2);
                }

                /* Check for string concatenation */
                if (T1 == TypeDenoter::Types::StringType)
                    CommonTDenoter = TDen1;
                else if (T2 == TypeDenoter::Types::StringType)
                    CommonTDenoter = TDen2;
                /* Check for arithmetic types */
                else if ( ( T1 == TypeDenoter::Types::FloatType || T1 == TypeDenoter::Types::IntType ) &&
                          ( T2 == TypeDenoter::Types::FloatType || T2 == TypeDenoter::Types::IntType ) )
                {
                    /* Take leading left common type */
                    CommonTDenoter = TDen1;
                }
                else
                {
                    //!TODO! -> this should be a temporary solution !!!
                    /* Per default use the leading type */
                    CommonTDenoter = TDen1;
                }
            }

            /* Check for arithmetic type conversions */
            if ( T1 != T2 && 
                 ( T1 == TypeDenoter::Types::FloatType || T1 == TypeDenoter::Types::IntType ) &&
                 ( T2 == TypeDenoter::Types::FloatType || T2 == TypeDenoter::Types::IntType ) )
            {
                /* Throw warning on possible data loss */
                throw CompilerWarning("Type conversion between floating-point and integer -> possible data loss");
            }
        }

        bool IsConst() const
        {
            return Expr1->IsConst() && Expr2->IsConst();
        }

        /**
        Returns true if this is a relational binary epxression (i.e. with operator '<', '>', '<=' or '>=')
        and the 2nd sub-expression is also a relational binary expression,
        e.g. for "0 < num < 10" the AST node for the first occurance of '<'
        will return true, but the second one will return false.
        */
        bool IsRelationWithFollowingRelation(BinaryExpression** SubBinExpr = nullptr) const
        {
            if (Op->IsRelation() && Expr2->Type() == Expression::Types::BinaryExpr)
            {
                auto BinExpr2 = dynamic_cast<BinaryExpression*>(Expr2.get());
                if (BinExpr2->Op->IsRelation())
                {
                    if (SubBinExpr != nullptr)
                        *SubBinExpr = BinExpr2;
                    return true;
                }
            }
            return false;
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<BinaryExpression>(
                Expr1->Copy(), std::dynamic_pointer_cast<BinaryOperator>(Op->Copy()), Expr2->Copy()
            );
        }

        BinaryOperatorPtr Op;
        ExpressionPtr Expr1, Expr2;

    private:
        
        static bool IsValidModuloAndBitwiseType(const TypeDenoter::Types& T)
        {
            return
                T == TypeDenoter::Types::IntType ||
                T == TypeDenoter::Types::CustomType;
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================