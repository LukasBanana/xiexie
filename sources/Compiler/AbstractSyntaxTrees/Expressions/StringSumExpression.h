/*
 * String sum expression AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_EXPRESSION_STRING_SUM_H__
#define __XX_AST_EXPRESSION_STRING_SUM_H__


#include "Expression.h"
#include "ScopeManager.h"


namespace AbstractSyntaxTrees
{


//! \deprecated
class StringSumExpression : public Expression
{
    
    public:
        
        StringSumExpression(ExpressionPtr E1, ExpressionPtr E2) :
            Expression  (E1->Pos(), Expression::Types::StrSumExpr   ),
            Expr1       (E1                                         ),
            Expr2       (E2                                         )
        {
        }
        ~StringSumExpression()
        {
        }

        DefineASTVisitProc(StringSumExpression)

        virtual void PrintAST()
        {
            Deb("String Sum Expr");
            ScopedIndent Unused;
            Expr1->PrintAST();
            Expr2->PrintAST();
        }

        void SetupCommonTypeDenoter(const ScopeManager& ScopeMngr)
        {
            SetupCommonTypeDenoterPrim(ScopeMngr, TypeDenoter::Types::StringType);
        }

        bool IsConst() const
        {
            return Expr1->IsConst() && Expr2->IsConst();
        }

        bool IsString() const
        {
            return true;
        }

        ExpressionPtr Copy() const
        {
            return std::make_shared<StringSumExpression>(Expr1->Copy(), Expr2->Copy());
        }

        ExpressionPtr Expr1, Expr2;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================