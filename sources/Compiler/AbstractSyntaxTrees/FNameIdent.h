/*
 * FNameIdent AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_FNAMEIDENT_H__
#define __XX_AST_FNAMEIDENT_H__


#include "AST.h"
#include "Identifier.h"
#include "TemplateParameterList.h"
#include "TemplateArgumentList.h"
#include "ArrayIndexList.h"


namespace AbstractSyntaxTrees
{


//! Function name identifier (can also be used for 'import' command, or static member identifiers).
class FNameIdent : public AST
{
    
    public:
        
        struct Flags
        {
            typedef unsigned char DataType;
            static const DataType FlagsTypeDenoter;
            static const DataType FlagsEntry;
            static const DataType FlagsProc;
            static const DataType EnumEntry;
            static const DataType EnumProc;
        };

        //! FName parsing input flags.
        struct InFlags
        {
            typedef unsigned char DataType;
            static const DataType AllowTemplateParams;
            static const DataType AllowTemplateArgs;
        };

        //! FName parsing output flags.
        struct OutFlags
        {
            typedef unsigned char DataType;
            static const DataType HasMultiIdent;
            static const DataType HasTemplateParams;
            static const DataType HasTemplateArgs;
        };

        FNameIdent(IdentifierPtr NameIdent, FNameIdentPtr NextName = nullptr) :
            AST         (NameIdent->Pos()   ),
            Ident       (NameIdent          ),
            Next        (NextName           ),
            Link        (nullptr            ),
            IdentFlags  (0                  )
        {
        }
        ~FNameIdent()
        {
        }

        DefineASTVisitProc(FNameIdent)

        void PrintAST()
        {
            Deb("FName Identifier \"" + Ident->Spell + "\"");
            ScopedIndent Unused;

            if (TParamList != nullptr)
                TParamList->PrintAST();
            if (TArgList != nullptr)
                TArgList->PrintAST();
            if (ArrayList != nullptr)
                ArrayList->PrintAST();

            if (Next != nullptr)
                Next->PrintAST();
        }

        /**
        Returns the full function identifier name, e.g. "ClassX.FunctionA" or "FunctionB" but
        without template parameters and array indices.
        */
        std::string FullName() const
        {
            std::string Str;

            Str += Ident->Spell;
            if (Next != nullptr)
                Str += "." + Next->FullName();

            return Str;
        }

        bool IsResolved() const
        {
            if (Next != nullptr && !Next->IsResolved())
                return false;
            return Link != nullptr;
        }

        FNameIdentPtr GetLastIdent()
        {
            if (Next == nullptr)
                return ThisPtr<FNameIdent>();
            return Next->GetLastIdent();
        }

        /**
        Inserts the new FName identifier (like 'push_front').
        After insertion the new FName identifier points to this FName.
        \code
        FNameAST = FNameAST->InsertFName(NewParentFNameAST);
        \endcode
        */
        FNameIdentPtr InsertFName(FNameIdentPtr FNameIdentAST)
        {
            if (FNameIdentAST != nullptr)
            {
                FNameIdentAST->GetLastIdent()->Next = ThisPtr<FNameIdent>();
                return FNameIdentAST;
            }
            return nullptr;
        }

        FNameIdentPtr Copy() const
        {
            auto CopyObj = std::make_shared<FNameIdent>(Ident->Copy());

            CopyObj->Link       = Link;
            CopyObj->IdentFlags = IdentFlags;

            if (TParamList != nullptr)
                CopyObj->TParamList = TParamList->Copy();
            if (TArgList != nullptr)
                CopyObj->TArgList = TArgList->Copy();
            if (ArrayList != nullptr)
                CopyObj->ArrayList = ArrayList->Copy();

            if (Next != nullptr)
                CopyObj->Next = Next->Copy();

            return CopyObj;
        }

        //! Returns true if this FName has scopes, i.e. further FName identifier follow in the linked list.
        inline bool HasScopes() const
        {
            return Next != nullptr;
        }

        //! Returns true if this is the last FName identifier in the linked list.
        inline bool IsLast() const
        {
            return Next == nullptr;
        }

        static FNameIdentPtr Create(const std::string& SingleIdent)
        {
            /* Create single identifier for FName */
            auto Ident = Identifier::Create(SingleIdent);
            return std::make_shared<FNameIdent>(Ident);
        }

        //! Creates an FName identifier linked list with the giben iterator 'Begin' and 'End'.
        template <typename T> static FNameIdentPtr Create(T Begin, T End)
        {
            FNameIdentPtr RootFName, FName, PrevFName;
            
            while (Begin != End)
            {
                /* Create next FName identifier */
                FName = FNameIdent::Create(*Begin);
                
                /* Store root FName AST node */
                if (RootFName == nullptr)
                    RootFName = FName;

                /* Conncet previous FName with the current FName identifier */
                if (PrevFName != nullptr)
                    PrevFName->Next = FName;
                PrevFName = FName;

                ++Begin;
            }

            return RootFName;
        }

        IdentifierPtr Ident;                //!< Identifier.

        TemplateParamListPtr TParamList;    //!< Optional template parameter list.
        TemplateArgListPtr TArgList;        //!< Optional template argument list.
        ArrayIndexListPtr ArrayList;        //!< Optional array index list.

        FNameIdentPtr Next;                 //!< Optional next FNameIdent.

        Object* Link;                       //!< Information for the decorated AST.
        Flags::DataType IdentFlags;         //!< Information for the decorated AST.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================
