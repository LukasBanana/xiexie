/*
 * Terminal AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_TERMINAL_H__
#define __XX_AST_TERMINAL_H__


#include "AST.h"


namespace AbstractSyntaxTrees
{


class Terminal : public AST
{
    
    public:
        
        Terminal(const SourcePosition &Pos, const std::string &Spelling) :
            AST     (Pos        ),
            Spell   (Spelling   )
        {
        }
        virtual ~Terminal()
        {
        }

        std::string Spell;  //!< Terminal spelling.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================