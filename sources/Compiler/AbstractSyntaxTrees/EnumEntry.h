/*
 * Enumeration entry AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_ENUMENTRY_H__
#define __XX_AST_ENUMENTRY_H__


#include "AST.h"
#include "Identifier.h"
#include "Expression.h"
#include "StringLiteral.h"


namespace AbstractSyntaxTrees
{


class EnumEntry : public AST
{
    
    public:
        
        EnumEntry(IdentifierPtr EntryIdent) :
            AST     (EntryIdent->Pos()  ),
            Ident   (EntryIdent         )
        {
        }
        ~EnumEntry()
        {
        }

        DefineASTVisitProc(EnumEntry)

        void PrintAST()
        {
            Deb("Enum Entry \"" + Ident->Spell + "\"");
            ScopedIndent Unused;

            if (Id != nullptr)
                Id->PrintAST();
            if (Expr != nullptr)
                Expr->PrintAST();
        }

        EnumEntryPtr Copy() const
        {
            auto CopyObj = std::make_shared<EnumEntry>(Ident->Copy());

            if (Id != nullptr)
                CopyObj->Id = std::dynamic_pointer_cast<StringLiteral>(Id->Copy());
            if (Expr != nullptr)
                CopyObj->Expr = Expr->Copy();

            return CopyObj;
        }

        IdentifierPtr Ident;
        StringLiteralPtr Id;    //!< Optional identification string.
        ExpressionPtr Expr;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================