/*
 * Package object AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OBJECT_PACKAGE_H__
#define __XX_AST_OBJECT_PACKAGE_H__


#include "Object.h"


namespace AbstractSyntaxTrees
{


//! Package objects are actually only used for contextual analysis, i.e. they will not appear in the AST.
class PackageObject : public Object
{
    
    public:
        
        PackageObject(IdentifierPtr Name) :
            Object(Name, Name->Pos(), Object::Types::PackageObj)
        {
        }
        ~PackageObject()
        {
        }

        std::string TypeDesc() const
        {
            return "Package";
        }

        //! Packages are distributable.
        bool IsDistrib() const
        {
            return true;
        }

        // !TODO! -> seperate these three functions in to parent
        //   class "NamespaceObject" (PackageObject -> NamespaceObject -> Object -> AST)!!!

        //! \throw std::string
        bool DecorateSub(FNameIdentPtr FName)
        {
            if (FName == nullptr)
                return false;

            const auto& Spell = FName->Ident->Spell;

            /* Search for member object */
            auto it = MemberObjects.find(Spell);

            if (it == MemberObjects.end())
                throw std::string("Package \"" + Name() + "\" has no member named \"" + Spell + "\"");

            /* Decorate FName with member object */
            return it->second->Decorate(FName);
        }

        //! \throw std::string
        ObjectPtr FindSub(FNameIdentPtr FName)
        {
            if (FName == nullptr)
                return nullptr;

            const auto& Spell = FName->Ident->Spell;

            /* Search for member object */
            auto it = MemberObjects.find(Spell);

            if (it == MemberObjects.end())
                throw std::string("Package \"" + Name() + "\" has no member named \"" + Spell + "\"");

            /* Decorate FName with member object */
            return it->second->Find(FName);
        }

        void AddMember(Object* Obj)
        {
            if (Obj != nullptr)
            {
                /* Insert object to member-table */
                MemberObjects[Obj->Name()] = Obj;
            }
        }

        ObjectPtr Copy() const
        {
            return std::make_shared<PackageObject>(Ident->Copy());
        }

        std::map<std::string, Object*> MemberObjects;   //!< Information for the decorated AST (all member variables, functions, classes and sub-packages).

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================