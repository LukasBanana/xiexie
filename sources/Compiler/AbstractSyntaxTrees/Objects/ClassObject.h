/*
 * Class object AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OBJECT_CLASS_H__
#define __XX_AST_OBJECT_CLASS_H__


#include "Object.h"
#include "FNameIdent.h"
#include "ClassBlockCommand.h"
#include "CustomTypeDenoter.h"
#include "VariableObject.h"
#include "AttributeList.h"

#include <vector>


namespace AbstractSyntaxTrees
{


class ClassObject : public Object
{
    
    public:
        
        struct Flags
        {
            typedef unsigned char DataType;
            static const DataType CopyProcMarker    = (1 << 0);
            static const DataType IsPattern         = (1 << 1); //!< This class has a pattern definition (e.g. "class Test<pattern T> { ... }").
            static const DataType Instantiated      = (1 << 2); //!< This class was instantiated by a template.
            static const DataType IsAbstract        = (1 << 3); //!< This class is abstract since it has abstract procedures.
        };

        //! Constructor for standard class objects.
        ClassObject(IdentifierPtr Name, ClassBlockCommandPtr ClassBlock) :
            Object          (Name, Name->Pos(), Object::Types::ClassObj ),
            ClassBlockCmd   (ClassBlock                                 ),
            TDenoter        (nullptr                                    ),
            DefFlags        (0                                          )
        {
            ClassBlockCmd->Parent = this;
        }
        //! Constructor for anonymous class objects.
        ClassObject(ClassBlockCommandPtr ClassBlock) :
            Object          (nullptr, ClassBlock->Pos(), Object::Types::ClassObj),
            ClassBlockCmd   (ClassBlock                                         ),
            TDenoter        (nullptr                                            ),
            DefFlags        (0                                                  )
        {
            ClassBlockCmd->Parent = this;
        }
        ~ClassObject()
        {
        }

        DefineASTVisitProc(ClassObject)

        void PrintAST()
        {
            if (Ident != nullptr)
                Deb("Class Obj \"" + Ident->Spell + "\"");
            else
                Deb("Class Obj < Anonymous >");
            ScopedIndent Unused;

            if (TParamList != nullptr)
                TParamList->PrintAST();
            if (AttribList != nullptr)
                AttribList->PrintAST();

            if (!Inheritance.empty())
            {
                Deb("Inherit Classes:");
                ScopedIndent Unused2;

                for (auto Parent : Inheritance)
                    Parent->PrintAST();
            }

            ClassBlockCmd->PrintAST();
        }

        TypeDenoter* GetTypeDenoter() const
        {
            return TDenoter;
        }

        std::string TypeDesc() const
        {
            return "Class";
        }

        Object* FindMember(const std::string& Name) const
        {
            /* Search in local member object list */
            auto it = MemberObjects.find(Name);
            if (it != MemberObjects.end())
                return it->second;

            /* Search in parent class member object lists */
            for (auto ParentFName : Inheritance)
            {
                /* Get parent class object */
                auto ParentObj = ParentFName->GetLastIdent()->Link;
                if (ParentObj != nullptr)
                {
                    /* Search in current parent class' member object list */
                    auto MemberObj = ParentObj->FindMember(Name);
                    if (MemberObj != nullptr)
                        return MemberObj;
                }
            }

            return nullptr;
        }

        //! \throw std::string
        bool DecorateSub(FNameIdentPtr FName)
        {
            if (FName == nullptr)
                return false;

            const auto& Spell = FName->Ident->Spell;

            /* Search for member object */
            auto Obj = FindMember(Spell);

            if (Obj == nullptr)
                throw std::string("Class \"" + Name() + "\" has no member named \"" + Spell + "\"");

            /* Decorate FName with member object */
            return Obj->Decorate(FName);
        }

        //! \throw std::string
        ObjectPtr FindSub(FNameIdentPtr FName)
        {
            if (FName == nullptr)
                return nullptr;

            const auto& Spell = FName->Ident->Spell;

            /* Search for member object */
            auto Obj = FindMember(Spell);

            if (Obj == nullptr)
                throw std::string("Class \"" + Name() + "\" has no member named \"" + Spell + "\"");

            /* Decorate FName with member object */
            return Obj->Find(FName);
        }

        void AddMember(Object* Obj)
        {
            if (Obj != nullptr)
            {
                /* Insert object to member-table */
                MemberObjects[Obj->Name()] = Obj;
            }
        }

        bool IsClassDenoter() const
        {
            return true;
        }

        /**
        Marks this and all sub classes to get the 'copy' procedure.
        \return True if this class can have the copy procedure.
        */
        bool MarkForCopyProc()
        {
            /* Check if it's already marked (to avoid cycles) */
            if ((DefFlags & Flags::CopyProcMarker) != 0)
                return true;

            //!TODO! -> check if this class was marked with attribute to be 'unique'
            // (i.e. those obejcts can not be copied using the 'copy' keyword).
            // if (is this class unique?) return false;
            //!TODO! -> also check if this class has the default constructor (with no parameters)!!!

            /* Mark this class object with the copy-proc marker falg */
            DefFlags |= Flags::CopyProcMarker;

            /* Mark all member classes */
            for (auto it : MemberObjects)
            {
                /* Find member variable */
                auto Obj = it.second;
                if (Obj->Type() != Object::Types::VarObj)
                    continue;

                auto VarObj = dynamic_cast<VariableObject*>(Obj);

                /* Get custom type denoter */
                auto TDenoter = VarObj->TDenoter->GetResolvedType();
                if (TDenoter->Type() != TypeDenoter::Types::CustomType)
                    continue;

                auto CTDenoter = dynamic_cast<CustomTypeDenoter*>(TDenoter);

                /* Check if this type denoter is a class */
                auto SubObj = CTDenoter->Link.get();
                if (SubObj == nullptr || SubObj->Type() != Object::Types::ClassObj)
                    continue;

                auto ClassObj = dynamic_cast<ClassObject*>(SubObj);

                /* Mark member's class for 'copy' procedure */
                if (ClassObj->MarkForCopyProc())
                    return false;
            }
            
            return true;
        }

        ObjectPtr Copy() const
        {
            auto CopyObj = std::make_shared<ClassObject>(
                Ident->Copy(), std::dynamic_pointer_cast<ClassBlockCommand>(ClassBlockCmd->Copy())
            );

            CopyObj->DefFlags = DefFlags;

            for (auto Entry : Inheritance)
                CopyObj->Inheritance.push_back(Entry->Copy());

            if (AttribList != nullptr)
                CopyObj->AttribList = AttribList->Copy();
            if (TParamList != nullptr)
                CopyObj->TParamList = TParamList->Copy();

            return CopyObj;
        }

        TemplateParamListPtr TParamList;                //!< Optional template parameter list.
        std::vector<FNameIdentPtr> Inheritance;         //!< Optional inherit class names.
        AttributeListPtr AttribList;                    //!< Optional Attribute list.

        ClassBlockCommandPtr ClassBlockCmd;

        CustomTypeDenoter* TDenoter;                    //!< Type denoter for this class.

        std::map<std::string, Object*> MemberObjects;   //!< Information for the decorated AST (all member variables, functions and classes).
        std::string AnonymousId;                        //!< Information for the decorated AST (anonymous ID string).

        Flags::DataType DefFlags;                       //!< Definition flags.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================