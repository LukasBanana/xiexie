/*
 * Function object AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OBJECT_FUNCTION_H__
#define __XX_AST_OBJECT_FUNCTION_H__


#include "Object.h"
#include "FNameIdent.h"
#include "BlockCommand.h"
#include "ParameterList.h"


namespace AbstractSyntaxTrees
{


class FunctionObject : public Object
{
    
    public:
        
        struct Flags
        {
            typedef unsigned char DataType;
            static const DataType IsClassMember = (1 << 0); //!< This procedure is a member of a class.
            static const DataType IsReadonly    = (1 << 1); //!< This procedure is read-only. Requires 'IsClassMember'.
            static const DataType IsVirtual     = (1 << 2); //!< This procedure is virtual. Requires 'IsClassMember'.
            static const DataType IsAbstract    = (1 << 3); //!< This procedure is abstract (pure virtual). Requires 'IsClassMember'.
        };

        enum class StdProcs
        {
            None,
            String,
            Flags,
            Enum
        };

        FunctionObject(TypeDenoterPtr ReturnType, IdentifierPtr Name, BlockCommandPtr FuncBody = nullptr) :
            Object      (Name, ReturnType->Pos(), Object::Types::FuncObj),
            TDenoter    (ReturnType                                     ),
            CmdBlock    (FuncBody                                       ),
            DefFlags    (0                                              ),
            StdProcType (StdProcs::None                                 )
        {
        }
        ~FunctionObject()
        {
        }

        DefineASTVisitProc(FunctionObject)

        void PrintAST()
        {
            Deb("Function Obj \"" + Ident->Spell + "\"");
            ScopedIndent Unused;

            TDenoter->PrintAST();
            if (ParamList != nullptr)
                ParamList->PrintAST();

            if (CmdBlock != nullptr)
                CmdBlock->PrintAST();
        }

        std::string TypeDesc() const
        {
            return "Procedure";
        }

        ObjectPtr Copy() const
        {
            auto CopyObj = std::make_shared<FunctionObject>(TDenoter->Copy(), Ident->Copy());

            CopyObj->DefFlags = DefFlags;

            if (CmdBlock != nullptr)
                CopyObj->CmdBlock = std::dynamic_pointer_cast<BlockCommand>(CmdBlock->Copy());
            if (FName != nullptr)
                CopyObj->FName = FName->Copy();

            return CopyObj;
        }

        //! Is this just a declaration function?
        inline bool IsDecl() const
        {
            return CmdBlock == nullptr && (DefFlags & Flags::IsAbstract) == 0;
        }

        inline std::string FullName() const
        {
            return FName != nullptr ? FName->FullName() : Ident->Spell;
        }

        inline bool IsStdProc() const
        {
            return StdProcType != StdProcs::None;
        }

        TypeDenoterPtr TDenoter;    //!< Return type denoter.
        ParameterListPtr ParamList; //!< Parameter list.
        BlockCommandPtr CmdBlock;   //!< Function body.

        FNameIdentPtr FName;        //!< FName for class member function definition outside of the class declaration.

        Flags::DataType DefFlags;
        StdProcs StdProcType;       //!< Type of standard procedure. By default StdProcs::None.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================