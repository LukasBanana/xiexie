/*
 * Pattern object AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OBJECT_PATTERN_H__
#define __XX_AST_OBJECT_PATTERN_H__


#include "Object.h"
#include "CustomTypeDenoter.h"
#include "FNameIdent.h"
#include "Expression.h"


namespace AbstractSyntaxTrees
{


class PatternObject : public Object
{
    
    public:
        
        PatternObject(IdentifierPtr Name) :
            Object(Name, Name->Pos(), Object::Types::PatternObj)
        {
        }
        ~PatternObject()
        {
        }

        DefineASTVisitProc(PatternObject)

        void PrintAST()
        {
            Deb("Pattern Obj \"" + Ident->Spell + "\"");
            ScopedIndent Unused;

            if (TDenoter != nullptr)
                TDenoter->PrintAST();
        }

        TypeDenoter* GetTypeDenoter() const
        {
            return TDenoter != nullptr ? TDenoter.get() : nullptr;
        }
        
        std::string TypeDesc() const
        {
            return "Pattern";
        }

        //! \throw std::string
        bool DecorateSub(FNameIdentPtr FName)
        {
            /* Get resolved custom type denoter */
            if (TDenoter == nullptr)
                return false;

            auto ResTDenoter = TDenoter->GetResolvedType();

            if (ResTDenoter->Type() != TypeDenoter::Types::CustomType)
                return false;

            auto CTDenoter = dynamic_cast<CustomTypeDenoter*>(ResTDenoter);

            /* Decorate FName identifier with linked object */
            if (CTDenoter->Link != nullptr)
                return CTDenoter->Link->DecorateSub(FName);

            return false;
        }

        //! \throw std::string
        ObjectPtr FindSub(FNameIdentPtr FName)
        {
            /* Get resolved custom type denoter */
            if (TDenoter == nullptr)
                return nullptr;

            auto ResTDenoter = TDenoter->GetResolvedType();

            if (ResTDenoter->Type() != TypeDenoter::Types::CustomType)
                return nullptr;

            auto CTDenoter = dynamic_cast<CustomTypeDenoter*>(ResTDenoter);

            /* Find FName identifier with linked object */
            if (CTDenoter->Link != nullptr)
                return CTDenoter->Link->FindSub(FName);

            return nullptr;
        }

        ObjectPtr Copy() const
        {
            auto CopyObj = std::make_shared<PatternObject>(Ident->Copy());

            if (DefTDenoter != nullptr)
                CopyObj->DefTDenoter = DefTDenoter->Copy();

            return CopyObj;
        }

        TypeDenoterPtr DefTDenoter; //!< Default pattern type denoter.
        TypeDenoterPtr TDenoter;    //!< Active pattern type denoter (setup by template instantiation).

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================