/*
 * Type-definition object AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OBJECT_TYPE_H__
#define __XX_AST_OBJECT_TYPE_H__


#include "Object.h"
#include "TypeDenoter.h"


namespace AbstractSyntaxTrees
{


class TypeObject : public Object
{
    
    public:
        
        TypeObject(IdentifierPtr Name, TypeDenoterPtr TDenoterAST) :
            Object  (Name, Name->Pos(), Object::Types::TypeObj  ),
            TDenoter(TDenoterAST                                )
        {
        }
        ~TypeObject()
        {
        }

        DefineASTVisitProc(TypeObject)

        void PrintAST()
        {
            Deb("Type Obj \"" + Ident->Spell + "\"");
            ScopedIndent Unused;
            TDenoter->PrintAST();
        }

        std::string TypeDesc() const
        {
            return "Type Alias";
        }

        bool IsClassDenoter() const;

        ObjectPtr Copy() const
        {
            return std::make_shared<TypeObject>(Ident->Copy(), TDenoter->Copy());
        }

        TypeDenoterPtr TDenoter;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================