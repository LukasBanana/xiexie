/*
 * Enumeration object AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OBJECT_ENUM_H__
#define __XX_AST_OBJECT_ENUM_H__


#include "Object.h"
#include "EnumEntry.h"
#include "CustomTypeDenoter.h"

#include <vector>


namespace AbstractSyntaxTrees
{


class EnumObject : public Object
{
    
    public:
        
        EnumObject(IdentifierPtr Name) :
            Object  (Name, Name->Pos(), Object::Types::EnumObj  ),
            TDenoter(nullptr                                    )
        {
        }
        ~EnumObject()
        {
        }

        DefineASTVisitProc(EnumObject)

        void PrintAST()
        {
            Deb("Enum Obj \"" + Ident->Spell + "\"");
            ScopedIndent Unused;
            for (auto Entry : Entries)
                Entry->PrintAST();
        }

        TypeDenoter* GetTypeDenoter() const
        {
            return TDenoter;
        }

        std::string TypeDesc() const
        {
            return "Enum";
        }

        bool Decorate(FNameIdentPtr FName)
        {
            /* Decorate FName with this object */
            FName->Link = this;

            if (FName->Next == nullptr)
                return true;
            
            /* Decorate next entry */
            FName = FName->Next;

            const auto& Spell = FName->Ident->Spell;

            if (!HasEntry(Spell))
            {
                #if 0
                /* Check for enumeration standard function "Num" */
                if (Spell == "Num")
                {
                    #if 0
                    /* Decorate FName with the standard function */
                    FName->Link = this;//!!!
                    //FName->Link = get "Num" enumeration standard function <--- !TODO!
                    return FName->Next == nullptr;
                    #else
                    throw std::string("TODO -> enumeration standard function \"Num\" has not been integrated yet");
                    #endif
                }
                else
                #endif
                    throw std::string("Undeclared enumeration entry \"" + Spell + "\"");
            }
            
            /*
            Decorate FName with this object (it's actually the entry,
            but there's no extra object class for enum entries)
            */
            FName->Link = this;

            /* Return with success, if there is no further FName identifier */
            return FName->Next == nullptr;
        }

        bool DecorateSub(FNameIdentPtr FName)
        {
            /* Search for standard enumeration procedures */
            if (EnumObject::IsStdProc(FName->Ident->Spell))
            {
                /*
                Decorate FName with this object and
                the information that it's a standard enumeration procedure.
                */
                FName->Link = this;
                FName->IdentFlags |= FNameIdent::Flags::EnumProc;
                return true;
            }

            return false;
        }

        bool DecorateLocal(FNameIdentPtr FName)
        {
            if (FName == nullptr)
                return false;

            const auto& Spell = FName->Ident->Spell;

            if (!HasEntry(Spell))
                throw std::string("Undeclared enumeration entry \"" + Spell + "\"");
            
            /* Decorate FName with this object and the information that it's just an entry */
            FName->Link = this;
            FName->IdentFlags |= FNameIdent::Flags::EnumEntry;

            /* Return with success, if there is no further FName identifier */
            return FName->Next == nullptr;
        }

        ObjectPtr FindSub(FNameIdentPtr FName)
        {
            /* Search for standard enumeration procedures */
            return EnumObject::IsStdProc(FName->Ident->Spell) ? ThisPtr<EnumObject>() : nullptr;
        }

        static bool IsStdProc(const std::string& Spell)
        {
            return Spell == "Num" || Spell == "Str" || Spell == "Is" || Spell == "Set";
        }

        //! Returns true if the specified entry is contained in the flags enumeration.
        bool HasEntry(const std::string& Entry) const
        {
            if (!Entry.empty())
            {
                for (auto EnumEntry : Entries)
                {
                    if (EnumEntry->Ident->Spell == Entry)
                        return true;
                }
            }
            return false;
        }

        ObjectPtr Copy() const
        {
            auto CopyObj = std::make_shared<EnumObject>(Ident->Copy());

            for (auto Entry : Entries)
                CopyObj->Entries.push_back(Entry->Copy());

            return CopyObj;
        }

        std::vector<EnumEntryPtr> Entries;
        CustomTypeDenoter* TDenoter;        //!< Type denoter for this enumeration.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================