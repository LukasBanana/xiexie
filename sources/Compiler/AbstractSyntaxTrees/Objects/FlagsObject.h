/*
 * Flags object AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OBJECT_FLAGS_H__
#define __XX_AST_OBJECT_FLAGS_H__


#include "Object.h"
#include "CustomTypeDenoter.h"

#include <vector>
#include <list>


namespace AbstractSyntaxTrees
{


class FlagsObject : public Object
{
    
    public:
        
        FlagsObject(IdentifierPtr Name) :
            Object  (Name, Name->Pos(), Object::Types::FlagsObj ),
            TDenoter(nullptr                                    ),
            DataType(DataTypes::UInt32                          )
        {
        }
        ~FlagsObject()
        {
        }

        DefineASTVisitProc(FlagsObject)

        void PrintAST()
        {
            Deb("Flags Obj \"" + Ident->Spell + "\"");
            ScopedIndent Unused;

            if (!Inheritance.empty())
            {
                Deb("Inherit Flags:");
                ScopedIndent Unused2;
                for (auto Parent : Inheritance)
                    Parent->PrintAST();
            }

            for (IdentifierPtr Flag : Flags)
                Deb(Flag->Spell);
        }

        TypeDenoter* GetTypeDenoter() const
        {
            return TDenoter;
        }

        std::string TypeDesc() const
        {
            return "Flags";
        }

        bool Decorate(FNameIdentPtr FName)
        {
            /* Decorate FName with this object */
            FName->Link = this;
            FName->IdentFlags |= FNameIdent::Flags::FlagsTypeDenoter;

            /* Decorate next entry */
            return FName->Next != nullptr ? DecorateLocal(FName->Next) : true;
        }

        bool DecorateSub(FNameIdentPtr FName)
        {
            /* Search for standard flags procedures */
            if (FlagsObject::IsStdProc(FName->Ident->Spell))
            {
                /*
                Decorate FName with this object and
                the information that it's a standard flags procedure.
                */
                FName->Link = this;
                FName->IdentFlags |= FNameIdent::Flags::FlagsProc;
                return true;
            }

            return false;
        }

        bool DecorateLocal(FNameIdentPtr FName)
        {
            if (FName == nullptr)
                return false;

            const auto& Spell = FName->Ident->Spell;

            if (!HasEntry(Spell))
            {
                /* Check for flags enumeration default entries */
                if (Spell != "None" && Spell != "All")
                    throw std::string("Undeclared flags entry \"" + Spell + "\"");
            }
            
            /* Decorate FName with this object and the information that it's just an entry */
            FName->Link = this;
            FName->IdentFlags |= FNameIdent::Flags::FlagsEntry;

            /* Return with success, if there is no further FName identifier */
            return FName->Next == nullptr;
        }

        ObjectPtr FindSub(FNameIdentPtr FName)
        {
            /* Search for standard flags procedures */
            return FlagsObject::IsStdProc(FName->Ident->Spell) ? ThisPtr<FlagsObject>() : nullptr;
        }

        static bool IsStdProc(const std::string& Spell)
        {
            return Spell == "Add" || Spell == "Remove" || Spell == "Has";
        }

        //! Data types for bit-field. Numbers specify the size (in bytes).
        enum class DataTypes
        {
            UInt8   = 1,
            UInt16  = 2,
            UInt32  = 4,
            UInt64  = 8,
        };

        //! Returns true if the specified entry is contained in the flags enumeration.
        bool HasEntry(const std::string& Entry) const
        {
            if (!Entry.empty())
            {
                for (auto Flag : Flags)
                {
                    if (Flag->Spell == Entry)
                        return true;
                }
            }
            return false;
        }

        ObjectPtr Copy() const
        {
            auto CopyObj = std::make_shared<FlagsObject>(Ident->Copy());

            CopyObj->DataType = DataType;

            for (auto Entry : Flags)
                CopyObj->Flags.push_back(Entry->Copy());
            for (auto Entry : Inheritance)
                CopyObj->Inheritance.push_back(Entry->Copy());

            return CopyObj;
        }

        std::list<IdentifierPtr> Flags;
        std::vector<FNameIdentPtr> Inheritance; //!< Optional inherit flags names.
        CustomTypeDenoter* TDenoter;            //!< Type denoter for this flags enumeration.

        DataTypes DataType;                     //!< Information for decorated AST.

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================