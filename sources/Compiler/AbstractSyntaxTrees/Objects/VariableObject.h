/*
 * Variable object AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OBJECT_VARIABLE_H__
#define __XX_AST_OBJECT_VARIABLE_H__


#include "Object.h"
#include "CustomTypeDenoter.h"
#include "FNameIdent.h"
#include "Expression.h"
#include "Constant.h"


namespace AbstractSyntaxTrees
{


class VariableObject : public Object
{
    
    public:
        
        //!TODO! -> currently unused!!! (remove VariableDefCommand::DefFlags)
        struct Attributes
        {
            typedef unsigned int DataType;
            static const DataType Unique = (1 << 0); //!< This variable is unique and can not by copied by the 'copy' procedure (in classes only).
            static const DataType Setter = (1 << 1); //!< Generate setter function for this variable (in classes only).
            static const DataType Getter = (1 << 2); //!< Generate getter function for this variable (in classes only).
        };

        struct Flags
        {
            typedef unsigned int DataType;
            static const DataType IsStatic = (1 << 0);
        };

        VariableObject(TypeDenoterPtr Type, IdentifierPtr Name) :
            Object  (Name, Type->Pos(), Object::Types::VarObj   ),
            TDenoter(Type                                       ),
            Attribs (0                                          ),
            DefFlags(0                                          )
        {
        }
        ~VariableObject()
        {
        }

        DefineASTVisitProc(VariableObject)

        void PrintAST()
        {
            Deb("Variable Obj \"" + Ident->Spell + "\"");
            ScopedIndent Unused;

            if (Const != nullptr)
                Deb("Static Const \"" + Const->Str() + "\"");

            TDenoter->PrintAST();

            if (Expr != nullptr)
                Expr->PrintAST();
        }

        TypeDenoter* GetTypeDenoter() const
        {
            return TDenoter.get();
        }
        
        std::string TypeDesc() const
        {
            return "Variable";
        }

        //! \throw std::string
        bool Decorate(FNameIdentPtr FName)
        {
            /* Decorate FName with this object */
            FName->Link = this;

            if (FName->Next == nullptr)
                return true;

            /* Decorate next entry */
            FName = FName->Next;

            /* Get resolved type */
            TypeDenoter* TDen = TDenoter->GetResolvedType();
            if (TDen == nullptr)
                throw std::string("Invalid type denoter");

            /* Decorate with type denoter */
            return TDen->DecorateSub(FName);
        }

        //! \throw std::string
        ObjectPtr Find(FNameIdentPtr FName)
        {
            if (FName->Next == nullptr)
                return ThisPtr<Object>();

            /* Decorate next entry */
            FName = FName->Next;

            /* Get resolved type */
            auto TDen = TDenoter->GetResolvedType();
            if (TDen == nullptr)
                return nullptr;

            /* Find with type denoter */
            return TDen->FindSub(FName);
        }

        ObjectPtr Copy() const
        {
            auto CopyObj = std::make_shared<VariableObject>(TDenoter->Copy(), Ident->Copy());

            CopyObj->Attribs = Attribs;
            CopyObj->DefFlags = DefFlags;

            if (Expr != nullptr)
                CopyObj->Expr = Expr->Copy();
            if (Const != nullptr)
                CopyObj->Const = Const->Copy();

            return CopyObj;
        }

        //! Returns true if this is a static-constant (variable-) object.
        inline bool IsStaticConst() const
        {
            return (DefFlags & Flags::IsStatic) != 0 && TDenoter->Type() == TypeDenoter::Types::ConstType;
        }

        static VariableObjectPtr Create(const TypeDenoterPtr& TDenoterAST, const IdentifierPtr& NameIdentAST)
        {
            return std::make_shared<VariableObject>(TDenoterAST, NameIdentAST);
        }

        TypeDenoterPtr TDenoter;            //!< Variable type.
        ExpressionPtr Expr;                 //!< Initialization expression (optional).

        Attributes::DataType Attribs;       //!< Variable attributes.

        Flags::DataType DefFlags;

        ConstantFolding::ConstantPtr Const; //!< Information for the decorated AST (optional static constant value).

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================