/*
 * Object AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OBJECT_H__
#define __XX_AST_OBJECT_H__


#include "AST.h"
#include "Identifier.h"
#include "FNameIdent.h"


namespace ContextualAnalyzer
{
    class NameVisibility;
}

namespace AbstractSyntaxTrees
{


class Object : public AST
{
    
    public:
        
        enum class Types
        {
            PackageObj, //!< Package.
            ClassObj,   //!< Class.
            EnumObj,    //!< Standard enumeration.
            FlagsObj,   //!< Flags enumeration.
            FuncObj,    //!< Function.
            TypeObj,    //!< Type-definition.
            VarObj,     //!< Variable.
            PatternObj, //!< Pattern.
        };

        virtual ~Object()
        {
        }

        inline Types Type() const
        {
            return Type_;
        }

        virtual TypeDenoter* GetTypeDenoter() const
        {
            return nullptr;
        }

        virtual std::string GetDefaultValue() const
        {
            return "";
        }

        /**
        Returns true if this object class is distributable
        (e.g. "package" objects are distributable over several source files). By default false.
        */
        virtual bool IsDistrib() const
        {
            return false;
        }

        //! Returns a short description about this type (e.g. "Class").
        virtual std::string TypeDesc() const = 0;

        virtual bool Decorate(FNameIdentPtr FName)
        {
            /* Decorate FName with this object */
            FName->Link = this;

            /* Return with success, if there is no further FName identifier */
            return FName->Next == nullptr;
        }

        virtual bool DecorateSub(FNameIdentPtr FName)
        {
            /* Per default sub-members are not allowed */
            return false;
        }

        virtual bool DecorateLocal(FNameIdentPtr FName)
        {
            /* Per default there are not local entries */
            return false;
        }

        virtual ObjectPtr Find(FNameIdentPtr FName)
        {
            return FName->Next != nullptr ? nullptr : ThisPtr<Object>();
        }

        virtual ObjectPtr FindSub(FNameIdentPtr FName)
        {
            /* Per default sub-members are not allowed */
            return nullptr;
        }

        //! Searchs for a member object (with single name).
        virtual Object* FindMember(const std::string& Name) const
        {
            return nullptr;
        }

        //! Returns true if this object denotes a class (also true for type-definitions refering to a class).
        virtual bool IsClassDenoter() const
        {
            return false;
        }

        //! Returns the name of this object. This is equivalent to "this->Ident->Spell".
        inline std::string Name() const
        {
            return Ident != nullptr ? Ident->Spell : "";
        }

        //! Returns true if this object is anonymous.
        inline bool IsAnonymous() const
        {
            return Ident == nullptr;
        }

        virtual ObjectPtr Copy() const = 0;

        /* === Members === */

        IdentifierPtr Ident;                            //!< Identifier (can be a null pointer if this object is anonymous).

        ContextualAnalyzer::NameVisibility* NameVis;    //!< Information for the decorated AST.

    protected:
        
        Object(IdentifierPtr IdentAST, const SourcePosition &Pos, const Types &Type) :
            AST     (Pos        ),
            Ident   (IdentAST   ),
            NameVis (nullptr    ),
            Type_   (Type       )
        {
        }

    private:
        
        Types Type_;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================