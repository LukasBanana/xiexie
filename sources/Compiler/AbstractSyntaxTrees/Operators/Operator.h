/*
 * Operator AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OPERATOR_H__
#define __XX_AST_OPERATOR_H__


#include "Terminal.h"
#include "Token.h"


namespace AbstractSyntaxTrees
{


class Operator : public Terminal
{
    
    public:
        
        enum class Types
        {
            BinaryOp,
            UnaryOp,
            AssignOp,
        };

        virtual ~Operator()
        {
        }

        inline Types Type() const
        {
            return Type_;
        }

        virtual void PrintAST()
        {
            Deb("Operator \"" + Spell + "\"");
        }

        virtual OperatorPtr Copy() const = 0;

    protected:

        Operator(const Types &Type, TokenPtr TokenOp) :
            Terminal(TokenOp->Pos(), TokenOp->Spell()   ),
            Type_   (Type                               )
        {
        }
        Operator(const Types &Type, const SourcePosition& Pos, const std::string& Spell) :
            Terminal(Pos, Spell ),
            Type_   (Type       )
        {
        }

    private:
        
        Types Type_;

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================