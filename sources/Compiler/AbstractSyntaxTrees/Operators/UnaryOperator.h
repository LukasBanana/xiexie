/*
 * Unary operator AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OPERATOR_UNARY_H__
#define __XX_AST_OPERATOR_UNARY_H__


#include "Operator.h"


namespace AbstractSyntaxTrees
{


class UnaryOperator : public Operator
{
    
    public:
        
        UnaryOperator(TokenPtr Op) :
            Operator(Operator::Types::UnaryOp, Op)
        {
        }
        UnaryOperator(const SourcePosition& Pos, const std::string& Spell) :
            Operator(Operator::Types::UnaryOp, Pos, Spell)
        {
        }
        ~UnaryOperator()
        {
        }

        DefineASTVisitProc(UnaryOperator)

        OperatorPtr Copy() const
        {
            return std::make_shared<UnaryOperator>(Pos(), Spell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================