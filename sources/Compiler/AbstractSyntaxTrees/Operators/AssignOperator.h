/*
 * Assign operator AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OPERATOR_ASSIGN_H__
#define __XX_AST_OPERATOR_ASSIGN_H__


#include "Operator.h"


namespace AbstractSyntaxTrees
{


class AssignOperator : public Operator
{
    
    public:
        
        AssignOperator(TokenPtr Op) :
            Operator(Operator::Types::AssignOp, Op)
        {
        }
        AssignOperator(const SourcePosition& Pos, const std::string& Spell) :
            Operator(Operator::Types::AssignOp, Pos, Spell)
        {
        }
        ~AssignOperator()
        {
        }

        DefineASTVisitProc(AssignOperator)

        OperatorPtr Copy() const
        {
            return std::make_shared<AssignOperator>(Pos(), Spell);
        }

        static AssignOperatorPtr Create(const std::string& Spell)
        {
            return std::make_shared<AssignOperator>(SourcePosition::Ignore, Spell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================