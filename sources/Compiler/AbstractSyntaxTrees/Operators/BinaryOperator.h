/*
 * Binary operator AST header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_OPERATOR_BINARY_H__
#define __XX_AST_OPERATOR_BINARY_H__


#include "Operator.h"


namespace AbstractSyntaxTrees
{


class BinaryOperator : public Operator
{
    
    public:
        
        BinaryOperator(TokenPtr Op) :
            Operator(Operator::Types::BinaryOp, Op)
        {
        }
        BinaryOperator(const SourcePosition& Pos, const std::string& Spell) :
            Operator(Operator::Types::BinaryOp, Pos, Spell)
        {
        }
        ~BinaryOperator()
        {
        }

        DefineASTVisitProc(BinaryOperator)

        inline bool IsRelation() const
        {
            return Token::IsBooleanRelationBinaryOperator(Spell);
        }
        inline bool IsEquality() const
        {
            return Token::IsBooleanEqualityBinaryOperator(Spell);
        }
        inline bool IsArithmetic() const
        {
            return Token::IsArithmeticBinaryOperator(Spell);
        }
        inline bool IsBoolean() const
        {
            return IsRelation() || IsEquality();
        }
        inline bool IsBitwise() const
        {
            return Token::IsBitwiseOperator(Spell);
        }
        inline bool IsBinaryLink() const
        {
            return Token::IsBinaryLink(Spell);
        }

        OperatorPtr Copy() const
        {
            return std::make_shared<BinaryOperator>(Pos(), Spell);
        }

};


} // /namespace SyntacticAnalyzer


#endif



// ================================================================================