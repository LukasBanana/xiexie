/*
 * Visitor interface header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_AST_VISITOR_H__
#define __XX_AST_VISITOR_H__


#include "ASTDeclarations.h"


namespace AbstractSyntaxTrees
{


/* === Macros === */

#define VISIT           Visit(ThisPtr())
#define VISIT_ARG(p)    Visit(ThisPtr(), p)

#define DefAbstractVisitProc(n)                             \
    virtual void Visit##n(n##Ptr AST, void* Args = nullptr) \
    {                                                       \
        /* Dummy procedure (do nothing) */                  \
    }

#define DeclVisitProc(n) \
    void Visit##n(n##Ptr AST, void* Args = nullptr);

#define DefVisitProc(c, n) \
    void c::Visit##n(n##Ptr AST, void* Args)

#define class_visitor(n)                \
    class n;                            \
    typedef std::shared_ptr<n> n##Ptr;  \
    class n : public Visitor


/* === Visitor interface === */

class Visitor
{
    
    public:
        
        Visitor()
        {
        }
        virtual ~Visitor()
        {
        }

        /* === Commands === */

        DefAbstractVisitProc(AssertCommand          )
        DefAbstractVisitProc(AssignCommand          )
        DefAbstractVisitProc(BlockCommand           )
        DefAbstractVisitProc(CallCommand            )
        DefAbstractVisitProc(FunctionDefCommand     )
        DefAbstractVisitProc(ImportCommand          )
        DefAbstractVisitProc(PackageCommand         )
        DefAbstractVisitProc(ReturnCommand          )
        DefAbstractVisitProc(VariableDefCommand     )
        DefAbstractVisitProc(IfCommand              )
        DefAbstractVisitProc(InfiniteLoopCommand    )
        DefAbstractVisitProc(WhileLoopCommand       )
        DefAbstractVisitProc(RangeForLoopCommand    )
        DefAbstractVisitProc(InlineLangCommand      )
        DefAbstractVisitProc(PrimitiveCommand       )
        DefAbstractVisitProc(EnumDeclCommand        )
        DefAbstractVisitProc(FlagsDeclCommand       )
        DefAbstractVisitProc(ClassDeclCommand       )
        DefAbstractVisitProc(ClassBlockCommand      )
        DefAbstractVisitProc(InitCommand            )
        DefAbstractVisitProc(ReleaseCommand         )
        DefAbstractVisitProc(TryCatchCommand        )
        DefAbstractVisitProc(ThrowCommand           )
        DefAbstractVisitProc(SwitchCommand          )
        DefAbstractVisitProc(TypeDefCommand         )

        /* === Expressions === */

        DefAbstractVisitProc(BracketExpression      )
        DefAbstractVisitProc(BinaryExpression       )
        DefAbstractVisitProc(UnaryExpression        )
        DefAbstractVisitProc(StringSumExpression    )
        DefAbstractVisitProc(AssignExpression       )
        DefAbstractVisitProc(LiteralExpression      )
        DefAbstractVisitProc(CallExpression         )
        DefAbstractVisitProc(CastExpression         )
        DefAbstractVisitProc(ObjectExpression       )
        DefAbstractVisitProc(AllocExpression        )
        DefAbstractVisitProc(CopyExpression         )
        DefAbstractVisitProc(ValidationExpression   )
        DefAbstractVisitProc(InitListExpression     )
        DefAbstractVisitProc(LambdaExpression       )

        /* === Type denoters === */

        DefAbstractVisitProc(VoidTypeDenoter        )
        DefAbstractVisitProc(ConstTypeDenoter       )
        DefAbstractVisitProc(AutoTypeDenoter        )
        DefAbstractVisitProc(BoolTypeDenoter        )
        DefAbstractVisitProc(IntTypeDenoter         )
        DefAbstractVisitProc(FloatTypeDenoter       )
        DefAbstractVisitProc(StringTypeDenoter      )
        DefAbstractVisitProc(CustomTypeDenoter      )
        DefAbstractVisitProc(MngPtrTypeDenoter      )
        DefAbstractVisitProc(RawPtrTypeDenoter      )
        DefAbstractVisitProc(RefTypeDenoter         )
        DefAbstractVisitProc(ProcTypeDenoter        )
        DefAbstractVisitProc(ArrayTypeDenoter       )

        /* === Lists === */

        DefAbstractVisitProc(ArgumentList           )
        DefAbstractVisitProc(ParameterList          )
        DefAbstractVisitProc(TemplateParamList      )
        DefAbstractVisitProc(TemplateArgList        )
        DefAbstractVisitProc(InitializerList        )
        DefAbstractVisitProc(AttributeList          )
        DefAbstractVisitProc(ArrayIndexList         )

        /* === Literals === */

        DefAbstractVisitProc(BoolLiteral            )
        DefAbstractVisitProc(PointerLiteral         )
        DefAbstractVisitProc(IntegerLiteral         )
        DefAbstractVisitProc(FloatLiteral           )
        DefAbstractVisitProc(StringLiteral          )

        /* === Objects === */

        DefAbstractVisitProc(ClassObject            )
        DefAbstractVisitProc(EnumObject             )
        DefAbstractVisitProc(FlagsObject            )
        DefAbstractVisitProc(FunctionObject         )
        DefAbstractVisitProc(TypeObject             )
        DefAbstractVisitProc(VariableObject         )
        DefAbstractVisitProc(PatternObject          )

        /* === Operators === */

        DefAbstractVisitProc(AssignOperator         )
        DefAbstractVisitProc(BinaryOperator         )
        DefAbstractVisitProc(UnaryOperator          )

        /* === Others === */

        DefAbstractVisitProc(Identifier             )
        DefAbstractVisitProc(FNameIdent             )
        DefAbstractVisitProc(Program                )
        DefAbstractVisitProc(EnumEntry              )
        DefAbstractVisitProc(CaseBlock              )
        
    protected:
        
        /* === Inline functions === */

        inline Visitor* ThisPtr()
        {
            return this;
        }

};

#undef DefAbstractVisitProc


} // /namespace AbstractSyntaxTrees


#endif



// ================================================================================