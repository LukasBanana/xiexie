/*
 * XieXie assembler header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_COMPILER_ASM_H__
#define __XX_COMPILER_ASM_H__


#include "AsmScanner.h"
#include "Instruction.h"
#include "SourceFile.h"
#include "ByteCode.h"

#include <string>
#include <map>


using namespace SyntacticAnalyzer;
using namespace XVM;


class XieXieAssembler
{
    
    public:
        
        XieXieAssembler();
        ~XieXieAssembler();

        /* === Functions === */

        bool AssembleFile(const std::string& Filename);

    private:
        
        /* === Enumerations === */

        enum class MnemonicCategories
        {
            Data,
            Reg1,
            Jump,
            Mem,
            Special,
            Extra,
        };

        /* === Structures === */

        struct MnemonicCategory
        {
            MnemonicCategory(
                const MnemonicCategories& MnCategory = MnemonicCategories::Data,
                const Instruction::OpCodes::opcode_t& MnOpCode = 0u,
                const Instruction::OpCodes::opcode_t& MnAltOpCode = 0u) :
                    Category    (MnCategory ),
                    OpCode      (MnOpCode   ),
                    AltOpCode   (MnAltOpCode)
            {
            }
            ~MnemonicCategory()
            {
            }

            /* Members */
            MnemonicCategories Category;
            Instruction::OpCodes::opcode_t OpCode, AltOpCode;
        };

        struct BackPatchAddr
        {
            BackPatchAddr(unsigned int Index = 0u, unsigned int Base = 0u) :
                InstrIndex(Index),
                OffsetBase(Base )
            {
            }
            ~BackPatchAddr()
            {
            }

            /* Members */
            unsigned int InstrIndex;
            unsigned int OffsetBase;
        };

        /* === Functions === */

        TokenPtr Accept(const Token::Types &Type);
        TokenPtr AcceptIt();

        void Error(const std::string &Msg);
        void ErrorUnexpected(const std::string& Msg);
        void ErrorUnexpected(const Token::Types& Type);

        void EstablishMnemonics();

        void ParseLabel(const TokenPtr& Ident);
        void BackPatchAddress(const unsigned int Instr, const unsigned int Addr);

        void ParseInstruction(const TokenPtr& Ident);

        Instruction::Registers::reg_t ParseRegister();
        unsigned int ParseAddress(bool UseAsOffset = false);
        unsigned int ParseIntegerLiteral();

        void AddInstr(const Instruction& Instr);

        void ParseInstructionData       (const std::string& Mnemonic);
        void ParseInstructionDataString ();
        void ParseInstructionDataInt    ();
        void ParseInstructionDataLong   ();
        void ParseInstructionDataFloat  ();
        void ParseInstructionDataDouble ();
        void ParseInstructionDataQuad   ();

        void ParseInstructionReg1   (const std::string& Mnemonic, const MnemonicCategory& MnCateogry);
        void ParseInstructionJump   (const std::string& Mnemonic, const MnemonicCategory& MnCateogry);
        void ParseInstructionMem    (const std::string& Mnemonic, const MnemonicCategory& MnCateogry);
        void ParseInstructionSpecial(const std::string& Mnemonic, const MnemonicCategory& MnCateogry);
        void ParseInstructionExtra  (const std::string& Mnemonic, const MnemonicCategory& MnCateogry);

        /* === Inline functions === */

        inline Token::Types TokenType() const
        {
            return Tkn_->Type();
        }

        inline unsigned int InstrCount() const
        {
            return InstrCounter_;
        }
        inline unsigned int InstrAddr() const
        {
            return InstrCount() * 4;
        }

        /* === Members === */

        AsmScanner Scanner_;
        TokenPtr Tkn_;

        SourceFilePtr SourceFile_;
        ByteCode* ByteCode_;

        std::map<std::string, MnemonicCategory> Mnemonics_;

        std::map<std::string, unsigned int> LabelAddresses_;                        //!< [ Label name | Label instruction index ].
        std::map< std::string, std::vector<BackPatchAddr> > BackPatchingLabels_;    //!< [ Label name | Instruction indices which are to be back-patched ].

        unsigned int InstrCounter_;

};


#endif



// ================================================================================