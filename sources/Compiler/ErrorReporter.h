/*
 * Error reporter header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_ERROR_REPORTER_H__
#define __XX_ERROR_REPORTER_H__


#include "CompilerMessage.h"

#include <string>
#include <list>


class ErrorReporter
{
    
    public:
        
        ~ErrorReporter();

        /* === Functions === */

        //! Pushes the new message at the back of the (FIFO) queue.
        void Push(const CompilerMessage& Msg);
        //! Pops a message from the front of the (FIFO) queue.
        bool Pop(CompilerMessage& Msg);

        //! Returns true if there are any errors stored in the (FIFO) queue.
        bool HasErrors() const;

        //! Counts the two categories: errors and warning.
        void CountCategories(size_t& NumErrors, size_t& NumWarnings) const;

        /* === Inline functions === */

        //! Returns the number of messages in the (FIFO) queue.
        inline size_t Num() const
        {
            return Messages_.size();
        }
        //! Returns true if there are any messages stored in the (FIFO) queue.
        inline bool HasMessages() const
        {
            return Num() > 0;
        }

        //! Global error reporter
        static ErrorReporter Instance;

    private:
        
        ErrorReporter();

        /* === Members === */

        std::list<CompilerMessage> Messages_;
        
};


#endif



// ================================================================================