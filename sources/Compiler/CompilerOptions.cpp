/*
 * Compiler options file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "CompilerOptions.h"
#include "ConsoleOutput.h"

#include <stack>
#include <regex>

#include <iostream>//!!!


namespace CompilerOptions
{


/* === Global members === */

ConfigSettings Settings;


/* === Internal members === */

static std::stack<ConfigSettings> SettingsStack;


/* === ConfigSettings structure === */

ConfigSettings::ConfigSettings() :
    CppTarget                   (CppTargets::Core   ),
    EnableCpp11                 (true               ),
    EnableComments              (true               ),
    BloatExpressions            (true               ),
    AllowBlanks                 (true               ),
    Indentation                 (true               ),
    UseClassicHeaderGuard       (true               ),
    UseHeaderGuardName          (true               ),
    WarnUninitializedVariables  (false              ),
    Optimize                    (false              ),
    ImmediateOutput             (false              ),
    SimplifiedExpr              (false              ),
    AutoConstRef                (false              ),
    GenDebugInfo                (false              ),
    AllowExtMathExpr            (true               ),
    Obfuscate                   (false              )
{
}
ConfigSettings::~ConfigSettings()
{
}

void ConfigSettings::WarnAll(bool Enable)
{
    /* Enable all warning options */
    WarnUninitializedVariables = Enable;
}

bool ConfigSettings::Setup(const std::string& Flag, bool Enable)
{
    /* --- Standard configuration --- */
    if (Flag == "-se" || Flag == "--simp-expr")
        SimplifiedExpr = Enable;
    else if (Flag == "-Wall" || Flag == "--warn-all")
        WarnAll(Enable);
    else if (Flag == "-O" || Flag == "--optimize")
        Optimize = Enable;
    else if (Flag == "-D" || Flag == "--debug")
        GenDebugInfo = Enable;
    else if (Flag == "-c98")
        EnableCpp11 = !Enable;
    else if (Flag == "-imm")
        ImmediateOutput = Enable;
    else if (Flag == "--auto-cref")
        AutoConstRef = Enable;

    /* --- Extended code generation --- */
    else if (Flag == "--no-expr-bloat")
        BloatExpressions = !Enable;
    else if (Flag == "--no-comments")
        EnableComments = !Enable;
    else if (Flag == "--no-blanks")
        AllowBlanks = !Enable;
    else if (Flag == "--no-indent")
        Indentation = !Enable;
    else if (Flag == "--no-classic-guard")
        UseClassicHeaderGuard = !Enable;
    else if (Flag == "--use-guard-index")
        UseHeaderGuardName = !Enable;
    else if (Flag == "--no-ext-math-expr")
        AllowExtMathExpr = !Enable;
    else if (Flag == "--obfuscate")
        Obfuscate = Enable;
    else
        return false;

    return true;
}


/* === Global functions === */

bool PushConfig(std::string CmdStr)
{
    ConfigSettings Config;

    /* Parse command string */
    const std::regex RegExFlag("'[\\-|a-z|A-Z|0-9|_]*'");
    const std::regex RegExValue("ON|OFF");
    std::smatch Match;

    while (std::regex_search(CmdStr, Match, RegExFlag))
    {
        /* Get flag string and trim command string */
        auto FlagStr = Match.begin()->str();
        CmdStr = Match.suffix().str();

        FlagStr = FlagStr.substr(1, FlagStr.size() - 2);

        /* No search for new setting */
        if (std::regex_search(CmdStr, Match, RegExValue))
        {
            /* Get value string and trim command string */
            auto ValueStr = Match.begin()->str();
            CmdStr = Match.suffix().str();

            if (!Config.Setup(FlagStr, ValueStr == "ON"))
            {
                ConsoleOutput::Error("Unknown option \"" + FlagStr + "\"");
                return false;
            }
        }
        else
        {
            ConsoleOutput::Error("Missing value for option \"" + FlagStr + "\"");
            return false;
        }
    }

    /* Push previous config onto the stack and setup new config */
    SettingsStack.push(Settings);
    Settings = Config;

    return true;
}

bool PopConfig()
{
    if (!SettingsStack.empty())
    {
        Settings = SettingsStack.top();
        SettingsStack.pop();
        return true;
    }
    return false;
}


} // /namespace CompilerOptions



// ================================================================================