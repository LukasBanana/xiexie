/*
 * Compiler message header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_COMPILER_MESSAGE_H__
#define __XX_COMPILER_MESSAGE_H__


#include "SourcePosition.h"


using SyntacticAnalyzer::SourcePosition;

class CompilerMessage : public std::exception
{
    
    public:
        
        /* === Enumerations === */

        enum class Categories
        {
            Message,
            Warning,
            SyntaxError,
            ContextError,
            CodeGenError,
            StateError,
            FileError,
        };

        /* === Constructors & destructor === */

        CompilerMessage(const Categories& Category = Categories::Message);
        CompilerMessage(const CompilerMessage& Other);
        CompilerMessage(
            const std::string &Msg,
            const Categories& Category = Categories::Message
        );
        CompilerMessage(
            const SourcePosition &Pos, const std::string &Msg,
            const Categories& Category = Categories::Message
        );
        CompilerMessage(
            const SourcePosition &Pos, const std::string &Msg,
            const std::string& Line, const std::string& Mark,
            const Categories& Category = Categories::Message
        );
        virtual ~CompilerMessage();

        /* === Functions === */

        const char* what() const throw();

        /* === Static functions === */

        static std::string ConstructMessage(const Categories Category, const SourcePosition& Pos, const std::string& Msg);
        static std::string GetCategoryString(const Categories Category);

        /* === Inline functions === */

        inline Categories Category() const
        {
            return Category_;
        }

        inline const std::string& Message() const
        {
            return Msg_;
        }
        inline const std::string& Line() const
        {
            return Line_;
        }
        inline const std::string& Mark() const
        {
            return Mark_;
        }

        inline bool HasLineWithMark() const
        {
            return !Line_.empty() && !Mark_.empty();
        }

        inline bool IsError() const
        {
            return
                Category_ == Categories::SyntaxError    ||
                Category_ == Categories::ContextError   ||
                Category_ == Categories::CodeGenError   ||
                Category_ == Categories::StateError     ||
                Category_ == Categories::FileError;
        }

    private:
        
        /* === Members === */

        Categories Category_;

        SourcePosition Pos_;

        std::string Msg_;       //!< Output message.
        std::string Line_;      //!< Optional line where the error has been occured.
        std::string Mark_;      //!< Optional mark to show where the error has been occured.

};


//! Compiler message comfort class (inherits from CompilerMessage but does not extend the functionality).
template <CompilerMessage::Categories C> class CommonCompilerMessage : public CompilerMessage
{
    public:
        CommonCompilerMessage(const std::string &Msg) :
            CompilerMessage(Msg, C)
        {
        }
        CommonCompilerMessage(const SourcePosition &Pos, const std::string &Msg) :
            CompilerMessage(Pos, Msg, C)
        {
        }
        CommonCompilerMessage(
            const SourcePosition &Pos, const std::string &Msg,
            const std::string& Line, const std::string& Mark) :
                CompilerMessage(Pos, Msg, Line, Mark, C)
        {
        }
};


typedef CommonCompilerMessage< CompilerMessage::Categories::Warning         > CompilerWarning;
typedef CommonCompilerMessage< CompilerMessage::Categories::SyntaxError     > SyntaxError;
typedef CommonCompilerMessage< CompilerMessage::Categories::ContextError    > ContextError;
typedef CommonCompilerMessage< CompilerMessage::Categories::CodeGenError    > CodeGenError;
typedef CommonCompilerMessage< CompilerMessage::Categories::StateError      > StateError;
typedef CommonCompilerMessage< CompilerMessage::Categories::FileError       > FileError;


#endif



// ================================================================================