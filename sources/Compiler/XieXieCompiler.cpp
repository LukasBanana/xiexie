/*
 * XieXie compiler file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "XieXieCompiler.h"
#include "ConsoleOutput.h"
#include "ConsoleManip.h"
#include "StringMod.h"

#include "CompilerOptions.h"
#include "Program.h"
#include "Parser.h"
#include "Checker.h"
#include "ExpressionEvaluator.h"
#include "ProcedureFactory.h"
#include "PackageCommand.h"

#include "CppCodeGenerator.h"
#include "AsmCodeGenerator.h"

#include <string>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>


/*
 * Internal functions
 */

static CodeGeneratorPtr GetCodeGenerator(unsigned int Options)
{
    if ((Options & XieXieCompiler::Flags::UseASM) != 0)
        return std::make_shared<AsmCodeGenerator>();
    return std::make_shared<CppCodeGenerator>();
}


/*
 * XieXieCompiler class
 */

XieXieCompiler::XieXieCompiler()
{
}
XieXieCompiler::~XieXieCompiler()
{
}

bool XieXieCompiler::CompileSingleFile(const std::string& Filename, unsigned int Options)
{
    /* Get path from filename string */
    const std::string SourcePath = xxFilePath(Filename);

    try
    {
        /* Store start time */
        const auto StartTime = std::chrono::system_clock::now();

        /* Parse program */
        Parser Prs;
        auto ProgramAST = Prs.ParseProgram(Filename);

        if (ProgramAST == nullptr)
            throw std::string("Parsing failed");

        /* Check if XASM intrinsics must be added */
        if ((Options & XieXieCompiler::Flags::UseASM) != 0)
            ProgramAST->Commands.push_front(ProcedureFactory::GenXASMIntrinsics());

        /* Decorate AST */
        auto Checker = std::make_shared<ContextualAnalyzer::Checker>();
        if (!Checker->DecorateAST(ProgramAST))
            throw std::string("Contaxtual analysis failed");

        /* Setup code generator */
        CodeGeneratorPtr Generator = GetCodeGenerator(Options);
        
        /* Generate code */
        if (!Generator->GenerateCode(SourcePath + "/", ProgramAST))
            throw std::string("Compilation failed");

        /* Return with success message */
        auto EndTime = std::chrono::system_clock::now();
        auto Duration = std::chrono::duration_cast<std::chrono::milliseconds>(EndTime - StartTime).count();

        ConsoleOutput::Success("Compilation successful");
        ConsoleOutput::Message("Duration: " + xxStr(Duration) + " ms.");

        /* Show debug output */
        if ((Options & Flags::ShowAST) != 0)
            ProgramAST->PrintAST();
        if ((Options & Flags::ShowNameVisTable) != 0)
            Checker->PrintNameVisibilityTable();

        return true;
    }
    catch (const std::exception& Err)
    {
        ConsoleOutput::Error(Err.what());
    }
    catch (const std::string& Err)
    {
        ConsoleOutput::Error(Err);
    }

    return false;
}

bool XieXieCompiler::EvaluateExpression(const std::string& ExprStr, unsigned int Options)
{
    try
    {
        /* Parse program */
        Parser Prs;
        auto ProgramAST = Prs.ParseExpression(ExprStr);

        if (ProgramAST == nullptr)
            throw std::string("Parsing failed");

        /* Evaluate finally expression result */
        auto Evaluator = std::make_shared<ExpressionEvaluator>();

        LongNumber Result;
        if (!Evaluator->EvaluateExpr(ProgramAST, Result))
            throw std::string("Evaluating expression failed");

        if (!CompilerOptions::Settings.ImmediateOutput)
        {
            /* Show debug output */
            if ((Options & Flags::ShowAST) != 0)
                ProgramAST->PrintAST();

            /* Return with success message */
            ConsoleOutput::Message("Evaluation successful");
        }

        /* Print single line output */
        if (Result.IsReal())
            ConsoleOutput::Message(xxStr(Result.Real()));
        else
            ConsoleOutput::Message(xxStr(Result.Integral()));

        return true;
    }
    catch (const std::exception& Err)
    {
        ConsoleOutput::Error(Err.what());
    }
    catch (const std::string& Err)
    {
        ConsoleOutput::Error(Err);
    }

    return false;
}



// ================================================================================