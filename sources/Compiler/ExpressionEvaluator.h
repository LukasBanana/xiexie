/*
 * Expression evaluator header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_EXPRESSION_EVALUATOR_H__
#define __XX_EXPRESSION_EVALUATOR_H__


#include "Visitor.h"
#include "LongNumber.h"

#include <string>
#include <map>
#include <stack>
#include <functional>
#include <vector>


using namespace AbstractSyntaxTrees;


//! The ExpressionEvaluator is actually only used for the external GUI-based calculator application.
class_visitor(ExpressionEvaluator)
{
    
    public:
        
        ExpressionEvaluator();
        ~ExpressionEvaluator();

        /* === Functions === */

        bool EvaluateExpr(ExpressionPtr ExprAST, LongNumber& Result);

        /* === Expressions === */

        DeclVisitProc(BracketExpression     )
        DeclVisitProc(BinaryExpression      )
        DeclVisitProc(UnaryExpression       )
        DeclVisitProc(AssignExpression      )
        DeclVisitProc(LiteralExpression     )
        DeclVisitProc(CallExpression        )
        DeclVisitProc(CastExpression        )
        DeclVisitProc(ObjectExpression      )

        /* === Literals === */

        DeclVisitProc(BoolLiteral           )
        DeclVisitProc(FloatLiteral          )
        DeclVisitProc(IntegerLiteral        )
        DeclVisitProc(StringLiteral         )

        /* === Objects === */

        DeclVisitProc(ClassObject           )
        DeclVisitProc(EnumObject            )
        DeclVisitProc(FlagsObject           )
        DeclVisitProc(FunctionObject        )
        DeclVisitProc(TypeObject            )
        DeclVisitProc(VariableObject        )

        /* === Operators === */

        DeclVisitProc(AssignOperator        )
        DeclVisitProc(BinaryOperator        )
        DeclVisitProc(UnaryOperator         )

    private:
        
        /* === Structures === */

        struct StdFunction
        {
            typedef std::function<LongNumber(std::vector<LongNumber>)> FuncT;

            StdFunction() :
                NumParams(0)
            {
            }
            StdFunction(size_t Num, const FuncT& FuncPtr) :
                NumParams   (Num    ),
                Func        (FuncPtr)
            {
            }

            /* Members */
            size_t NumParams;
            FuncT Func;
        };

        /* === Functions === */

        void EstablishStdFunctions();

        void Error(const std::string& Message, ASTPtr AST = nullptr);

        void Push(const LongNumber& Value);
        LongNumber Pop();

        static LongNumber::integral_t Faculty(LongNumber::integral_t Num);

        /* === Members === */

        std::stack<LongNumber> NumberStack_;

        std::map<std::string, StdFunction> StdFunctions_;

        struct StdVariables
        {
            static const LongNumber::real_t PI;
            static const LongNumber::real_t E;
        };

};


#endif



// ================================================================================