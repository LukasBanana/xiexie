/*
 * XieXie assembler file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "AsmCompiler.h"
#include "ByteCode.h"
#include "ConsoleOutput.h"
#include "StringMod.h"
#include "CompilerMessage.h"


XieXieAssembler::XieXieAssembler() :
    ByteCode_       (nullptr),
    InstrCounter_   (0      )
{
    EstablishMnemonics();
}
XieXieAssembler::~XieXieAssembler()
{
}

bool XieXieAssembler::AssembleFile(const std::string& Filename)
{
    ByteCode OutputCode;
    ByteCode_ = &OutputCode;
    InstrCounter_ = 0;

    /* Open file for reading */
    ConsoleOutput::Message("Assembling source file \"" + Filename + "\" ...");
    ScopedIndent Unused;

    SourceFile_ = std::make_shared<SourceFile>();
    if (!SourceFile_->ReadFile(Filename))
    {
        ConsoleOutput::Error("Reading file failed");
        return false;
    }

    /* Parse program*/
    Scanner_.ScanSource(SourceFile_);

    Tkn_ = Scanner_.Next();

    try
    {
        /* Parse instructions until <end-of-file> token */
        while (TokenType() != Token::Types::EndOfFile)
        {
            auto Ident = Accept(Token::Types::Identifier);

            if (TokenType() == Token::Types::Colon)
                ParseLabel(Ident);
            else
                ParseInstruction(Ident);
        }

        /* Check if all labels could be resolved */
        if (!BackPatchingLabels_.empty())
        {
            for (auto it : BackPatchingLabels_)
                ConsoleOutput::Error("Unresolved label \"" + it.first + "\"");
            throw std::string("Byte-code generation failed");
        }

        /* Save instructions in byte-code file */
        OutputCode.WriteToFile(Filename + ".xbc");

        ByteCode_ = nullptr;

        return true;
    }
    catch (const SyntaxError &Err)
    {
        ConsoleOutput::Error(Err.what());
    }
    catch (const std::exception& Err)
    {
        ConsoleOutput::Error(Err.what());
    }
    catch (const std::string& Err)
    {
        ConsoleOutput::Error(Err);
    }

    ByteCode_ = nullptr;

    return false;
}


/*
 * ======= Private: =======
 */

TokenPtr XieXieAssembler::Accept(const Token::Types &Type)
{
    /* Accept token on type match */
    if (Tkn_ && TokenType() == Type)
        return AcceptIt();
    
    /* Exit with error */
    ErrorUnexpected(Type);

    return nullptr;
}

TokenPtr XieXieAssembler::AcceptIt()
{
    /* Return previous token */
    auto PrevTkn = Tkn_;

    /* Scan next token and check for error */
    Tkn_ = Scanner_.Next();
    if (Tkn_ == nullptr)
        Error("Scanning token failed");

    return PrevTkn;
}

void XieXieAssembler::Error(const std::string& Msg)
{
    if (Tkn_ != nullptr)
        throw SyntaxError(Tkn_->Pos(), Msg);
    else
        throw SyntaxError(Msg);
}

void XieXieAssembler::ErrorUnexpected(const std::string& Msg)
{
    Error("Unexpected token: '" + Token::Spell(TokenType()) + "' (" + Msg + ")");
}

void XieXieAssembler::ErrorUnexpected(const Token::Types& Type)
{
    ErrorUnexpected("Expected '" + Token::Spell(Type) + "'");
}

void XieXieAssembler::EstablishMnemonics()
{
    Mnemonics_["data.string"] = MnemonicCategories::Data;
    Mnemonics_["data.int"   ] = MnemonicCategories::Data;
    Mnemonics_["data.long"  ] = MnemonicCategories::Data;
    Mnemonics_["data.float" ] = MnemonicCategories::Data;
    Mnemonics_["data.double"] = MnemonicCategories::Data;

    Mnemonics_["pop"    ] = MnemonicCategory(MnemonicCategories::Reg1, Instruction::OpCodes::Reg1::POP);
    Mnemonics_["inc"    ] = MnemonicCategory(MnemonicCategories::Reg1, Instruction::OpCodes::Reg1::INC);
    Mnemonics_["dec"    ] = MnemonicCategory(MnemonicCategories::Reg1, Instruction::OpCodes::Reg1::DEC);

    Mnemonics_["jmp"    ] = MnemonicCategory(MnemonicCategories::Jump, Instruction::OpCodes::Jump::JMP);
    Mnemonics_["je"     ] = MnemonicCategory(MnemonicCategories::Jump, Instruction::OpCodes::Jump::JE);
    Mnemonics_["jne"    ] = MnemonicCategory(MnemonicCategories::Jump, Instruction::OpCodes::Jump::JNE);
    Mnemonics_["jg"     ] = MnemonicCategory(MnemonicCategories::Jump, Instruction::OpCodes::Jump::JG);
    Mnemonics_["jl"     ] = MnemonicCategory(MnemonicCategories::Jump, Instruction::OpCodes::Jump::JL);
    Mnemonics_["jge"    ] = MnemonicCategory(MnemonicCategories::Jump, Instruction::OpCodes::Jump::JGE);
    Mnemonics_["jle"    ] = MnemonicCategory(MnemonicCategories::Jump, Instruction::OpCodes::Jump::JLE);
    Mnemonics_["call"   ] = MnemonicCategory(MnemonicCategories::Jump, Instruction::OpCodes::Jump::CALL);

    Mnemonics_["stop"   ] = MnemonicCategory(MnemonicCategories::Special, Instruction::OpCodes::Special::STOP);
    Mnemonics_["ret"    ] = MnemonicCategory(MnemonicCategories::Special, Instruction::OpCodes::Special::RET);
    Mnemonics_["push"   ] = MnemonicCategory(MnemonicCategories::Special, Instruction::OpCodes::Special::PUSH, Instruction::OpCodes::Reg1::PUSH);

    Mnemonics_["ldb"    ] = MnemonicCategory(MnemonicCategories::Mem, Instruction::OpCodes::Mem::LDB, Instruction::OpCodes::MemOff::LDB);
    Mnemonics_["stb"    ] = MnemonicCategory(MnemonicCategories::Mem, Instruction::OpCodes::Mem::STB, Instruction::OpCodes::MemOff::STB);
    Mnemonics_["ldw"    ] = MnemonicCategory(MnemonicCategories::Mem, Instruction::OpCodes::Mem::LDW, Instruction::OpCodes::MemOff::LDW);
    Mnemonics_["stw"    ] = MnemonicCategory(MnemonicCategories::Mem, Instruction::OpCodes::Mem::STW, Instruction::OpCodes::MemOff::STW);

    Mnemonics_["mov"    ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::MOV, Instruction::OpCodes::Reg2::MOV);
    Mnemonics_["not"    ] = MnemonicCategory(MnemonicCategories::Extra,                              0u, Instruction::OpCodes::Reg2::NOT);
    Mnemonics_["and"    ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::AND, Instruction::OpCodes::Reg2::AND);
    Mnemonics_["or"     ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::OR , Instruction::OpCodes::Reg2::OR );
    Mnemonics_["xor"    ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::XOR, Instruction::OpCodes::Reg2::XOR);
    Mnemonics_["add"    ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::ADD, Instruction::OpCodes::Reg2::ADD);
    Mnemonics_["sub"    ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::SUB, Instruction::OpCodes::Reg2::SUB);
    Mnemonics_["mul"    ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::MUL, Instruction::OpCodes::Reg2::MUL);
    Mnemonics_["div"    ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::DIV, Instruction::OpCodes::Reg2::DIV);
    Mnemonics_["mod"    ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::MOD, Instruction::OpCodes::Reg2::MOD);
    Mnemonics_["sll"    ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::SLL, Instruction::OpCodes::Reg2::SLL);
    Mnemonics_["slr"    ] = MnemonicCategory(MnemonicCategories::Extra, Instruction::OpCodes::Reg1::SLR, Instruction::OpCodes::Reg2::SLR);
    Mnemonics_["cmp"    ] = MnemonicCategory(MnemonicCategories::Extra,                              0u, Instruction::OpCodes::Reg2::CMP);
    Mnemonics_["fti"    ] = MnemonicCategory(MnemonicCategories::Extra,                              0u, Instruction::OpCodes::Reg2::FTI);
    Mnemonics_["tif"    ] = MnemonicCategory(MnemonicCategories::Extra,                              0u, Instruction::OpCodes::Reg2::ITF);
}

void XieXieAssembler::ParseLabel(const TokenPtr& Ident)
{
    /* Parse label format */
    const auto& Spell = Ident->Spell();
    Accept(Token::Types::Colon);

    /* Store instruction address for label */
    const unsigned int Addr = InstrCount();
    LabelAddresses_[Spell] = Addr;

    /* Check for back-patching */
    auto it = BackPatchingLabels_.find(Spell);

    if (it != BackPatchingLabels_.end())
    {
        /* Back-patch all addresses */
        for (auto Instr : it->second)
            BackPatchAddress(Instr.InstrIndex, Addr - Instr.OffsetBase);

        /* Remove this entry from the map */
        BackPatchingLabels_.erase(it);
    }
}

void XieXieAssembler::BackPatchAddress(const unsigned int Instr, const unsigned int Addr)
{
    if (Instr < ByteCode_->Instructions().size())
        ByteCode_->Instructions().at(Instr).BackPatchAddress(Addr);
    else
        Error("Address 'back-patching' failed (Index out of bounds)");
}

void XieXieAssembler::ParseInstruction(const TokenPtr& Ident)
{
    /* Get mnemonic in lower case */
    const std::string Mnemonic = xxLower(Ident->Spell());

    /* Find instruction type */
    auto it = Mnemonics_.find(Mnemonic);

    if (it == Mnemonics_.end())
        Error("Unknown instruction mnemonic \"" + Mnemonic + "\"");
        
    /* Parse instruction */
    auto MnCategory = it->second;

    switch (MnCategory.Category)
    {
        case MnemonicCategories::Data:
            ParseInstructionData(Mnemonic);
            break;
        case MnemonicCategories::Reg1:
            ParseInstructionReg1(Mnemonic, MnCategory);
            break;
        case MnemonicCategories::Jump:
            ParseInstructionJump(Mnemonic, MnCategory);
            break;
        case MnemonicCategories::Mem:
            ParseInstructionMem(Mnemonic, MnCategory);
            break;
        case MnemonicCategories::Special:
            ParseInstructionSpecial(Mnemonic, MnCategory);
            break;
        case MnemonicCategories::Extra:
            ParseInstructionExtra(Mnemonic, MnCategory);
            break;
        default:
            Error("Unknown instruction mnemonic");
            break;
    }
}

Instruction::Registers::reg_t XieXieAssembler::ParseRegister()
{
    /* Parse register */
    auto TknReg = Accept(Token::Types::Register);
    const std::string& Spell = TknReg->Spell();
    return static_cast<Instruction::Registers::reg_t>(Spell[0]);
}

unsigned int XieXieAssembler::ParseAddress(bool UseAsOffset)
{
    /* Check if concrete address value is used */
    if (TokenType() == Token::Types::IntLiteral || TokenType() == Token::Types::SubOp)
        return ParseIntegerLiteral();

    /* Parse label identifier */
    auto Ident = Accept(Token::Types::Identifier);
    const auto& Spell = Ident->Spell();

    /* Check if label already exists */
    auto it = LabelAddresses_.find(Spell);

    if (it != LabelAddresses_.end())
    {
        auto Addr = it->second;
        return UseAsOffset ? Addr - InstrCount() : Addr;
    }

    /* Append current instruction to list for 'address back-patching' */
    BackPatchingLabels_[Spell].push_back(
        BackPatchAddr(InstrCount(), UseAsOffset ? InstrCount() : 0u)
    );

    return 0u;
}

unsigned int XieXieAssembler::ParseIntegerLiteral()
{
    /* Parse optional negation */
    bool Negate = false;
    if (TokenType() == Token::Types::SubOp)
    {
        AcceptIt();
        Negate = true;
    }

    /* Parse integer literal */
    auto Literal = Accept(Token::Types::IntLiteral);
    const auto& Spell = Literal->Spell();

    /* Convert to 22-bit unsigned integer */
    int Num = xxNumber<int>(Spell);
    if (Negate)
        Num = -Num;

    unsigned int Value = (Num & 0x003fffff);

    return Value;
}

void XieXieAssembler::AddInstr(const Instruction& Instr)
{
    /* Add new instruction to byte-code */
    ByteCode_->AddInstruction(Instr);

    /* Increment instruction counter for label addressing */
    ++InstrCounter_;
}

void XieXieAssembler::ParseInstructionData(const std::string& Mnemonic)
{
    if (Mnemonic == "data.string")
        ParseInstructionDataString();
    else if (Mnemonic == "data.int")
        ParseInstructionDataInt();
    else if (Mnemonic == "data.long")
        ParseInstructionDataLong();
    else if (Mnemonic == "data.float")
        ParseInstructionDataFloat();
    else if (Mnemonic == "data.double")
        ParseInstructionDataDouble();
    else
        Error("Unexcepted data field \"" + Mnemonic + "\"");
}

void XieXieAssembler::ParseInstructionDataString()
{
    /* Parse string literal */
    auto StrLiteral = Accept(Token::Types::StringLiteral);
    const auto& Spell = StrLiteral->Spell();

    /* Add instructions to store string literal as data field */
    const char* Str = Spell.c_str();

    Instruction::code_t Code = 0;
    int i = 0;

    while (true)
    {
        /* Append character to instruction code */
        Code |= (*Str) << (i*8);

        if (++i == 4)
        {
            AddInstr(Instruction(Code));
            Code = 0;
            i = 0;
        }

        /* Stop after null character */
        if (*Str == 0)
        {
            if (i > 0)
                AddInstr(Instruction(Code));
            break;
        }

        /* Get next character */
        ++Str;
    }
}

void XieXieAssembler::ParseInstructionDataInt()
{
    /* Parse integer literal */
    auto Literal = Accept(Token::Types::IntLiteral);
    const auto& Spell = Literal->Spell();

    /* Add instruction to store integer literal as data field */
    auto Num = xxNumber<int>(Spell);

    auto Code = static_cast<Instruction::code_t>(Num);
    AddInstr(Instruction(Code));
}

void XieXieAssembler::ParseInstructionDataLong()
{
    /* Parse integer literal */
    auto Literal = Accept(Token::Types::IntLiteral);
    const auto& Spell = Literal->Spell();

    /* Add instruction to store integer literal as data field */
    auto Num = xxNumber<long long int>(Spell);

    auto Code = reinterpret_cast<Instruction::code_t*>(&Num);
    AddInstr(Instruction(*Code));

    ++Code;
    AddInstr(Instruction(*Code));
}

void XieXieAssembler::ParseInstructionDataFloat()
{
    /* Parse float literal */
    auto Literal = Accept(Token::Types::FloatLiteral);
    const auto& Spell = Literal->Spell();

    /* Add instruction to store floating-point literal as data field */
    auto Num = xxNumber<float>(Spell);

    auto Code = *reinterpret_cast<Instruction::code_t*>(&Num);
    AddInstr(Instruction(Code));
}

void XieXieAssembler::ParseInstructionDataDouble()
{
    /* Parse float literal */
    auto Literal = Accept(Token::Types::FloatLiteral);
    const auto& Spell = Literal->Spell();

    /* Add instruction to store floating-point literal as data field */
    auto Num = xxNumber<double>(Spell);

    auto Code = reinterpret_cast<Instruction::code_t*>(&Num);
    AddInstr(Instruction(*Code));

    ++Code;
    AddInstr(Instruction(*Code));
}

void XieXieAssembler::ParseInstructionReg1(const std::string& Mnemonic, const MnemonicCategory& MnCateogry)
{
    /* Parse 1st operand */
    auto Reg = ParseRegister();

    /* Add 1-register instruction */
    AddInstr(Instruction::CreateReg1(MnCateogry.OpCode, Reg, 0u));
}

void XieXieAssembler::ParseInstructionJump(const std::string& Mnemonic, const MnemonicCategory& MnCateogry)
{
    Instruction::Registers::reg_t Reg = Instruction::Registers::pc;

    /* Parse jump address */
    unsigned int Addr = 0;

    if (TokenType() == Token::Types::Register)
        Reg = ParseRegister();
    else
        Addr = ParseAddress(true);

    /* Add jump instruction */
    AddInstr(Instruction::CreateJump(MnCateogry.OpCode, Reg, Addr));
}

void XieXieAssembler::ParseInstructionMem(const std::string& Mnemonic, const MnemonicCategory& MnCateogry)
{
    /* Parse 1st operand */
    auto Reg1 = ParseRegister();

    Accept(Token::Types::Comma);

    /* Check if an address with offset is used */
    if (TokenType() == Token::Types::LBracket)
    {
        /* Parse 2nd operand */
        AcceptIt();
        auto Reg2 = ParseRegister();
        Accept(Token::Types::RBracket);

        /* Parse offset */
        auto Offset = ParseIntegerLiteral();

        /* Add instruction */
        AddInstr(Instruction::CreateLdStOff(MnCateogry.AltOpCode, Reg1, Reg2, Offset));
    }
    else
    {
        /* Parse memory address */
        auto Addr = ParseAddress();
        
        /* Add instruction */
        AddInstr(Instruction::CreateLdSt(MnCateogry.OpCode, Reg1, Addr));
    }
}

void XieXieAssembler::ParseInstructionSpecial(const std::string& Mnemonic, const MnemonicCategory& MnCateogry)
{
    if (MnCateogry.OpCode == Instruction::OpCodes::Special::PUSH)
    {
        if (TokenType() == Token::Types::Register)
        {
            /* Parse register */
            auto Reg = ParseRegister();

            /* Add register-1 instruction */
            AddInstr(Instruction::CreateReg1(Instruction::OpCodes::Reg1::PUSH, Reg));
        }
        else if (TokenType() == Token::Types::IntLiteral)
        {
            /* Parse integer literal */
            auto Value = ParseIntegerLiteral();

            /* Add special instruction */
            AddInstr(Instruction::CreateSpecial(MnCateogry.OpCode, Value));
        }
        else
        {
            /* Parse address label */
            auto Addr = ParseAddress();

            /* Add special instruction */
            AddInstr(Instruction::CreateSpecial(MnCateogry.OpCode, Addr));
        }
    }
    else if (MnCateogry.OpCode == Instruction::OpCodes::Special::RET)
    {
        /* Parse 1st operand */
        Accept(Token::Types::LBracket);
        auto ResultSize = ParseIntegerLiteral();
        Accept(Token::Types::RBracket);

        /* Parse 2nd operand */
        auto ArgSize = ParseIntegerLiteral();

        /* Add special instruction */
        AddInstr(Instruction::CreateSpecial(MnCateogry.OpCode, ResultSize, ArgSize));
    }
    else
    {
        /* Just add the instruction with the specified op-code */
        AddInstr(Instruction::CreateSpecial(MnCateogry.OpCode));
    }
}

void XieXieAssembler::ParseInstructionExtra(const std::string& Mnemonic, const MnemonicCategory& MnCateogry)
{
    /* Parse 1st operand */
    auto Reg1 = ParseRegister();

    Accept(Token::Types::Comma);

    if (TokenType() == Token::Types::Register)
    {
        /* Parse 2nd operand (register) */
        auto Reg2 = ParseRegister();

        /* Add 2-register instruction */
        unsigned int Flags = 0u;
        AddInstr(Instruction::CreateReg2(MnCateogry.AltOpCode, Reg1, Reg2, Flags));
    }
    else
    {
        /* Parse 2nd operand (address or simple integer literal) */
        unsigned int Value = ParseAddress();

        /* Add 1-register instruction */
        AddInstr(Instruction::CreateReg1(MnCateogry.OpCode, Reg1, Value));
    }
}



// ================================================================================