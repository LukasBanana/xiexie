/*
 * XieXie compiler header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_COMPILER_XIEXIE_H__
#define __XX_COMPILER_XIEXIE_H__


#include <string>


class XieXieCompiler
{
    
    public:
        
        /* === Structures === */

        struct Flags
        {
            typedef unsigned int flag_t;
            static const flag_t ShowAST             = (1 << 0);
            static const flag_t UseASM              = (1 << 1);
            static const flag_t ShowNameVisTable    = (1 << 2);
        };

        /* === Constructor & destructor === */

        XieXieCompiler();
        ~XieXieCompiler();

        /* === Functions === */

        /**
        Compiles a single file.
        \param[in] Filename XieXie source file which is to be compiled.
        \param[in] Options Specifies the compilation options. This can be a combination of the
        flags defined in the "Flags" structure.
        \see XieXieCompiler::Flags
        */
        bool CompileSingleFile(const std::string& Filename, unsigned int Options = 0);

        /**
        Evaluates a single expression string.
        \param[in] ExprStr XieXie source code string which is to be evaluated.
        \param[in] Options Specifies the compilation options. This can be a combination of the
        flags defined in the "Flags" structure.
        \see XieXieCompiler::Flags
        */
        bool EvaluateExpression(const std::string& ExprStr, unsigned int Options = 0);

};


#endif



// ================================================================================