/*
 * Error reporter file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ErrorReporter.h"


ErrorReporter ErrorReporter::Instance;

ErrorReporter::ErrorReporter()
{
}
ErrorReporter::~ErrorReporter()
{
}

void ErrorReporter::Push(const CompilerMessage& Msg)
{
    Messages_.push_back(Msg);
}

bool ErrorReporter::Pop(CompilerMessage& Msg)
{
    if (!Messages_.empty())
    {
        Msg = Messages_.front();
        Messages_.pop_front();
        return true;
    }
    return false;
}

bool ErrorReporter::HasErrors() const
{
    for (auto Msg : Messages_)
    {
        if (Msg.IsError())
            return true;
    }
    return false;
}

void ErrorReporter::CountCategories(size_t& NumErrors, size_t& NumWarnings) const
{
    for (auto Msg : Messages_)
    {
        if (Msg.IsError())
            ++NumErrors;
        else if (Msg.Category() == CompilerMessage::Categories::Warning)
            ++NumWarnings;
    }
}



// ================================================================================