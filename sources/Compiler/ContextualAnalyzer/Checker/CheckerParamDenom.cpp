/*
 * Checker (parameter denomination resolving) helper file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Checker.h"
#include "ArgumentList.h"
#include "ParameterList.h"
#include "StringMod.h"

#include <algorithm>


namespace ContextualAnalyzer
{


/*
This function is used for parameter denomination support (or rather named-parameters) in a function call.
Consider the function "Test(int a = 0, int b = 0)", that "Test(b: 3*x)" translates to "Test(0, 3*x)".
Process:
1. Fill ordered argument list (store parameter and and argument expression).
2. Sort list by their indices.
3. Construct new argument list and fill the gaps between unused parameter arguments.
*/
ArgumentListPtr Checker::ResolveDenomArgumentList(ArgumentListPtr ArgList, ParameterListPtr ParamList)
{
    if (ParamList == nullptr)
    {
        Error("Argument list used for function without parameters", ArgList);
        return nullptr;
    }

    /* Structures for argument sort */
    struct IndexedArg
    {
        IndexedArg() :
            Index(0)
        {
        }
        IndexedArg(ArgumentListPtr IdxArg, int IdxValue) :
            Arg     (IdxArg     ),
            Index   (IdxValue   )
        {
        }

        ArgumentListPtr Arg;
        int Index;
    };

    /* (1) Fill ordered argument list */
    std::vector<IndexedArg> OrderedArgList;

    int ParamIndex = 0;
    bool IsDenomSequence = false;
    bool HasFailed = false;

    while (ArgList != nullptr)
    {
        if (ArgList->Denom != nullptr)
            IsDenomSequence = true;

        if (IsDenomSequence)
        {
            if (ArgList->Denom == nullptr)
            {
                /* Error -> denominator required but does not exist */
                Error("Missing denominator in argument list", ArgList);
                return nullptr;
            }

            /* Find parameter index by denominator */
            ParamIndex = ParamList->FindIndexByDenom(ArgList->Denom->Spell);

            if (ParamIndex == -1)
            {
                Error("Parameter denominator \"" + ArgList->Denom->Spell + "\" not found", ArgList);
                HasFailed = true;
            }
        }

        /* Add the current argument into the ordered list */
        if (!HasFailed)
            OrderedArgList.push_back(IndexedArg(ArgList, ParamIndex++));

        /* Get next argument */
        ArgList = ArgList->Next;
    }

    if (HasFailed)
        return nullptr;

    /* (2) Sort ordered list by the parameter indices */
    std::sort(
        OrderedArgList.begin(), OrderedArgList.end(),
        [](const IndexedArg& a, const IndexedArg& b) -> bool
        {
            return a.Index < b.Index;
        }
    );

    /* Search for duplicates */
    auto it = OrderedArgList.begin(), itPrev = it;

    for (++it; it != OrderedArgList.end(); ++it)
    {
        if (itPrev->Index == it->Index)
        {
            Error("Duplicate parameter denomination in argument list (At index " + xxStr(it->Index) + ")", it->Arg);
            return nullptr;
        }
        itPrev = it;
    }

    /* Temporary lambda for AST node conncetion */
    int PrevParamIndex = -1;
    ArgumentListPtr PrevArgList, RootArgList;

    auto LinkASTNodes = [&](int Index)
    {
        /* Setup AST node connection */
        if (PrevArgList != nullptr)
            PrevArgList->Next = ArgList;
        PrevArgList = ArgList;

        /* Store new root argument list AST node */
        if (Index == 0)
            RootArgList = ArgList;
    };

    /* (3) Generate new argument list */
    for (auto Entry : OrderedArgList)
    {
        ParamIndex = Entry.Index;

        /*
        First generate new argument AST nodes if there is
        a gap between the previous index and the current one.
        */
        for (int i = PrevParamIndex + 1; i < ParamIndex; ++i)
        {
            /* Get default expression by denominator and create AST node */
            auto Expr = ParamList->FindDefaultExprByIndex(i);
            
            if (Expr == nullptr)
            {
                /* Error -> now default expression found */
                Error("Parameter has no default expression for implicit argument (At index " + xxStr(i) + ")", ParamList);
                HasFailed = true;
            }
            else
                ArgList = std::make_shared<ArgumentList>(Expr);

            LinkASTNodes(i);
        }

        /* Setup current parameter */
        ArgList = Entry.Arg;

        LinkASTNodes(ParamIndex);
        PrevParamIndex = ParamIndex;
    }

    /* Cut off last argument connection */
    ArgList->Next = nullptr;

    return HasFailed ? nullptr : RootArgList;
}


} // /namespace ContextualAnalyzer



// ================================================================================