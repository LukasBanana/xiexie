/*
 * Checker standard procedures file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Checker.h"
#include "ConsoleOutput.h"
#include "ObjectExpression.h"
#include "CallCommand.h"
#include "FunctionObject.h"
#include "ArrayTypeDenoter.h"
#include "FlagsObject.h"
#include "EnumObject.h"


namespace ContextualAnalyzer
{


static const std::string ERR_TOO_MANY_ARGS  = "Too many arguments for parameter list";
static const std::string ERR_TOO_FEW_ARGS   = "Too few arguments for parameter list";

static const std::string TYPENAME_FLAGS     = "falgs";
static const std::string TYPENAME_ENUM      = "enumeration";

/*
 * ======= Private: =======
 */

static std::string StdProcDesc(const std::string& TypeName)
{
    return "standard " + TypeName + " procedure";
}

/* --- COMMON --- */

CustomTypeDenoter* Checker::DeriveCustomTDenoterFromFName(
    const FNameIdentPtr& FName, const std::string& TypeName, const std::string& TypeIdentifier)
{
    /* Get flags' variable */
    auto VarFName = FName;

    if (VarFName->Next != nullptr)
    {
        /* Find second to last FName identifier */
        while (!VarFName->Next->IsLast())
            VarFName = VarFName->Next;
    }

    if (VarFName->Link == nullptr || VarFName->Link->Type() != Object::Types::VarObj)
    {
        Error("Identifier \"" + VarFName->Ident->Spell + "\" does not name a variable of valid " + TypeName + " type", VarFName);
        return nullptr;
    }

    auto VarObj = dynamic_cast<VariableObject*>(VarFName->Link);

    /* Get variable's type denoter full FName identifier */
    if (VarObj->TDenoter->Type() != TypeDenoter::Types::CustomType)
    {
        Error(
            "Type denoter \"" + VarObj->TDenoter->Spell +
            "\" does not name the required " + TypeName + " type (Expected \"" +
            TypeIdentifier + "\")", VarObj->TDenoter
        );
        return nullptr;
    }

    return dynamic_cast<CustomTypeDenoter*>(VarObj->TDenoter.get());
}

bool Checker::CheckStdProcArgListNum(
    const ArgumentListPtr& ArgList, const std::string& TypeName, const size_t MinAndMaxNum)
{
    return CheckStdProcArgListNum(ArgList, TypeName, MinAndMaxNum, MinAndMaxNum);
}

bool Checker::CheckStdProcArgListNum(
    const ArgumentListPtr& ArgList, const std::string& TypeName, const size_t MinNum, const size_t MaxNum)
{
    const size_t Num = (ArgList != nullptr ? ArgList->GetArgNum() : 0);

    if (Num < MinNum)
        Error(ERR_TOO_FEW_ARGS + " in " + StdProcDesc(TypeName), ArgList);
    else if (Num > MaxNum)
        Error(ERR_TOO_MANY_ARGS + " in " + StdProcDesc(TypeName), ArgList);
    else
        return true;

    return false;
}

bool Checker::CheckStdProcLink(
    const FNameIdentPtr& FName, const FNameIdentPtr& LastFName, const std::string& TypeName, const FNameIdent::Flags::DataType& ProcFlag)
{
    /* Check if this is a standard <TypeName> procedure */
    if ((LastFName->IdentFlags & ProcFlag) == 0)
    {
        Error("Identifier \"" + FName->FullName() + "\" does not name a " + StdProcDesc(TypeName), LastFName);
        return false;
    }
    return true;
}

bool Checker::CheckStdProcArgFlag(
    const FNameIdentPtr& FName, const FNameIdent::Flags::DataType& EntryFlag)
{
    if ((FName->IdentFlags & EntryFlag) == 0)
    {
        Error("Identifier \"" + FName->FullName() + "\" does not name a valid flags entry", FName);
        return false;
    }
    return true;
}

/* --- FLAGS --- */

bool Checker::CheckFlagsStdProcCall(
    FlagsObject* FlagsObj, const CallCommandPtr& AST, const FNameIdentPtr& FName, const FNameIdentPtr& LastFName)
{
    /* Check if this is a standard flags procedure */
    if (!CheckStdProcLink(FName, LastFName, TYPENAME_FLAGS, FNameIdent::Flags::FlagsProc))
        return false;

    /* Check argument list */
    if (!CheckStdProcArgListNum(AST->ArgList, TYPENAME_FLAGS, 1))
        return false;

    /* Check argument type */
    auto Expr = AST->ArgList->Expr;

    if (Expr->Type() != Expression::Types::ObjectExpr)
    {
        Error("Invalid argument for " + StdProcDesc(TYPENAME_FLAGS), Expr);
        return false;
    }
    //else -> !TODO! -> allow type casts!!!

    auto ObjExpr = dynamic_cast<ObjectExpression*>(Expr.get());

    if (!CheckStdProcArgFlag(ObjExpr->FName, FNameIdent::Flags::FlagsEntry))
        return false;

    /* Get flags' variable's type denoter full FName identifier */
    auto CTDenoter = DeriveCustomTDenoterFromFName(FName, TYPENAME_FLAGS, FlagsObj->Ident->Spell);

    if (CTDenoter == nullptr)
        return false;

    /* Insert namespace identifier */
    auto ParentFName = CTDenoter->FName->Copy();
    ObjExpr->FName = ObjExpr->FName->InsertFName(ParentFName);

    return true;
}

/* --- ENUMERATION --- */

bool Checker::CheckEnumStdProcCall(
    EnumObject* EnumObj, const CallCommandPtr& AST, const FNameIdentPtr& FName, const FNameIdentPtr& LastFName)
{
    /* Check if this is a standard enumeration procedure */
    if (!CheckStdProcLink(FName, LastFName, TYPENAME_ENUM, FNameIdent::Flags::EnumProc))
        return false;

    /* Check which procedure is used */
    const auto& ProcName = LastFName->Ident->Spell;

    if (ProcName == "Str" || ProcName == "Num")
    {
        /* Check argument list */
        return CheckStdProcArgListNum(AST->ArgList, TYPENAME_ENUM, 0);
    }
    else if (ProcName == "Is" || ProcName == "Set")
    {
        /* Check argument list */
        if (!CheckStdProcArgListNum(AST->ArgList, TYPENAME_ENUM, 1))
            return false;

        /* Check argument type */
        auto Expr = AST->ArgList->Expr;

        if (Expr->Type() != Expression::Types::ObjectExpr)
        {
            Error("Invalid argument for " + StdProcDesc(TYPENAME_ENUM), Expr);
            return false;
        }
        //else -> !TODO! -> allow type casts!!!

        auto ObjExpr = dynamic_cast<ObjectExpression*>(Expr.get());

        if (!CheckStdProcArgFlag(ObjExpr->FName, FNameIdent::Flags::EnumEntry))
            return false;

        /* Get flags' variable's type denoter full FName identifier */
        auto CTDenoter = DeriveCustomTDenoterFromFName(FName, TYPENAME_ENUM, EnumObj->Ident->Spell);

        if (CTDenoter == nullptr)
            return false;

        /* Insert namespace identifier */
        auto ParentFName = CTDenoter->FName->Copy();
        ObjExpr->FName = ObjExpr->FName->InsertFName(ParentFName);
    }

    return true;
}

/* --- MAIN --- */

bool Checker::CheckMainProcedure(FunctionObjectPtr AST)
{
    try
    {
        /* Check for correct parameters */
        if (AST->ParamList != nullptr)
        {
            static const char* MPArgErr =
                "\"Main\" function may only have zero or one arrays of strings as "
                "parameters (i.e. 'Main()' or 'Main(string[] args)')";

            /* Check that the parameter list only consists of one parameter */
            if (AST->ParamList->Next != nullptr)
                throw std::string(MPArgErr);

            /* Check that this single parameter is an array of strings */
            auto VarObj = AST->ParamList->Obj.get();

            if (VarObj == nullptr || VarObj->TDenoter->Type() != TypeDenoter::Types::ArrayType)
                throw std::string(MPArgErr);

            auto AryTDenoter = dynamic_cast<ArrayTypeDenoter*>(VarObj->TDenoter.get());

            if (AryTDenoter->TDenoter->Type() != TypeDenoter::Types::StringType)
                throw std::string(MPArgErr);
        }

        /* Check for correct return type */
        if (AST->TDenoter->Type() != TypeDenoter::Types::IntType && AST->TDenoter->Type() != TypeDenoter::Types::VoidType)
            throw std::string("\"Main\" function may only has 'integer' or 'void' as return type");

        return true;
    }
    catch (const std::string& Err)
    {
        Error(Err, AST->ParamList);
    }

    return false;
}


} // /namespace ContextualAnalyzer



// ================================================================================