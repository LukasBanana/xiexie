/*
 * Checker header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_CHECKER_H__
#define __XX_CHECKER_H__


#include "Visitor.h"
#include "ScopeManager.h"
#include "Program.h"
#include "StackWrapper.h"
#include "Counter.h"

#include <string>
#include <map>


namespace ContextualAnalyzer
{


using namespace AbstractSyntaxTrees;


class_visitor(Checker)
{
    
    public:
        
        Checker();
        ~Checker();

        /* === Functions === */

        bool DecorateAST(ProgramPtr ProgramAST);

        void PrintNameVisibilityTable();

        /* === Commands === */

        DeclVisitProc(AssertCommand         )
        DeclVisitProc(AssignCommand         )
        DeclVisitProc(BlockCommand          )
        DeclVisitProc(CallCommand           )
        DeclVisitProc(FunctionDefCommand    )
        DeclVisitProc(ImportCommand         )
        DeclVisitProc(PackageCommand        )
        DeclVisitProc(ReturnCommand         )
        DeclVisitProc(VariableDefCommand    )
        DeclVisitProc(IfCommand             )
        DeclVisitProc(InfiniteLoopCommand   )
        DeclVisitProc(WhileLoopCommand      )
        DeclVisitProc(RangeForLoopCommand   )
        DeclVisitProc(InlineLangCommand     )
        DeclVisitProc(PrimitiveCommand      )
        DeclVisitProc(EnumDeclCommand       )
        DeclVisitProc(FlagsDeclCommand      )
        DeclVisitProc(ClassDeclCommand      )
        DeclVisitProc(ClassBlockCommand     )
        DeclVisitProc(InitCommand           )
        DeclVisitProc(ReleaseCommand        )
        DeclVisitProc(TryCatchCommand       )
        DeclVisitProc(ThrowCommand          )
        DeclVisitProc(SwitchCommand         )
        DeclVisitProc(TypeDefCommand        )

        /* === Expressions === */

        DeclVisitProc(BracketExpression     )
        DeclVisitProc(BinaryExpression      )
        DeclVisitProc(UnaryExpression       )
        DeclVisitProc(StringSumExpression   )
        DeclVisitProc(AssignExpression      )
        DeclVisitProc(LiteralExpression     )
        DeclVisitProc(CallExpression        )
        DeclVisitProc(CastExpression        )
        DeclVisitProc(ObjectExpression      )
        DeclVisitProc(AllocExpression       )
        DeclVisitProc(CopyExpression        )
        DeclVisitProc(ValidationExpression  )
        DeclVisitProc(InitListExpression    )
        DeclVisitProc(LambdaExpression      )

        /* === Type denoters === */

        DeclVisitProc(VoidTypeDenoter       )
        DeclVisitProc(ConstTypeDenoter      )
        DeclVisitProc(BoolTypeDenoter       )
        DeclVisitProc(IntTypeDenoter        )
        DeclVisitProc(FloatTypeDenoter      )
        DeclVisitProc(StringTypeDenoter     )
        DeclVisitProc(CustomTypeDenoter     )
        DeclVisitProc(MngPtrTypeDenoter     )
        DeclVisitProc(RawPtrTypeDenoter     )
        DeclVisitProc(RefTypeDenoter        )
        DeclVisitProc(ProcTypeDenoter       )
        DeclVisitProc(ArrayTypeDenoter      )
        
        /* === Lists === */

        DeclVisitProc(ParameterList         )
        DeclVisitProc(ArgumentList          )
        DeclVisitProc(TemplateParamList     )
        DeclVisitProc(TemplateArgList       )
        DeclVisitProc(InitializerList       )
        DeclVisitProc(ArrayIndexList        )

        /* === Objects === */

        DeclVisitProc(ClassObject           )
        DeclVisitProc(EnumObject            )
        DeclVisitProc(FlagsObject           )
        DeclVisitProc(FunctionObject        )
        DeclVisitProc(TypeObject            )
        DeclVisitProc(VariableObject        )
        DeclVisitProc(PatternObject         )

        /* === Operators === */

        DeclVisitProc(AssignOperator        )
        DeclVisitProc(BinaryOperator        )
        DeclVisitProc(UnaryOperator         )

        /* === Others === */

        DeclVisitProc(Identifier            )
        DeclVisitProc(FNameIdent            )
        DeclVisitProc(Program               )
        DeclVisitProc(EnumEntry             )
        DeclVisitProc(CaseBlock             )

    private:
        
        /* === Structures === */

        struct AnalysisState
        {
            AnalysisState(const std::string& Description) :
                Desc(Description)
            {
            }

            /* Members */
            std::string Desc;
        };

        struct InstanceCounters
        {
            Counter CmdOnce;
            Counter AnonymousClass;
            Counter TemplateInstance;
            Counter ForLoopIterator;
        };

        /* === Functions === */

        void EstablishStdPackages();

        void RegisterInclude(const std::string& Filename, bool IsGlobal = true);
        void RegisterSmartPointerInclude();
        void RegisterCopyProc(const TypeDenoter* TDenoter);

        void ImportPackage(const std::string& PackageName);

        void Warning(const std::string& Message, ASTPtr AST = nullptr);
        void Error(const std::string& Message, ASTPtr AST = nullptr);

        void ErrorUndefinedCustomType(const FNameIdentPtr& AST);

        void PushState(const AnalysisState& State);
        void PopState();
        const AnalysisState* CurrentState() const;

        /* --- Checker functions --- */

        void CheckProgramHeaderGuard(ProgramPtr AST);
        void CheckOnceStatement(PrimitiveCommandPtr AST);

        void CheckFName();
        void CheckTypeCompatibility(ASTPtr AST, const TypeDenoter* T1, const TypeDenoter* T2);
        
        void CheckExpressionType(
            const ExpressionPtr& Expr, const TypeDenoter::Types RequiredType, const std::string& StatementDesc
        );

        void CheckObjectAssignment(const Object* Obj, ASTPtr AST = nullptr);
        
        void CheckReturnStatement(const TypeDenoter* RetType, const BlockCommand* CmdBlock, unsigned int& NumRet);
        
        void CheckSwitchCaseExpressions(const std::list<CaseBlockPtr>& CaseBlocks);

        /* --- Standard procedures --- */

        CustomTypeDenoter* DeriveCustomTDenoterFromFName(
            const FNameIdentPtr& FName, const std::string& TypeName, const std::string& TypeIdentifier
        );

        bool CheckStdProcArgListNum(const ArgumentListPtr& ArgList, const std::string& TypeName, const size_t MinAndMaxNum);
        bool CheckStdProcArgListNum(const ArgumentListPtr& ArgList, const std::string& TypeName, const size_t MinNum, const size_t MaxNum);

        bool CheckStdProcLink(
            const FNameIdentPtr& FName, const FNameIdentPtr& LastFName,
            const std::string& TypeName, const FNameIdent::Flags::DataType& ProcFlag
        );

        bool CheckStdProcArgFlag(
            const FNameIdentPtr& FName, const FNameIdent::Flags::DataType& EntryFlag
        );

        bool CheckFlagsStdProcCall(
            FlagsObject* FlagsObj, const CallCommandPtr& AST,
            const FNameIdentPtr& FName, const FNameIdentPtr& LastFName
        );

        bool CheckEnumStdProcCall(
            EnumObject* EnumObj, const CallCommandPtr& AST,
            const FNameIdentPtr& FName, const FNameIdentPtr& LastFName
        );

        bool CheckMainProcedure(FunctionObjectPtr AST);

        /* --- Template instantiation functions --- */

        //! \throw std::string
        void CheckForTemplateInstantiation(TypeDenoter* TDenoter);
        void InstantiateTemplateClass(TypeDenoter* TDenoter, ClassObject* ClassObj, const TemplateArgList* TArgList);

        std::string GenerateTemplateInstanceName(const std::string& OrigName);

        /* --- Decoration functions --- */

        bool DecorateExpr(const ExpressionPtr& AST);

        void DecorateCopyProcObjects();

        void DecorateAndAppendClassMemberProcs(
            ClassObject* ClassObj, const std::vector<FunctionDefCommandPtr>& List
        );

        bool DecorateFNameArrayIndexList(const FNameIdentPtr& FName);

        /* --- Generation functions --- */

        std::string GenAnonymousClassName(const ClassObject* ClassObj);
        std::string GenDefaultForLoopIterator();

        /* --- Other contextual helper functions --- */

        ArgumentListPtr ResolveDenomArgumentList(ArgumentListPtr ArgList, ParameterListPtr ParamList);

        /* === Members === */

        ScopeManager ScopeMngr_;

        ProgramPtr ProgramAST_;                             //!< Keep track of the program AST node.

        InstanceCounters Counters_;

        std::map<std::string, std::string> StdPackages_;    //!< Standard library packages <package, filename>.
        ClassObjectSetPtr CopyProcObjects_;                 //!< Set of all class objects for which the 'copy' procedure must be generated.

        std::list<AnalysisState> StateStack_;

        /**
        This is a stack for temporary local scopes, e.g. a flags object.
        This is used to support the comfort, that a namespace must not be written,
        inside a standard function, e.g. "myFlags.Add(Entry0)" instead of "myFlags.Add(MyFlagsType.Entry0)".
        */
        StackWrapper<Object*> LocalScopeStack_;

};


} // /namespace ContextualAnalyzer


#endif



// ================================================================================