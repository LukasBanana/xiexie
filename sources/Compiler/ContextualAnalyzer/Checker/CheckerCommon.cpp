/*
 * Checker common file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Checker.h"
#include "ConsoleOutput.h"
#include "ASTIncludes.h"
#include "CompilerMessage.h"
#include "CompilerOptions.h"
#include "StringMod.h"
#include "CustomTypeDenoter.h"
#include "ErrorReporter.h"
#include "ConstExpressionEvaluator.h"


using namespace ConstantFolding;

namespace ContextualAnalyzer
{


/*
 * ======= Private: =======
 */

void Checker::EstablishStdPackages()
{
    std::map<std::string, std::string> Table;

    Table["Console" ] = "IO/Console.xx";
    Table["Math"    ] = "Math/Math.xx";

    StdPackages_ = Table;
}

void Checker::RegisterInclude(const std::string& Filename, bool IsGlobal)
{
    if (ProgramAST_)
        ProgramAST_->IncludeFiles[Filename] = IsGlobal;
}

void Checker::RegisterSmartPointerInclude()
{
    RegisterInclude(
        CompilerOptions::Settings.EnableCpp11 ? "memory" : "boost/shared_ptr.hpp"
    );
}

void Checker::RegisterCopyProc(const TypeDenoter* TDenoter)
{
    /* Register only custom type denoters to classes */
    TDenoter = TDenoter->GetResolvedType();

    if (TDenoter != nullptr && TDenoter->Type() == TypeDenoter::Types::CustomType)
    {
        auto CTDenoter = dynamic_cast<const CustomTypeDenoter*>(TDenoter);

        auto Obj = CTDenoter->Link.get();

        if (Obj != nullptr && Obj->Type() == Object::Types::ClassObj)
        {
            auto ClassObj = dynamic_cast<ClassObject*>(Obj);
            CopyProcObjects_->insert(ClassObj);
        }
    }
}

void Checker::ImportPackage(const std::string& PackageName)
{
    auto it = StdPackages_.find(PackageName);

    if (it != StdPackages_.end())
        RegisterInclude("XieXie/" + it->second);
}

void Checker::Warning(const std::string &Msg, ASTPtr AST)
{
    if (AST != nullptr)
        ErrorReporter::Instance.Push(CompilerWarning(AST->Pos(), Msg));
    else
        ErrorReporter::Instance.Push(CompilerWarning(Msg));
}

void Checker::Error(const std::string &Msg, ASTPtr AST)
{
    if (!StateStack_.empty())
    {
        for (auto State : StateStack_)
        {
            /* Push state message */
            const auto StateDesc = "[ State: " + State.Desc + " ] ";

            if (AST != nullptr)
                ErrorReporter::Instance.Push(StateError(AST->Pos(), StateDesc + Msg));
            else
                ErrorReporter::Instance.Push(StateError(StateDesc + Msg));
        }
    }
    else
    {
        if (AST != nullptr)
            ErrorReporter::Instance.Push(ContextError(AST->Pos(), Msg));
        else
            ErrorReporter::Instance.Push(ContextError(Msg));
    }
}

void Checker::ErrorUndefinedCustomType(const FNameIdentPtr& AST)
{
    Error("Undeclared custom type denoter \"" + AST->FullName() + "\"", AST);
}

void Checker::PushState(const AnalysisState& State)
{
    StateStack_.push_back(State);
}

void Checker::PopState()
{
    if (!StateStack_.empty())
        StateStack_.pop_back();
}

const Checker::AnalysisState* Checker::CurrentState() const
{
    return StateStack_.empty() ? nullptr : &(StateStack_.back());
}

/* --- Checker functions --- */

void Checker::CheckProgramHeaderGuard(ProgramPtr AST)
{
    /* Check if first token in program hints for a header guard */
    if (AST->Commands.empty())
        return;
    
    CommandPtr FirstCmd = AST->Commands.front();

    if (FirstCmd->Type() != Command::Types::PrimCmd)
        return;
    
    PrimitiveCommandPtr PrimCmdAST = std::dynamic_pointer_cast<PrimitiveCommand>(FirstCmd);

    if (PrimCmdAST->PrimType != Token::Types::Once)
        return;
    
    /* Decorate AST */
    AST->IsHeaderGuardUsed = true;
}

void Checker::CheckOnceStatement(PrimitiveCommandPtr AST)
{
    if (!ProgramAST_->IsHeaderGuardUsed && Counters_.CmdOnce == 0)
        Warning("Keyword \"once\" can only be used as the first statement", AST);
    if (Counters_.CmdOnce == 1)
        Warning("Keyword \"once\" can not be used several times", AST);
    ++Counters_.CmdOnce;
}

void Checker::CheckExpressionType(
    const ExpressionPtr& Expr, const TypeDenoter::Types RequiredType, const std::string& StatementDesc)
{
    auto TDenoter = Expr->CommonTDenoter;

    if (TDenoter == nullptr || TDenoter->Type() != RequiredType)
    {
        /* Setup type description from expression type */
        std::string TypeDesc;

        switch (RequiredType)
        {
            case TypeDenoter::Types::BoolType:
                TypeDesc = "a boolean";
                break;
            case TypeDenoter::Types::RawPtrType:
            case TypeDenoter::Types::MngPtrType:
                TypeDesc = "a pointer";
                break;
            case TypeDenoter::Types::ConstType:
                TypeDesc = "a constant";
                break;
            case TypeDenoter::Types::IntType:
                TypeDesc = "an integer";
                break;
            case TypeDenoter::Types::FloatType:
                TypeDesc = "a floating-point";
                break;
            case TypeDenoter::Types::StringType:
                TypeDesc = "a string";
                break;
            default:
                return;
        }

        if (TDenoter == nullptr)
            Warning("Expression in " + StatementDesc + " statement must be " + TypeDesc + " expression (This expression has an unknown type)", Expr);
        else
            Error("Expression in " + StatementDesc + " statement must be " + TypeDesc + " expression", Expr);
    }
}

void Checker::CheckTypeCompatibility(ASTPtr AST, const TypeDenoter* T1, const TypeDenoter* T2)
{
    /* Check for type compatibility */
    if (T1 != nullptr && T2 != nullptr)
    {
        if (!T1->Compare(T2))
        {
            Warning(
                "Incompatible types ('" +
                T1->Spell + "' and '" + T2->Spell  + "')",
                AST
            );
        }
    }
    else
        Warning("Unknown types", AST);
}

void Checker::CheckObjectAssignment(const Object* Obj, ASTPtr AST)
{
    if (Obj == nullptr)
        return;

    switch (Obj->Type())
    {
        case Object::Types::FuncObj:
        {
            /* Get procedure object */
            auto FuncObj = dynamic_cast<const FunctionObject*>(Obj);

            if (FuncObj->TDenoter->Type() != TypeDenoter::Types::RefType)
                Error("Function return values can only have assignments if they are reference types", AST);
        }
        break;

        case Object::Types::VarObj:
        {
            /* Get variable object */
            auto VarObj = dynamic_cast<const VariableObject*>(Obj);

            if (VarObj->TDenoter->Type() == TypeDenoter::Types::ConstType)
                Error("Constant variables can not have assignments", AST);
        }
        break;

        default:
            Error("Assignment is only allowed to variables or function reference return values", AST);
            break;
    }
}

void Checker::CheckReturnStatement(
    const TypeDenoter* RetType, const BlockCommand* CmdBlock, unsigned int& NumRet)
{
    if (CmdBlock == nullptr)
        return;

    const bool IsVoidType = (RetType->Type() == TypeDenoter::Types::VoidType);

    /* Check if there is a correct return type */
    for (auto Cmd : CmdBlock->Commands)
    {
        if (Cmd->Type() != Command::Types::RetCmd)
            continue;
        
        ++NumRet;

        /* Check return type */
        auto RetCmd = std::dynamic_pointer_cast<ReturnCommand>(Cmd);

        if (RetCmd->Expr == nullptr)
        {
            if (!IsVoidType)
                Error("Missing expression in return statement for function's return type", RetCmd);
        }
        else
        {
            if (IsVoidType)
                Error("Expression not allowed in return statement for function with 'void' return type", RetCmd);

            /* Compare expression type with return type */
            auto RetTDenoter = RetCmd->Expr->GetTypeDenoter(ScopeMngr_);

            CheckTypeCompatibility(RetCmd->Expr, RetTDenoter, RetType);
        }
    }
}

void Checker::CheckSwitchCaseExpressions(const std::list<CaseBlockPtr>& CaseBlocks)
{
    std::vector<ConstantPtr> ComparisionList;

    for (auto CaseBlockAST : CaseBlocks)
    {
        /* Check expressions constness */
        auto Expr = CaseBlockAST->Expr;

        if (!Expr->IsConst())
            Error("Case blocks must always have constant expressions", CaseBlockAST);

        /* Evaluate constant expression */
        ConstExpressionEvaluator ConstExpEval;
        auto Const = ConstExpEval.Evaluate(Expr);

        if (Const != nullptr)
        {
            /* Compare this constant with the others in the comparision list */
            bool FoundConflict = false;

            for (auto Other : ComparisionList)
            {
                if (Const->Compare(Other.get()))
                {
                    Error("Duplicated case expressions", Expr);
                    FoundConflict = true;
                    break;
                }
            }

            /* Store constant in comparision list */
            if (!FoundConflict)
                ComparisionList.push_back(Const);
        }
        else
            Error("Could not evaluate constant expression", Expr);
    }
}

/* --- Decoration functions --- */

bool Checker::DecorateExpr(const ExpressionPtr& AST)
{
    try
    {
        AST->SetupCommonTypeDenoter(ScopeMngr_);
    }
    catch (const ContextError& Err)
    {
        Error(Err.Message(), AST);
        return false;
    }
    catch (const CompilerWarning& Warn)
    {
        Warning(Warn.Message(), AST);
    }
    return true;
}

void Checker::DecorateCopyProcObjects()
{
    for (auto Obj : *CopyProcObjects_)
        Obj->MarkForCopyProc();
}

void Checker::DecorateAndAppendClassMemberProcs(
    ClassObject* ClassObj, const std::vector<FunctionDefCommandPtr>& List)
{
    if (ClassObj != nullptr)
    {
        for (auto CmdAST : List)
        {
            /* Add procedure as member to the class object */
            ClassObj->AddMember(CmdAST->Obj.get());

            /* Decorate new procedure defintion AST node */
            CmdAST->Obj->DefFlags |= FunctionObject::Flags::IsClassMember;
            CmdAST->VISIT;
        }
    }
}

bool Checker::DecorateFNameArrayIndexList(const FNameIdentPtr& FName)
{
    if (FName == nullptr)
        return false;
    if (FName->ArrayList == nullptr)
        return true;

    /* Get FName's object */
    auto Obj = FName->Link;

    if (Obj == nullptr)
        return false;

    /* Get object's type-denoter */
    TypeDenoterPtr TDenoter;

    switch (Obj->Type())
    {
        case Object::Types::VarObj:
        {
            auto VarObj = dynamic_cast<VariableObject*>(Obj);
            TDenoter = VarObj->TDenoter;
        }
        break;

        //TO BE CONTINUED ... !!!

        default:
            Error("\"" + FName->FullName() + "\" does not name an array-indexable object", FName);
            return false;
    }

    /* Decorate all array index lists */
    auto ArrayList = FName->ArrayList;

    while (ArrayList != nullptr)
    {
        /* Check if current type denoter is an indexable */
        if ( TDenoter == nullptr ||
             ( TDenoter->Type() != TypeDenoter::Types::ArrayType &&
               TDenoter->Type() != TypeDenoter::Types::RawPtrType &&
               TDenoter->Type() != TypeDenoter::Types::StringType ) )
        {
            Error("Index access can only be used for arrays, raw-pointers and strings", ArrayList);
            return false;
        }

        /* Decorate array index list with current type-denoter */
        ArrayList->Link = TDenoter.get();

        /* Get next type-denoter */
        switch (TDenoter->Type())
        {
            case TypeDenoter::Types::ArrayType:
                TDenoter = std::dynamic_pointer_cast<ArrayTypeDenoter>(TDenoter)->TDenoter;
                break;
            case TypeDenoter::Types::RawPtrType:
                TDenoter = std::dynamic_pointer_cast<RawPtrTypeDenoter>(TDenoter)->TDenoter;
                break;
            case TypeDenoter::Types::StringType:
                if (ArrayList->Next != nullptr)
                {
                    Error("Index access can not be used for string elements", ArrayList->Next);
                    return false;
                }
                break;
            default:
                break;
        }
        
        /* Get next array index list AST node */
        ArrayList = ArrayList->Next;
    }

    return true;
}

/* --- Generation functions --- */

std::string Checker::GenAnonymousClassName(const ClassObject* ClassObj)
{
    return "__XX__AnonymousClass__" + xxStr(++Counters_.AnonymousClass);
}

std::string Checker::GenDefaultForLoopIterator()
{
    return "__XX__It" + xxStr(++Counters_.ForLoopIterator);
}


} // /namespace ContextualAnalyzer



// ================================================================================