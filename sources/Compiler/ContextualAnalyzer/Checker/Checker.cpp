/*
 * Checker file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Checker.h"
#include "ASTIncludes.h"
#include "CompilerMessage.h"
#include "CompilerOptions.h"
#include "CustomTypeDenoter.h"
#include "ErrorReporter.h"
#include "ConstExpressionEvaluator.h"
#include "ProcedureFactory.h"


using namespace ConstantFolding;

namespace ContextualAnalyzer
{


static const std::string ErrDuplicated  = "Duplicate definitions of ";
static const std::string AutoGenErrPart = " is generated automatically and must not be overwritten";

Checker::Checker() :
    Visitor         (                                   ),
    CopyProcObjects_(std::make_shared<ClassObjectSet>() )
{
    EstablishStdPackages();
}
Checker::~Checker()
{
}

bool Checker::DecorateAST(ProgramPtr ProgramAST)
{
    ConsoleOutput::Message("Contextual analysis ...");
    ScopedIndent Unused;

    if (!ProgramAST)
        return false;

    ProgramAST_ = ProgramAST;

    /* Generate all standard procedures */
    ProcedureFactory::GenStdStringProcs();

    try
    {
        /* Decoreate program AST nodes */
        ProgramAST->VISIT;

        if (ErrorReporter::Instance.HasMessages())
        {
            /* Print all error messages */
            const bool HasErrors = ErrorReporter::Instance.HasErrors();
            ConsoleOutput::PrintErrorReport(ErrorReporter::Instance);

            if (HasErrors)
                return false;
        }
    }
    catch (const std::exception& Err)
    {
        ConsoleOutput::Error(Err.what());
        return false;
    }

    DecorateCopyProcObjects();

    ProgramAST_ = nullptr;

    return true;
}

void Checker::PrintNameVisibilityTable()
{
    ScopeMngr_.GlobalNamespace()->PrintTable();
}

/* === Commands === */

DefVisitProc(Checker, AssertCommand)
{
    RegisterInclude("XieXie/Lang/AssertCore.h");

    AST->Expr->VISIT;
    if (AST->InfoExpr != nullptr)
        AST->InfoExpr->VISIT;
}

DefVisitProc(Checker, AssignCommand)
{
    /* Check FName, operator and expression */
    AST->FName->VISIT;
    AST->Op->VISIT;
    AST->Expr->VISIT;

    /* Get assignment variable object AST node */
    auto FName = AST->FName->GetLastIdent();
    auto Obj = FName->Link;

    if (Obj != nullptr)
    {
        if (Obj->Type() != Object::Types::VarObj)
            Error("Assignment to non-variable object \"" + FName->Ident->Spell + "\"", FName);

        auto VarObj = dynamic_cast<const VariableObject*>(Obj);

        /* Check for type compatibility */
        auto T1 = VarObj->GetTypeDenoter();
        auto T2 = AST->Expr->GetTypeDenoter(ScopeMngr_);

        CheckTypeCompatibility(AST, T1, T2);
    }
}

DefVisitProc(Checker, BlockCommand)
{
    if (AST->OpensScope)
        ScopeMngr_.PushScope();

    for (const auto& CmdAST : AST->Commands)
        CmdAST->VISIT;

    if (AST->OpensScope)
        ScopeMngr_.Pop();
}

DefVisitProc(Checker, CallCommand)
{
    /* Decorate identifier */
    auto& FName = AST->FName;

    FName->VISIT;

    /* Check if local namespace must be used temporarily */
    auto LastFName = FName->GetLastIdent();

    /* Decorate argument list */
    if (AST->ArgList != nullptr)
    {
        const bool UseLocalNameSpace =
            LastFName->Link != nullptr &&
            ( (LastFName->IdentFlags & FNameIdent::Flags::FlagsProc) != 0 ||
              (LastFName->IdentFlags & FNameIdent::Flags::EnumProc) != 0 );

        if (UseLocalNameSpace)
            LocalScopeStack_.Push(LastFName->Link);
        
        AST->ArgList->VISIT;

        if (UseLocalNameSpace)
            LocalScopeStack_.Pop();
    }

    /* Find procedure or procedure type object (by FName identifier) */
    ObjectPtr Obj;

    try
    {
        Obj = ScopeMngr_.Find(FName);
    }
    catch (const std::string& Err)
    {
        Error(Err, FName);
    }

    if (Obj == nullptr)
    {
        ErrorUndefinedCustomType(FName);
        return;
    }

    /* Define temporary lambda */
    static const std::string ERR_TOO_MANY_ARGS = "Too many arguments for parameter list";

    auto ResolveAndCheckArgList = [&](const ParameterListPtr& ParamList)
    {
        /* Resolve argument list if denominators are used */
        if (AST->ArgList != nullptr && AST->ArgList->HasDenom())
        {
            auto NewArgList = ResolveDenomArgumentList(AST->ArgList, ParamList);
            if (NewArgList != nullptr)
                AST->ArgList = NewArgList;
        }

        /* Check if parameter list fits into procedure's argument list */
        if (ParamList != nullptr)
            ParamList->Compare(AST->ArgList.get(), ScopeMngr_);
        else if (AST->ArgList != nullptr)
            throw ERR_TOO_MANY_ARGS;
    };

    /* Catch error strings from "ParameterList::Compare" function */
    try
    {
        switch (Obj->Type())
        {
            case Object::Types::VarObj:
            {
                /* Check if variable is from a procedure type */
                auto VarObj = std::dynamic_pointer_cast<VariableObject>(Obj);
                auto ProcTDenoter = VarObj->TDenoter->GetResolvedProcType();

                if (ProcTDenoter == nullptr)
                    Error("Variable \"" + FName->FullName() + "\" does not name a procedure type object", FName->GetLastIdent());
                else
                    ResolveAndCheckArgList(ProcTDenoter->ParamList);
            }
            break;

            case Object::Types::FuncObj:
            {
                /* Decorate function object */
                AST->Obj = std::dynamic_pointer_cast<FunctionObject>(Obj);

                ResolveAndCheckArgList(AST->Obj->ParamList);
            }
            break;

            case Object::Types::FlagsObj:
            {
                /* Decorate call expressions of standard flags procedures */
                CheckFlagsStdProcCall(
                    dynamic_cast<FlagsObject*>(Obj.get()), AST, FName, LastFName
                );
            }
            break;

            case Object::Types::EnumObj:
            {
                /* Decorate call expressions of standard enumeration procedures */
                CheckEnumStdProcCall(
                    dynamic_cast<EnumObject*>(Obj.get()), AST, FName, LastFName
                );
            }
            break;

            default:
            {
                Error("Identifier \"" + FName->FullName() + "\" does not name a procedure or procedure type object", LastFName);
            }
            break;
        }
    }
    catch (const std::string& Err)
    {
        Error(Err, AST);
    }
}

DefVisitProc(Checker, FunctionDefCommand)
{
    AST->Obj->VISIT;
}

DefVisitProc(Checker, ImportCommand)
{
    /* Import package names */
    auto FNameAST = AST->FName;

    while (FNameAST)
    {
        ImportPackage(FNameAST->Ident->Spell);
        FNameAST = FNameAST->Next;
    }
}

DefVisitProc(Checker, PackageCommand)
{
    /* Open package namespaces */
    auto FName = AST->FName;
    size_t Num = 0;

    while (FName != nullptr)
    {
        ScopeMngr_.PushPackage(FName->Ident);
        FName = FName->Next;
        ++Num;
    }

    /* Decorate command block */
    AST->BlockCmd->OpensScope = false;
    AST->BlockCmd->VISIT;

    /* Close package namespaces */
    for (; Num > 0; --Num)
        ScopeMngr_.Pop();
}

DefVisitProc(Checker, ReturnCommand)
{
    if (AST->Expr != nullptr)
        AST->Expr->VISIT;
}

DefVisitProc(Checker, VariableDefCommand)
{
    auto Obj = AST->Obj;
    
    /* Check attributes validity */
    AttributeList::ForEach(
        AST->AttribList,
        [&](const AttributeList& Attrib)
        {
            if (!Attrib.HasAttrib(AttributeList::Attributes::Set, false) &&
                !Attrib.HasAttrib(AttributeList::Attributes::Get, false))
            {
                Error("Invalid attribute for varaible \"" + Obj->Name() + "\" definition: \"" + Attrib.Ident->Spell + "\"", AST);
            }
        }
    );

    /* Decorate current variable object */
    Obj->VISIT;

    /* Check if reference variable is initialized correctly */
    if (Obj->TDenoter->Type() == TypeDenoter::Types::RefType)
    {
        if (Obj->Expr != nullptr)
        {
            /* Compare initialization with reference data type */
            //todo ... !!!
        }
        else
            Error("Reference variables must be initialized with a suitable single memory object", Obj);
    }

    /* Add variable to local scope */
    if (!ScopeMngr_.AddObject(Obj))
        Error(ErrDuplicated + "variable \"" + Obj->Ident->Spell + "\"", AST);

    /* Get next variable definition */
    if (AST->Next != nullptr)
        AST->Next->VISIT;
}

DefVisitProc(Checker, IfCommand)
{
    /* Decoreate expression and command-block AST nodes */
    if (AST->Expr != nullptr)
        AST->Expr->VISIT;

    AST->CmdBlock->VISIT;

    if (AST->Expr != nullptr)
    {
        CheckExpressionType(AST->Expr, TypeDenoter::Types::BoolType, "<If>");

        if (AST->ElseIfCmd)
            AST->ElseIfCmd->VISIT;
    }
}

DefVisitProc(Checker, InfiniteLoopCommand)
{
    AST->CmdBlock->VISIT;
}

DefVisitProc(Checker, WhileLoopCommand)
{
    AST->Expr->VISIT;
    AST->CmdBlock->VISIT;

    CheckExpressionType(AST->Expr, TypeDenoter::Types::BoolType, "<While-Loop>");
}

DefVisitProc(Checker, RangeForLoopCommand)
{
    /* Check attributes validity */
    AttributeList::ForEach(
        AST->AttribList,
        [&](const AttributeList& Attrib)
        {
            if (!Attrib.HasAttrib(AttributeList::Attributes::Unroll, false))
                Error("Invalid attribute for range-based for loop: \"" + Attrib.Ident->Spell + "\"", AST);
        }
    );

    /* Check if an iterator identifier must be generated automatically */
    if (AST->Ident == nullptr)
        AST->Ident = Identifier::Create(GenDefaultForLoopIterator());

    /* Create iterator variable object for loop command */
    AST->Iterator = std::make_shared<VariableObject>(
        ScopeMngr_.FindType(TypeDenoter::Types::IntType), AST->Ident
    );

    /* Decorate command block */
    ScopeMngr_.PushScope();
    {
        ScopeMngr_.AddObject(AST->Iterator);
        
        AST->CmdBlock->OpensScope = false;
        AST->CmdBlock->VISIT;
    }
    ScopeMngr_.Pop();
}

DefVisitProc(Checker, InlineLangCommand)
{
}

DefVisitProc(Checker, PrimitiveCommand)
{
    const auto& Spell = AST->Spell;

    switch (AST->PrimType)
    {
        case Token::Types::PushConfig:
            CompilerOptions::PushConfig(Spell);
            break;
        case Token::Types::PopConfig:
            CompilerOptions::PopConfig();
            break;
        case Token::Types::Once:
            /* Check how many times the keyword "once" is used */
            CheckOnceStatement(AST);
            break;
        default:
            break;
    }
}

DefVisitProc(Checker, EnumDeclCommand)
{
    RegisterInclude("XieXie/Lang/EnumCore.h");
    RegisterInclude("string");

    AST->Obj->VISIT;

    /* Add enumeration to scope */
    if (!ScopeMngr_.AddObject(AST->Obj))
        Error(ErrDuplicated + "enumeration \"" + AST->Obj->Ident->Spell + "\"", AST);
}

DefVisitProc(Checker, FlagsDeclCommand)
{
    RegisterInclude("XieXie/Lang/FlagsCore.h");

    AST->Obj->VISIT;

    /* Add flags enumeration to scope */
    if (!ScopeMngr_.AddObject(AST->Obj))
        Error(ErrDuplicated + "flags \"" + AST->Obj->Ident->Spell + "\"", AST);
}

DefVisitProc(Checker, ClassDeclCommand)
{
    AST->Obj->VISIT;
}

//!TODO! -> refactor this function -- (too large and too unclear)!!!
DefVisitProc(Checker, ClassBlockCommand)
{
    /* Get parent object information */
    auto Obj = AST->Parent;

    const bool IsPack = (Obj->AttribList != nullptr && Obj->AttribList->HasAttrib(AttributeList::Attributes::Pack));

    /* Get class name */
    std::string ClassName;

    if (Obj->IsAnonymous())
    {
        ClassName = GenAnonymousClassName(Obj);
        ProgramAST_->AnonymousClassObjects.push_back(Obj);
        Obj->AnonymousId = ClassName;
    }
    else
        ClassName = Obj->Ident->Spell;

    /* Hold lists for additional commands */
    std::vector<VariableDefCommandPtr> Members;
    std::vector<InitCommandPtr> InitCommands;
    std::vector<FunctionDefCommandPtr> SetterGetterProcCommands, MemberProcDefCommands;

    /*
    The sub-command lists are used for visit the commands inside the class block in different order:
    1.) Enumeration-, flags- and type-def declarations.
    2.) Sub-class declarations.
    3.) Variable declarations.
    4.) All other commands (except procedure definitions).
    5.) Procedure definitions (after decoration with 'read-only' property).
    */
    std::vector<CommandPtr> SubCommands[4];

    for (size_t i = 0; i < 3; ++i)
    {
        for (auto CmdAST : AST->Commands[i])
        {
            /* Fill commands in separated lists for visiting in different orders */
            switch (CmdAST->Type())
            {
                case Command::Types::TypeDefCmd:
                case Command::Types::EnumDeclCmd:
                case Command::Types::FlagsDeclCmd:
                    SubCommands[0].push_back(CmdAST);
                    break;
                case Command::Types::ClassDeclCmd:
                    SubCommands[1].push_back(CmdAST);
                    break;
                case Command::Types::VarDeclCmd:
                    SubCommands[2].push_back(CmdAST);
                    break;

                case Command::Types::FuncDefCmd:
                {
                    auto ProcDefCmdAST = std::dynamic_pointer_cast<FunctionDefCommand>(CmdAST);

                    /* Check if this procedure makes this class abstract */
                    if ((ProcDefCmdAST->Obj->DefFlags & FunctionObject::Flags::IsAbstract) != 0)
                        Obj->DefFlags |= ClassObject::Flags::IsAbstract;

                    MemberProcDefCommands.push_back(ProcDefCmdAST);
                }
                break;

                default:
                    SubCommands[3].push_back(CmdAST);
                    break;
            }
        }
    }

    for (size_t i = 0; i < 4; ++i)
    {
        for (auto CmdAST : SubCommands[i])
            CmdAST->VISIT;
    }

    /* Iterate over all command AST nodes from this class block */
    ReleaseCommandPtr ReleaseCmdAST;

    for (size_t i = 0; i < 3; ++i)
    {
        for (const auto& CmdAST : AST->Commands[i])
        {
            /* Additional AST decorations */
            switch (CmdAST->Type())
            {
                case Command::Types::ReleaseCmd:
                {
                   /* Check if there are multiple release commands */
                    if (ReleaseCmdAST != nullptr)
                        Error("Multiple \"release\" commands inside class definition", CmdAST);
                    ReleaseCmdAST = std::dynamic_pointer_cast<ReleaseCommand>(CmdAST);
                }
                break;

                case Command::Types::InitCmd:
                {
                    /* Decorate AST */
                    auto InitCmdAST = std::dynamic_pointer_cast<InitCommand>(CmdAST);
                    InitCmdAST->ClassName = ClassName;

                    /* Store in temporary list */
                    InitCommands.push_back(InitCmdAST);
                }
                break;

                case Command::Types::ClassDeclCmd:
                {
                    /* Get procedure definition AST node and insert object to member-table of class object */
                    auto ClassDeclAST = std::dynamic_pointer_cast<ClassDeclCommand>(CmdAST);
                    Obj->AddMember(ClassDeclAST->Obj.get());
                }
                break;

                case Command::Types::VarDeclCmd:
                {
                    /* Get variable definition AST node */
                    auto VarDefAST = std::dynamic_pointer_cast<VariableDefCommand>(CmdAST);

                    AttributeListPtr AttribListAST;

                    while (VarDefAST != nullptr)
                    {
                        auto VarObjAST = VarDefAST->Obj;

                        /* Store in temporary list */
                        //!TODO! -> only add initializable member variables (i.e. no arrays etc.) !!!
                        //if (...) {
                        if (VarObjAST->Expr != nullptr || !VarObjAST->TDenoter->GetDefaultValue().empty())
                            Members.push_back(VarDefAST);
                        //}

                        /* Decorate variable AST node */
                        VarDefAST->DefFlags |= VariableDefCommand::Flags::IsClassMember;

                        /* Get active attribute */
                        if (VarDefAST->AttribList != nullptr)
                            AttribListAST = VarDefAST->AttribList;

                        /* Generate optional setter/getter procedures */
                        if (AttribListAST != nullptr)
                        {
                            if (AttribListAST->HasAttrib(AttributeList::Attributes::Get))
                            {
                                SetterGetterProcCommands.push_back(
                                    ProcedureFactory::GenProcDefCmdGetter(VarDefAST->Obj)
                                );
                            }
                            if (AttribListAST->HasAttrib(AttributeList::Attributes::Set))
                            {
                                SetterGetterProcCommands.push_back(
                                    ProcedureFactory::GenProcDefCmdSetter(VarDefAST->Obj)
                                );
                            }
                        }

                        /* Add current variable to member-table of class object */
                        Obj->AddMember(VarObjAST.get());

                        /* Get next variable */
                        VarDefAST = VarDefAST->Next;
                    }
                }
                break;

                default:
                break;
            }
        }
    }

    /* Generate default init command is there is none but member variables (which must be intialized) */
    if (!Members.empty() && InitCommands.empty())
    {
        /* Create simple init-command */
        auto EmptyBlockCmdAST = std::make_shared<BlockCommand>(SourcePosition::Ignore);
        auto InitCmdAST = std::make_shared<InitCommand>(EmptyBlockCmdAST);

        InitCmdAST->ClassName = ClassName;

        /* Append default init command */
        AST->Commands[ClassBlockCommand::Privacy::Public].push_back(InitCmdAST);
        InitCommands.push_back(InitCmdAST);
    }

    /* Generate default release command if there is none */
    if (ReleaseCmdAST == nullptr)
    {
        /* Create simple release-command */
        auto EmptyBlockCmdAST = std::make_shared<BlockCommand>(SourcePosition::Ignore);
        ReleaseCmdAST = std::make_shared<ReleaseCommand>(EmptyBlockCmdAST);

        /* Append default release command */
        AST->Commands[ClassBlockCommand::Privacy::Public].push_back(ReleaseCmdAST);
    }

    /* Append further generated commands */
    AST->Commands[ClassBlockCommand::Privacy::Public].insert(
        AST->Commands[ClassBlockCommand::Privacy::Public].end(),
        SetterGetterProcCommands.begin(), SetterGetterProcCommands.end()
    );

    DecorateAndAppendClassMemberProcs(Obj, SetterGetterProcCommands);
    DecorateAndAppendClassMemberProcs(Obj, MemberProcDefCommands);

    /* Decorate release-command AST node */
    ReleaseCmdAST->ClassName = ClassName;

    if (IsPack)
        ReleaseCmdAST->VirtualDisabled = true;

    /* Decorate init-command AST nodes */
    for (auto InitCmdAST : InitCommands)
        InitCmdAST->Members = Members;
}

DefVisitProc(Checker, InitCommand)
{
    ScopeMngr_.PushScope();
    {
        if (AST->ParamList != nullptr)
            AST->ParamList->VISIT;

        AST->CmdBlock->OpensScope = false;
        AST->CmdBlock->VISIT;
    }
    ScopeMngr_.Pop();
}

DefVisitProc(Checker, ReleaseCommand)
{
    AST->CmdBlock->VISIT;
}

DefVisitProc(Checker, TryCatchCommand)
{
    AST->TryBlock->VISIT;

    ScopeMngr_.PushScope();
    {
        if (AST->ExceptParam != nullptr)
            AST->ExceptParam->VISIT;

        AST->CatchBlock->OpensScope = false;
        AST->CatchBlock->VISIT;
    }
    ScopeMngr_.Pop();
}

DefVisitProc(Checker, ThrowCommand)
{
    AST->Expr->VISIT;
}

DefVisitProc(Checker, SwitchCommand)
{
    AST->Expr->VISIT;

    /* Decorate switch flags */
    auto TDenoter = AST->Expr->GetTypeDenoter(ScopeMngr_);

    if (TDenoter != nullptr)
    {
        TDenoter = TDenoter->GetNonConstType();

        if (TDenoter->Type() == TypeDenoter::Types::StringType)
            AST->IfCmds = true;
    }
    else
        Error("Unknown switch expression type", AST->Expr);

    /* Decorate case blocks */
    for (auto BlockAST : AST->CaseBlocks)
        BlockAST->VISIT;

    /* Check if there are duplicated case expressions */
    if (!AST->IfCmds)
        CheckSwitchCaseExpressions(AST->CaseBlocks);

    /* Decorate default block */
    if (AST->DefBlock != nullptr)
        AST->DefBlock->VISIT;
}

DefVisitProc(Checker, TypeDefCommand)
{
    AST->Obj->VISIT;
}

/* === Expressions === */

DefVisitProc(Checker, BracketExpression)
{
    AST->Expr->VISIT;
    DecorateExpr(AST);
}

DefVisitProc(Checker, BinaryExpression)
{
    /* Check sub-expressions */
    AST->Expr1->VISIT;
    AST->Op->VISIT;
    AST->Expr2->VISIT;

    DecorateExpr(AST);

    /* Check for type compatibility */
    auto T1 = AST->Expr1->GetTypeDenoter(ScopeMngr_);
    auto T2 = AST->Expr2->GetTypeDenoter(ScopeMngr_);

    //CheckTypeCompatibility(AST, T1, T2);

    /* Check for flags to set */
    if ( T1 != nullptr && T2 != nullptr &&
         ( T1->GetResolvedType()->Type() == TypeDenoter::Types::StringType ||
           T2->GetResolvedType()->Type() == TypeDenoter::Types::StringType ) )
    {
        /* Mark flags to wrap the expression around a string */
        AST->DefFlags |= Expression::Flags::StringWrap;
    }
}

DefVisitProc(Checker, UnaryExpression)
{
    AST->Op->VISIT;
    AST->Expr->VISIT;

    DecorateExpr(AST);
}

DefVisitProc(Checker, AssignExpression)
{
    AST->Op->VISIT;
    AST->Expr->VISIT;

    DecorateExpr(AST);
}

DefVisitProc(Checker, StringSumExpression)
{
    AST->Expr1->VISIT;
    AST->Expr2->VISIT;

    DecorateExpr(AST);
}

DefVisitProc(Checker, LiteralExpression)
{
    DecorateExpr(AST);
}

DefVisitProc(Checker, CallExpression)
{
    AST->Call->VISIT;

    DecorateExpr(AST);
}

DefVisitProc(Checker, CastExpression)
{
    /* Decorate expression which is to be casted */
    AST->TDenoter->VISIT;
    AST->Expr->VISIT;

    DecorateExpr(AST);

    /* Decorate type casting */
    auto TDenoter = AST->TDenoter;
    
    if (TDenoter->Type() == TypeDenoter::Types::MngPtrType)
    {
        //!TODO! -> check if const_cast is required.
        AST->IsSmartPointer = true;
        AST->Casting = CastExpression::TypeCasts::DynamicCast;
    }
    else if (TDenoter->Type() == TypeDenoter::Types::RawPtrType)
    {
        //!TODO! -> check if reinterpret_cast is required.
        //!TODO! -> check if const_cast is required.
        AST->Casting = CastExpression::TypeCasts::DynamicCast;
    }
}

DefVisitProc(Checker, ObjectExpression)
{
    AST->FName->VISIT;

    /* Decorate AST with linked object */
    AST->Obj = AST->FName->GetLastIdent()->Link;

    if (AST->AssignExpr != nullptr)
    {
        /* Check if object can have an assignment */
        CheckObjectAssignment(AST->Obj, AST->AssignExpr);

        AST->AssignExpr->VISIT;
    }

    DecorateExpr(AST);
}

DefVisitProc(Checker, AllocExpression)
{
    RegisterSmartPointerInclude();

    AST->TDenoter->VISIT;

    if (AST->ArgList != nullptr)
        AST->ArgList->VISIT;
    if (AST->AnonymousClass != nullptr)
        AST->AnonymousClass->VISIT;

    DecorateExpr(AST);
}

DefVisitProc(Checker, CopyExpression)
{
    AST->FName->VISIT;

    /* Register copy function for object's type denoter */
    auto LastIdent = AST->FName->GetLastIdent();
    auto Obj = LastIdent->Link;

    if (Obj == nullptr || Obj->Type() != Object::Types::VarObj)
        Error("Identifier \"" + AST->FName->FullName() + "\" does not name a variable object", LastIdent);

    auto VarObj = dynamic_cast<VariableObject*>(Obj);

    RegisterCopyProc(VarObj->TDenoter.get());

    DecorateExpr(AST);
}

DefVisitProc(Checker, ValidationExpression)
{
    AST->ObjExpr->VISIT;

    DecorateExpr(AST);
}

DefVisitProc(Checker, InitListExpression)
{
    AST->InitList->VISIT;

    DecorateExpr(AST);
}

DefVisitProc(Checker, LambdaExpression)
{
    AST->ProcTDenoter->VISIT;
    AST->BlockCmd->VISIT;

    DecorateExpr(AST);
}

/* === Type denoters === */

DefVisitProc(Checker, VoidTypeDenoter)
{
}

DefVisitProc(Checker, ConstTypeDenoter)
{
    AST->TDenoter->VISIT;
}

DefVisitProc(Checker, BoolTypeDenoter)
{
}

DefVisitProc(Checker, IntTypeDenoter)
{
}

DefVisitProc(Checker, FloatTypeDenoter)
{
}

DefVisitProc(Checker, StringTypeDenoter)
{
    RegisterInclude("XieXie/Lang/StringCore.h");
}

DefVisitProc(Checker, CustomTypeDenoter)
{
    AST->FName->VISIT;

    /* Find custom type object in active name-visibility */
    auto Obj = ScopeMngr_.Find(AST->FName);

    if (Obj == nullptr)
        ErrorUndefinedCustomType(AST->FName);

    /* Decorate AST */
    AST->Link = Obj;

    /* Check if templates must be instantiated */
    try
    {
        CheckForTemplateInstantiation(AST.get());
    }
    catch (const std::string& Err)
    {
        Error(Err, AST);
    }
}

DefVisitProc(Checker, MngPtrTypeDenoter)
{
    RegisterSmartPointerInclude();

    AST->TDenoter->VISIT;
}

DefVisitProc(Checker, RawPtrTypeDenoter)
{
    AST->TDenoter->VISIT;
}

DefVisitProc(Checker, RefTypeDenoter)
{
    AST->TDenoter->VISIT;
}

DefVisitProc(Checker, ProcTypeDenoter)
{
    RegisterInclude(CompilerOptions::Settings.EnableCpp11 ? "functional" : "boost/function.hpp");

    AST->TDenoter->VISIT;
    
    if (AST->ParamList != nullptr)
    {
        /*
        If this proc-type-denoter is used as header for a lambda expression:
        -> Prevent proc-type-denoter to skip parameters from scope.
        */
        if ((AST->DefFlags & ProcTypeDenoter::Flags::LambdaHeader) == 0)
            AST->ParamList->DefFlags |= ParameterList::Flags::SkipFromScope;

        AST->ParamList->VISIT;
    }
}

DefVisitProc(Checker, ArrayTypeDenoter)
{
    if (AST->IsStatic())
        RegisterInclude(CompilerOptions::Settings.EnableCpp11 ? "array" : "boost/array.hpp");
    else
        RegisterInclude("vector");

    AST->TDenoter->VISIT;
}

/* === Lists === */

DefVisitProc(Checker, ParameterList)
{
    AST->Obj->VISIT;

    /* Check if this parameter is to be skiped from scope */
    if ((AST->DefFlags & ParameterList::Flags::SkipFromScope) != 0)
    {
        /* Spread this flag */
        if (AST->Next != nullptr)
            AST->Next->DefFlags |= ParameterList::Flags::SkipFromScope;
    }
    /* Otherwise add to local scope */
    else if (!ScopeMngr_.AddObject(AST->Obj))
        Error(ErrDuplicated + "variable \"" + AST->Obj->Ident->Spell + "\"", AST);

    /* Decorate next parameter list */
    if (AST->Next != nullptr)
        AST->Next->VISIT;
}

DefVisitProc(Checker, ArgumentList)
{
    AST->Expr->VISIT;
    if (AST->Next != nullptr)
        AST->Next->VISIT;
}

DefVisitProc(Checker, TemplateParamList)
{
    auto Obj = AST->Obj;

    if (Obj != nullptr)
    {
        Obj->VISIT;

        if (Obj->Type() == Object::Types::VarObj)
        {
            auto VarObj = dynamic_cast<VariableObject*>(Obj.get());

            /* Check for valid template parameters */
            if (!VarObj->TDenoter->IsExact())
                Error("Only type denoters with exact representation are allowed for template parameters", VarObj->TDenoter);
        }

        /* Add variable to local scope */
        if (!ScopeMngr_.AddObject(Obj))
            Error(ErrDuplicated + "variable \"" + Obj->Ident->Spell + "\"", AST);
    }

    if (AST->Next != nullptr)
        AST->Next->VISIT;
}

DefVisitProc(Checker, TemplateArgList)
{
    if (AST->Expr != nullptr)
        AST->Expr->VISIT;
    if (AST->TDenoter != nullptr)
        AST->TDenoter->VISIT;
    if (AST->Next != nullptr)
        AST->Next->VISIT;
}

DefVisitProc(Checker, InitializerList)
{
    AST->Expr->VISIT;
    if (AST->Next != nullptr)
        AST->Next->VISIT;
}

DefVisitProc(Checker, ArrayIndexList)
{
    /* Decorate index expressions only -> actual array type-denoter decoration must be done in upper AST nodes */
    AST->Expr->VISIT;

    if (AST->Next != nullptr)
        AST->Next->VISIT;
}

/* === Objects === */

DefVisitProc(Checker, ClassObject)
{
    /* Check attributes validity */
    AttributeList::ForEach(
        AST->AttribList,
        [&](const AttributeList& Attrib)
        {
            if (!Attrib.HasAttrib(AttributeList::Attributes::Pack, false))
                Error("Invalid attribute for class declaration: \"" + Attrib.Ident->Spell + "\"", AST);
        }
    );

    /* Decorate inheritance class denoter */
    for (auto BaseClassName : AST->Inheritance)
    {
        BaseClassName->VISIT;

        /* Check if FName denotes a class definition */
        auto FName = BaseClassName->GetLastIdent();

        if (FName->Link == nullptr || !FName->Link->IsClassDenoter())
            Error("\"" + BaseClassName->FullName() + "\" does not name a class", BaseClassName);
    }

    /* Add class as new namespace or scope if it's an anonymous class */
    if (!ScopeMngr_.PushNamespace(AST))
        Error(ErrDuplicated + "class \"" + AST->Ident->Spell + "\"", AST);
    
    /* Decorate optional template parameters */
    if (AST->TParamList != nullptr)
        AST->TParamList->VISIT;

    /* Check if patterns are used */
    if ((AST->DefFlags & ClassObject::Flags::Instantiated) == 0 && AST->TParamList != nullptr && AST->TParamList->HasPattern())
    {
        /* Decorate this class object as a pattern class */
        AST->DefFlags |= ClassObject::Flags::IsPattern;
    }
    /*
    Do not decorate the class body if patterns are used!
    -> further analysis and AST decoration will be made on template instantiation.
    */
    else
    {
        /* Decorate class body */
        AST->ClassBlockCmd->VISIT;
    }

    ScopeMngr_.Pop();
}

DefVisitProc(Checker, EnumObject)
{
    for (auto Entry : AST->Entries)
        Entry->VISIT;
}

DefVisitProc(Checker, FlagsObject)
{
    const auto& FlagsName = AST->Ident->Spell;

    /* Decorate inheritance flags */
    for (auto it = AST->Inheritance.rbegin(); it != AST->Inheritance.rend(); ++it)
    {
        auto Parent = *it;

        Parent->VISIT;

        /* Check if identifier denotes another flags declaration */
        auto Link = Parent->GetLastIdent()->Link;

        if (Link != nullptr && Link->Type() == Object::Types::FlagsObj)
        {
            /* Append parent flags */
            auto FlagsObj = dynamic_cast<FlagsObject*>(Link);
            AST->Flags.insert(AST->Flags.begin(), FlagsObj->Flags.begin(), FlagsObj->Flags.end());
        }
        else
            Error("Flags can only inherit from other flags", Parent);
    }

    /* Check if there are dublicated flags */
    std::map<std::string, bool> FlagsMap;

    for (auto Flag : AST->Flags)
    {
        /* Check for reserved flag name */
        const auto FlagName = Flag->Spell;

        if (FlagName == "None" || FlagName == "All" || FlagName == "DataType")
            Error("Flags entry \"" + FlagName + "\"" + AutoGenErrPart, AST);

        /* Find already existing flag entry in hash-map */
        auto it = FlagsMap.find(FlagName);

        /* Insert new flag or throw exception */
        if (it == FlagsMap.end())
            FlagsMap[FlagName] = true;
        else
            Error("Duplicated entry \"" + FlagName + "\" in flags \"" + FlagsName + "\"", AST);
    }

    /* Check which data type is required for the flags */
    const auto Num = AST->Flags.size();

    if (Num > 64)
    {
        Error(
            "Too many entries in flags \"" + FlagsName +
            "\" (Maximal number of entries are 64)", AST
        );
    }

    if (Num <= 8)
        AST->DataType = FlagsObject::DataTypes::UInt8;
    else if (Num <= 16)
        AST->DataType = FlagsObject::DataTypes::UInt16;
    else if (Num <= 32)
        AST->DataType = FlagsObject::DataTypes::UInt32;
    else
        AST->DataType = FlagsObject::DataTypes::UInt64;

    /* Visit all flags */
    for (auto Flag : AST->Flags)
        Flag->VISIT;
}

DefVisitProc(Checker, FunctionObject)
{
    /* Check for special functions */
    const std::string FullName = AST->FullName();
    const bool IsMainProc = (FullName == "Main");

    /* Check for valid flags */
    const bool IsClassMember    = (AST->DefFlags & FunctionObject::Flags::IsClassMember ) != 0;
    const bool IsReadonly       = (AST->DefFlags & FunctionObject::Flags::IsReadonly    ) != 0;
    const bool IsVirtual        = (AST->DefFlags & FunctionObject::Flags::IsVirtual     ) != 0;
    const bool IsAbstract       = (AST->DefFlags & FunctionObject::Flags::IsAbstract    ) != 0;

    if ( !IsClassMember && ( IsReadonly || IsVirtual || IsAbstract ) )
        Error("Read-only-, virtual- and abstract procedures must be a class member", AST);

    /* Add procedure as new object */
    if (!ScopeMngr_.AddObject(AST))
        Error(ErrDuplicated + "procedure \"" + AST->Ident->Spell + "\"", AST);
    
    /* Check function body */
    AST->TDenoter->VISIT;
    AST->FName->VISIT;

    ScopeMngr_.PushScope();
    {
        /* Decorate optional template parameters */
        auto TParamList = AST->FName->GetLastIdent()->TParamList;

        if (TParamList != nullptr)
            TParamList->VISIT;

        /* Decorate parameter list */
        if (AST->ParamList != nullptr)
        {
            AST->ParamList->VISIT;

            if (!AST->ParamList->HasCommonDefaultParams())
                Error("Unordered default parameters are not supported yet", AST->ParamList);
        }

        if (IsMainProc)
        {
            if (!CheckMainProcedure(AST))
                return;
        }
        else if (FullName.size() == 4 && xxLower(FullName) == "main")
            Warning("\"" + FullName + "\" function must be written as \"Main\" to be the main program entry point", AST);

        /* Decorate function body */
        if (AST->CmdBlock != nullptr)
        {
            AST->CmdBlock->OpensScope = false;
            AST->CmdBlock->VISIT;
        }
    }
    ScopeMngr_.Pop();

    /* Check for correct return statement */
    if (AST->CmdBlock != nullptr)
    {
        unsigned int NumRet = 0;
        CheckReturnStatement(AST->TDenoter.get(), AST->CmdBlock.get(), NumRet);
    
        if (AST->TDenoter->Type() != TypeDenoter::Types::VoidType && NumRet == 0)
            Error("Missing return statement inside function's main block", AST->TDenoter);
    }
}

DefVisitProc(Checker, TypeObject)
{
    AST->TDenoter->VISIT;

    ScopeMngr_.AddObject(AST);
}

DefVisitProc(Checker, VariableObject)
{
    /* Decorate type denoter */
    AST->TDenoter->VISIT;

    /* Check if type denoter is a custom type denoter and store meta information */
    FlagsObjectPtr FlagsObj;

    if (AST->TDenoter->Type() == TypeDenoter::Types::CustomType)
    {
        auto CTDenoter = std::dynamic_pointer_cast<CustomTypeDenoter>(AST->TDenoter);
        auto Obj = CTDenoter->Link;

        if (Obj != nullptr && Obj->Type() == Object::Types::FlagsObj)
            FlagsObj = std::dynamic_pointer_cast<FlagsObject>(Obj);
    }

    if (AST->Expr != nullptr)
    {
        /* Decorate expression AST first */
        AST->Expr->VISIT;

        /* Get expression type */
        #if 0 //!INCOMPLETE! !!!

        auto TDenoter = AST->Expr->GetTypeDenoter(ScopeMngr_);

        if (TDenoter != nullptr)
        {
            /* Check for type equivalence */
            if (!AST->TDenoter->Compare(TDenoter))
            {
                if (FlagsObj != nullptr)
                    Warning("Flags enumeration variables can only be initialized and modified by a bitwise combination of their flags entries", AST->Expr);
                else
                    Warning("Variable initializer expression has incompatible type", AST->Expr);
            }
        }
        else
        {
            if (FlagsObj != nullptr)
                Warning("Flags enumeration initializer expression has unknown type", AST->Expr);
            else
                Warning("Variable initializer expression has unknown type", AST->Expr);
        }

        #endif
    }

    /* Check for static constant */
    if (AST->IsStaticConst())
    {
        if (AST->Expr == nullptr || !AST->Expr->IsConst())
            Error("Static constants must be initialized with a static constant expression", AST);
        else
        {
            /* Evaluate constant expression */
            ConstExpressionEvaluator ConstExpEval;
            auto Const = ConstExpEval.Evaluate(AST->Expr);

            if (Const != nullptr)
            {
                /* Store static constant value for (variable-) object */
                AST->Const = Const;

                #if 0//!!!DEBUG!!!
                Warning("DEB CONST EXPR: " + AST->Ident->Spell + " = " + Const->Str(), AST->Expr);
                #endif

                /* Optimize expression to a single literal (if optimization is enabled) */
                if (CompilerOptions::Settings.Optimize)
                    AST->Expr = Const->ToLiteralExpr();
            }
        }
    }
}

DefVisitProc(Checker, PatternObject)
{
    if (AST->TDenoter != nullptr)
        AST->TDenoter->VISIT;
}

/* === Operators === */

DefVisitProc(Checker, AssignOperator)
{
}

DefVisitProc(Checker, BinaryOperator)
{
}

DefVisitProc(Checker, UnaryOperator)
{
}

/* === Others === */

DefVisitProc(Checker, Identifier)
{
}

DefVisitProc(Checker, FNameIdent)
{
    try
    {
        /* Decorate FName identifiers */
        if (!LocalScopeStack_.Empty())
        {
            auto LocalObj = LocalScopeStack_.Top();
            if (!LocalObj->DecorateLocal(AST))
                throw std::string("Undeclared identifier \"" + AST->FullName() + "\" in local object \"" + LocalObj->Ident->Spell + "\"");
        }
        else if (!ScopeMngr_.Decorate(AST))
            throw std::string("Undeclared identifier \"" + AST->FullName() + "\"");

        /* Decorate optional template arguments and/or array index lists */
        auto FName = AST;

        while (FName != nullptr)
        {
            /* Decorate optional template argument list */
            if (FName->TArgList != nullptr)
                FName->TArgList->VISIT;

            /* Decorate optional array index list */
            if (FName->ArrayList != nullptr)
            {
                FName->ArrayList->VISIT;
                DecorateFNameArrayIndexList(FName);
            }

            /* Get next FName idenetifier AST node */
            FName = FName->Next;
        }
    }
    catch (const std::string& Err)
    {
        Error(Err, AST);
    }
}

DefVisitProc(Checker, Program)
{
    CheckProgramHeaderGuard(AST);

    /* Decorate all AST nodes */
    for (CommandPtr Cmd : AST->Commands)
        Cmd->VISIT;

    /* Decorate program AST node with the copy procedure objects */
    AST->CopyProcObjects = CopyProcObjects_;
}

DefVisitProc(Checker, EnumEntry)
{
    /*const auto& Spell = AST->Ident->Spell;

    if (Spell == "Num")
        Error("Enumeration entry \"Num\"" + AutoGenErrPart, AST);*/
}

DefVisitProc(Checker, CaseBlock)
{
    const bool IsDefBlock = (AST->Expr == nullptr);

    if (!IsDefBlock)
        AST->Expr->VISIT;

    ScopeMngr_.PushScope();
    
    for (auto it = AST->Commands.begin(); it != AST->Commands.end();)
    {
        auto CmdAST = *it;

        /* Check for special primitive commands */
        if (CmdAST->Type() == Command::Types::PrimCmd)
        {
            auto PrimCmd = dynamic_cast<PrimitiveCommand*>(CmdAST.get());

            if (PrimCmd->PrimType == Token::Types::Next)
            {
                /* Check if end of the command list has been reached */
                auto itTemp = it;
                ++itTemp;

                if (itTemp == AST->Commands.end())
                {
                    /* Store information about skipping the 'break' keyword and remove this primitive command */
                    AST->SkipBreak = true;
                    it = AST->Commands.erase(it);
                    continue;
                }
                else
                    Error("\"next\" keyword can only be used at the end of a case/def block", CmdAST);
            }
        }

        /* Decorate current command AST node */
        CmdAST->VISIT;

        ++it;
    }
    
    ScopeMngr_.Pop();
}


} // /namespace ContextualAnalyzer



// ================================================================================