/*
 * Checker (pattern instantiation) helper file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Checker.h"
#include "StringMod.h"
#include "CustomTypeDenoter.h"
#include "ClassObject.h"
#include "PatternObject.h"
#include "ClassDeclCommand.h"


namespace ContextualAnalyzer
{


void Checker::CheckForTemplateInstantiation(TypeDenoter* TDenoter)
{
    /* Get resolved custom type denoter */
    TDenoter = TDenoter->GetResolvedType();

    if (TDenoter->Type() != TypeDenoter::Types::CustomType)
        return;

    auto CTDenoter = dynamic_cast<CustomTypeDenoter*>(TDenoter);

    /* Instantiate templates for each FName identifier part */
    auto FName = CTDenoter->FName;

    while (FName != nullptr)
    {
        /* Check if current FName identifier has a template argument list */
        auto TArgList = FName->TArgList.get();

        if (TArgList != nullptr)
        {
            /* Check if FName is linked with a template class */
            auto Obj = FName->Link;

            if (Obj == nullptr || Obj->Type() != Object::Types::ClassObj)
                throw std::string("Identifier \"" + FName->Ident->Spell + "\" does not name a template class");

            auto ClassObj = dynamic_cast<ClassObject*>(Obj);

            /* Compare template argument list with template parameter list */
            if (ClassObj->TParamList == nullptr)
                throw std::string("Class \"" + ClassObj->Ident->Spell + "\" has no template parameters");

            ClassObj->TParamList->Compare(TArgList, ScopeMngr_);

            /* Instantiate template class */
            InstantiateTemplateClass(TDenoter, ClassObj, TArgList);
        }

        /* Get next FName identifier */
        FName = FName->Next;
    }
}

void Checker::InstantiateTemplateClass(
    TypeDenoter* TDenoter, ClassObject* ClassObj, const TemplateArgList* TArgList)
{
    if (ClassObj == nullptr || TArgList == nullptr)
        return;

    PushState(AnalysisState(
        "Instantiating template class \"" + ClassObj->Ident->Spell + "\" " + TDenoter->Pos().GetString()
    ));

    /* Instantiate new template class */
    auto ClassInst = std::dynamic_pointer_cast<ClassObject>(ClassObj->Copy());

    /* Setup template parameters with concrete arguments */
    auto TParamList = ClassInst->TParamList;

    while (TParamList != nullptr)
    {
        /* Get template parameter object */
        auto ParamObj = TParamList->Obj.get();

        if (ParamObj->Type() == Object::Types::PatternObj)
        {
            auto PatternObj = dynamic_cast<PatternObject*>(ParamObj);

            /* Setup concrete pattern type denoter */
            if (TArgList != nullptr)
                PatternObj->TDenoter = TArgList->TDenoter;
            else
                PatternObj->TDenoter = PatternObj->DefTDenoter;
        }
        else if (ParamObj->Type() == Object::Types::VarObj)
        {
            auto VarObj = dynamic_cast<VariableObject*>(ParamObj);
            
            /* Setup concrete expression */
            if (TArgList != nullptr)
                VarObj->Expr = TArgList->Expr;
        }

        /* Get next template parameter and argument */
        TParamList = TParamList->Next;

        if (TArgList != nullptr)
            TArgList = TArgList->Next.get();
    }

    /* Generate template instance name */
    ClassInst->Ident->Spell = GenerateTemplateInstanceName(ClassInst->Ident->Spell);

    /* Create new class definition */
    auto ClassDecl = std::make_shared<ClassDeclCommand>(ClassInst);

    /* No finally decorate the instantiated class object */
    auto GlobalScope = ScopeMngr_.GlobalNamespace();
    auto NameVis = ScopeMngr_.GetActiveNameVisibility();

    ScopeMngr_.SetActiveNameVisibility(GlobalScope.get());
    {
        ClassInst->DefFlags = ClassObject::Flags::Instantiated;
        ClassInst->VISIT;
    }
    ScopeMngr_.SetActiveNameVisibility(NameVis);

    /* Insert class-declaration into program AST */
    ProgramAST_->Commands.push_front(ClassDecl);

    PopState();
}

std::string Checker::GenerateTemplateInstanceName(const std::string& OrigName)
{
    return "__XX__TemplateInstance" + xxStr(++Counters_.TemplateInstance) + "__" + OrigName;
}


} // /namespace ContextualAnalyzer



// ================================================================================