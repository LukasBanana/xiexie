/*
 * Constant header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_CONSTANT_H__
#define __XX_CONSTANT_H__


#include "ASTDeclarations.h"

#include <memory>
#include <string>


using namespace AbstractSyntaxTrees;

namespace ConstantFolding
{


/* === Macros === */

#define DefConstArithOp(C, F, OP)                   \
    void C::F(const Constant* Other)                \
    {                                               \
        C Compat;                                   \
        if (Compat.CastFrom(Other))                 \
            Value_ OP Compat.Value();               \
        else                                        \
            throw Constant::ERR_INCOMPATIBLE_TYPES; \
    }

class Constant;

typedef std::shared_ptr<Constant> ConstantPtr;

/**
The "Constant" class is used for constant folding and stores - as the name implies - a constant literal.
This is the base class. There are sub classes like "ConstantInt", "ConstantFloat", "ConstantString" etc.
*/
class Constant
{
    
    public:
        
        enum class Types
        {
            IntConst,
            FloatConst,
            BoolConst,
            StringConst,
        };

        virtual ~Constant()
        {
        }

        //! Copy the specified constant's value to this value with type conversion.
        virtual bool CastFrom(const Constant* Other) = 0;

        //! Creates a copy of this constant object.
        virtual ConstantPtr Copy() const = 0;
        //! Converts this constant to a literal expression AST node.
        virtual LiteralExpressionPtr ToLiteralExpr() const = 0;

        //! Returns this constant literal as string.
        virtual std::string Str() const = 0;

        virtual bool Compare(const Constant* Other) const = 0;

        virtual void Add(const Constant* Other) = 0;
        virtual void Sub(const Constant* Other) = 0;
        virtual void Mul(const Constant* Other) = 0;
        virtual void Div(const Constant* Other) = 0;
        virtual void Mod(const Constant* Other) = 0;
        virtual void ShiftL(const Constant* Other) = 0;
        virtual void ShiftR(const Constant* Other) = 0;

        /**
        Returns the type of this constant.
        \see Types
        */
        inline Types Type() const
        {
            return Type_;
        }

    protected:
        
        static const std::string ERR_INCOMPATIBLE_TYPES;
        static const std::string ERR_DIVISION_BY_ZERO;

        Constant(const Types Type) :
            Type_(Type)
        {
        }

    private:
        
        Types Type_;

};


/**
Dynamic cast template for constant values.
\see Constant
*/
template <typename To, typename From> std::shared_ptr<To> dynamic_const_cast(const std::shared_ptr<From>& Obj)
{
    if (Obj != nullptr)
    {
        auto CastObj = std::make_shared<To>();
        if (CastObj->CastFrom(Obj.get()))
            return CastObj;
    }
    return nullptr;
}


} // /namespace ConstantFolding

#endif



// ================================================================================