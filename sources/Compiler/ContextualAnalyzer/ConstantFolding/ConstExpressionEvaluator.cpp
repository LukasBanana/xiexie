/*
 * Constant expression evaluator file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ConstExpressionEvaluator.h"
#include "ErrorReporter.h"

#include "ConstantInt.h"
#include "ConstantFloat.h"

#include "Expression.h"
#include "BracketExpression.h"
#include "BinaryExpression.h"
#include "UnaryExpression.h"
#include "StringSumExpression.h"
#include "AssignExpression.h"
#include "LiteralExpression.h"
#include "CallExpression.h"
#include "CastExpression.h"
#include "ObjectExpression.h"
#include "AllocExpression.h"
#include "CopyExpression.h"
#include "ValidationExpression.h"

#include "VariableObject.h"


namespace ConstantFolding
{


static const std::string ERR_NOT_STATIC_CONST = " expression can not be static constant";

ConstExpressionEvaluator::ConstExpressionEvaluator()
{
}
ConstExpressionEvaluator::~ConstExpressionEvaluator()
{
}

ConstantPtr ConstExpressionEvaluator::Evaluate(AbstractSyntaxTrees::ExpressionPtr Expr)
{
    if (Expr != nullptr)
    {
        try
        {
            Expr->VISIT;
            return Pop();
        }
        catch (const CompilerMessage& Err)
        {
            ErrorReporter::Instance.Push(Err);
        }
    }
    return nullptr;
}

/* === Expressions === */

DefVisitProc(ConstExpressionEvaluator, BracketExpression)
{
    AST->Expr->VISIT;
}

DefVisitProc(ConstExpressionEvaluator, BinaryExpression)
{
    /* Evaluate sub expressions */
    AST->Expr1->VISIT;
    AST->Expr2->VISIT;

    /* Pop constants from stack */
    auto Const2 = Pop();
    auto Const1 = Pop();

    if (Const1 == nullptr || Const2 == nullptr)
        Error("Internal error: missing constants for binary expression", AST);

    /* Compute binary concatenation */
    const auto& Spell = AST->Op->Spell;

    static const std::string ERR_UNSUPPORTED_OP = "Unsupported operator for constant expression evaluation";

    try
    {
        if (Spell.size() == 1)
        {
            /* Make some optimized string comparisions (with single character comparision) */
            switch (Spell[0])
            {
                case '+': Const1->Add(Const2.get()); break;
                case '-': Const1->Sub(Const2.get()); break;
                case '*': Const1->Mul(Const2.get()); break;
                case '/': Const1->Div(Const2.get()); break;
                case '%': Const1->Mod(Const2.get()); break;
                default:
                    Error(ERR_UNSUPPORTED_OP + ": \"" + Spell + "\"", AST);
                    break;
            }
        }
        else if (Spell.size() == 2)
        {
            /* Check for further operators */
                 if (Spell == "<<") Const1->ShiftL(Const2.get());
            else if (Spell == ">>") Const1->ShiftR(Const2.get());
            else
                Error(ERR_UNSUPPORTED_OP + Spell + "\"", AST);
        }
    }
    catch (const std::string& Err)
    {
        Error(Err, AST);
    }

    /* Push result onto stack */
    Push(Const1);
}

DefVisitProc(ConstExpressionEvaluator, UnaryExpression)
{
    AST->Expr->VISIT;
}

DefVisitProc(ConstExpressionEvaluator, StringSumExpression)
{
    AST->Expr1->VISIT;
    AST->Expr2->VISIT;
}

DefVisitProc(ConstExpressionEvaluator, AssignExpression)
{
    Error("Assignment" + ERR_NOT_STATIC_CONST, AST);
}

DefVisitProc(ConstExpressionEvaluator, LiteralExpression)
{
    AST->Literal->VISIT;
}

DefVisitProc(ConstExpressionEvaluator, CallExpression)
{
    Error("Function call" + ERR_NOT_STATIC_CONST, AST);
}

DefVisitProc(ConstExpressionEvaluator, CastExpression)
{
    /* Evaluate sub expression */
    AST->Expr->VISIT;

    /* Get constant from stack top */
    auto Const = Pop();

    if (Const == nullptr)
        Error("Internal error: missing constant for cast expression", AST);

    /* Cast constant type */
    ConstantPtr CastedConst;

    switch (AST->TDenoter->Type())
    {
        case TypeDenoter::Types::IntType:
            CastedConst = dynamic_const_cast<ConstantInt>(Const);
            break;
        case TypeDenoter::Types::FloatType:
            CastedConst = dynamic_const_cast<ConstantFloat>(Const);
            break;
        default:
            break;
    }

    /* Push new casted constant onto stack */
    if (CastedConst != nullptr)
        Push(CastedConst);
    else
        Error("Invalid type cast for constant expression", AST->TDenoter);
}

DefVisitProc(ConstExpressionEvaluator, ObjectExpression)
{
    /* Get constant (variable-) object */
    if (AST->Obj == nullptr)
        Error("Memory object \"" + AST->FName->FullName() + "\" not found for constant expression", AST);

    if (AST->Obj->Type() != Object::Types::VarObj)
        Error("Memory object \"" + AST->FName->FullName() + "\" does neither name a constant nor a variable", AST);

    auto VarObj = dynamic_cast<VariableObject*>(AST->Obj);

    /* Get constant value */
    if ((VarObj->DefFlags & VariableObject::Flags::IsStatic) == 0)
        Error("Memory object \"" + AST->FName->FullName() + "\" is not static constant", AST);

    if (VarObj->Const == nullptr)
        Error("Memory object \"" + AST->FName->FullName() + "\" has unresolved constant value", AST);

    /*
    Push (copy of-) constant onto stack.
    -> Copying is necessary! Otherwise we would re-use a
    shared-pointer from another (variable-) object's constant.
    */
    Push(VarObj->Const->Copy());
}

DefVisitProc(ConstExpressionEvaluator, AllocExpression)
{
    Error("Memory allocation" + ERR_NOT_STATIC_CONST, AST);
}

DefVisitProc(ConstExpressionEvaluator, CopyExpression)
{
    Error("Copy allocation" + ERR_NOT_STATIC_CONST, AST);
}

DefVisitProc(ConstExpressionEvaluator, ValidationExpression)
{
    Error("Pointer validation" + ERR_NOT_STATIC_CONST, AST);
}

DefVisitProc(ConstExpressionEvaluator, BoolLiteral)
{
    //... ConstantBool
}

DefVisitProc(ConstExpressionEvaluator, FloatLiteral)
{
    const auto Value = xxNumber<ConstantFloat::DataType>(AST->Spell);
    Push(ConstantFloat::Create(Value));
}

DefVisitProc(ConstExpressionEvaluator, IntegerLiteral)
{
    const auto Value = xxNumber<ConstantInt::DataType>(AST->Spell);
    Push(ConstantInt::Create(Value));
}

DefVisitProc(ConstExpressionEvaluator, StringLiteral)
{
    //... ConstantString
}


/*
 * ======= Private: =======
 */

void ConstExpressionEvaluator::Error(const std::string& Message, ASTPtr AST)
{
    if (AST != nullptr)
        throw ContextError(AST->Pos(), Message);
    else
        throw ContextError(Message);
}

void ConstExpressionEvaluator::Push(ConstantPtr ConstValue)
{
    if (ConstValue != nullptr)
        ConstStack_.push(ConstValue);
}

ConstantPtr ConstExpressionEvaluator::Pop()
{
    if (!ConstStack_.empty())
    {
        auto ConstValue = ConstStack_.top();
        ConstStack_.pop();
        return ConstValue;
    }
    return nullptr;
}

ConstantPtr ConstExpressionEvaluator::Top()
{
    return !ConstStack_.empty() ? ConstStack_.top() : nullptr;
}


} // /namespace ConstantFolding



// ================================================================================