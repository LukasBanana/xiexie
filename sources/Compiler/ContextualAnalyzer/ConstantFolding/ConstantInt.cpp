/*
 * Constant integer file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ConstantInt.h"
#include "ConstantFloat.h"
#include "StringMod.h"
#include "LiteralExpression.h"

#include <sstream>


namespace ConstantFolding
{


// Define "Constant" static member here (although they are not part of the "ConstantInt" class (TODO -> change this place)
const std::string Constant::ERR_INCOMPATIBLE_TYPES  = "Incompatible constant types";
const std::string Constant::ERR_DIVISION_BY_ZERO    = "Division by zero in constant expression";


ConstantInt::ConstantInt(const DataType& Value) :
    Constant(Constant::Types::IntConst  ),
    Value_  (Value                      )
{
}
ConstantInt::~ConstantInt()
{
}

bool ConstantInt::CastFrom(const Constant* Other)
{
    if (Other != nullptr)
    {
        switch (Other->Type())
        {
            case Constant::Types::IntConst:
                Value_ = dynamic_cast<const ConstantInt*>(Other)->Value();
                break;
            case Constant::Types::FloatConst:
                Value_ = static_cast<DataType>(dynamic_cast<const ConstantFloat*>(Other)->Value());
                break;
            default:
                return false;
        }
        return true;
    }
    return false;
}

ConstantPtr ConstantInt::Copy() const
{
    return std::make_shared<ConstantInt>(Value());
}

LiteralExpressionPtr ConstantInt::ToLiteralExpr() const
{
    return LiteralExpression::Create(Value());
}

std::string ConstantInt::Str() const
{
    return xxStr(Value_);
}

bool ConstantInt::Compare(const Constant* Other) const
{
    ConstantInt Compat;
    return Compat.CastFrom(Other) ? Compat.Value() == Value_ : false;
}

void ConstantInt::Div(const Constant* Other)
{
    ConstantInt Compat;
    if (Compat.CastFrom(Other))
    {
        if (Compat.Value() != 0)
            Value_ /= Compat.Value();
        else
            throw Constant::ERR_DIVISION_BY_ZERO;
    }
    else
        throw Constant::ERR_INCOMPATIBLE_TYPES;
}

void ConstantInt::Mod(const Constant* Other)
{
    ConstantInt Compat;
    if (Compat.CastFrom(Other))
    {
        if (Compat.Value() != 0)
            Value_ %= Compat.Value();
        else
            throw Constant::ERR_DIVISION_BY_ZERO;
    }
    else
        throw Constant::ERR_INCOMPATIBLE_TYPES;
}

DefConstArithOp(ConstantInt, Add, +=)
DefConstArithOp(ConstantInt, Sub, -=)
DefConstArithOp(ConstantInt, Mul, *=)
DefConstArithOp(ConstantInt, ShiftL, <<=)
DefConstArithOp(ConstantInt, ShiftR, >>=)


} // /namespace ConstantFolding



// ================================================================================