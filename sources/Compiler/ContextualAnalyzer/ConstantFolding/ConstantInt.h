/*
 * Constant integer header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_CONSTANT_INT_H__
#define __XX_CONSTANT_INT_H__


#include "Constant.h"


namespace ConstantFolding
{


class ConstantInt;

typedef std::shared_ptr<ConstantInt> ConstantIntPtr;

class ConstantInt : public Constant
{
    
    public:
        
        typedef long long int DataType;

        ConstantInt(const DataType& Value = 0ull);
        ~ConstantInt();
        
        bool CastFrom(const Constant* Other);

        ConstantPtr Copy() const;
        LiteralExpressionPtr ToLiteralExpr() const;

        std::string Str() const;

        bool Compare(const Constant* Other) const;

        void Add(const Constant* Other);
        void Sub(const Constant* Other);
        void Mul(const Constant* Other);
        void Div(const Constant* Other);
        void Mod(const Constant* Other);
        void ShiftL(const Constant* Other);
        void ShiftR(const Constant* Other);

        static inline ConstantIntPtr Create(const DataType& Value)
        {
            return std::make_shared<ConstantInt>(Value);
        }

        inline DataType Value() const
        {
            return Value_;
        }

    private:
        
        DataType Value_;

};


} // /namespace ConstantFolding

#endif



// ================================================================================