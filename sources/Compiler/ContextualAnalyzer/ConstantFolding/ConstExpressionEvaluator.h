/*
 * Constant expression evaluator header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_CONST_EXPR_EVALUATOR_H__
#define __XX_CONST_EXPR_EVALUATOR_H__


#include "Constant.h"
#include "ASTDeclarations.h"
#include "Visitor.h"

#include <memory>
#include <stack>


using namespace AbstractSyntaxTrees;

namespace ConstantFolding
{


//! Main class for evaluating constant expressions.
class ConstExpressionEvaluator : public Visitor
{
    
    public:
        
        ConstExpressionEvaluator();
        ~ConstExpressionEvaluator();
        
        ConstantPtr Evaluate(ExpressionPtr Expr);
        
        /* === Expressions === */

        DeclVisitProc(BracketExpression     )
        DeclVisitProc(BinaryExpression      )
        DeclVisitProc(UnaryExpression       )
        DeclVisitProc(StringSumExpression   )
        DeclVisitProc(AssignExpression      )
        DeclVisitProc(LiteralExpression     )
        DeclVisitProc(CallExpression        )
        DeclVisitProc(CastExpression        )
        DeclVisitProc(ObjectExpression      )
        DeclVisitProc(AllocExpression       )
        DeclVisitProc(CopyExpression        )
        DeclVisitProc(ValidationExpression  )

        /* === Literals === */

        DeclVisitProc(BoolLiteral           )
        DeclVisitProc(FloatLiteral          )
        DeclVisitProc(IntegerLiteral        )
        DeclVisitProc(StringLiteral         )

    private:
        
        /* === Functions === */

        void Error(const std::string& Message, ASTPtr AST = nullptr);

        void Push(ConstantPtr ConstValue);
        ConstantPtr Pop();
        ConstantPtr Top();

        /* === Members === */

        std::stack<ConstantPtr> ConstStack_;

};


} // /namespace ConstantFolding

#endif



// ================================================================================