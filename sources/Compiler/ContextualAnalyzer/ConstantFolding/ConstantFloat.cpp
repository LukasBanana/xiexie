/*
 * Constant float file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ConstantFloat.h"
#include "ConstantInt.h"
#include "StringMod.h"
#include "LiteralExpression.h"

#include <sstream>


namespace ConstantFolding
{


const std::string ConstantFloat::ERR_BIT_SHIFT = "Bit-shift not possible for constant floating-points";

ConstantFloat::ConstantFloat(const DataType& Value) :
    Constant(Constant::Types::FloatConst),
    Value_  (Value                      )
{
}
ConstantFloat::~ConstantFloat()
{
}

bool ConstantFloat::CastFrom(const Constant* Other)
{
    if (Other != nullptr)
    {
        switch (Other->Type())
        {
            case Constant::Types::IntConst:
                Value_ = static_cast<DataType>(dynamic_cast<const ConstantInt*>(Other)->Value());
                break;
            case Constant::Types::FloatConst:
                Value_ = dynamic_cast<const ConstantFloat*>(Other)->Value();
                break;
            default:
                return false;
        }
        return true;
    }
    return false;
}

ConstantPtr ConstantFloat::Copy() const
{
    return std::make_shared<ConstantFloat>(Value());
}

LiteralExpressionPtr ConstantFloat::ToLiteralExpr() const
{
    return LiteralExpression::Create(Value());
}

std::string ConstantFloat::Str() const
{
    return xxStr(Value_);
}

bool ConstantFloat::Compare(const Constant* Other) const
{
    ConstantFloat Compat;
    return Compat.CastFrom(Other) ? Compat.Value() == Value_ : false;
}

void ConstantFloat::Mod(const Constant* Other)
{
    throw std::string("Modulo operator not allowed for constant floating-points");
}

void ConstantFloat::ShiftL(const Constant* Other)
{
    throw ConstantFloat::ERR_BIT_SHIFT;
}

void ConstantFloat::ShiftR(const Constant* Other)
{
    throw ConstantFloat::ERR_BIT_SHIFT;
}

DefConstArithOp(ConstantFloat, Add, +=)
DefConstArithOp(ConstantFloat, Sub, -=)
DefConstArithOp(ConstantFloat, Mul, *=)
DefConstArithOp(ConstantFloat, Div, /=)


} // /namespace ConstantFolding



// ================================================================================