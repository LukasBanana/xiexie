/*
 * Big integer header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_BIG_INTEGER_H__
#define __XX_BIG_INTEGER_H__


#include <vector>
#include <string>


/**
Big integer class with arbitrary bit width.
\note This is currently just for experimental use. It is created to be used in constant folding,
but constant folding is currently done with standard data types.
*/
class BigInteger
{
    
    public:
        
        static const unsigned int BitBase = 64;

        typedef unsigned long long int UInt64;

        BigInteger();
        BigInteger(const UInt64 Value);
        BigInteger(const std::string& Value, const size_t& Radix = 10);
        BigInteger(const BigInteger& Other);
        ~BigInteger();

        /* === Operators === */

        BigInteger& operator += (const BigInteger& Other);
        BigInteger& operator -= (const BigInteger& Other);
        BigInteger& operator *= (const BigInteger& Other);
        BigInteger& operator /= (const BigInteger& Other);
        BigInteger& operator %= (const BigInteger& Other);

        BigInteger& operator |= (const BigInteger& Other);
        BigInteger& operator &= (const BigInteger& Other);
        BigInteger& operator ^= (const BigInteger& Other);

        BigInteger& operator <<= (const size_t& Shift);
        BigInteger& operator >>= (const size_t& Shift);

        BigInteger operator ++ ();
        BigInteger operator ++ (int);
        BigInteger operator -- ();
        BigInteger operator -- (int);

        BigInteger operator - () const;

        /* === Functions === */

        bool Equal(const BigInteger& Other) const;
        bool Less(const BigInteger& Other, bool IsSigned = true) const;
        bool Greater(const BigInteger& Other, bool IsSigned = true) const;

        bool IsZero() const;

        BigInteger& DoFlipBits();
        BigInteger FlipBits() const;

        void Str(const std::string& Value, const size_t& Radix = 10);
        std::string Str(const size_t& Radix = 10, bool AppendPrefix = false) const;

        void SetBit(size_t Index, bool Value);
        bool GetBit(size_t Index) const;

        /* === Inline functions === */

        inline size_t BitWidth() const
        {
            return Bits_.size() * BitBase;
        }

        inline const std::vector<UInt64>& Bits() const
        {
            return Bits_;
        }

    private:
        
        /* === Functions === */

        //! Expands the bit storage so that the number of bit fragments fits into it.
        void Fit(size_t NumBitFrags);
        //! Shrinks the bit storage by removing all unused bit fragments.
        void Shrink();

        static unsigned int Digit(char Chr);

        /* === Inline functions === */

        inline void Fit(const BigInteger& Other)
        {
            Fit(Other.Bits_.size());
        }

        /* === Members === */

        /**
        This is the bit storage and contains 1 or more 'bit fragments' (each 64 bit wide).
        The least significant bit fragment is at the first index (i.e. Bits_[0]).
        */
        std::vector<UInt64> Bits_;

};


bool operator == (const BigInteger& Left, const BigInteger& Right);
bool operator != (const BigInteger& Left, const BigInteger& Right);
bool operator < (const BigInteger& Left, const BigInteger& Right);
bool operator <= (const BigInteger& Left, const BigInteger& Right);
bool operator > (const BigInteger& Left, const BigInteger& Right);
bool operator >= (const BigInteger& Left, const BigInteger& Right);

BigInteger operator ~ (const BigInteger& Value);

BigInteger operator + (const BigInteger& Left, const BigInteger& Right);
BigInteger operator - (const BigInteger& Left, const BigInteger& Right);
BigInteger operator * (const BigInteger& Left, const BigInteger& Right);
BigInteger operator / (const BigInteger& Left, const BigInteger& Right);
BigInteger operator % (const BigInteger& Left, const BigInteger& Right);

BigInteger operator | (const BigInteger& Left, const BigInteger& Right);
BigInteger operator & (const BigInteger& Left, const BigInteger& Right);
BigInteger operator ^ (const BigInteger& Left, const BigInteger& Right);

BigInteger operator << (const BigInteger& Left, const size_t& Right);
BigInteger operator >> (const BigInteger& Left, const size_t& Right);


#endif



// ================================================================================