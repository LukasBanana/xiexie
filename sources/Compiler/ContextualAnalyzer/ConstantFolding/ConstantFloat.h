/*
 * Constant float header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_CONSTANT_FLOAT_H__
#define __XX_CONSTANT_FLOAT_H__


#include "Constant.h"


namespace ConstantFolding
{


class ConstantFloat;

typedef std::shared_ptr<ConstantFloat> ConstantFloatPtr;

class ConstantFloat : public Constant
{
    
    public:
        
        typedef long double DataType;

        ConstantFloat(const DataType& Value = 0.0);
        ~ConstantFloat();
        
        bool CastFrom(const Constant* Other);

        ConstantPtr Copy() const;
        LiteralExpressionPtr ToLiteralExpr() const;

        std::string Str() const;

        bool Compare(const Constant* Other) const;

        void Add(const Constant* Other);
        void Sub(const Constant* Other);
        void Mul(const Constant* Other);
        void Div(const Constant* Other);
        void Mod(const Constant* Other);
        void ShiftL(const Constant* Other);
        void ShiftR(const Constant* Other);

        static inline ConstantFloatPtr Create(const DataType& Value)
        {
            return std::make_shared<ConstantFloat>(Value);
        }

        inline DataType Value() const
        {
            return Value_;
        }

    private:
        
        static const std::string ERR_BIT_SHIFT;

        DataType Value_;

};


} // /namespace ConstantFolding

#endif



// ================================================================================