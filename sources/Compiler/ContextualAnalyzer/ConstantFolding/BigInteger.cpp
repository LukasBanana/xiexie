/*
 * Bit integer file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "BigInteger.h"


/*
 * BigInteger class
 */

BigInteger::BigInteger() :
    Bits_(1)
{
    Bits_[0] = 0u;
}

BigInteger::BigInteger(const UInt64 Value) :
    Bits_(1)
{
    Bits_[0] = Value;
}

BigInteger::BigInteger(const std::string& Value, const size_t& Radix) :
    Bits_(1)
{
    Str(Value, Radix);
}

BigInteger::BigInteger(const BigInteger& Other) :
    Bits_(Other.Bits_)
{
}

BigInteger::~BigInteger()
{
}

bool BigInteger::Equal(const BigInteger& Other) const
{
    /* Make first simple comparision */
    if (Bits_.size() != Other.Bits_.size())
        return false;

    /* Compare all bit fragments */
    for (size_t i = 0, n = Bits_.size(); i < n; ++i)
    {
        if (Bits_[i] != Other.Bits_[i])
            return false;
    }

    /* Equility comparision succeeded */
    return true;
}

bool BigInteger::Less(const BigInteger& Other, bool IsSigned) const
{
    //...
    return false;
}

bool BigInteger::Greater(const BigInteger& Other, bool IsSigned) const
{
    //...
    return false;
}

bool BigInteger::IsZero() const
{
    for (auto Frag : Bits_)
    {
        if (Frag != 0ull)
            return false;
    }
    return true;
}

BigInteger& BigInteger::DoFlipBits()
{
    /* Flip bits of all bit fragments */
    for (auto Frag : Bits_)
        Frag = ~Frag;
    return *this;
}

BigInteger BigInteger::FlipBits() const
{
    BigInteger Result(*this);
    Result.DoFlipBits();
    return Result;
}

void BigInteger::Str(const std::string& Value, const size_t& Radix)
{
    /* Check if shift can be used instead of multiply */
    const size_t Shift = (Radix == 16 ? 4 : Radix == 8 ? 3 : Radix == 2 ? 1 : 0);

    /* Store temporary big integers */
    BigInteger BIDigit;
    BigInteger BIRadix(Radix);

    /* Check if number is negative */
    bool IsNeg = false;

    auto it = Value.begin();

    for (; *it == '-' && it != Value.end(); ++it)
        IsNeg = !IsNeg;

    /* Iterate over string from right to left */
    for (; it != Value.end(); ++it)
    {
        /* Get next digit from string */
        auto Digit = BigInteger::Digit(*it);

        /* Shift or multiply current value by radix */
        if (Shift > 0)
            *this <<= Shift;
        else
            *this *= BIRadix;

        /* Add new value */
        BIDigit.Bits_[0] = Digit;
        *this += BIDigit;
    }

    /* Convert to two's complement format (if value is negative) */
    if (IsNeg)
    {
        --(*this);
        DoFlipBits();
    }
}

std::string BigInteger::Str(const size_t& Radix, bool AppendPrefix) const
{
    /* Setup literal prefix */
    std::string Prefix;

    if (AppendPrefix)
    {
        switch (Radix)
        {
            case 2: Prefix = "0b"; break;
            case 8: Prefix = "0"; break;
            case 16: Prefix = "0x"; break;
        }
    }

    /* Check for zero value */
    if (IsZero())
        return Prefix + "0";

    /* Construct literal as string */
    static const char Digits[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    BigInteger Temp(*this);
    std::string Str;

    if (Radix == 2 || Radix == 8 || Radix == 16)
    {
        const size_t Shift = (Radix == 16 ? 4 : Radix == 8 ? 3 : 1);
        const size_t Mask = Radix - 1u;
        
        while (!Temp.IsZero())
        {
            unsigned int Digit = (Temp.Bits_[0] & Mask);
            Str += Digits[Digit];
            Temp >>= Shift;
        }
    }
    else
    {
        //todo...
    }

    return Prefix + std::string(Str.rbegin(), Str.rend());
}

void BigInteger::SetBit(size_t Index, bool Value)
{
    const size_t FragIndex = Index / BitBase;
    const size_t BitIndex = Index % BitBase;

    if (FragIndex < Bits_.size())
    {
        if (Value)
            Bits_[FragIndex] |= (1ull << BitIndex);
        else
            Bits_[FragIndex] ^= (((Bits_[FragIndex] >> BitIndex) & 1ull) << BitIndex);
    }
    else if (Value)
    {
        Fit(FragIndex + 1);
        Bits_[FragIndex] |= (1ull << BitIndex);
    }
}

bool BigInteger::GetBit(size_t Index) const
{
    const size_t FragIndex = Index / BitBase;
    const size_t BitIndex = Index % BitBase;

    if (FragIndex < Bits_.size())
        return ((Bits_[FragIndex] >> BitIndex) & 1ull) != 0;

    return false;
}

BigInteger& BigInteger::operator += (const BigInteger& Other)
{
    Fit(Other);

    bool Carry = false;
    UInt64 Limit = 0ull;
    const size_t n = Bits_.size(), m = Other.Bits_.size();

    for (size_t i = 0; i < n; ++i)
    {
        if (i < m)
        {
            Limit = std::min(Bits_[i], Other.Bits_[i]);
            Bits_[i] += Other.Bits_[i] + (Carry ? 1u : 0u);
        }
        else
        {
            Limit = Bits_[i];
            Bits_[i] += (Carry ? 1u : 0u);
        }

        Carry = (Bits_[i] < Limit || Carry && Bits_[i] == Limit);
    }

    if (Carry)
        Bits_.push_back(1u);

    Shrink();

    return *this;
}

BigInteger& BigInteger::operator -= (const BigInteger& Other)
{
    Fit(Other);

    bool Borrow = false;
    const size_t n = Bits_.size(), m = Other.Bits_.size();

    for (size_t i = 0; i < n; ++i)
    {
        UInt64 Sub = (i < m ? Other.Bits_[i] : 0u);
        UInt64 Temp = (Borrow ? Bits_[i] - 1u : Bits_[i]);
        Borrow = (Sub > Temp || (Borrow && Bits_[i] == 0u));
        Bits_[i] = Temp - Sub;
    }

    Shrink();

    return *this;
}

BigInteger& BigInteger::operator *= (const BigInteger& Other)
{
    //...
    return *this;
}

BigInteger& BigInteger::operator /= (const BigInteger& Other)
{
    //...
    return *this;
}

BigInteger& BigInteger::operator %= (const BigInteger& Other)
{
    //...
    return *this;
}

BigInteger& BigInteger::operator |= (const BigInteger& Other)
{
    Fit(Other);

    const size_t n = std::min(Bits_.size(), Other.Bits_.size());

    for (size_t i = 0; i < n; ++i)
        Bits_[i] |= Other.Bits_[i];

    Shrink();

    return *this;
}

BigInteger& BigInteger::operator &= (const BigInteger& Other)
{
    Fit(Other);

    const size_t n = std::min(Bits_.size(), Other.Bits_.size());
    const size_t m = Bits_.size();

    for (size_t i = 0; i < n; ++i)
        Bits_[i] &= Other.Bits_[i];
    for (size_t i = n; i < m; ++i)
        Bits_[i] = 0u;

    Shrink();

    return *this;
}

BigInteger& BigInteger::operator ^= (const BigInteger& Other)
{
    Fit(Other);

    const size_t n = std::min(Bits_.size(), Other.Bits_.size());
    const size_t m = Bits_.size();

    for (size_t i = 0; i < n; ++i)
        Bits_[i] ^= Other.Bits_[i];
    for (size_t i = n; i < m; ++i)
        Bits_[i] ^= 0u;

    Shrink();

    return *this;
}

BigInteger& BigInteger::operator <<= (const size_t& Shift)
{
    for (size_t n = Bits_.size(), i = n; i > 0; --i)
    {
        for (size_t j = BitBase; j > 0; --j)
        {
            size_t LocalIndex = (i - 1)*BitBase + (j - 1);
            SetBit(LocalIndex + Shift, GetBit(LocalIndex));
        }
    }

    for (size_t i = Shift; i > 0; --i)
        SetBit(i - 1, false);

    return *this;
}

BigInteger& BigInteger::operator >>= (const size_t& Shift)
{
    for (size_t i = 0, n = Bits_.size(); i < n; ++i)
    {
        for (size_t j = 0; j < BitBase; ++j)
        {
            size_t LocalIndex = i*BitBase + j;
            SetBit(LocalIndex, GetBit(LocalIndex + Shift));
        }
    }

    Shrink();

    return *this;
}

BigInteger BigInteger::operator ++ ()
{
    *this += BigInteger(1);
    return *this;
}

BigInteger BigInteger::operator ++ (int)
{
    BigInteger Temp(*this);
    ++(*this);
    return Temp;
}

BigInteger BigInteger::operator -- ()
{
    *this -= BigInteger(1);
    return *this;
}

BigInteger BigInteger::operator -- (int)
{
    BigInteger Temp(*this);
    --(*this);
    return Temp;
}

BigInteger BigInteger::operator - () const
{
    BigInteger Result(0u);
    Result -= *this;
    return Result;
}


/*
 * ======= Private: =======
 */

void BigInteger::Fit(size_t NumBitFrags)
{
    if (Bits_.size() < NumBitFrags)
        Bits_.resize(NumBitFrags);
}

void BigInteger::Shrink()
{
    /* Count all unused bit fragments (from left to right) */
    size_t Num = Bits_.size();

    for (auto it = Bits_.rbegin(); it != Bits_.rend(); ++it)
    {
        if (*it != 0u)
            break;
        --Num;
    }

    /* Shrink bit storage (minimal number of bit fragment is 1) */
    Bits_.resize(Num > 1 ? Num : 1);
}

unsigned int BigInteger::Digit(char Chr)
{
    if (Chr >= '0' && Chr <= '9')
        return Chr - '0';
    if (Chr >= 'a' && Chr <= 'z')
        return 10 + Chr - 'a';
    if (Chr >= 'A' && Chr <= 'Z')
        return 10 + Chr - 'A';
    return 0;
}


/*
 * Global operators
 */

bool operator == (const BigInteger& Left, const BigInteger& Right)
{
    return Left.Equal(Right);
}

bool operator != (const BigInteger& Left, const BigInteger& Right)
{
    return !Left.Equal(Right);
}

bool operator < (const BigInteger& Left, const BigInteger& Right)
{
    return Left.Less(Right);
}

bool operator <= (const BigInteger& Left, const BigInteger& Right)
{
    return Left.Equal(Right) || Left.Less(Right);
}

bool operator > (const BigInteger& Left, const BigInteger& Right)
{
    return Left.Greater(Right);
}

bool operator >= (const BigInteger& Left, const BigInteger& Right)
{
    return Left.Equal(Right) || Left.Greater(Right);
}

BigInteger operator ~ (const BigInteger& Value)
{
    return Value.FlipBits();
}

BigInteger operator + (const BigInteger& Left, const BigInteger& Right)
{
    BigInteger Result = Left;
    Result += Right;
    return Result;
}

BigInteger operator - (const BigInteger& Left, const BigInteger& Right)
{
    BigInteger Result = Left;
    Result -= Right;
    return Result;
}

BigInteger operator * (const BigInteger& Left, const BigInteger& Right)
{
    BigInteger Result = Left;
    Result *= Right;
    return Result;
}

BigInteger operator / (const BigInteger& Left, const BigInteger& Right)
{
    BigInteger Result = Left;
    Result /= Right;
    return Result;
}

BigInteger operator % (const BigInteger& Left, const BigInteger& Right)
{
    BigInteger Result = Left;
    Result %= Right;
    return Result;
}

BigInteger operator | (const BigInteger& Left, const BigInteger& Right)
{
    BigInteger Result = Left;
    Result |= Right;
    return Result;
}

BigInteger operator & (const BigInteger& Left, const BigInteger& Right)
{
    BigInteger Result = Left;
    Result &= Right;
    return Result;
}

BigInteger operator ^ (const BigInteger& Left, const BigInteger& Right)
{
    BigInteger Result = Left;
    Result ^= Right;
    return Result;
}

BigInteger operator << (const BigInteger& Left, const size_t& Right)
{
    BigInteger Result = Left;
    Result <<= Right;
    return Result;
}

BigInteger operator >> (const BigInteger& Left, const size_t& Right)
{
    BigInteger Result = Left;
    Result >>= Right;
    return Result;
}



// ================================================================================