/*
 * Namespace file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Namespace.h"
#include "Scope.h"
#include "ConsoleOutput.h"
#include "ClassObject.h"


namespace ContextualAnalyzer
{


Namespace::Namespace(NameVisibility* Parent, ObjectPtr Object) :
    NameVisibility  (Parent),
    Object_         (Object)
{
}
Namespace::~Namespace()
{
}

bool Namespace::IsTemporal() const
{
    /* A namespace exists during the whole program run-time */
    return false;
}

void Namespace::PrintTable()
{
    if (Parent_ != nullptr)
    {
        if (GetObject() != nullptr)
            Deb("Namespace \"" + Name() + "\" (" + GetObject()->TypeDesc() + ")");
        else
            Deb("Namespace \"" + Name() + "\"");
    }
    else
        Deb("Namespace < Global >");
    ScopedIndent Unused;
    PrintTableBase();
}


/*
 * ======= Private: =======
 */

ObjectPtr Namespace::FindObject(const std::string& Name) const
{
    /* Search for name in object-table */
    auto it = ObjectTable_.find(Name);
    if (it != ObjectTable_.end())
        return it->second;
    
    /* Check for inheritance in a class */
    auto Obj = Object_.get();

    if (Obj != nullptr && Obj->Type() == Object::Types::ClassObj)
    {
        auto ClassObj = dynamic_cast<ClassObject*>(Obj);

        /* Search for name in all base-classes name spaces */
        for (auto ParentFName : ClassObj->Inheritance)
        {
            /* Get linked object */
            auto FName = ParentFName->GetLastIdent();
            if (FName->Link == nullptr)
                continue;
            
            /* Get linked name-visibility */
            auto NameVis = FName->Link->NameVis;
            if (NameVis == nullptr)
                continue;
            
            /* Search for name in parent class' name-visiblity */
            auto SubObj = NameVis->FindObject(Name);
            if (SubObj != nullptr)
                return SubObj;
        }
    }

    return nullptr;
}

bool Namespace::DecorateBottomUpThis(FNameIdentPtr FName) const
{
    /* Check if this namespace has a class object */
    auto Obj = Object_.get();

    if (Obj != nullptr && Obj->Type() == Object::Types::ClassObj)
    {
        /*
        Decorate AST with the namespace's object (class or package)
        -> From this point no more "this" pointers are allowed!
        */
        return DecorateTopDown(FName);
    }

    /* Search in upper name-visibility for a class namespace */
    return Parent_ != nullptr ? Parent_->DecorateBottomUpThis(FName) : false;
}

bool Namespace::DecorateTopDown(FNameIdentPtr FName) const
{
    if (FName == nullptr)
    {
        /* Return without success on invalid input */
        return false;
    }

    /* Decorate AST with the namespace's object (class or package) */
    FName->Link = GetObject().get();

    if (FName->Next == nullptr)
    {
        /* Quit decoration process if finished */
        return true;
    }

    /*
    Search in sub-namespace for next identifier.
    -> don't search in scopes, they are invisible from here.
    */
    FName = FName->Next;

    const auto& Spell = FName->Ident->Spell;

    auto NameSpc = FindNamespace(Spell);

    if (NameSpc != nullptr)
    {
        /* Decorate AST with sub-namespace */
        return NameSpc->DecorateTopDown(FName);
    }

    /* Then search in this namespace's object-table */
    auto Obj = FindObject(Spell);

    if (Obj != nullptr)
    {
        /* If found, decorate FName with object */
        return Obj->Decorate(FName);
    }

    /* Named sub-namespace not found */
    return false;
}

ObjectPtr Namespace::FindTopDown(FNameIdentPtr FName) const
{
    if (FName == nullptr)
    {
        /* Return without success on invalid input */
        return nullptr;
    }

    /* Decorate AST with the namespace's object (class or package) */
    if (FName->Next == nullptr)
        return GetObject();

    /*
    Search in sub-namespace for next identifier.
    -> don't search in scopes, they are invisible from here.
    */
    FName = FName->Next;

    const auto& Spell = FName->Ident->Spell;

    auto NameSpc = FindNamespace(Spell);

    if (NameSpc != nullptr)
    {
        /* Decorate AST with sub-namespace */
        return NameSpc->FindTopDown(FName);
    }

    /* Then search in this namespace's object-table */
    auto Obj = FindObject(Spell);

    if (Obj != nullptr)
    {
        /* If found, decorate FName with object */
        return Obj->Find(FName);
    }

    /* Named sub-namespace not found */
    return nullptr;
}


} // /namespace ContextualAnalyzer



// ================================================================================