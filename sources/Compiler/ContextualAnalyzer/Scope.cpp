/*
 * Scope file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Scope.h"
#include "Namespace.h"
#include "ConsoleOutput.h"
#include "StringMod.h"


namespace ContextualAnalyzer
{


Scope::Scope(NameVisibility* Parent) :
    NameVisibility  (Parent                                             ),
    Desc_           ("Local Scope (Level " + xxStr(TrackLevel()) + ")"  )
{
}
Scope::Scope(NameVisibility* Parent, const std::string& Desc) :
    NameVisibility  (Parent ),
    Desc_           (Desc   )
{
}
Scope::~Scope()
{
}

bool Scope::IsTemporal() const
{
    /* A scope does only exist, while the program is inside it */
    return true;
}

void Scope::PrintTable()
{
    Deb("Scope \"" + Desc() + "\"");
    ScopedIndent Unused;
    PrintTableBase();
}

/*
 * ======= Private: =======
 */

bool Scope::DecorateTopDown(FNameIdentPtr FName) const
{
    if (FName == nullptr)
    {
        /* Return without success on invalid input */
        return false;
    }

    /*
    Search in sub-namespace for next identifier.
    -> don't search in scopes, they are invisible from here.
    */
    const auto& Spell = FName->Ident->Spell;

    auto NameSpc = FindNamespace(Spell);

    if (NameSpc != nullptr)
    {
        /* Decorate AST with sub-namespace */
        return NameSpc->DecorateTopDown(FName);
    }

    /* Then search in this namespace's object-table */
    auto Obj = FindObject(Spell);

    if (Obj != nullptr)
    {
        /* If found, decorate FName with object */
        return Obj->Decorate(FName);
    }

    /* Named object not found */
    return false;
}

ObjectPtr Scope::FindTopDown(FNameIdentPtr FName) const
{
    if (FName == nullptr)
    {
        /* Return without success on invalid input */
        return nullptr;
    }

    /*
    Search in sub-namespace for next identifier.
    -> don't search in scopes, they are invisible from here.
    */
    const auto& Spell = FName->Ident->Spell;

    auto NameSpc = FindNamespace(Spell);

    if (NameSpc != nullptr)
    {
        /* Decorate AST with sub-namespace */
        return NameSpc->FindTopDown(FName);
    }

    /* Then search in this namespace's object-table */
    auto Obj = FindObject(Spell);

    if (Obj != nullptr)
    {
        /* If found, decorate FName with object */
        return Obj->Find(FName);
    }

    /* Named object not found */
    return nullptr;
}



} // /namespace ContextualAnalyzer



// ================================================================================