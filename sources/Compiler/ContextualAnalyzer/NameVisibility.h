/*
 * Name-visibility header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_NAME_VISIBILITY_H__
#define __XX_NAME_VISIBILITY_H__


#include "Object.h"
#include "FNameIdent.h"

#include <vector>


namespace ContextualAnalyzer
{


using namespace SyntacticAnalyzer;
using namespace AbstractSyntaxTrees;


class NameVisibility;
class Scope;
class Namespace;

typedef std::shared_ptr<Namespace> NamespacePtr;
typedef std::shared_ptr<Scope> ScopePtr;
typedef std::shared_ptr<NameVisibility> NameVisibilityPtr;


//! Interface for "Scope" and "Namespace" classes.
class NameVisibility
{
    
    public:
        
        NameVisibility(NameVisibility* Parent);
        virtual ~NameVisibility();

        /* === Functions === */

        /**
        Returns true if this name-visibility is temporal or not.
        Scopes are temporal and namespaces are not temporal.
        */
        virtual bool IsTemporal() const = 0;

        //! Prints debug output to view the namespace- and scope levels.
        virtual void PrintTable() = 0;

        /**
        Decorates the FName identifier AST.
        \param[in] FName Specifies the FName identifier AST node.
        \return True if the FName could be resolved completely.
        Otherwise the searched object could not be found with the specified FName identifier.
        \throw std::string
        */
        bool Decorate(FNameIdentPtr FName) const;

        /**
        Uses the "Decorate" function with a temporary copy of the specified
        FName identifier to find the named object.
        \return Pointer to the object if it was found. Otherwise nullptr.
        \see Decorate
        */
        ObjectPtr Find(const FNameIdentPtr FName) const;

        //! Tracks the visibility level. The global namespace has the level 0.
        unsigned int TrackLevel() const;

        /**
        Adds a new package namespace for the specified identifier or uses
        an existing namespace with the same identifier.
        \see Namespace
        \see Identifier
        */
        NamespacePtr AddPackage(IdentifierPtr Ident);

        /**
        Creates a new namespace for the specified object.
        \return New created namespace instance or nullptr if an object or
        namespace with the same identifier already exists.
        \see Namespace
        \see Object
        */
        NamespacePtr AddNamespace(ObjectPtr Object);

        /**
        Creats a new scope. This scope should be pushed into a stack
        and later poped off from the stack.
        \return New creates scope instance.
        \see Scope
        */
        ScopePtr AddScope();

        /**
        Creats a new scope with a specified description string.
        \see AddScope()
        */
        ScopePtr AddScope(const std::string& Desc);

        /**
        Adds the specified object to the table.
        \param[in] Object Specifies the object which is to be added to the table.
        Its identifier must be unique inside this name-visibility.
        \return True if the object could be added. Otherwise another object
        or namespace with the same identifier already exists.
        */
        bool AddObject(ObjectPtr Object);

        /**
        Returns true if the specified name is unique in this name-visibility.
        Otherwise another object or namespace with the same identifier already exists.
        */
        bool IsUnique(const std::string& Name) const;

        /* === Inline functions === */

        //! Returns the parent name-visibility object or nullptr if this is the global namespace.
        inline NameVisibility* Parent() const
        {
            return Parent_;
        }

    protected:
        
        friend class Scope;
        friend class Namespace;

        /* === Functions === */

        void Deb(const std::string &Str);
        void PrintTableBase();

        NamespacePtr CreateNamespace(const std::string& Name, ObjectPtr Object);

        virtual NamespacePtr FindNamespace(const std::string& Name) const;
        virtual ObjectPtr FindObject(const std::string& Name) const;

        bool DecorateUpper(FNameIdentPtr FName) const;
        ObjectPtr FindUpper(FNameIdentPtr FName) const;

        bool DecorateBottomUp(FNameIdentPtr FName) const;
        virtual bool DecorateBottomUpThis(FNameIdentPtr FName) const;
        virtual bool DecorateTopDown(FNameIdentPtr FName) const = 0;

        ObjectPtr FindBottomUp(FNameIdentPtr FName) const;
        virtual ObjectPtr FindTopDown(FNameIdentPtr FName) const = 0;

        /* === Members === */

        NameVisibility* Parent_;                                //!< Parent name-visibility reference.

        std::map<std::string, ObjectPtr> ObjectTable_;          //!< Object table.
        std::map<std::string, NamespacePtr> NamespaceTable_;    //!< Sub-namespace table.

};


} // /namespace ContextualAnalyzer


#endif



// ================================================================================