/*
 * Namespace header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_NAMESPACE_H__
#define __XX_NAMESPACE_H__


#include "NameVisibility.h"


namespace ContextualAnalyzer
{


class Namespace : public NameVisibility
{
    
    public:
        
        Namespace(NameVisibility* Parent, ObjectPtr Object);
        ~Namespace();

        /* === Functions === */

        bool IsTemporal() const;

        void PrintTable();

        /* === Inline functions === */

        inline ObjectPtr GetObject() const
        {
            return Object_;
        }

        inline std::string Name() const
        {
            return Object_ != nullptr ? Object_->Name() : "";
        }

    private:
        
        friend class Scope;
        friend class NameVisibility;

        /* === Functions === */

        ObjectPtr FindObject(const std::string& Name) const;

        bool DecorateBottomUpThis(FNameIdentPtr FName) const;
        bool DecorateTopDown(FNameIdentPtr FName) const;

        ObjectPtr FindTopDown(FNameIdentPtr FName) const;

        /* === Members === */

        ObjectPtr Object_; //!< The object which denotes this namespace (class or package).

};


} // /namespace ContextualAnalyzer


#endif



// ================================================================================