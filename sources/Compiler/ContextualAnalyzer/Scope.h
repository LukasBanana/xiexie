/*
 * Scope header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_SCOPE_H__
#define __XX_SCOPE_H__


#include "NameVisibility.h"


namespace ContextualAnalyzer
{


class Scope : public NameVisibility
{
    
    public:
        
        Scope(NameVisibility* Parent);
        Scope(NameVisibility* Parent, const std::string& Desc);
        ~Scope();

        /* === Functions === */

        bool IsTemporal() const;

        void PrintTable();

        /* === Inline functions === */

        //! Returns the scope description.
        inline const std::string& Desc() const
        {
            return Desc_;
        }

    private:
        
        friend class Namespace;
        friend class NameVisibility;

        /* === Functions === */

        bool DecorateTopDown(FNameIdentPtr FName) const;

        ObjectPtr FindTopDown(FNameIdentPtr FName) const;

        /* === Members === */

        std::string Desc_;

};


} // /namespace ContextualAnalyzer


#endif



// ================================================================================