/*
 * Procedure factory file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ProcedureFactory.h"
#include "ASTIncludes.h"


namespace ProcedureFactory
{


/* --- Common --- */

RefTypeDenoterPtr GenConstRefTypeDenoter(const TypeDenoterPtr& TDenoter)
{
    if (TDenoter == nullptr)
        return nullptr;

    RefTypeDenoterPtr RefTDenoter;

    if (TDenoter->Type() == TypeDenoter::Types::RefType)
    {
        /* Copy reference type-denoter */
        RefTDenoter = std::dynamic_pointer_cast<RefTypeDenoter>(TDenoter->Copy());
    }
    else
    {
        /* Create new reference type-denoter */
        RefTDenoter = RefTypeDenoter::Create(TDenoter->Copy());
    }

    if (RefTDenoter->TDenoter->Type() != TypeDenoter::Types::ConstType)
    {
        /* Insert const type-denoter */
        RefTDenoter->TDenoter = ConstTypeDenoter::Create(RefTDenoter->TDenoter);
    }

    return RefTDenoter;
}

std::string GetSetterGetterProcName(const VariableObjectPtr& VarObj)
{
    std::string FuncName = VarObj->Ident->Spell;

    if (!FuncName.empty())
    {
        /* Make first character upper case */
        char& FirstChr = FuncName[0];
        if (FirstChr >= 'a' && FirstChr <= 'z')
            FirstChr += ('A' - 'a');
    }

    return FuncName;
}

FunctionDefCommandPtr GenProcDecl(
    const std::string& Name, const TypeDenoterPtr& TDenoter, const ParameterListPtr& ParamList)
{
    /* Create procedure- object and definition AST nodes */
    auto ProcObjAST = std::make_shared<FunctionObject>(TDenoter, Identifier::Create(Name));

    ProcObjAST->FName = FNameIdent::Create(Name);
    ProcObjAST->ParamList = ParamList;

    return std::make_shared<FunctionDefCommand>(ProcObjAST);
}

ParameterListPtr GenParameter(const std::string& Name, const TypeDenoterPtr& TDenoter)
{
    /* Create variable object- and parameter list AST nodes */
    auto VarObjAST = VariableObject::Create(TDenoter, Identifier::Create(Name));

    return std::make_shared<ParameterList>(VarObjAST);
}

/* --- Setter --- */

FunctionDefCommandPtr GenProcDefCmdSetter(const VariableObjectPtr& VarObj)
{
    return std::make_shared<FunctionDefCommand>(GenProcCmdSetter(VarObj));
}

FunctionObjectPtr GenProcCmdSetter(const VariableObjectPtr& VarObj)
{
    const auto& VarName = VarObj->Ident->Spell;

    /* Generate procedure name and return type-denoter */
    auto NameAST = FNameIdent::Create("Set" + GetSetterGetterProcName(VarObj));
    auto TDenoterAST = VoidTypeDenoter::Create();

    /* Setup FName identifier list */
    std::vector<std::string> AssignVarNameList(2);
    AssignVarNameList[0] = "this";
    AssignVarNameList[1] = VarName;

    /* Generate procedure body with assignment */
    auto BlockCmdAST = BlockCommand::Create();

    auto AssignCmdAST = std::make_shared<AssignCommand>(
        FNameIdent::Create(AssignVarNameList.begin(), AssignVarNameList.end()),
        AssignOperator::Create(":="),
        GenSingleObjExpr(VarName)
    );

    BlockCmdAST->Commands.push_back(AssignCmdAST);

    /* Create function object AST node */
    auto ProcObjAST = std::make_shared<FunctionObject>(TDenoterAST, NameAST->Ident, BlockCmdAST);
    ProcObjAST->FName = NameAST;

    /* Generate parameter list */
    auto ParamTDenoterAST = GenConstRefTypeDenoter(VarObj->TDenoter);

    ProcObjAST->ParamList = std::make_shared<ParameterList>(
        VariableObject::Create(ParamTDenoterAST, Identifier::Create(VarName))
    );

    return ProcObjAST;
}

/* --- Getter --- */

FunctionDefCommandPtr GenProcDefCmdGetter(const VariableObjectPtr& VarObj)
{
    return std::make_shared<FunctionDefCommand>(GenProcCmdGetter(VarObj));
}

FunctionObjectPtr GenProcCmdGetter(const VariableObjectPtr& VarObj)
{
    /* Generate procedure name and return type-denoter */
    auto NameAST = FNameIdent::Create("Get" + GetSetterGetterProcName(VarObj));
    auto TDenoterAST = GenConstRefTypeDenoter(VarObj->TDenoter);

    /* Generate procedure body with return statement */
    auto BlockCmdAST = BlockCommand::Create();

    BlockCmdAST->Commands.push_back(
        ReturnCommand::Create(GenSingleObjExpr(VarObj->Ident->Spell))
    );

    /* Create function object AST node */
    auto ProcObjAST = std::make_shared<FunctionObject>(TDenoterAST, NameAST->Ident, BlockCmdAST);

    ProcObjAST->DefFlags |= FunctionObject::Flags::IsReadonly;
    ProcObjAST->FName = NameAST;

    return ProcObjAST;
}

/* --- Expressions --- */

ObjectExpressionPtr GenSingleObjExpr(const std::string& Spell)
{
    return ObjectExpression::Create(FNameIdent::Create(Spell));
}

/* --- String standard procedures --- */

static FunctionObjectPtr SetupStdProc(
    const FunctionObjectPtr& ProcObj, const FunctionObject::StdProcs& Type, bool IsReadOnly = false)
{
    ProcObj->StdProcType = Type;

    if (IsReadOnly)
        ProcObj->DefFlags |= FunctionObject::Flags::IsReadonly;

    return ProcObj;
}

static FunctionObjectPtr GenStringProcEmpty()
{
    return SetupStdProc(
        std::make_shared<FunctionObject>(BoolTypeDenoter::Create(), Identifier::Create("Empty")),
        FunctionObject::StdProcs::String,
        true
    );
}

static FunctionObjectPtr GenStringProcSize()
{
    //!TODO! -> use "SizeType" instead of "uint" as return type!!!
    return SetupStdProc(
        std::make_shared<FunctionObject>(IntTypeDenoter::Create("uint"), Identifier::Create("Size")),
        FunctionObject::StdProcs::String,
        true
    );
}

static FunctionObjectPtr GenStringProcClear()
{
    return SetupStdProc(
        std::make_shared<FunctionObject>(VoidTypeDenoter::Create(), Identifier::Create("Clear")),
        FunctionObject::StdProcs::String
    );
}

static FunctionObjectPtr GenStringProcResize()
{
    auto ProcObj = SetupStdProc(
        std::make_shared<FunctionObject>(VoidTypeDenoter::Create(), Identifier::Create("Resize")),
        FunctionObject::StdProcs::String
    );

    //!TODO! -> use "SizeType" instead of "uint" as input type!!!
    ProcObj->ParamList = GenParameter("size", IntTypeDenoter::Create("uint"));

    return ProcObj;
}

static FunctionObjectPtr GenStringProcFind()
{
    //!TODO! -> use "SizeType" instead of "uint" as return type!!!
    auto ProcObj = SetupStdProc(
        std::make_shared<FunctionObject>(IntTypeDenoter::Create("uint"), Identifier::Create("Find")),
        FunctionObject::StdProcs::String,
        true
    );

    ProcObj->ParamList = GenParameter("subStr", StringTypeDenoter::CreateConstRef());
    //!TODO! -> use "SizeType" instead of "uint" as input type!!!
    auto ParamStartPos = ProcObj->ParamList->Next = GenParameter("startPos", IntTypeDenoter::Create("uint"));

    ParamStartPos->Obj->Expr = LiteralExpression::Create(0);

    return ProcObj;
}

static FunctionObjectPtr GenStringProcSub()
{
    auto ProcObj = SetupStdProc(
        std::make_shared<FunctionObject>(StringTypeDenoter::Create(), Identifier::Create("Sub")),
        FunctionObject::StdProcs::String,
        true
    );

    //!TODO! -> use "SizeType" instead of "uint" as input types!!!
    ProcObj->ParamList = GenParameter("startPos", IntTypeDenoter::Create("uint"));
    auto ParamLen = ProcObj->ParamList->Next = GenParameter("len", IntTypeDenoter::Create("uint"));

    //!TODO! -> use "(SizeType)-1" instead of "-1"!!!
    ParamLen->Obj->Expr = LiteralExpression::Create(-1);

    return ProcObj;
}

static FunctionObjectPtr GenStringProcLeft()
{
    auto ProcObj = SetupStdProc(
        std::make_shared<FunctionObject>(StringTypeDenoter::Create(), Identifier::Create("Left")),
        FunctionObject::StdProcs::String,
        true
    );

    //!TODO! -> use "SizeType" instead of "uint" as input types!!!
    ProcObj->ParamList = GenParameter("len", IntTypeDenoter::Create("uint"));

    return ProcObj;
}

static FunctionObjectPtr GenStringProcRight()
{
    auto ProcObj = SetupStdProc(
        std::make_shared<FunctionObject>(StringTypeDenoter::Create(), Identifier::Create("Right")),
        FunctionObject::StdProcs::String,
        true
    );

    //!TODO! -> use "SizeType" instead of "uint" as input types!!!
    ProcObj->ParamList = GenParameter("len", IntTypeDenoter::Create("uint"));

    return ProcObj;
}

static FunctionObjectPtr GenStringProcLower()
{
    return SetupStdProc(
        std::make_shared<FunctionObject>(StringTypeDenoter::Create(), Identifier::Create("Lower")),
        FunctionObject::StdProcs::String,
        true
    );
}

static FunctionObjectPtr GenStringProcUpper()
{
    return SetupStdProc(
        std::make_shared<FunctionObject>(StringTypeDenoter::Create(), Identifier::Create("Lower")),
        FunctionObject::StdProcs::String,
        true
    );
}

static std::map<std::string, FunctionObjectPtr> StdStringProcMap;

std::map<std::string, FunctionObjectPtr> GenStdStringProcs()
{
    StdStringProcMap.clear();

    StdStringProcMap["Empty"    ] = GenStringProcEmpty  ();
    StdStringProcMap["Size"     ] = GenStringProcSize   ();
    StdStringProcMap["Clear"    ] = GenStringProcClear  ();
    StdStringProcMap["Resize"   ] = GenStringProcResize ();
    StdStringProcMap["Find"     ] = GenStringProcFind   ();
    StdStringProcMap["Sub"      ] = GenStringProcSub    ();
    StdStringProcMap["Left"     ] = GenStringProcLeft   ();
    StdStringProcMap["Right"    ] = GenStringProcRight  ();
    StdStringProcMap["Lower"    ] = GenStringProcLower  ();
    StdStringProcMap["Upper"    ] = GenStringProcUpper  ();

    //...

    return StdStringProcMap;
}

bool DecorateWithStdStringProc(const FNameIdentPtr& FName)
{
    const auto& Spell = FName->Ident->Spell;

    auto it = StdStringProcMap.find(Spell);
    if (it != StdStringProcMap.end())
    {
        FName->Link = it->second.get();
        return true;
    }

    return false;
}

FunctionObjectPtr FindStdStringProc(const FNameIdentPtr& FName)
{
    const auto& Spell = FName->Ident->Spell;

    auto it = StdStringProcMap.find(Spell);
    if (it != StdStringProcMap.end())
        return it->second;

    return nullptr;
}

/* --- Intrinsics (for XASM) --- */

PackageCommandPtr GenXASMIntrinsics()
{
    /* Create block command AST node */
    auto BlockCmdAST = BlockCommand::Create();

    auto AddIntrinsic = [&](const std::string& Name, const TypeDenoterPtr& TDenoter, const ParameterListPtr& ParamList)
    {
        BlockCmdAST->Commands.push_back(GenProcDecl(Name, TDenoter, ParamList));
    };

    /* Generate common type denoters */
    auto VoidTDenoterAST    = VoidTypeDenoter::Create();
    auto VoidPtrTDenoterAST = RawPtrTypeDenoter::Create(VoidTDenoterAST);
    auto IntTDenoterAST     = IntTypeDenoter::Create();

    auto StrParam       = GenParameter("text", StringTypeDenoter::Create());
    auto IntParam       = GenParameter("value", IntTypeDenoter::Create());
    auto UIntParam      = GenParameter("value", IntTypeDenoter::Create("uint"));
    auto FltParam       = GenParameter("value", FloatTypeDenoter::Create());
    auto VoidPtrParam   = GenParameter("addr", VoidPtrTDenoterAST);

    /* Generate intrinsic procedure AST nodes */
    AddIntrinsic("AllocMem",    VoidPtrTDenoterAST, UIntParam   );
    AddIntrinsic("FreeMem",     VoidTDenoterAST,    VoidPtrParam);

    AddIntrinsic("SysCall",     VoidTDenoterAST,    StrParam    );
    AddIntrinsic("ClearTerm",   VoidTDenoterAST,    nullptr     );
    AddIntrinsic("Print",       VoidTDenoterAST,    StrParam    );
    AddIntrinsic("PrintLn",     VoidTDenoterAST,    StrParam    );
    AddIntrinsic("PrintInt",    VoidTDenoterAST,    IntParam    );
    AddIntrinsic("PrintFloat",  VoidTDenoterAST,    FltParam    );
    AddIntrinsic("InputInt",    IntTDenoterAST,     nullptr     );
    AddIntrinsic("Sleep",       VoidPtrTDenoterAST, IntParam    );

    // to be continued ...

    /* Create package command AST node */
    return PackageCommand::Create(FNameIdent::Create("Intr"), BlockCmdAST);
}


} // /namespace ProcedureFactory



// ================================================================================