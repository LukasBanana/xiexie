/*
 * Name-visibility file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "NameVisibility.h"
#include "Scope.h"
#include "Namespace.h"
#include "ConsoleOutput.h"
#include "PackageObject.h"


namespace ContextualAnalyzer
{


NameVisibility::NameVisibility(NameVisibility* Parent) :
    Parent_(Parent)
{
}
NameVisibility::~NameVisibility()
{
}

bool NameVisibility::Decorate(FNameIdentPtr FName) const
{
    return DecorateBottomUp(FName);
}

ObjectPtr NameVisibility::Find(const FNameIdentPtr FName) const
{
    return FindBottomUp(FName);
}

unsigned int NameVisibility::TrackLevel() const
{
    return Parent_ != nullptr ? Parent_->TrackLevel() + 1 : 0;
}

NamespacePtr NameVisibility::AddPackage(IdentifierPtr Ident)
{
    if (Ident == nullptr)
        return nullptr;
    
    /* Check if package namespace already exists */
    const auto& Name = Ident->Spell;

    auto NameSpc = FindNamespace(Name);

    if (NameSpc != nullptr)
        return NameSpc;

    /* Create new package object */
    auto Object = std::make_shared<PackageObject>(Ident);

    /* Create new package namespace */
    return CreateNamespace(Name, Object);
}

NamespacePtr NameVisibility::AddNamespace(ObjectPtr Object)
{
    if (Object == nullptr)
        return nullptr;

    /* If object is anonymous, then create namespace without integrating into this name-visibility */
    if (Object->IsAnonymous())
        return std::make_shared<Namespace>(this, Object);

    /* Check if name is unique */
    const auto& Name = Object->Name();

    if (!IsUnique(Name))
        return nullptr;

    /* Create new namespace for the object */
    return CreateNamespace(Name, Object);
}

ScopePtr NameVisibility::AddScope()
{
    return std::make_shared<Scope>(this);
}

ScopePtr NameVisibility::AddScope(const std::string& Desc)
{
    return std::make_shared<Scope>(this, Desc);
}

bool NameVisibility::AddObject(ObjectPtr Object)
{
    if (Object == nullptr)
        return false;

    /* Check if name is unique */
    const auto& Name = Object->Name();

    if (!IsUnique(Name))
        return false;

    /* Insert object into the object-table */
    ObjectTable_[Name] = Object;

    return true;
}

bool NameVisibility::IsUnique(const std::string& Name) const
{
    /* Search in namesapce-table first */
    auto itName = NamespaceTable_.find(Name);

    if (itName != NamespaceTable_.end())
        return false;
    
    /* Then search in object-table */
    auto itObj = ObjectTable_.find(Name);

    if (itObj != ObjectTable_.end())
        return false;

    return true;
}


/*
 * ======= Protected: =======
 */

void NameVisibility::Deb(const std::string &Str)
{
    ConsoleOutput::Message(Str);
}

void NameVisibility::PrintTableBase()
{
    for (auto Obj : ObjectTable_)
        Deb("Object \"" + Obj.second->Ident->Spell + "\" (" + Obj.second->TypeDesc() + ")");
    for (auto Obj : NamespaceTable_)
        Obj.second->PrintTable();
}

NamespacePtr NameVisibility::CreateNamespace(const std::string& Name, ObjectPtr Object)
{
    /* Create new namespace for the object and insert it into the namespace-table */
    auto NewNamespace = std::make_shared<Namespace>(this, Object);

    /* Store namespace in namespace-table */
    NamespaceTable_[Name] = NewNamespace;

    /* Decorate object AST with the new namespace */
    Object->NameVis = NewNamespace.get();

    return NewNamespace;
}

NamespacePtr NameVisibility::FindNamespace(const std::string& Name) const
{
    /* Search for name in namespace-table */
    auto it = NamespaceTable_.find(Name);
    return it != NamespaceTable_.end() ? it->second : nullptr;
}

ObjectPtr NameVisibility::FindObject(const std::string& Name) const
{
    /* Search for name in object-table */
    auto it = ObjectTable_.find(Name);
    return it != ObjectTable_.end() ? it->second : nullptr;
}

bool NameVisibility::DecorateUpper(FNameIdentPtr FName) const
{
    return Parent_ != nullptr ? Parent_->Decorate(FName) : false;
}

ObjectPtr NameVisibility::FindUpper(FNameIdentPtr FName) const
{
    return Parent_ != nullptr ? Parent_->Find(FName) : false;
}

bool NameVisibility::DecorateBottomUp(FNameIdentPtr FName) const
{
    /* Get spelling from FName identifier */
    const auto& Spell = FName->Ident->Spell;

    /* Check if identifier names the active class object */
    if (Spell == "this")
        return DecorateBottomUpThis(FName);

    /* Then search in this namespace's namespace-table */
    auto NameSpc = FindNamespace(Spell);

    if (NameSpc != nullptr)
    {
        if (NameSpc->DecorateTopDown(FName))
        {
            /* Decorated FName completely -> return with success */
            return true;
        }
        else
        {
            /* Then search in the upper name-visibility */
            return DecorateUpper(FName);
        }
    }

    /* Then search in this namespace's object-table */
    auto Obj = FindObject(Spell);

    if (Obj != nullptr)
    {
        /*
        !TODO!
        check if object is a 'PatternObject'
        -> if so, decorate pattern's linked object (this could be a class,
        so we need to find the class' name-visiblity) !!!
        */

        /* If found, decorate FName with object */
        return Obj->Decorate(FName);
    }

    /* Then search in the upper name-visibility */
    return DecorateUpper(FName);
}

bool NameVisibility::DecorateBottomUpThis(FNameIdentPtr FName) const
{
    /* By default -> move on */
    return Parent_ != nullptr ? Parent_->DecorateBottomUpThis(FName) : false;
}

ObjectPtr NameVisibility::FindBottomUp(FNameIdentPtr FName) const
{
    /* Get spelling from Fname identifier */
    const auto& Spell = FName->Ident->Spell;

    /* Then search in this namespace's namespace-table */
    auto NameSpc = FindNamespace(Spell);

    if (NameSpc != nullptr)
    {
        auto Obj = NameSpc->FindTopDown(FName);

        if (Obj != nullptr)
        {
            /* Decorated FName completely -> return with success */
            return Obj;
        }
        else
        {
            /* Then search in the upper name-visibility */
            return FindUpper(FName);
        }
    }

    /* Then search in this namespace's object-table */
    auto Obj = FindObject(Spell);

    if (Obj != nullptr)
    {
        /* If found, decorate FName with object */
        return Obj->Find(FName);
    }

    /* Then search in the upper name-visibility */
    return FindUpper(FName);
}


} // /namespace ContextualAnalyzer



// ================================================================================