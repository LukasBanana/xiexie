/*
 * Scope manager file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ScopeManager.h"

#include "VoidTypeDenoter.h"
#include "BoolTypeDenoter.h"
#include "IntTypeDenoter.h"
#include "FloatTypeDenoter.h"
#include "StringTypeDenoter.h"
#include "ManagedPtrTypeDenoter.h"

#include "FlagsObject.h"
#include "EnumObject.h"
#include "VariableObject.h"
#include "ClassObject.h"
#include "TypeObject.h"

#include "BinaryOperator.h"
#include "Token.h"


namespace ContextualAnalyzer
{


ScopeManager::ScopeManager() :
    GlobalNamespace_(nullptr),
    ActiveNameVis_  (nullptr)
{
    /* Create the global namespace */
    GlobalNamespace_ = std::make_shared<Namespace>(nullptr, nullptr);
    ActiveNameVis_ = GlobalNamespace_.get();

    /* Establish all default entries */
    EstablishDefaultTypes();
}
ScopeManager::~ScopeManager()
{
}

/* --- Search functions --- */

TypeDenoterPtr ScopeManager::FindType(const TypeDenoter::Types& Type) const
{
    switch (Type)
    {
        case TypeDenoter::Types::VoidType:      return FindType("void"  );
        case TypeDenoter::Types::BoolType:      return FindType("bool"  );
        case TypeDenoter::Types::IntType:       return FindType("int"   );
        case TypeDenoter::Types::FloatType:     return FindType("float" );
        case TypeDenoter::Types::StringType:    return FindType("string");
        case TypeDenoter::Types::MngPtrType:    return NullPointerTDenoter_;
        default:                                break;
    }
    return nullptr;
}

TypeDenoterPtr ScopeManager::FindType(const std::string& Name) const
{
    /* Find type object */
    auto Obj = Find(FNameIdent::Create(Name));

    if (Obj == nullptr)
        return nullptr;

    /* Check that the object is a type-object */
    if (Obj->Type() != Object::Types::TypeObj)
        return nullptr;

    /* Return type denoter */
    auto TObj = std::dynamic_pointer_cast<TypeObject>(Obj);

    return TObj->TDenoter;
}

/* --- Name-visibility functions --- */

bool ScopeManager::PushPackage(IdentifierPtr Ident)
{
    return Push(ActiveNameVis_->AddPackage(Ident));
}

bool ScopeManager::PushNamespace(ObjectPtr Object)
{
    return Push(ActiveNameVis_->AddNamespace(Object));
}

bool ScopeManager::PushScope()
{
    return Push(ActiveNameVis_->AddScope());
}

bool ScopeManager::Push(NameVisibilityPtr NameVis)
{
    if (NameVis != nullptr)
    {
        NameVisStack_.push(NameVis);
        ActiveNameVis_ = NameVis.get();
        return true;
    }
    return false;
}

void ScopeManager::Pop()
{
    if (!NameVisStack_.empty())
    {
        /* Pop previous active name-visibility from stack */
        NameVisStack_.pop();
        
        if (NameVisStack_.empty())
            ActiveNameVis_ = GlobalNamespace_.get();
        else
            ActiveNameVis_ = NameVisStack_.top().get();
    }
}

bool ScopeManager::AddObject(ObjectPtr Object)
{
    return ActiveNameVis_->AddObject(Object);
}

bool ScopeManager::Decorate(FNameIdentPtr FName) const
{
    return ActiveNameVis_->Decorate(FName);
}

ObjectPtr ScopeManager::Find(FNameIdentPtr FName) const
{
    return ActiveNameVis_->Find(FName);
}

bool ScopeManager::IsUnique(const std::string& Name) const
{
    return ActiveNameVis_->IsUnique(Name);
}

/* --- Other functions --- */

const TypeDenoter* ScopeManager::FindCommonDenomNoneConst(
    const TypeDenoter* TDenoter1, const TypeDenoter* TDenoter2) const
{
    const TypeDenoter::Types T1 = TDenoter1->Type(), T2 = TDenoter2->Type();

    /* Void type denoter can not be used for expressions (only as function return type) */
    if (T1 == TypeDenoter::Types::VoidType || T2 == TypeDenoter::Types::VoidType)
        return nullptr;

    if (T1 == T2)
    {
        /* Check for custom types */
        if (T1 == TypeDenoter::Types::CustomType)
        {
            /* Get custom type denoters */
            auto CTDenoter1 = dynamic_cast<const CustomTypeDenoter*>(TDenoter1);
            auto CTDenoter2 = dynamic_cast<const CustomTypeDenoter*>(TDenoter2);

            if (CTDenoter1 == nullptr || CTDenoter2 == nullptr)
                return nullptr;

            /* Compare linked objects */
            if (CTDenoter1->Link == CTDenoter2->Link)
                return TDenoter1;

            return nullptr;
        }

        /* Use T1 type denoter if T1 and T2 are equal or T1's size is larger than T2's size */
        if (TDenoter1->Spell == TDenoter2->Spell || TDenoter1->GetTypeSize() > TDenoter2->GetTypeSize())
            return TDenoter1;
        return TDenoter2;
    }

    /* Boolean type can not be mixed with other types (except strings) */
    if (T1 == TypeDenoter::Types::BoolType || T2 == TypeDenoter::Types::BoolType)
        return nullptr;

    /* String types have higher priority than floats */
    if (T1 == TypeDenoter::Types::StringType)
        return TDenoter1;
    if (T2 == TypeDenoter::Types::StringType)
        return TDenoter2;

    /* Float types have higher priority than integers */
    if (T1 == TypeDenoter::Types::FloatType)
        return TDenoter1;
    if (T2 == TypeDenoter::Types::FloatType)
        return TDenoter2;

    /* No common denominator has found */
    return nullptr;
}

bool ScopeManager::AreTypesComparable(
    const BinaryOperator& Op, const TypeDenoter& TDenoter1, const TypeDenoter& TDenoter2) const
{
    const TypeDenoter::Types T1 = TDenoter1.Type(), T2 = TDenoter2.Type();

    /* Equal types are always comparable (integer & integer, float & float) */
    if (T1 == T2)
        return true;

    /* Check for the special case of strings */
    if (T1 == TypeDenoter::Types::StringType || T2 == TypeDenoter::Types::StringType)
    {
        /* Strings can only compared with another type, if the operator is an equality relation */
        return Op.Spell == "=" || Op.Spell == "!=";
    }

    /* For the other types casting is required */
    return false;
}


/* 
 * ======= Private: =======
 */

template <typename T> inline std::shared_ptr<T> MakeTypeToken(const Token::Types& Type, const std::string& Spell)
{
    return std::make_shared<T>(std::make_shared<Token>(SourcePosition::Ignore, Type, Spell));
}

void ScopeManager::EstablishDefaultTypes()
{
    /* Create default type denoter AST nodes */
    auto TypeTable = Token::TypeDenoterTable();

    for (const auto& TDenoter : TypeTable)
    {
        switch (TDenoter.second)
        {
            case Token::Types::BoolTypeDenoter:
                AddDefaultType<BoolTypeDenoter>(TDenoter.first, TDenoter.second);
                break;
            case Token::Types::IntTypeDenoter:
                AddDefaultType<IntTypeDenoter>(TDenoter.first, TDenoter.second);
                break;
            case Token::Types::FloatTypeDenoter:
                AddDefaultType<FloatTypeDenoter>(TDenoter.first, TDenoter.second);
                break;
            case Token::Types::StringTypeDenoter:
                AddDefaultType<StringTypeDenoter>(TDenoter.first, TDenoter.second);
                break;
            default:
                break;
        }
    }

    /* Create default null-pointer type */
    auto NullPointerTkn = std::make_shared<Token>(SourcePosition::Ignore, Token::Types::PointerLiteral, "null");
    NullPointerTDenoter_ = std::make_shared<MngPtrTypeDenoter>(NullPointerTkn);
}

CustomTypeDenoterPtr ScopeManager::CreateCustomTypeDenoter(const std::string& Name, ObjectPtr Entry)
{
    /* Create new custom type denoter AST node */
    auto Ident = std::make_shared<Identifier>(SourcePosition::Ignore, Name);
    auto FName = std::make_shared<FNameIdent>(Ident);
    auto TDenoter = std::make_shared<CustomTypeDenoter>(FName, Entry);
    return TDenoter;
}


} // /namespace ContextualAnalyzer



// ================================================================================