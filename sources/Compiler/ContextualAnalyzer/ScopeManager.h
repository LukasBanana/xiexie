/*
 * Scope manager header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_SCOPE_MANAGER_H__
#define __XX_SCOPE_MANAGER_H__


#include "TypeDenoter.h"
#include "VariableObject.h"
#include "Namespace.h"
#include "Scope.h"

#include <stack>


namespace ContextualAnalyzer
{


using namespace SyntacticAnalyzer;
using namespace AbstractSyntaxTrees;


/**
This is the common scope manager class. It manages all local variables,
type denoters and other AST nodes in scope levels.
\see ScopeManager
\see CommonScope
*/
class ScopeManager
{
    
    public:
        
        ScopeManager();
        ~ScopeManager();

        /* === Functions === */

        /* --- Search functions --- */

        TypeDenoterPtr FindType(const TypeDenoter::Types& Type) const;
        TypeDenoterPtr FindType(const std::string& Name) const;

        /* --- Name-visibility functions --- */

        //! Creats a new package namespace or uses an existing one.
        bool PushPackage(IdentifierPtr Ident);

        /**
        Creats a new namespace, pushes it onto the name-visibility stack and
        adds the specified object to the created namespace instance.
        This namespace is the new active name-visibility.
        */
        bool PushNamespace(ObjectPtr Object);

        /**
        Creates a new scope and pushes it onto the name-visibility stack.
        This scope is the new active name-visibility.
        */
        bool PushScope();

        /**
        Pops the active name-visibility from the stack.
        \see PushNamespace
        \see PushScope
        */
        void Pop();

        //! Adds the specified object into the active name-visibility.
        bool AddObject(ObjectPtr Object);

        /**
        Decorates the specified FName identifier by searching
        the named object from the active name-visibility.
        */
        bool Decorate(FNameIdentPtr FName) const;

        //! Searched the named object from the active name-visibility.
        ObjectPtr Find(FNameIdentPtr FName) const;

        //! Returns true if the specified name is unique in active name-visibility.
        bool IsUnique(const std::string& Name) const;

        /* --- Other functions --- */
        
        //! Trys to find a common denominator for the two specified none-constant types or returns nullptr if the two types are incompatible.
        const TypeDenoter* FindCommonDenomNoneConst(
            const TypeDenoter* TDenoter1, const TypeDenoter* TDenoter2
        ) const;

        bool AreTypesComparable(
            const BinaryOperator& Op, const TypeDenoter& TDenoter1, const TypeDenoter& TDenoter2
        ) const;

        /* === Inline functions === */

        //! Returns the global namespace.
        inline NamespacePtr GlobalNamespace() const
        {
            return GlobalNamespace_;
        }

        /**
        Sets the active name-visibility.
        \see NameVisibility
        */
        inline void SetActiveNameVisibility(NameVisibility* NameVis)
        {
            ActiveNameVis_ = (NameVis != nullptr ? NameVis : GlobalNamespace().get());
        }
        /**
        Returns the active name-visibility.
        \see NameVisibility
        */
        inline NameVisibility* GetActiveNameVisibility() const
        {
            return ActiveNameVis_;
        }

    private:
        
        /* === Functions === */

        void EstablishDefaultTypes();

        CustomTypeDenoterPtr CreateCustomTypeDenoter(const std::string& Name, ObjectPtr Entry);
        
        bool Push(NameVisibilityPtr NameVis);

        /* === Templates === */

        template <typename T> void AddDefaultType(const std::string& Name, const Token::Types& Type)
        {
            auto Tkn = std::make_shared<Token>(SourcePosition::Ignore, Type, Name);

            /* Create type-denoter and type-object */
            TypeDenoterPtr TDenoter = std::make_shared<T>(Tkn);
            auto Ident = std::make_shared<Identifier>(Tkn);

            auto Obj = std::make_shared<TypeObject>(Ident, TDenoter);

            AddObject(Obj);
        }

        /* === Members === */

        NamespacePtr GlobalNamespace_;

        std::stack<NameVisibilityPtr> NameVisStack_;
        NameVisibility* ActiveNameVis_;

        TypeDenoterPtr NullPointerTDenoter_;            //!< Special case for type denoter of null pointer literal.

};


} // /namespace ContextualAnalyzer


#endif



// ================================================================================