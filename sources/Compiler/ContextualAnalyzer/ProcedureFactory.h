/*
 * Procedure factory header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_PROCEDURE_FACTORY_H__
#define __XX_PROCEDURE_FACTORY_H__


#include "ASTDeclarations.h"

#include <string>
#include <map>


using namespace AbstractSyntaxTrees;

/**
The procedure factory generates procedures for the AST.
This is mainly used by the contextual analyzer to fill the AST with new nodes.
*/
namespace ProcedureFactory
{


/* --- Common --- */

RefTypeDenoterPtr GenConstRefTypeDenoter(const TypeDenoterPtr& TDenoter);
std::string GetSetterGetterProcName(const VariableObjectPtr& AST);

FunctionDefCommandPtr GenProcDecl(
    const std::string& Name, const TypeDenoterPtr& TDenoter, const ParameterListPtr& ParamList = nullptr
);

ParameterListPtr GenParameter(const std::string& Name, const TypeDenoterPtr& TDenoter);

/* --- Setter --- */

FunctionDefCommandPtr GenProcDefCmdSetter(const VariableObjectPtr& VarObj);
FunctionObjectPtr GenProcCmdSetter(const VariableObjectPtr& VarObj);

/* --- Getter --- */

FunctionDefCommandPtr GenProcDefCmdGetter(const VariableObjectPtr& VarObj);
FunctionObjectPtr GenProcCmdGetter(const VariableObjectPtr& VarObj);

/* --- Expressions --- */

ObjectExpressionPtr GenSingleObjExpr(const std::string& Spell);

/* --- String standard procedures --- */

std::map<std::string, FunctionObjectPtr> GenStdStringProcs();

bool DecorateWithStdStringProc(const FNameIdentPtr& FName);
FunctionObjectPtr FindStdStringProc(const FNameIdentPtr& FName);

/* --- Intrinsics (for XASM) --- */

PackageCommandPtr GenXASMIntrinsics();


} // /namespace ProcedureFactory

#endif



// ================================================================================