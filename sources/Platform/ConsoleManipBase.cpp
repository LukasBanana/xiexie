/*
 * Console manipulation namespace base file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ConsoleManip.h"


namespace Platform
{


namespace ConsoleManip
{

const color_t Colors::Red       = (1 << 0);
const color_t Colors::Green     = (1 << 1);
const color_t Colors::Blue      = (1 << 2);

const color_t Colors::Intens    = (1 << 3);

const color_t Colors::Black     = 0;
const color_t Colors::Gray      = Colors::Red | Colors::Green | Colors::Blue;
const color_t Colors::White     = Colors::Gray | Colors::Intens;

const color_t Colors::Yellow    = Colors::Red | Colors::Green | Colors::Intens;
const color_t Colors::Pink      = Colors::Red | Colors::Blue | Colors::Intens;
const color_t Colors::Cyan      = Colors::Green | Colors::Blue | Colors::Intens;

} // /namespace ConsoleManip


} // /namespace Platform



// ================================================================================
