/*
 * Null frame header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_NULL_FRAME_H__
#define __XX_NULL_FRAME_H__


#include "Frame.h"

#include <string>


namespace Platform
{


class Frame
{
    
    public:
        
        Frame(const std::string& Title);
        ~Frame();

        /* === Functions === */

        void SetPosition(int X, int Y);
        void GetPosition(int &X, int &Y);

        void SetSize(int W, int H);
        void GetSize(int &W, int &H);

        void SetTitle(const std::string& Title);
        std::string GetTitle() const;

        void AddEntry(const std::string& Text);
        void ClearEntries();

        void UpdateEvents();

        bool IsOpen() const;

    private:
        
        /* === Members === */

        std::string Title_;

        int X_, Y_;
        int W_, H_;
};


} // /namespace Platform


#endif



// ================================================================================
