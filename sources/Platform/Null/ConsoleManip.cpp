/*
 * Null console manipulation namespace file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ConsoleManip.h"


namespace Platform
{


namespace ConsoleManip
{

void SetColor(const color_t& Front)
{
    // Do nothing
}
void SetColor(const color_t& Front, const color_t& Back)
{
    // Do nothing
}

void PushAttrib()
{
    // Do nothing
}
void PopAttrib()
{
    // Do nothing
}

} // /namespace ConsoleManip


} // /namespace Platform



// ================================================================================