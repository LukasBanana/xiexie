/*
 * Null frame file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Frame.h"


namespace Platform
{


Frame::Frame(const std::string& Title) :
    Title_  (Title  ),
    X_      (0      ),
    Y_      (0      ),
    W_      (0      ),
    H_      (0      )
{
}
Frame::~Frame()
{
}

void Frame::SetPosition(int X, int Y)
{
    X_ = X;
    Y_ = Y;
}
void Frame::GetPosition(int &X, int &Y)
{
    X = X_;
    Y = Y_;
}

void Frame::SetSize(int W, int H)
{
    W_ = W;
    H_ = H;
}
void Frame::GetSize(int &W, int &H)
{
    W = W_;
    H = H_;
}

void Frame::SetTitle(const std::string& Title)
{
    Title_ = Title;
}
std::string Frame::GetTitle() const
{
    return Title_;
}

void Frame::AddEntry(const std::string& Text)
{
    // Do nothing
}
void Frame::ClearEntries()
{
    // Do nothing
}

void Frame::UpdateEvents()
{
    // Do nothing
}

bool Frame::IsOpen() const
{
    return false;
}


} // /namespace Platform



// ================================================================================
