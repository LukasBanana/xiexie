/*
 * Windows class header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_WIN_CLASS_H__
#define __XX_WIN_CLASS_H__


#include <Windows.h>
#include <string>


namespace Platform
{


class WinClass
{
    
    public:
        
        WinClass(const std::string& Name);
        ~WinClass();

        /* === Inline functions === */

        inline const std::string& GetName() const
        {
            return Name_;
        }

    private:

        /* === Members === */
        
        std::string Name_;

};


} // /namespace Platform


#endif



// ================================================================================