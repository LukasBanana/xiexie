/*
 * Windows console manipulation namespace file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "ConsoleManip.h"

#include <Windows.h>
#include <stack>


namespace Platform
{


namespace ConsoleManip
{

static std::stack<CONSOLE_SCREEN_BUFFER_INFO> ScrBufferInfoStack;

static inline HANDLE StdOut()
{
    return GetStdHandle(STD_OUTPUT_HANDLE);
}

void SetColor(const color_t& Front)
{
    /* Get current console screen buffer infor */
    CONSOLE_SCREEN_BUFFER_INFO BufInfo;
	GetConsoleScreenBufferInfo(StdOut(), &BufInfo);

    /* Setup attributes for new console color */
    WORD Attrib = (BufInfo.wAttributes & 0xFFF0);
    
    if ((Front & Colors::Red    ) != 0) Attrib |= FOREGROUND_RED;
    if ((Front & Colors::Green  ) != 0) Attrib |= FOREGROUND_GREEN;
    if ((Front & Colors::Blue   ) != 0) Attrib |= FOREGROUND_BLUE;
    if ((Front & Colors::Intens ) != 0) Attrib |= FOREGROUND_INTENSITY;
    
    /* Set new console attribute */
    SetConsoleTextAttribute(StdOut(), Attrib);
}

void SetColor(const color_t& Front, const color_t& Back)
{
    /* Setup attributes for new console color */
    WORD Attrib = 0;

    if ((Front & Colors::Red    ) != 0) Attrib |= FOREGROUND_RED;
    if ((Front & Colors::Green  ) != 0) Attrib |= FOREGROUND_GREEN;
    if ((Front & Colors::Blue   ) != 0) Attrib |= FOREGROUND_BLUE;
    if ((Front & Colors::Intens ) != 0) Attrib |= FOREGROUND_INTENSITY;

    if ((Back & Colors::Red     ) != 0) Attrib |= BACKGROUND_RED;
    if ((Back & Colors::Green   ) != 0) Attrib |= BACKGROUND_GREEN;
    if ((Back & Colors::Blue    ) != 0) Attrib |= BACKGROUND_BLUE;
    if ((Back & Colors::Intens  ) != 0) Attrib |= BACKGROUND_INTENSITY;

    /* Set new console attribute */
    SetConsoleTextAttribute(StdOut(), Attrib);
}

void PushAttrib()
{
    /* Get current console screen buffer info */
    CONSOLE_SCREEN_BUFFER_INFO BufInfo;
	GetConsoleScreenBufferInfo(StdOut(), &BufInfo);

    /* Push buffer info onto stack */
    ScrBufferInfoStack.push(BufInfo);
}

void PopAttrib()
{
    if (!ScrBufferInfoStack.empty())
    {
        /* Push buffer info onto stack */
        const CONSOLE_SCREEN_BUFFER_INFO BufInfo = ScrBufferInfoStack.top();
        ScrBufferInfoStack.pop();

        /* Reset previous console screen buffer info */
        SetConsoleTextAttribute(StdOut(), BufInfo.wAttributes);
    }
}

} // /namespace ConsoleManip


} // /namespace Platform



// ================================================================================