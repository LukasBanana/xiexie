/*
 * Windows frame file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Frame.h"

#include <commctrl.h>


namespace Platform
{


Frame::Frame(const std::string& Title) :
    WinClass_   ("XieXie"                   ),
    Window_     (CreateFrameWindow(Title)   ),
    ListView_   (CreateListViewWindow()     )
{
}
Frame::~Frame()
{
    if (Window_ != nullptr)
        DestroyWindow(Window_);
    if (ListView_ != nullptr)
        DestroyWindow(ListView_);
}

void Frame::SetPosition(int X, int Y)
{
    if (Window_ != nullptr)
        SetWindowPos(Window_, HWND_TOP, X, Y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
}

void Frame::GetPosition(int &X, int &Y)
{
    if (Window_ != nullptr)
    {
        RECT Rect;
        GetWindowRect(Window_, &Rect);
        X = Rect.left;
        Y = Rect.top;
    }
    else
        X = Y = 0;
}

void Frame::SetSize(int W, int H)
{
    if (Window_ != nullptr)
        SetWindowPos(Window_, HWND_TOP, 0, 0, W, H, SWP_NOMOVE | SWP_NOZORDER);
}

void Frame::GetSize(int &W, int &H)
{
    if (Window_ != nullptr)
    {
        RECT Rect;
        GetWindowRect(Window_, &Rect);
        W = Rect.right - Rect.left;
        H = Rect.bottom - Rect.top;
    }
    else
        W = H = 0;
}

void Frame::SetTitle(const std::string& Title)
{
    if (Window_ != nullptr)
        SetWindowText(Window_, Title.c_str());
}

std::string Frame::GetTitle() const
{
    if (Window_ != nullptr)
    {
        char Title[MAX_PATH];
        GetWindowText(Window_, Title, MAX_PATH);
        return std::string(Title);
    }
    return "";
}

void Frame::AddEntry(const std::string& Text)
{
    if (ListView_ != nullptr)
        SendMessage(ListView_, LB_ADDSTRING, 0, reinterpret_cast<LPARAM>(Text.c_str()));
}

void Frame::ClearEntries()
{
    if (ListView_ != nullptr)
    {
        LRESULT Result = SendMessage(ListView_, LB_GETCOUNT, 0, 0);

        auto NumItems = static_cast<unsigned int>(Result);

        while (NumItems-- > 0)
            SendMessage(ListView_, LB_DELETESTRING, 0, 0);
    }
}

void Frame::UpdateEvents()
{
    /* Update all queued messages */
    MSG Message;

    while (PeekMessage(&Message, nullptr, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&Message);
        DispatchMessage(&Message);
    }
}

bool Frame::IsOpen() const
{
    return Window_ != nullptr;
}


/*
 * ======= Private: =======
 */

DWORD Frame::GetWindowStyle() const
{
    return WS_OVERLAPPEDWINDOW | WS_VISIBLE;
}

DWORD Frame::GetListViewStyle() const
{
    return WS_VISIBLE | WS_CHILD | WS_VSCROLL | LBS_NOINTEGRALHEIGHT | LBS_NOTIFY;
}

HWND Frame::CreateFrameWindow(const std::string& Title)
{
    /* Setup default window position */
    const int SW = GetSystemMetrics(SM_CXSCREEN);
    const int SH = GetSystemMetrics(SM_CYSCREEN);

    int W = min(800, SW - 100);
    int H = SH - 150;
    int X = SW/2 - W/2;
    int Y = SH/2 - H/2;

    /* Create frame window object */
    HWND Wnd = CreateWindow(
        WinClass_.GetName().c_str(),
        Title.c_str(),
        GetWindowStyle(),
        X,
        Y,
        W,
        H,
        HWND_DESKTOP,
        nullptr,
        GetModuleHandle(nullptr),
        nullptr
    );

    /* Setup user data */
    SetWindowLongPtr(Wnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(&Window_));

    return Wnd;
}

HWND Frame::CreateListViewWindow()
{
    /* Get size for list view */
    RECT Rect;
    GetClientRect(Window_, &Rect);
    
    /* Create list view window object */
    HWND Wnd = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        reinterpret_cast<LPCSTR>(WC_LISTBOX),
        "",
        GetListViewStyle(),
        0,
        0,
        Rect.right - Rect.left,
        Rect.bottom - Rect.top,
        Window_,
        nullptr,
        GetModuleHandle(nullptr),
        nullptr
    );

    /* Setup user data */
    SetWindowLongPtr(Wnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(&ListView_));

    /* Create and setup default font */
    int FontHeight = -MulDiv(8, GetDeviceCaps(GetDC(0), LOGPIXELSY), 72);

    HFONT Font = CreateFont(
        FontHeight,
        0,
        0,
        0,
        FW_NORMAL,              // No bold type
        0,                      // No italic
        0,                      // No underlined
        0,                      // No striked-out
        ANSI_CHARSET,
        OUT_DEFAULT_PRECIS,
        CLIP_DEFAULT_PRECIS,
        DEFAULT_QUALITY,
        DEFAULT_PITCH,
        "courier new"           // Default MS/Windows font type
    );

    SendMessage(Wnd, WM_SETFONT, reinterpret_cast<WPARAM>(Font), 0);

    return Wnd;
}


} // /namespace Platform



// ================================================================================