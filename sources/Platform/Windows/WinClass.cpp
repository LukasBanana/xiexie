/*
 * Windows class file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "WinClass.h"

#include <commctrl.h>


namespace Platform
{


/*
 * Internal window callback procedure
 */

BOOL CALLBACK EnumChildWindowCallback(HWND Window, LPARAM LParam)
{
    if (LParam == 0)
        return FALSE;

    /* Get parent window's client area */
    RECT* Rect = reinterpret_cast<RECT*>(LParam);

    /* Resize child window */
    int W = Rect->right - Rect->left;
    int H = Rect->bottom - Rect->top;

    SetWindowPos(Window, HWND_TOP, 0, 0, W, H, SWP_NOMOVE | SWP_NOZORDER);

    return TRUE;
}

LRESULT CALLBACK WindowCallback(HWND Window, UINT Message, WPARAM wParam, LPARAM lParam)
{
    switch (Message)
    {
        case WM_DESTROY:
        {
            /* Get window owner frame */
            LONG_PTR UserData = GetWindowLongPtr(Window, GWLP_USERDATA);
            if (UserData != 0)
            {
                /* Clear window handle */
                HWND* Wnd = reinterpret_cast<HWND*>(UserData);
                *Wnd = nullptr;
            }
        }
        break;

        case WM_SIZE:
        {
            /* Get window client size */
            RECT Rect;
            GetClientRect(Window, &Rect);

            /* Resize all children windows */
            EnumChildWindows(Window, EnumChildWindowCallback, reinterpret_cast<LPARAM>(&Rect));
        }
        break;

        case WM_KEYDOWN:
        {
            if (wParam == VK_ESCAPE)
                DestroyWindow(Window);
        }
        break;

        default:
        break;
    }

    return DefWindowProc(Window, Message, wParam, lParam);
}


/*
 * WinClass class
 */

WinClass::WinClass(const std::string& Name) :
    Name_(Name)
{
    /* Setup window class information */
    WNDCLASS WC;
    memset(&WC, 0, sizeof(WC));
    
    WC.style            = CS_HREDRAW | CS_VREDRAW | CS_OWNDC | CS_DBLCLKS;
    WC.hInstance        = GetModuleHandle(nullptr);
    WC.lpfnWndProc      = reinterpret_cast<WNDPROC>(WindowCallback);
    WC.hIcon            = LoadIcon(0, IDI_APPLICATION);
    WC.hCursor          = LoadCursor(0, IDC_ARROW);
    WC.hbrBackground    = reinterpret_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
    WC.cbClsExtra       = 0;
    WC.cbWndExtra       = 0;
    WC.lpszMenuName     = nullptr;
    WC.lpszClassName    = Name_.c_str();
    
    /* Register window class */
    if (!RegisterClass(&WC))
        throw std::string("Could not register window class \"" + Name + "\"");

    /* Initialize common controls */
    INITCOMMONCONTROLSEX CommCtrl;
    {
        CommCtrl.dwSize = sizeof(CommCtrl);
        CommCtrl.dwICC  = ICC_LISTVIEW_CLASSES;
    }
    InitCommonControlsEx(&CommCtrl);
}
WinClass::~WinClass()
{
    /* Unregister window class */
    UnregisterClass(Name_.c_str(), GetModuleHandle(nullptr));
}


} // /namespace Platform



// ================================================================================