/*
 * Console manipulation namespace header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX_CONSOLEMANIP_H__
#define __XX_CONSOLEMANIP_H__


namespace Platform
{


namespace ConsoleManip
{

/* === Structures === */

typedef unsigned int color_t;

struct Colors
{
    static const color_t Red;
    static const color_t Green;
    static const color_t Blue;

    static const color_t Intens;

    static const color_t Black;
    static const color_t Gray;
    static const color_t White;

    static const color_t Yellow;
    static const color_t Pink;
    static const color_t Cyan;
};


/* === Functions === */

void SetColor(const color_t& Front);
void SetColor(const color_t& Front, const color_t& Back);

void PushAttrib();
void PopAttrib();


/* === Additional structures === */

struct ScopedColor
{
    ScopedColor(const color_t& Front)
    {
        PushAttrib();
        SetColor(Front);
    }
    ScopedColor(const color_t& Front, const color_t& Back)
    {
        PushAttrib();
        SetColor(Front, Back);
    }
    ~ScopedColor()
    {
        PopAttrib();
    }
};


} // /namespace ConsoleManip


} // /namespace Platform


#endif



// ================================================================================
