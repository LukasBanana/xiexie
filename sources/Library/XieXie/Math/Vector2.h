/*
 * Vector2 class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__VECTOR2_H__
#define __XX__VECTOR2_H__


#include "Math.h"


namespace Math
{


template <typename T> class Vector2
{
	
	public:
		
		static const size_t num = 2;
		
		Vector2() :
			x(0),
			y(0)
		{
		}
		Vector2(const T &size) :
			x(size),
			y(size)
		{
		}
		Vector2(const T &vecX, const T &vecY) :
			x(vecX),
			y(vecY)
		{
		}
		Vector2(const Vector3<T> &other) :
			x(other.x),
			y(other.y)
		{
		}
		
		inline Vector2<T>& Normalize()
		{
			Math::Normalize<T, Vector2<T>::num(Ptr())
			return *this;
		}
		
		inline T LengthSq() const
		{
			return Math::LengthSq<T, Vector2<T>::num>(Ptr());
		}
		inline T Length() const
		{
			return Math::Length<T, Vector2<T>::num>(Ptr());
		}
		
		inline Vector2<T>& operator = (const Vector2<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		inline Vector2<T>& operator += (const Vector2<T>& other)
		{
			Math::Add(*this, other);
			return *this;
		}
		inline Vector2<T>& operator -= (const Vector2<T>& other)
		{
			Math::Sub(*this, other);
			return *this;
		}
		inline Vector2<T>& operator *= (const Vector2<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		inline Vector2<T>& operator /= (const Vector2<T>& other)
		{
			Math::Div(*this, other);
			return *this;
		}
		
		T x, y;
		
};


__XX__DEFINE_TYPEDEFS__(Vector2)


}

#endif
