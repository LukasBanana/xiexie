/*
 * Vector4 class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__VECTOR4_H__
#define __XX__VECTOR4_H__


#include "Math.h"


namespace Math
{


template <typename T> class Vector4
{
	
	public:
		
		static const size_t num = 4;
		
		Vector4() :
			x(0),
			y(0),
			z(0),
			w(1)
		{
		}
		Vector4(const T& size) :
			x(size	),
			y(size	),
			z(size	),
			w(1		)
		{
		}
		Vector4(const T& vecX, const T& vecY, const T& vecZ, const T& vecW = T(1)) :
			x(vecX),
			y(vecY),
			z(vecZ),
			w(vecW)
		{
		}
		Vector4(const Vector4<T> &other) :
			x(other.x),
			y(other.y),
			z(other.z),
			w(other.w)
		{
		}
		
		inline Vector4<T>& Normalize()
		{
			Math::Normalize<T, Vector4<T>::num(Ptr())
			return *this;
		}
		
		inline T LengthSq() const
		{
			return Math::LengthSq<T, Vector4<T>::num>(Ptr());
		}
		inline T Length() const
		{
			return Math::Length<T, Vector4<T>::num>(Ptr());
		}
		
		inline Vector4<T>& operator = (const Vector4<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		inline Vector4<T>& operator += (const Vector4<T>& other)
		{
			Math::Add(*this, other);
			return *this;
		}
		inline Vector4<T>& operator -= (const Vector4<T>& other)
		{
			Math::Sub(*this, other);
			return *this;
		}
		inline Vector4<T>& operator *= (const Vector4<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		inline Vector4<T>& operator /= (const Vector4<T>& other)
		{
			Math::Div(*this, other);
			return *this;
		}
		
		T x, y, z, w;
		
};


__XX__DEFINE_TYPEDEFS__(Vector4)


}

#endif
