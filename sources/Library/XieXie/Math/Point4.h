/*
 * Point4 class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__POINT4_H__
#define __XX__POINT4_H__


#include "Math.h"
#include "Vector4.h"


namespace Math
{


template <typename T> class Point4
{
	
	public:
		
		static const size_t num = 4;
		
		Point4() :
			x(0),
			y(0),
			z(0),
			w(1)
		{
		}
		Point4(const T& pntX, const T& pntY, const T& pntZ, const T& pntW = T(1)) :
			x(pntX),
			y(pntY),
			z(pntZ),
			w(pntW)
		{
		}
		Point4(const Point4<T> &other) :
			x(other.x),
			y(other.y),
			z(other.z),
			w(other.w)
		{
		}
		
		inline Point4<T>& operator = (const Point4<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		
		inline Point4<T>& operator += (const Point4<T>& other)
		{
			Math::Add(*this, other);
			return *this;
		}
		inline Point4<T>& operator += (const Vector4<T>& vec)
		{
			Math::Add(*this, vec);
			return *this;
		}
		
		inline Point4<T>& operator -= (const Point4<T>& other)
		{
			Math::Sub(*this, other);
			return *this;
		}
		inline Point4<T>& operator -= (const Vector4<T>& vec)
		{
			Math::Sub(*this, vec);
			return *this;
		}
		
		inline Point4<T>& operator *= (const Point4<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		inline Point4<T>& operator *= (const Vector4<T>& vec)
		{
			Math::Mul(*this, vec);
			return *this;
		}
		
		inline Point4<T>& operator /= (const Point4<T>& other)
		{
			Math::Div(*this, other);
			return *this;
		}
		inline Point4<T>& operator /= (const Vector4<T>& vec)
		{
			Math::Div(*this, vec);
			return *this;
		}
		
		T x, y, z, w;
		
};


__XX__DEFINE_TYPEDEFS__(Point4)


}

#endif
