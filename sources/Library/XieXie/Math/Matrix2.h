/*
 * Matrix2 class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__MATRIX2_H__
#define __XX__MATRIX2_H__


#include "Math.h"
#include "Vector2.h"


namespace Math
{


template <typename T> class Matrix2
{
	
	public:
		
		static const size_t num = 2;
		
		Matrix2() :
			x(1, 0),
			y(0, 1)
		{
		}
		Matrix2(const Vector2<T>& colX, const Vector2<T>& colY) :
			x(colX),
			y(colY)
		{
		}
		Matrix2(const Matrix2<T> &other) :
			x(other.x),
			y(other.y)
		{
		}
		
		inline Matrix2<T>& operator = (const Matrix2<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		
		inline Matrix2<T>& operator *= (const Matrix2<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		
		inline const Vector2<T>& operator [] (size_t index) const
		{
			return *((&x) + index);
		}
		inline Vector2<T>& operator [] (size_t index)
		{
			return *((&x) + index);
		}
		
		inline const T& operator () (size_t col, size_t row) const
		{
			return ((*this)[col])[row];
		}
		inline T& operator () (size_t col, size_t row)
		{
			return ((*this)[col])[row];
		}
		
		inline T Track() const
		{
			return Math::Track(*this);
		}
		
		Vector2<T> x, y;
		
};


__XX__DEFINE_TYPEDEFS__(Matrix2)


}

#endif
