/*
 * Matrix3 class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__MATRIX3_H__
#define __XX__MATRIX3_H__


#include "Math.h"
#include "Vector3.h"


namespace Math
{


template <typename T> class Matrix3
{
	
	public:
		
		static const size_t num = 3;
		
		Matrix3() :
			x(1, 0, 0),
			y(0, 1, 0),
			z(0, 0, 1)
		{
		}
		Matrix3(const Vector3<T>& colX, const Vector3<T>& colY, const Vector3<T>& colZ) :
			x(colX),
			y(colY),
			z(colZ)
		{
		}
		Matrix3(const Matrix3<T> &other) :
			x(other.x),
			y(other.y),
			z(other.z)
		{
		}
		
		inline Matrix3<T>& operator = (const Matrix3<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		
		inline Matrix3<T>& operator *= (const Matrix3<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		
		inline const Vector3<T>& operator [] (size_t index) const
		{
			return *((&x) + index);
		}
		inline Vector3<T>& operator [] (size_t index)
		{
			return *((&x) + index);
		}
		
		inline const T& operator () (size_t col, size_t row) const
		{
			return ((*this)[col])[row];
		}
		inline T& operator () (size_t col, size_t row)
		{
			return ((*this)[col])[row];
		}
		
		inline T Track() const
		{
			return Math::Track(*this);
		}
		
		Vector3<T> x, y, z;
		
};


__XX__DEFINE_TYPEDEFS__(Matrix3)


}

#endif
