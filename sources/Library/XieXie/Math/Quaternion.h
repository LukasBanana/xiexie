/*
 * Quaternion class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__QUATERNION_H__
#define __XX__QUATERNION_H__


#include "Math.h"


namespace Math
{


template <typename T> class Quaternion
{
	
	public:
		
		static const size_t num = 4;
		
		Quaternion() :
			x(0),
			y(0),
			z(0),
			w(1)
		{
		}
		Quaternion(const T& qtrX, const T& qtrY, const T& qtrZ, const T& qtrW) :
			x(qtrX),
			y(qtrY),
			z(qtrZ),
			w(qtrW)
		{
		}
		Quaternion(const Quaternion<T> &other) :
			x(other.x),
			y(other.y),
			z(other.z),
			w(other.w)
		{
		}
		
		inline Quaternion<T>& Normalize()
		{
			//...
			return *this;
		}
		
		inline Quaternion<T>& operator = (const Quaternion<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		
		T x, y, z, w;
		
};


__XX__DEFINE_TYPEDEFS__(Quaternion)


}

#endif
