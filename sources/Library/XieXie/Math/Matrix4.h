/*
 * Matrix4 class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__MATRIX4_H__
#define __XX__MATRIX4_H__


#include "Math.h"
#include "Vector4.h"


namespace Math
{


template <typename T> class Matrix4
{
	
	public:
		
		static const size_t num = 4;
		
		Matrix4() :
			x(1, 0, 0, 0),
			y(0, 1, 0, 0),
			z(0, 0, 1, 0),
			w(0, 0, 0, 1)
		{
		}
		Matrix4(const Vector4<T>& colX, const Vector4<T>& colY, const Vector4<T>& colZ, const Vector4<T>& colW) :
			x(colX),
			y(colY),
			z(colZ),
			w(colW)
		{
		}
		Matrix4(const Matrix4<T>& other) :
			x(other.x),
			y(other.y),
			z(other.z),
			w(other.w)
		{
		}
		
		inline Matrix4<T>& operator = (const Matrix4<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		
		inline Matrix4<T>& operator *= (const Matrix4<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		
		inline const Vector4<T>& operator [] (size_t index) const
		{
			return *((&x) + index);
		}
		inline Vector4<T>& operator [] (size_t index)
		{
			return *((&x) + index);
		}
		
		inline const T& operator () (size_t col, size_t row) const
		{
			return ((*this)[col])[row];
		}
		inline T& operator () (size_t col, size_t row)
		{
			return ((*this)[col])[row];
		}
		
		inline T Track() const
		{
			return Math::Track(*this);
		}
		
		Vector4<T> x, y, z, w;
		
};


__XX__DEFINE_TYPEDEFS__(Matrix4)


}

#endif
