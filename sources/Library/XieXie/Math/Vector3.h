/*
 * Vector3 class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__VECTOR3_H__
#define __XX__VECTOR3_H__


#include "Math.h"


namespace Math
{


template <typename T> class Vector3
{
	
	public:
		
		static const size_t num = 3;
		
		Vector3() :
			x(0),
			y(0),
			z(0)
		{
		}
		Vector3(const T &size) :
			x(size),
			y(size),
			z(size)
		{
		}
		Vector3(const T &vecX, const T &vecY, const T &vecZ) :
			x(vecX),
			y(vecY),
			z(vecZ)
		{
		}
		Vector3(const Vector3<T> &other) :
			x(other.x),
			y(other.y),
			z(other.z)
		{
		}
		
		inline Vector3<T>& Normalize()
		{
			Math::Normalize<T, Vector3<T>::num(Ptr())
			return *this;
		}
		
		inline T LengthSq() const
		{
			return Math::LengthSq<T, Vector3<T>::num>(Ptr());
		}
		inline T Length() const
		{
			return Math::Length<T, Vector3<T>::num>(Ptr());
		}
		
		inline Vector3<T>& operator = (const Vector3<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		inline Vector3<T>& operator += (const Vector3<T>& other)
		{
			Math::Add(*this, other);
			return *this;
		}
		inline Vector3<T>& operator -= (const Vector3<T>& other)
		{
			Math::Sub(*this, other);
			return *this;
		}
		inline Vector3<T>& operator *= (const Vector3<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		inline Vector3<T>& operator /= (const Vector3<T>& other)
		{
			Math::Div(*this, other);
			return *this;
		}
		
		T x, y, z;
		
};


__XX__DEFINE_TYPEDEFS__(Vector3)


}

#endif
