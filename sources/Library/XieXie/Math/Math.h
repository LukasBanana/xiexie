/*
 * Math namespace
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__MATH__H__
#define __XX__MATH__H__


#include "MathCore.h"


#endif
