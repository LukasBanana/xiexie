/*
 * Real class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__REAL_H__
#define __XX__REAL_H__


#include "Math.h"
#include "BasicString.h"


namespace Math
{


class Real
{
	
	public:
		
		Real()
		{
		}
		Real(const String& str) :
			container_(str.__XX__GetStr())
		{
		}
		Real(const Real& other) :
			container_(other.container_)
		{
		}
		
		Real& operator = (const Real& other)
		{
			container_ = other.container_;
			return *this;
		}
		
		Real& operator += (const Real& other)
		{
			//todo...
			return *this;
		}
		
		Real& operator -= (const Real& other)
		{
			//todo...
			return *this;
		}
		
		Real& operator *= (const Real& other)
		{
			//todo...
			return *this;
		}
		
		Real& operator /= (const Real& other)
		{
			//todo...
			return *this;
		}
		
		inline String ToString() const
		{
			return String(container_);
		}
		
	private:
		
		std::string container_;
		
};


inline Real operator + (const Real& a, const Real& b)
{
	Real result(a);
	result += b;
	return result;
}

inline Real operator - (const Real& a, const Real& b)
{
	Real result(a);
	result -= b;
	return result;
}

inline Real operator * (const Real& a, const Real& b)
{
	Real result(a);
	result *= b;
	return result;
}

inline Real operator / (const Real& a, const Real& b)
{
	Real result(a);
	result /= b;
	return result;
}


}

#endif
