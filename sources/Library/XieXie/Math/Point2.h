/*
 * Point2 class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__POINT2_H__
#define __XX__POINT2_H__


#include "Math.h"


namespace Math
{


template <typename T> class Point2
{
	
	public:
		
		static const size_t num = 2;
		
		Point2() :
			x(0),
			y(0)
		{
		}
		Point2(const T& pntX, const T& pntY) :
			x(pntX),
			y(pntY)
		{
		}
		Point2(const Point2<T> &other) :
			x(other.x),
			y(other.y)
		{
		}
		
		inline Point2<T>& operator = (const Point2<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		inline Point2<T>& operator += (const Point2<T>& other)
		{
			Math::Add(*this, other);
			return *this;
		}
		inline Point2<T>& operator -= (const Point2<T>& other)
		{
			Math::Sub(*this, other);
			return *this;
		}
		inline Point2<T>& operator *= (const Point2<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		inline Point2<T>& operator /= (const Point2<T>& other)
		{
			Math::Div(*this, other);
			return *this;
		}
		
		T x, y;
		
};


__XX__DEFINE_TYPEDEFS__(Point2)


}

#endif
