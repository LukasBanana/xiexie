/*
 * Point3 class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__POINT3_H__
#define __XX__POINT3_H__


#include "Math.h"


namespace Math
{


template <typename T> class Point3
{
	
	public:
		
		static const size_t num = 3;
		
		Point3() :
			x(0),
			y(0),
			z(0)
		{
		}
		Point3(const T& pntX, const T& pntY, const T& pntZ) :
			x(pntX),
			y(pntY),
			z(pntZ)
		{
		}
		Point3(const Point3<T> &other) :
			x(other.x),
			y(other.y),
			z(other.z)
		{
		}
		
		inline Point3<T>& operator = (const Point3<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		inline Point3<T>& operator += (const Point3<T>& other)
		{
			Math::Add(*this, other);
			return *this;
		}
		inline Point3<T>& operator -= (const Point3<T>& other)
		{
			Math::Sub(*this, other);
			return *this;
		}
		inline Point3<T>& operator *= (const Point3<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		inline Point3<T>& operator /= (const Point3<T>& other)
		{
			Math::Div(*this, other);
			return *this;
		}
		
		T x, y, z;
		
};


__XX__DEFINE_TYPEDEFS__(Point3)


}

#endif
