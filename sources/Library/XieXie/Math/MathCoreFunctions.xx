/*
 * Math core functions file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

once

cpp { #include <math.h> }


package Math {


/* --- Constants --- */

const quad pi = 3.14159265358979323846264338327950288419716939937510;
const quad e = 2.718281828459045235;

/* --- Basic functions --- */

T Abs<pattern T>(const T& value) {
	T result
	cpp { result = abs(value); }
	ret result
}

/* --- Trigonometric functions --- */

T Sin<pattern T>(const T& value) {
	T result
	cpp { result = sin(value); }
	ret result
}

T Cos<pattern T>(const T& value) {
	T result
	cpp { result = cos(value); }
	ret result
}

T Tan<pattern T>(const T& value) {
	T result
	cpp { result = tan(value); }
	ret result
}

T ASin<pattern T>(const T& value) {
	T result
	cpp { result = asin(value); }
	ret result
}

T ACos<pattern T>(const T& value) {
	T result
	cpp { result = acos(value); }
	ret result
}

T ATan<pattern T>(const T& value) {
	T result
	cpp { result = atan(value); }
	ret result
}

/* --- Exponential functions --- */

//! Square (value*value)
T Sq<pattern T>(const T& value) {
	ret value*value
}

//! Square root.
T Sqrt<pattern T>(const T& value) {
	T result
	cpp { result = sqrt(value); }
	ret result
}

//! Exponential function to base e.
T Exp<pattern T>(const T& value) {
	T result
	cpp { result = exp(value); }
	ret result
}

//! Exponential function to any base (also known as "pow"-function).
T Exp<pattern T>(const T& base, const T& exponent) {
	T result
	cpp { result = pos(base, exponent); }
	ret result
}

/* --- Logarithmic functions --- */

//! Natural logarithm to base-e.
T Log<pattern T>(const T& value) {
	T result
	cpp { result = log(value); }
	ret result
}

//! Logarithm to any base.
T Log<pattern T>(const T& value, const T& base) {
	T result
	cpp { result = log(value) / log(base); }
	ret result
}

//! Logarithm to base 10.
T Log10<pattern T>(const T& value) {
	T result
	cpp { result = log10(value); }
	ret result
}

//! Logarithm to base 2.
T Log2<pattern T>(const T& value) {
	T result
	cpp { result = log2(value); }
	ret result
}


} // /package Math



// ================================================================================