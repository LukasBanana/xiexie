/*
 * Math core header
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__MATH_CORE__H__
#define __XX__MATH_CORE__H__


#include <math.h>


namespace Math
{

/* --- Internal macros --- */

#define __XX__DEFINE_TYPEDEFS__(n)		\
	typedef n<bool			> n##b;		\
	typedef n<char			> n##c;		\
	typedef n<unsigned char	> n##uc;	\
	typedef n<short			> n##s;		\
	typedef n<unsigned short> n##us;	\
	typedef n<int			> n##i;		\
	typedef n<unsigned int	> n##ui;	\
	typedef n<float			> n##f;		\
	typedef n<double		> n##d;		\
	typedef n<long double	> n##q;

/* --- "Set" template --- */

template <typename T, size_t num> inline void Set(T* out, const T* in)
{
	for (size_t i = 0; i < num; ++i)
		out[i] = in[i];
}

template <template <typename> class Vec, typename T> inline void Set(Vec<T>& out, const Vec<T>& in)
{
	Set<T, Vec<T>::num>(&out[0], &in[0])
}

template <
	template <typename> class VecA,
	template <typename> class VecB,
	typename T >
inline void Set(VecA<T>& out, const VecB<T>& in)
{
	Set<
		T,
		VecA<T>::num < VecA<T>::num ?
			VecA<T>::num :
			VecB<T>::num >
	(
		&out[0], &in[0]
	)
}

/* --- "Add" template --- */

template <typename T, size_t num> inline void Add(T* out, const T* in)
{
	for (size_t i = 0; i < num; ++i)
		out[i] += in[i];
}

template <template <typename> class Vec, typename T> inline void Add(Vec<T>& out, const Vec<T>& in)
{
	Add<T, Vec<T>::num>(&out[0], &in[0])
}

template <
	template <typename> class VecA,
	template <typename> class VecB,
	typename T >
inline void Add(VecA<T>& out, const VecB<T>& in)
{
	Add<
		T,
		VecA<T>::num < VecA<T>::num ?
			VecA<T>::num :
			VecB<T>::num >
	(
		&out[0], &in[0]
	)
}

/* --- "Sub" template --- */

template <typename T, size_t num> inline void Sub(T* out, const T* in)
{
	for (size_t i = 0; i < num; ++i)
		out[i] -= in[i];
}

template <template <typename> class Vec, typename T> inline void Sub(Vec<T>& out, const Vec<T>& in)
{
	Sub<T, Vec<T>::num>(&out[0], &in[0])
}

template <
	template <typename> class VecA,
	template <typename> class VecB,
	typename T >
inline void Sub(VecA<T>& out, const VecB<T>& in)
{
	Sub<
		T,
		VecA<T>::num < VecA<T>::num ?
			VecA<T>::num :
			VecB<T>::num >
	(
		&out[0], &in[0]
	)
}

/* --- "Mul" template --- */

template <typename T, size_t num> inline void Mul(T* out, const T* in)
{
	for (size_t i = 0; i < num; ++i)
		out[i] *= in[i];
}

template <template <typename> class Vec, typename T> inline void Mul(Vec<T>& out, const Vec<T>& in)
{
	Mul<T, Vec<T>::num>(&out[0], &in[0])
}

template <
	template <typename> class VecA,
	template <typename> class VecB,
	typename T >
inline void Mul(VecA<T>& out, const VecB<T>& in)
{
	Mul<
		T,
		VecA<T>::num < VecA<T>::num ?
			VecA<T>::num :
			VecB<T>::num >
	(
		&out[0], &in[0]
	)
}

/* --- "Div" template --- */

template <typename T, size_t num> inline void Div(T* out, const T* in)
{
	for (size_t i = 0; i < num; ++i)
		out[i] /= in[i];
}

template <template <typename> class Vec, typename T> inline void Div(Vec<T>& out, const Vec<T>& in)
{
	Div<T, Vec<T>::num>(&out[0], &in[0])
}

template <
	template <typename> class VecA,
	template <typename> class VecB,
	typename T >
inline void Div(VecA<T>& out, const VecB<T>& in)
{
	Div<
		T,
		VecA<T>::num < VecA<T>::num ?
			VecA<T>::num :
			VecB<T>::num >
	(
		&out[0], &in[0]
	)
}


}

#endif
