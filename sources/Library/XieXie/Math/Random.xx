/*
 * Random package
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

once


include "RandomGenerator.xx"


package Math.Random
{


/* --- Integral random numbers --- */

int RandInt(RandomGenerator& generator) {
	return generator.Generate()
}

int RandInt(RandomGenerator& generator, int max) {
	if max < 0 {
		return -(RandInt(generator, max) % (1 - max))
	}
	return RandInt(generator) % (1 + max)
}

int RandInt(RandomGenerator& generator, int min, int max) {
	return min + RandInt(max - min)
}

/* --- Boolean random numbers --- */

bool RandBool(RandomGenerator& generator, int propability) {
	return RandInt(generator, propability) = 0
}

/* --- Real random numbers --- */

float RandFloat(RandomGenerator& generator) {
	return (float)RandInt(generator) / (float)generator.Max()
}

float RandFloat(RandomGenerator& generator, float max) {
	return RandFloat(generator) * max
}

float RandFloat(RandomGenerator& generator, float min, float max) {
	return min + RandFloat(generator) * (max - min)
}


} // /package Math.Random
