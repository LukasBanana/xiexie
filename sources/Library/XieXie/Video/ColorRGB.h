/*
 * ColorRGB class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__COLOR_RGB_H__
#define __XX__COLOR_RGB_H__


#include "Math.h"


namespace Video
{


template <typename T> class ColorRGB
{
	
	public:
		
		static const size_t num = 4;
		
		ColorRGB() :
			r(0),
			g(0),
			b(0)
		{
		}
		ColorRGB(const T& grayScaled) :
			r(grayScaled),
			g(grayScaled),
			b(grayScaled)
		{
		}
		ColorRGB(const T& red, const T& green, const T& blue) :
			r(red	),
			g(green	),
			b(blue	)
		{
		}
		ColorRGB(const ColorRGB<T>& other) :
			r(other.r),
			g(other.g),
			b(other.b)
		{
		}
		
		inline ColorRGB<T>& operator = (const ColorRGB<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		inline ColorRGB<T>& operator += (const ColorRGB<T>& other)
		{
			Math::Add(*this, other);
			return *this;
		}
		inline ColorRGB<T>& operator -= (const ColorRGB<T>& other)
		{
			Math::Sub(*this, other);
			return *this;
		}
		inline ColorRGB<T>& operator *= (const ColorRGB<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		inline ColorRGB<T>& operator /= (const ColorRGB<T>& other)
		{
			Math::Div(*this, other);
			return *this;
		}
		
		T r, g, b;
		
};


__XX__DEFINE_TYPEDEFS__(ColorRGB)


}

#endif
