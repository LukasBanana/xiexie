/*
 * ColorRGBA class
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__COLOR_RGBA_H__
#define __XX__COLOR_RGBA_H__


#include "Math.h"


namespace Video
{


template <typename T> class ColorRGBA
{
	
	public:
		
		static const size_t num = 4;
		
		ColorRGBA() :
			r(0),
			g(0),
			b(0),
			a(1)
		{
		}
		ColorRGBA(const T& grayScaled, const T& alpha = T(1)) :
			r(grayScaled),
			g(grayScaled),
			b(grayScaled),
			a(alpha		)
		{
		}
		ColorRGBA(const T& red, const T& green, const T& blue, const T& alpha = T(1)) :
			r(red	),
			g(green	),
			b(blue	),
			a(alpha	)
		{
		}
		ColorRGBA(const ColorRGBA<T>& other) :
			r(other.r),
			g(other.g),
			b(other.b),
			a(other.a)
		{
		}
		
		inline ColorRGBA<T>& operator = (const ColorRGBA<T>& other)
		{
			Math::Set(*this, other);
			return *this;
		}
		inline ColorRGBA<T>& operator += (const ColorRGBA<T>& other)
		{
			Math::Add(*this, other);
			return *this;
		}
		inline ColorRGBA<T>& operator -= (const ColorRGBA<T>& other)
		{
			Math::Sub(*this, other);
			return *this;
		}
		inline ColorRGBA<T>& operator *= (const ColorRGBA<T>& other)
		{
			Math::Mul(*this, other);
			return *this;
		}
		inline ColorRGBA<T>& operator /= (const ColorRGBA<T>& other)
		{
			Math::Div(*this, other);
			return *this;
		}
		
		T r, g, b, a;
		
};


__XX__DEFINE_TYPEDEFS__(ColorRGBA)


}

#endif
