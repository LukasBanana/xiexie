/*
 * String core file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__STRING_CORE__H__
#define __XX__STRING_CORE__H__


#include <string>
#include <sstream>


namespace Lang
{


/* --- String helper functions --- */

template <typename T> inline std::string __XX__ToStringTmpl(const T& val)
{
	std::stringstream sstr;
	sstr << val;
	return sstr.str();
}

#define __XX__DEFINE_TOSTRING_PROC__(t)			\
	inline std::string __XX__Str(t val)	\
	{											\
		return __XX__ToStringTmpl(val);			\
	}

__XX__DEFINE_TOSTRING_PROC__(char)
__XX__DEFINE_TOSTRING_PROC__(unsigned char)
__XX__DEFINE_TOSTRING_PROC__(short)
__XX__DEFINE_TOSTRING_PROC__(unsigned short)
__XX__DEFINE_TOSTRING_PROC__(int)
__XX__DEFINE_TOSTRING_PROC__(unsigned int)
__XX__DEFINE_TOSTRING_PROC__(long long int)
__XX__DEFINE_TOSTRING_PROC__(unsigned long long int)

__XX__DEFINE_TOSTRING_PROC__(float)
__XX__DEFINE_TOSTRING_PROC__(double)
__XX__DEFINE_TOSTRING_PROC__(long double)

#undef __XX__DEFINE_TOSTRING_PROC__

inline std::string __XX__Str(bool val)
{
	return val ? "true" : "false";
}

inline std::string __XX__Str(const std::string& val)
{
	return val;
}

inline std::string __XX__Str(const char* val)
{
	return std::string(val);
}


/* --- String external standard procedures --- */

template <typename T> inline size_t __XX__StrSub(const std::basic_string<T>& str, const size_t pos, const size_t len)
{
	if (pos >= str.size())
		throw ArrayIndexOutOfBoundsException(str, pos);
	else if (pos + len > str.size())
		throw ArrayIndexOutOfBoundsException(str, len);
	return str.substr(pos, len);
}

template <typename T> inline size_t __XX__StrLeft(const std::basic_string<T>& str, const size_t len)
{
	if (len > str.size())
		throw ArrayIndexOutOfBoundsException(str, len);
	return str.substr(0, len);
}

template <typename T> inline size_t __XX__StrRight(const std::basic_string<T>& str, const size_t len)
{
	if (len > str.size())
		throw ArrayIndexOutOfBoundsException(str, len);
	return str.substr(str.size() - len, len);
}

template <typename T> inline std::basic_string<T> __XX__StrLower(const std::basic_string<T>& str)
{
	std::basic_string<T> modifiedStr(str);
	for (auto& chr : modifiedStr)
	{
		if (chr >= 'A' && chr <= 'Z')
			chr += 'a' - 'A';
	}
	return modifiedStr;
}

template <typename T> inline std::basic_string<T> __XX__StrUpper(const std::basic_string<T>& str)
{
	std::basic_string<T> modifiedStr(str);
	for (auto& chr : modifiedStr)
	{
		if (chr >= 'a' && chr <= 'z')
			chr -= 'a' - 'A';
	}
	return modifiedStr;
}

template <typename T> inline T& __XX__Str_GetCharSafe(std::basic_string<T>& str, const size_t index)
{
	if (index >= str.size())
		throw ArrayIndexOutOfBoundsException(str, index);
	return str[index];
}

template <typename T> inline const T& __XX__Str_GetConstCharSafe(const std::basic_string<T>& str, const size_t index)
{
	if (index >= str.size())
		throw ArrayIndexOutOfBoundsException(str, index);
	return str[index];
}


} // /namespace Lang


#endif
