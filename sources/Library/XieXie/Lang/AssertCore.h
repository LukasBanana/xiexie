/*
 * Assertion core file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__ASSERT_CORE__H__
#define __XX__ASSERT_CORE__H__


#include <assert.h>
#include <iostream>


/* --- Assertion macros --- */

#define __XX__ASSERT(e) assert(e)

#define __XX__ASSERT_INFO(e, s)			\
	if (!(e))							\
	{									\
		std::cerr << s << std::endl;	\
		assert(e);						\
	}


#endif
