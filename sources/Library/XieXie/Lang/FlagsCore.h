/*
 * Flags core file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__FLAGS_CORE__H__
#define __XX__FLAGS_CORE__H__


/* --- Flags helper macros --- */

#define __XX__DEFINE_FLAGS_FUNCTIONS__(n)						\
	inline n() :												\
		__XX__BitField(0u)										\
	{															\
	}															\
	inline n(const __XX__Enum& flag) :							\
		__XX__BitField(flag)									\
	{															\
	}															\
																\
	inline operator __XX__DataType () const						\
	{															\
		return __XX__BitField;									\
	}															\
																\
	inline n& operator |= (const n& other)						\
	{															\
		__XX__BitField |= other.__XX__BitField;				 	\
		return *this;											\
	}															\
	inline n& operator &= (const n& other)						\
	{															\
		__XX__BitField &= other.__XX__BitField;				 	\
		return *this;											\
	}															\
	inline n& operator ^= (const n& other)						\
	{															\
		__XX__BitField ^= other.__XX__BitField;				 	\
		return *this;											\
	}															\
																\
	inline void Add(const n& other)								\
	{															\
		__XX__BitField |= other.__XX__BitField;					\
	}															\
	inline void Remove(const n& other)							\
	{															\
		__XX__BitField &= (~(other.__XX__BitField));			\
	}															\
	inline bool Has(const n& other) const						\
	{															\
		return (__XX__BitField & other.__XX__BitField) != 0;	\
	}

#define __XX__DEFINE_FLAGS_OPERATORS__(n)				\
	inline n operator | (const n& left, const n& right)	\
	{													\
		n result(left);									\
		result |= right;								\
		return result;									\
	}													\
	inline n operator & (const n& left, const n& right)	\
	{													\
		n result(left);									\
		result &= right;								\
		return result;									\
	}													\
	inline n operator ^ (const n& left, const n& right)	\
	{													\
		n result(left);									\
		result ^= right;								\
		return result;									\
	}


#endif
