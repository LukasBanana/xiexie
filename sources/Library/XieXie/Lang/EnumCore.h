/*
 * Enum core file
 * 
 * This file is part of the "XieXie-Compiler" (Copyright (c) 2013 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XX__ENUM_CORE__H__
#define __XX__ENUM_CORE__H__


/* --- Enum helper macros --- */

#define __XX__DEFINE_ENUM_DEFAULT_MEMBERS__(n)		\
	n() :											\
		__XX__Entry(__XX__Uninitialized)			\
	{												\
	}												\
	n(const __XX__Enum& entry) :					\
		__XX__Entry(entry)							\
	{												\
	}												\
	inline operator __XX__Enum () const				\
	{												\
		return __XX__Entry;							\
	}												\
	inline void Set(const __XX__Enum& entry)		\
	{												\
		__XX__Entry = entry;						\
	}												\
	inline bool Is(const __XX__Enum& entry) const	\
	{												\
		return __XX__Entry == entry;				\
	}												\
	__XX__Enum __XX__Entry;


#endif
