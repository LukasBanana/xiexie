
class NameVisibility
{
	
	public:
		
		NameVisibility() :
			Parent_(nullptr)
		{
		}
		virtual ~NameVisibility()
		{
		}
		
		virtual bool IsTemporal() const = 0;
		
		virtual bool Decorate(FNameIdentPtr FName) const = 0;
		
	protected:
		
		bool DecorateWithObject(FNameIdentPtr FName, Object* Object)
		{
			/* Decorate AST */
			FName->Link = Object;
			
			if (FName->Next != nullptr)
			{
				/* If further name identifiers follow, return with error -> this is not a namespace */
				return false;
			}
			
			return true;
		}
		
		NameVisibility* Parent_;
		
		std::map<std::string, ObjectPtr> ObjectTable_;
		std::map<std::string, NamespacePtr> NamespaceTable_;
		
		//std::list<ScopePtr> ScopeList_;
		
};


class Scope : public NameVisibility
{
	
	public:
		
		bool IsTemporal() const
		{
			return true;
		}
		
		//! Scopes can only search in bottom-up order.
		bool Decorate(FNameIdentPtr FName) const;
		
};


class Namespace : public NameVisibility
{
	
	public:
		
		bool IsTemporal() const
		{
			return false;
		}
		
		//! Namespaces first search in bottom-up order, and then in top-down order.
		bool Decorate(FNameIdentPtr FName) const;
		
		inline ObjectPtr GetObject() const
		{
			return Object_;
		}
		
		inline std::string Name() const
		{
			return Object_ != nullptr ? Object_->Name() : "";
		}

	private:
		
		ObjectPtr DecorateBottomUp(FNameIdentPtr FName) const;
		ObjectPtr DecorateTopDown(FNameIdentPtr FName) const;
		
		ObjectPtr Object_; //!< The object which denotes this namespace (class or package).
		
};

//! Scopes can only search in bottom-up order.
bool Scope::Decorate(FNameIdentPtr FName) const
{
	/* Get spelling from Fname identifier */
	const auto& Spell = FName->Ident->Spell;
	
	/* First search in this scope's namespace-table */
	auto itName = NamespaceTable_.find(Spell);
	
	if (itName != NamespaceTable_.end())
	{
		auto Namespace = itName->second;
		
		/* If further name identifiers follow, search in this namespace */
		if (FName->Next != nullptr)
			return Namespace->Decorate(FName->Next);
		
		/* Otherwise, return the namespace's object */
		return Namespace->GetObject();
	}
	
	/* Then search in this scope's object-table */
	auto itObj = ObjectTable_.find(Spell);
	
	if (itObj != ObjectTable_.end())
	{
		/* If found, return the object */
		return DecorateWithObject(FName, itObj->second.get());
	}
	
	/* Otherwise, search in upper name-visibility */
	if (Parent_ != nullptr)
		return Parent_->Decorate(FName);
	
	/* If there is no upper name-visibility -> FName not found! */
	return false;
}


//! Namespaces first search in bottom-up order, and then in top-down order.
bool Namespace::Decorate(FNameIdentPtr FName) const
{
	return DecorateBottomUp(FName);
}

bool Namespace::DecorateBottomUp(FNameIdentPtr FName) const
{
	/* Get spelling from Fname identifier */
	const auto& Spell = FName->Ident->Spell;
	
	/* Check if the first identifier names this namespace */
	if (Spell == Name())
	{
		/* Try to decorate FName with this namespace */
		if (DecorateTopDown(FName))
		{
			/* Decorated FName completely -> return with success */
			return true;
		}
		else
		{
			/* If failed, continue searching in upper name-visibility */
			if (Parent_ != nullptr)
				return Parent_->Decorate(FName);
			
			/* FName could not be decorated completely -> return without success */
			return false;
		}
	}
	
	/* Then search in this namespace's object-table */
	auto itObj = ObjectTable_.find(Spell);
	
	if (itObj != ObjectTable_.end())
	{
		/* If found, return the object */
		return DecorateWithObject(FName, itObj->second.get());
	}
	
	/* Then search in the upper name-visibility */
	if (Parent_ != nullptr)
		return Parent_->Decorate(FName);
	
	/* No object found with specified name */
	return nullptr;
}

bool Namespace::DecorateTopDown(FNameIdentPtr FName) const
{
	if (FName == nullptr)
	{
		/* Return without success on invalid input */
		return false;
	}
	
	/* Decorate AST */
	FName->Link = GetObject().get();
	
	if (FName->Next == nullptr)
		return true;
	
	/*
	Search in for sub-namespace for next identifier.
	-> don't search in scopes, they are invisible from here.
	*/
	FName = FName->Next;
	
	auto itName = NamespaceTable_.find(FName->Ident->Spell);
	
	if (itName != NamespaceTable_.end())
	{
		/* Decorate AST with sub-namespace */
		return itName->second->DecorateTopDown(FName);
	}
	
	/* Named sub-namespace not found */
	return false;
}


