; D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\HelloWorld/HelloWorld.xx.xbc.asm
; XieXie generated source file
; Thu Jan 16 23:41:06 2014

; --- Function definition "TestFunction"  ---
TestFunction:
    ; Variable initialization "pi"
    push 3.141592654
    pop i0
    mov [0x0001e240], i0
    
    ; <Object expression "x">
    ; </Object expression "x">
    ; <Object expression "pi">
    ; </Object expression "pi">
    ; <Object expression "y">
    ; </Object expression "y">
    pop i1
    pop i0
    mul i0, i1
    push i0
    pop i1
    pop i0
    add i0, i1
    push i0
    ret

; --- Function definition "Main"  ---
Main:
    ; Variable initialization "x"
    xor [0x0001e240], [0x0001e240]
    
    ; Variable initialization "a"
    push 7
    mov i0, 3
    mov i1, 2
    mul i0, i1
    push i0
    push 5
    mov i0, 6
    mov i1, 3
    add i0, i1
    push i0
    pop i1
    pop i0
    mul i0, i1
    push i0
    pop i1
    pop i0
    sub i0, i1
    push i0
    ; <Object expression "x">
    ; </Object expression "x">
    pop i0
    mov i1, 4
    sub i0, i1
    push i0
    pop i1
    pop i0
    add i0, i1
    push i0
    pop i1
    pop i0
    add i0, i1
    push i0
    pop i0
    mov [0x0001e240], i0
    
    ; Variable initialization "b"
    xor [0x0001e240], [0x0001e240]
    
    ; Variable initialization "c"
    xor [0x0001e240], [0x0001e240]
    
    ; TODO -> push parameters onto stack ...
    call IO.Console.PrintNL
    push 0
    ret

; ================
