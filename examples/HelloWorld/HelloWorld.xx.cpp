// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\HelloWorld/HelloWorld.xx.cpp
// XieXie generated source file
// Thu Jan 16 23:25:31 2014

#ifndef __XX__D__SOFTWAREENTWICKLUNG_C___HLC_TOOLS_XIEXIE_COMPILER_TRUNK_EXAMPLES_HELLOWORLD_HELLOWORLD_XX_CPP__H__
#define __XX__D__SOFTWAREENTWICKLUNG_C___HLC_TOOLS_XIEXIE_COMPILER_TRUNK_EXAMPLES_HELLOWORLD_HELLOWORLD_XX_CPP__H__

#include <XieXie/IO/Console.xx>
#include <XieXie/Lang/StringCore.h>
#include <memory>

;
namespace IO
{
namespace Console
{
//! StateFlags class.
class StateFlags
{
    public:
        std::string indent;
        std::string indentModifier;
        StateFlags() :
            indentModifier("  ")
        {
        }
        
        virtual ~StateFlags()
        {
        }
        
};

StateFlags state;
//! Print procecure.
//! \param[in] message Input parameter message.
void Print(std::string const message)
{
    // < Inline C++ Code >
 std::cout << message; 
    // < /Inline C++ Code >
}

//! PrintIndent procecure.
void PrintIndent()
{
    // < Inline C++ Code >
 std::cout << state.indent; 
    // < /Inline C++ Code >
}

//! PrintNL procecure.
//! \param[in] message Input parameter message.
void PrintNL(std::string const message)
{
    // < Inline C++ Code >
 std::cout << state.indent << message << std::endl; 
    // < /Inline C++ Code >
}

//! Input procecure.
//! \return string.
std::string Input()
{
    std::string in;
    // < Inline C++ Code >
 std::cin >> in; 
    // < /Inline C++ Code >
    return in;
}

} // /namespace IO

} // /namespace Console

using namespace Console;
using namespace Test.Test2;
//! TestFunction procecure.
//! \param[in] x Input parameter x.
//! \param[in] y Input parameter y.
//! \return int.
int TestFunction(int x, float y)
{
    long double const pi = static_cast< long double const >(3.141592654);
    return static_cast< int >((static_cast< long double >(x) + pi * static_cast< long double >(y)));
}

//! File class.
class File
{
    public:
        //! ReadUByte procecure.
        //! \return ubyte.
        unsigned char ReadUByte()
        {
            return 0;
        }
        
        virtual ~File()
        {
        }
        
};

//! Math class.
class Math
{
    public:
        //! Transform3Df class.
        class Transform3Df
        {
            public:
                virtual ~Transform3Df()
                {
                }
                
        };
        
        virtual ~Math()
        {
        }
        
};

//! Model class.
class Model
{
    public:
        //! Load procecure.
        //! \param[in] filename Input parameter filename.
        //! \return bool.
        bool Load(std::string const filename)
        {
            std::shared_ptr< File > file = std::make_shared< File >(filename);
            int x = 0;
            // While Loop
            while (x < 5)
            {
                unsigned char b = (*file).ReadUByte();
                x += 1;
            }
            
            return x > 5;
        }
        
        virtual ~Model()
        {
        }
        
    private:
        Math::Transform3Df transform;
    public:
        //! Setter for "transform" member variable.
        //! \param[in] transform Specifies the new value.
        //! \see transform
        inline void SetTransform(const Math::Transform3Df& transform)
        {
            this->transform = transform;
        }
        //! Getter for "transform" member variable.
        //! \see transform
        //! \return Current value.
        inline const Math::Transform3Df& GetTransform() const
        {
            return transform;
        }
        
};

//! Player class.
class Player
{
    public:
        Player(Gender const gender) :
            age(0u),
            attrib(Attributes::Jobless)
        {
            (*this).gender = gender;
            (*this).age = 0;
        }
        
        Player(Gender const gender, std::string const fname, std::string const lname, unsigned char const age) :
            age(0u),
            attrib(Attributes::Jobless)
        {
            (*this).gender = gender;
            (*this).fname = fname;
            (*this).lname = lname;
            (*this).age = age;
            model = Model::Load("Test/PlayerModel.3ds");
            if (age >= 21)
            {
                (*this).attrib ^= Attributes::Jobless;
            }
        }
        
        //! Gender enumeration.
        enum class Gender
        {
            Male,
            Female,
        };
        
        //! Attributes flags enumeration.
        struct Attributes
        {
            // --- Auto-Generated Flags --- //
            typedef unsigned char DataType;
            static const DataType None      = 0;
            static const DataType All       = ~0;
            // --- Custom Flags --- //
            static const DataType Rich      = (1 << 0);
            static const DataType Beautiful = (1 << 1);
            static const DataType Jobless   = (1 << 2);
        };
        
        virtual ~Player()
        {
        }
        
    private:
        std::string fname, lname;
        unsigned char age;
        Gender gender;
        Attributes::DataType attrib;
        Model model;
    public:
        //! Setter for "fname" member variable.
        //! \param[in] fname Specifies the new value.
        //! \see fname
        inline void SetFname(const std::string& fname)
        {
            this->fname = fname;
        }
        //! Getter for "fname" member variable.
        //! \see fname
        //! \return Current value.
        inline const std::string& GetFname() const
        {
            return fname;
        }
        
        //! Setter for "lname" member variable.
        //! \param[in] lname Specifies the new value.
        //! \see lname
        inline void SetLname(const std::string& lname)
        {
            this->lname = lname;
        }
        //! Getter for "lname" member variable.
        //! \see lname
        //! \return Current value.
        inline const std::string& GetLname() const
        {
            return lname;
        }
        
        //! Getter for "age" member variable.
        //! \see age
        //! \return Current value.
        inline const unsigned char& GetAge() const
        {
            return age;
        }
        
        //! Getter for "gender" member variable.
        //! \see gender
        //! \return Current value.
        inline const Gender& GetGender() const
        {
            return gender;
        }
        
        //! Setter for "attrib" member variable.
        //! \param[in] attrib Specifies the new value.
        //! \see attrib
        inline void SetAttrib(const Attributes::DataType& attrib)
        {
            this->attrib = attrib;
        }
        //! Getter for "attrib" member variable.
        //! \see attrib
        //! \return Current value.
        inline const Attributes::DataType& GetAttrib() const
        {
            return attrib;
        }
        
};

//! Main procecure.
//! \return int.
int main()
{
    //! TestEnum enumeration.
    enum class TestEnum
    {
        EntryA,
        EntryB,
        EntryC,
    };
    
    //! ColorFlags flags enumeration.
    struct ColorFlags
    {
        // --- Auto-Generated Flags --- //
        typedef unsigned char DataType;
        static const DataType None  = 0;
        static const DataType All   = ~0;
        // --- Custom Flags --- //
        static const DataType Red   = (1 << 0);
        static const DataType Green = (1 << 1);
        static const DataType Blue  = (1 << 2);
    };
    
    //! WindowFlags flags enumeration.
    struct WindowFlags
    {
        // --- Auto-Generated Flags --- //
        typedef unsigned short int DataType;
        static const DataType None        = 0;
        static const DataType All         = ~0;
        // --- Custom Flags --- //
        static const DataType Visible     = (1 <<  0);
        static const DataType Hidden      = (1 <<  1);
        static const DataType Border      = (1 <<  2);
        static const DataType Caption     = (1 <<  3);
        static const DataType ResizeBox   = (1 <<  4);
        static const DataType MinimizeBox = (1 <<  5);
        static const DataType MaximizeBox = (1 <<  6);
        static const DataType Popup       = (1 <<  7);
        static const DataType Overlap     = (1 <<  8);
        static const DataType SysMenu     = (1 <<  9);
        static const DataType Disabled    = (1 << 10);
        static const DataType OwnerDraw   = (1 << 11);
    };
    
    int x = 0;
    int a = 7 + 3 * 2 - 5 * (6 + 3) + x - 4;
    int b = 0;
    float c = 0.0f;
    IO::Console::PrintNL("Hello World,\nThis is the first \"XieXie\" program!\nlol");
    return 0;
}

#endif
// ================
