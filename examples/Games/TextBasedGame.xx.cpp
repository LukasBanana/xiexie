// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Games/TextBasedGame.xx.cpp
// XieXie generated source file
// Mon Jan 20 23:03:53 2014

#ifndef __XX__D__SOFTWAREENTWICKLUNG_C___HLC_TOOLS_XIEXIE_COMPILER_TRUNK_EXAMPLES_GAMES_TEXTBASEDGAME_XX_CPP__H__
#define __XX__D__SOFTWAREENTWICKLUNG_C___HLC_TOOLS_XIEXIE_COMPILER_TRUNK_EXAMPLES_GAMES_TEXTBASEDGAME_XX_CPP__H__

#include <XieXie/Lang/StringCore.h>
#include <memory>

;
namespace IO
{
namespace Console
{
//! StateFlags class.
class StateFlags
{
    public:
        std::string indent;
        std::string indentModifier;
        StateFlags() :
            indentModifier("  ")
        {
        }
        
        virtual ~StateFlags()
        {
        }
        
};

StateFlags state;
//! ColorFlags flags enumeration.
struct ColorFlags
{
    // --- Auto-Generated Flags --- //
    typedef unsigned char DataType;
    static const DataType None   = 0;
    static const DataType All    = ~0;
    // --- Custom Flags --- //
    static const DataType Red    = (1 << 0);
    static const DataType Green  = (1 << 1);
    static const DataType Blue   = (1 << 2);
    static const DataType Intens = (1 << 3);
};

//! Color class.
class Color
{
    public:
        ColorFlags::DataType front, back;
        Color() :
            front(0u),
            back(0u)
        {
        }
        
        virtual ~Color()
        {
        }
        
};

//! UpperIndent procecure.
void UpperIndent()
{
}

//! LowerIndent procecure.
void LowerIndent()
{
}

//! PushColorAttrib procecure.
void PushColorAttrib()
{
}

//! PopColorAttrib procecure.
void PopColorAttrib()
{
}

//! SetColor procecure.
//! \param[in] front Input parameter front.
void SetColor(ColorFlags::DataType const& front)
{
}

//! GetColor procecure.
//! \return <custom>.
Color GetColor()
{
    Color color;
    return color;
}

//! Print procecure.
//! \param[in] message Input parameter message.
void Print(std::string const message)
{
    // < Inline C++ Code >
 std::cout << message; 
    // < /Inline C++ Code >
}

//! PrintIndent procecure.
void PrintIndent()
{
    // < Inline C++ Code >
 std::cout << state.indent; 
    // < /Inline C++ Code >
}

//! PrintNL procecure.
//! \param[in] message Input parameter message.
void PrintNL(std::string const message)
{
    // < Inline C++ Code >
 std::cout << state.indent << message << std::endl; 
    // < /Inline C++ Code >
}

//! Input procecure.
//! \return string.
std::string Input()
{
    std::string in;
    // < Inline C++ Code >
 std::cin >> in; 
    // < /Inline C++ Code >
    return in;
}

//! ScopedIndent class.
class ScopedIndent
{
    public:
        ScopedIndent()
        {
            UpperIndent();
        }
        
        virtual ~ScopedIndent()
        {
            LowerIndent();
        }
        
};

//! ScopedColor class.
class ScopedColor
{
    public:
        ScopedColor(ColorFlags::DataType const& front)
        {
            PushColorAttrib();
        }
        
        ScopedColor(ColorFlags::DataType const& front, ColorFlags::DataType const& back)
        {
            PushColorAttrib();
        }
        
        virtual ~ScopedColor()
        {
            PopColorAttrib();
        }
        
};

} // /namespace IO

} // /namespace Console

//! PrintNL procecure.
//! \param[in] text Input parameter text.
void PrintNL(std::string const text)
{
    IO::Console::PrintNL(text);
}

//! Input procecure.
//! \return string.
std::string Input()
{
    return IO::Console::Input();
}

//! Player class.
class Player
{
    public:
        typedef unsigned char AgeDataType;
        Player(std::string const name, AgeDataType const age)
        {
            (*this).name = name;
            (*this).age = age;
        }
        
        std::string name;
        AgeDataType age;
        virtual ~Player()
        {
        }
        
};

//! StrToInt procecure.
//! \param[in] str Input parameter str.
//! \return int.
int StrToInt(std::string const str)
{
    int val = 0;
    // < Inline C++ Code >
 val = atoi(str.c_str()); 
    // < /Inline C++ Code >
    return val;
}

std::shared_ptr< Player > player = nullptr;
//! Main procecure.
//! \return int.
int main()
{
    PrintNL("First XieXie text based video game");
    PrintNL("----------- 20/01/2014 -----------");
    PrintNL("");
    PrintNL("Enter your name:");
    std::string name = Input();
    std::string age = Input();
    player = std::make_shared< Player >(name, static_cast< Player::AgeDataType >(StrToInt(age)));
    return 0;
}

#endif
// ================
