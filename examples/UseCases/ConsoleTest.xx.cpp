// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\UseCases/ConsoleTest.xx.cpp
// XieXie generated source file
// Sat Feb 22 21:32:14 2014

#ifndef __XX__D__SOFTWAREENTWICKLUNG_C___HLC_TOOLS_XIEXIE_COMPILER_TRUNK_EXAMPLES_USECASES_CONSOLETEST_XX_CPP__H__
#define __XX__D__SOFTWAREENTWICKLUNG_C___HLC_TOOLS_XIEXIE_COMPILER_TRUNK_EXAMPLES_USECASES_CONSOLETEST_XX_CPP__H__

#include <XieXie/Lang/EnumCore.h>
#include <XieXie/Lang/FlagsCore.h>
#include <XieXie/Lang/StringCore.h>
#include <string>

;
// < Inline C++ Code >
 #include <iostream> 
// < /Inline C++ Code >
namespace IO
{
namespace Console
{
//! StateFlags class.
class StateFlags
{
    public:
        std::string indent;
        std::string indentModifier;
        StateFlags() :
            indentModifier("  ")
        {
        }
        
        virtual ~StateFlags()
        {
        }
        
};

StateFlags state;
//! ColorFlags flags enumeration.
struct ColorFlags
{
    typedef unsigned char __XX__DataType;
    enum __XX__Enum : __XX__DataType
    {
        None   = 0,
        All    = 0x0f,
        Red    = (1 << 0),
        Green  = (1 << 1),
        Blue   = (1 << 2),
        Intens = (1 << 3),
    };
    __XX__DEFINE_FLAGS_FUNCTIONS__(ColorFlags)
    private:
    __XX__DataType __XX__BitField;
};
__XX__DEFINE_FLAGS_OPERATORS__(ColorFlags)

//! Color class.
class Color
{
    public:
        ColorFlags front, back;
        virtual ~Color()
        {
        }
        
};

//! UpperIndent procedure.
void UpperIndent()
{
    state.indent += state.indentModifier;
}

//! LowerIndent procedure.
void LowerIndent()
{
    if (state.indent.size() > state.indentModifier.size())
    {
        state.indent.resize(state.indent.size() - state.indentModifier.size());
    }
    else
    {
        state.indent.clear();
    }
}

//! PushColorAttrib procedure.
void PushColorAttrib()
{
}

//! PopColorAttrib procedure.
void PopColorAttrib()
{
}

//! SetColor procedure.
//! \param[in] front Input parameter front.
void SetColor(ColorFlags const& front)
{
}

//! GetColor procedure.
//! \return <custom>.
Color GetColor()
{
    Color color;
    return color;
}

//! Print procedure.
//! \param[in] message Input parameter message.
void Print(std::string const message)
{
    // < Inline C++ Code >
 std::cout << message; 
    // < /Inline C++ Code >
}

//! PrintIndent procedure.
void PrintIndent()
{
    // < Inline C++ Code >
 std::cout << state.indent; 
    // < /Inline C++ Code >
}

//! PrintNL procedure.
//! \param[in] message Input parameter message.
void PrintNL(std::string const message)
{
    // < Inline C++ Code >
 std::cout << state.indent << message << std::endl; 
    // < /Inline C++ Code >
}

//! Input procedure.
//! \return string.
std::string Input()
{
    std::string in;
    // < Inline C++ Code >
 std::getline(std::cin, in); 
    // < /Inline C++ Code >
    return in;
}

//! SubInput procedure.
//! \return string.
std::string SubInput()
{
    std::string in;
    // < Inline C++ Code >
 std::cin >> in; 
    // < /Inline C++ Code >
    return in;
}

//! Wait procedure.
void Wait()
{
    // < Inline C++ Code >
 system("pause"); 
    // < /Inline C++ Code >
}

//! ScopedIndent class.
class ScopedIndent
{
    public:
        ScopedIndent()
        {
            UpperIndent();
        }
        
        virtual ~ScopedIndent()
        {
            LowerIndent();
        }
        
};

//! ScopedColor class.
class ScopedColor
{
    public:
        ScopedColor(ColorFlags const& front)
        {
            PushColorAttrib();
        }
        
        ScopedColor(ColorFlags const& front, ColorFlags const& back)
        {
            PushColorAttrib();
        }
        
        virtual ~ScopedColor()
        {
            PopColorAttrib();
        }
        
};

} // /namespace IO

} // /namespace Console

;
namespace Lang
{
//! SysInfo class.
class SysInfo
{
    public:
        //! Family enumeration.
        struct Family
        {
            enum __XX__Enum
            {
                Unknown,
                Windows,
                Posix,
                OSX,
                __XX__Uninitialized
            };
            static inline size_t Num()
            {
                return 4;
            }
            std::string Str() const
            {
                switch (__XX__Entry)
                {
                    case Unknown: return "Unknown";
                    case Windows: return "Windows";
                    case Posix:   return "Posix";
                    case OSX:     return "OSX";
                }
                return "";
            }
            __XX__DEFINE_ENUM_DEFAULT_MEMBERS__(Family)
        };
        
        //! Id enumeration.
        struct Id
        {
            enum __XX__Enum
            {
                Unknown,
                Windows,
                Windows95,
                Windows98,
                Windows2000,
                WindowsNT,
                WindowsXP,
                WindowsVista,
                Windows7,
                Windows8,
                Windows8_1,
                Unix,
                Linux,
                FreeBSD,
                NetBSD,
                OpenBSD,
                OSX,
                MacOSXCheetah,
                MacOSXPuma,
                MacOSXJaguar,
                MacOSXPanther,
                MacOSXTiger,
                MacOSXLeopard,
                MacOSXSnowLeopard,
                MacOSXLion,
                OSXMountainLion,
                OSXMavericks,
                __XX__Uninitialized
            };
            static inline size_t Num()
            {
                return 27;
            }
            std::string Str() const
            {
                switch (__XX__Entry)
                {
                    case Unknown:           return "Unknown";
                    case Windows:           return "Windows";
                    case Windows95:         return "Windows 95";
                    case Windows98:         return "Windows 98";
                    case Windows2000:       return "Windows 2000";
                    case WindowsNT:         return "Windows NT";
                    case WindowsXP:         return "Windows XP";
                    case WindowsVista:      return "Windows Vista";
                    case Windows7:          return "Windows 7";
                    case Windows8:          return "Windows 8";
                    case Windows8_1:        return "Windows 8.1";
                    case Unix:              return "Unix";
                    case Linux:             return "Linux";
                    case FreeBSD:           return "FreeBSD";
                    case NetBSD:            return "NetBSD";
                    case OpenBSD:           return "OpenBSD";
                    case OSX:               return "OSX";
                    case MacOSXCheetah:     return "Mac OS X Cheetah";
                    case MacOSXPuma:        return "Mac OS X Puma";
                    case MacOSXJaguar:      return "Mac OS X Jaguar";
                    case MacOSXPanther:     return "Mac OS X Panther";
                    case MacOSXTiger:       return "Mac OS X Tiger";
                    case MacOSXLeopard:     return "Mac OS X Leopard";
                    case MacOSXSnowLeopard: return "Mac OS X Snow Leopard";
                    case MacOSXLion:        return "Mac OS X Lion";
                    case OSXMountainLion:   return "OSX Mountain Lion";
                    case OSXMavericks:      return "OSX Mavericks";
                }
                return "";
            }
            __XX__DEFINE_ENUM_DEFAULT_MEMBERS__(Id)
        };
        
        //! CPUType enumeration.
        struct CPUType
        {
            enum __XX__Enum
            {
                Unknown,
                CPU16Bit,
                CPU32Bit,
                CPU64Bit,
                __XX__Uninitialized
            };
            static inline size_t Num()
            {
                return 4;
            }
            std::string Str() const
            {
                switch (__XX__Entry)
                {
                    case Unknown:  return "Unknown";
                    case CPU16Bit: return "CPU 16 Bit";
                    case CPU32Bit: return "CPU 32 Bit";
                    case CPU64Bit: return "CPU 64 Bit";
                }
                return "";
            }
            __XX__DEFINE_ENUM_DEFAULT_MEMBERS__(CPUType)
        };
        
        Family family;
        Id id;
        CPUType cpuType;
        SysInfo() :
            family(Family::Unknown),
            id(Id::Unknown),
            cpuType(CPUType::Unknown)
        {
        }
        
        virtual ~SysInfo()
        {
        }
        
};

//! SysCall procedure.
//! \param[in] command Input parameter command.
void SysCall(std::string const& command)
{
    // < Inline C++ Code >
 system(command.c_str()); 
    // < /Inline C++ Code >
}

//! GetSystemInfo procedure.
//! \return <custom>.
SysInfo GetSystemInfo()
{
    SysInfo info;
    // < Inline C++ Code >

		// Setup OS family
		#if defined(_WIN32)
		info.family = SysInfo::Family::Windows;
		#elif defined(__APPLE__)
		info.family = SysInfo::Family::OSX;
		#elif defined(__unix__) || defined(linux) || defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__)
		info.family = SysInfo::Family::Posix;
		#endif
		
		// Setup OS ID
		#if defined(_WIN32)
		info.id = SysInfo::Id::Windows;
		#elif defined(__APPLE__)
		info.id = SysInfo::Id::OSX;
		#elif defined(__FreeBSD__)
		info.id = SysInfo::Id::FreeBSD;
		#elif defined(__NetBSD__)
		info.id = SysInfo::Id::NetBSD;
		#elif defined(__OpenBSD__)
		info.id = SysInfo::Id::OpenBSD;
		#elif defined(__unix__)
		info.id = SysInfo::Id::Unix;
		#elif defined(linux)
		info.id = SysInfo::Id::Linux;
		#endif
		
		// Setup CPU type
		//todo ...
		info.cpuType = SysInfo::CPUType::CPU32Bit;
	
    // < /Inline C++ Code >
    return info;
}

} // /namespace Lang

//! Main procedure.
int main()
{
    Lang::SysInfo sysInfo = Lang::GetSystemInfo();
    IO::Console::PrintNL("System Info:");
    {
        IO::Console::ScopedIndent Unused;
        IO::Console::PrintNL("OS Family:\t" + sysInfo.family.Str());
        IO::Console::PrintNL("OS ID:\t" + sysInfo.id.Str());
        IO::Console::PrintNL("CPU Type:\t" + sysInfo.cpuType.Str());
    }
    IO::Console::Print("Enter your name: ");
    std::string name;
    // While Loop
    while (name.empty())
    {
        name = IO::Console::Input();
    }
    
    IO::Console::PrintNL(Lang::__XX__Str("Hello \"") + Lang::__XX__Str(name) + Lang::__XX__Str("\", your name has ") + Lang::__XX__Str(name.size()) + Lang::__XX__Str(" character"));
    IO::Console::Wait();
    return 0;
}

#endif
// ================
