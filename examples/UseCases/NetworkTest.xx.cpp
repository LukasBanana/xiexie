// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\UseCases/NetworkTest.xx.cpp
// XieXie generated source file
// Sat Feb 22 21:53:45 2014

#ifndef __XX__D__SOFTWAREENTWICKLUNG_C___HLC_TOOLS_XIEXIE_COMPILER_TRUNK_EXAMPLES_USECASES_NETWORKTEST_XX_CPP__H__
#define __XX__D__SOFTWAREENTWICKLUNG_C___HLC_TOOLS_XIEXIE_COMPILER_TRUNK_EXAMPLES_USECASES_NETWORKTEST_XX_CPP__H__

#include <XieXie/Lang/EnumCore.h>
#include <XieXie/Lang/StringCore.h>
#include <memory>
#include <string>

;
;
// < Inline C++ Code >

	#if defined(_WIN32)
	#	define WIN32_LEAN_AND_MEAN
	#	include <winsock2.h>
	#	include <ws2tcpip.h>
	#	include <lmserver.h>
	#	if defined(__GNUC__)
	#		include <unistd.h>
	#	elif defined(_MSC_VER)
	#		include <windows.h>
	#		include <process.h>
	#	endif
	#elif defined(linux) || defined(__unix__)
	#	include <sys/types.h>
	#	include <sys/socket.h>
	#	include <sys/select.h>
	#	include <netinet/in.h>
	#	include <arpa/inet.h>
	#	include <netdb.h>
	#	include <unistd.h>
	#	ifndef SOCKET
	#		define SOCKET			int
	#	endif
	#	ifndef INVALID_SOCKET
	#		define INVALID_SOCKET	-1
	#	endif
	#	ifndef SOCKET_ERROR
	#		define SOCKET_ERROR		-1
	#	endif
	#endif

// < /Inline C++ Code >
;
namespace Net
{
//! Address class.
class Address
{
    public:
        typedef unsigned short PortType;
        typedef unsigned int IPv4AddrType;
        typedef unsigned long int IPv6AddrType;
        Address(PortType const& port)
        {
            // < Inline C++ Code >

			memset(&addr, 0, sizeof(sockaddr_in));
			addr.sin_family			= AF_INET;
			addr.sin_port			= htons(port);
			addr.sin_addr.s_addr	= htonl(INADDR_ANY);
		
            // < /Inline C++ Code >
        }
        
        //! GetPort procedure.
        //! \return <custom>.
        PortType GetPort() const
        {
            PortType port;
            // < Inline C++ Code >
 port = ntohs(addr.sin_port); 
            // < /Inline C++ Code >
            return port;
        }
        
        //! GetIPAddressString procedure.
        //! \return string.
        std::string GetIPAddressString() const
        {
            std::string str;
            unsigned char const* addrPtr = nullptr;
            // < Inline C++ Code >
 addrPtr = reinterpret_cast<const unsigned char*>(&(addr.sin_addr.s_addr)); 
            // < /Inline C++ Code >
            str += Lang::__XX__Str(static_cast< int >(addrPtr[0])) + Lang::__XX__Str(".");
            str += Lang::__XX__Str(static_cast< int >(addrPtr[1])) + Lang::__XX__Str(".");
            str += Lang::__XX__Str(static_cast< int >(addrPtr[2])) + Lang::__XX__Str(".");
            str += static_cast< int >(addrPtr[3]);
            return str;
        }
        
        virtual ~Address()
        {
        }
        
    private:
        // < Inline C++ Code >

		friend class Socket;
		sockaddr_in addr;
	
        // < /Inline C++ Code >
};

} // /namespace Net

namespace Net
{
//! Socket class.
class Socket
{
    public:
        //! Families enumeration.
        struct Families
        {
            enum __XX__Enum
            {
                Unknown,
                IPv4,
                IPv6,
                IPX,
                AppleTalk,
                NetBIOS,
                Infrared,
                Bluetooth,
                __XX__Uninitialized
            };
            static inline size_t Num()
            {
                return 8;
            }
            std::string Str() const
            {
                switch (__XX__Entry)
                {
                    case Unknown:   return "Unknown";
                    case IPv4:      return "IPv4";
                    case IPv6:      return "IPv6";
                    case IPX:       return "IPX";
                    case AppleTalk: return "AppleTalk";
                    case NetBIOS:   return "NetBIOS";
                    case Infrared:  return "Infrared";
                    case Bluetooth: return "Bluetooth";
                }
                return "";
            }
            __XX__DEFINE_ENUM_DEFAULT_MEMBERS__(Families)
        };
        
        //! Types enumeration.
        struct Types
        {
            enum __XX__Enum
            {
                Stream,
                Datagram,
                Raw,
                RDM,
                SeqPacket,
                __XX__Uninitialized
            };
            static inline size_t Num()
            {
                return 5;
            }
            std::string Str() const
            {
                switch (__XX__Entry)
                {
                    case Stream:    return "Stream";
                    case Datagram:  return "Datagram";
                    case Raw:       return "Raw";
                    case RDM:       return "RDM";
                    case SeqPacket: return "SeqPacket";
                }
                return "";
            }
            __XX__DEFINE_ENUM_DEFAULT_MEMBERS__(Types)
        };
        
        //! Protocols enumeration.
        struct Protocols
        {
            enum __XX__Enum
            {
                ICMP,
                IGMP,
                TCP,
                UDP,
                ICMPv6,
                __XX__Uninitialized
            };
            static inline size_t Num()
            {
                return 5;
            }
            std::string Str() const
            {
                switch (__XX__Entry)
                {
                    case ICMP:   return "ICMP";
                    case IGMP:   return "IGMP";
                    case TCP:    return "TCP";
                    case UDP:    return "UDP";
                    case ICMPv6: return "ICMPv6";
                }
                return "";
            }
            __XX__DEFINE_ENUM_DEFAULT_MEMBERS__(Protocols)
        };
        
        //! InvalidSocketException class.
        class InvalidSocketException
        {
            public:
                InvalidSocketException(Families const family, Types const type, Protocols const protocol)
                {
                    (*this).family = family;
                    (*this).type = type;
                    (*this).protocol = protocol;
                }
                
                //! Message procedure.
                //! \return string.
                std::string Message() const
                {
                    return "Invalid socket";
                }
                
                virtual ~InvalidSocketException()
                {
                }
                
                //! GetFamily procedure.
                //! \return .
                Families const& GetFamily() const
                {
                    return family;
                }
                
                //! GetType procedure.
                //! \return .
                Types const& GetType() const
                {
                    return type;
                }
                
                //! GetProtocol procedure.
                //! \return .
                Protocols const& GetProtocol() const
                {
                    return protocol;
                }
                
            private:
                Families family;
                Types type;
                Protocols protocol;
        };
        
        //! StartUpException class.
        class StartUpException
        {
            public:
                StartUpException(int errorCode) :
                    errorCode(0)
                {
                    (*this).errorCode = errorCode;
                }
                
                //! Message procedure.
                //! \return string.
                std::string Message() const
                {
                    return Lang::__XX__Str("Socket startup failed (Error Code: ") + Lang::__XX__Str(errorCode) + Lang::__XX__Str(")");
                }
                
                virtual ~StartUpException()
                {
                }
                
                //! GetErrorCode procedure.
                //! \return .
                int const& GetErrorCode() const
                {
                    return errorCode;
                }
                
            private:
                int errorCode;
        };
        
        Socket(Families const family, Types const type, Protocols const protocol)
        {
            (*this).family = family;
            (*this).type = type;
            (*this).protocol = protocol;
            // < Inline C++ Code >

			/* Setup low-level socket settings */
			int af = 0, tp = 0, pr = 0;
			
			switch (family)
			{
				case Families::Unknown:		af = AF_UNSPEC;		break;
				case Families::IPv4:		af = AF_INET;		break;
				case Families::IPv6:		af = AF_INET6;		break;
				case Families::IPX:			af = AF_IPX;		break;
				case Families::AppleTalk:	af = AF_APPLETALK;	break;
				case Families::NetBIOS:		af = AF_NETBIOS;	break;
				case Families::Infrared:	af = AF_IRDA;		break;
				case Families::Bluetooth:	af = AF_BTH;		break;
			}
			
			switch (type)
			{
				case Types::Stream:		tp = SOCK_STREAM;		break;
				case Types::Datagram:	tp = SOCK_DGRAM;		break;
				case Types::Raw:		tp = SOCK_RAW;			break;
				case Types::RDM:		tp = SOCK_RDM;			break;
				case Types::SeqPacket:	tp = SOCK_SEQPACKET;	break;
			}
			
			switch (protocol)
			{
				case Protocols::ICMP:	pr = IPPROTO_ICMP;		break;
				case Protocols::IGMP:	pr = IPPROTO_IGMP;		break;
				case Protocols::TCP:	pr = IPPROTO_TCP;		break;
				case Protocols::UDP:	pr = IPPROTO_UDP;		break;
				case Protocols::ICMPv6:	pr = IPPROTO_ICMPV6;	break;
			}
			
			/* Open socket */
			sock = socket(af, tp, pr);
			
			/* Throw exception on error */
			if (sock == INVALID_SOCKET)
			{
				throw InvalidSocketException(family, type, protocol);
			}
		
            // < /Inline C++ Code >
        }
        
        virtual ~Socket()
        {
            // < Inline C++ Code >

			#ifdef WIN32
			closesocket(sock);
			#else
			close(sock);
			#endif
		
            // < /Inline C++ Code >
        }
        
        //! StartUp procedure.
        void StartUp()
        {
            // < Inline C++ Code >

			#ifdef _WIN32
			WORD versionRequest = MAKEWORD(2, 2);
			WSADATA data;
			
			int err = WSAStartup(versionRequest, &data);
			
			if (err != 0)
			{
				throw StartUpException(err);
			}
			else if (LOBYTE(data.wVersion) != 2 || HIBYTE(data.wVersion) != 2)
			{
				throw StartUpException(-1);
			}
			#endif
		
            // < /Inline C++ Code >
        }
        
        //! CleanUp procedure.
        void CleanUp()
        {
            // < Inline C++ Code >

			#ifdef _WIN32
			WSACleanup();
			#endif
		
            // < /Inline C++ Code >
        }
        
        //! Bind procedure.
        //! \param[in] address Input parameter address.
        //! \return bool.
        bool Bind(Address const& address)
        {
            // < Inline C++ Code >

			/* Bind socket to address */
			int result = bind(
				sock,
				reinterpret_cast<const sockaddr*>(&(address.addr)),
				sizeof(sockaddr_in)
			);
			if (result != NO_ERROR)
			{
				return false;
			}
		
            // < /Inline C++ Code >
            return true;
        }
        
        //! Unbind procedure.
        //! \return bool.
        bool Unbind()
        {
            return false;
        }
        
        //! Send procedure.
        void Send()
        {
        }
        
        //! SendTo procedure.
        void SendTo()
        {
        }
        
        //! Recv procedure.
        void Recv()
        {
        }
        
        //! RecvFrom procedure.
        void RecvFrom()
        {
        }
        
        //! GetFamily procedure.
        //! \return .
        Families const& GetFamily() const
        {
            return family;
        }
        
        //! GetType procedure.
        //! \return .
        Types const& GetType() const
        {
            return type;
        }
        
        //! GetProtocol procedure.
        //! \return .
        Protocols const& GetProtocol() const
        {
            return protocol;
        }
        
    private:
        Families family;
        Types type;
        Protocols protocol;
        // < Inline C++ Code >
 SOCKET sock; 
        // < /Inline C++ Code >
};

} // /namespace Net

//! Main procedure.
int main()
{
    try
    {
        Net::Socket::StartUp();
        std::shared_ptr< Net::Socket > sock = std::make_shared< Net::Socket >(Net::Socket::Families::IPv4, Net::Socket::Types::Stream, Net::Socket::Protocols::TCP);
        if ((sock != nullptr) && (*sock).GetFamily() == Net::Socket::Families::IPv4 && (*sock).GetType() == Net::Socket::Types::Stream && (*sock).GetProtocol() == Net::Socket::Protocols::TCP)
        {
        }
    }
    catch (Net::Socket::InvalidSocketException err)
    {
    }
    
    Net::Socket::CleanUp();
    return 0;
}

#endif
// ================
