; D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/ExpressionTest1.xx.xbc.asm
; XieXie generated source file
; Sun Dec 29 16:16:47 2013

; --- Function definition "func"  ---
func:
    push x
    pop r0
    mov r1, 2
    mul r0, r1
    push r0
    pop r0
    mov r1, 3
    sub r0, r1
    push r0
    ret

; --- Function definition "Main"  ---
Main:
    ; Variable initialization "x"
    push 1
    push 2
    ; TODO -> push parameters onto stack ...
    call func
    pop r0
    mov r1, 4
    div r0, r1
    push r0
    pop r1
    pop r0
    mul r0, r1
    push r0
    pop r0
    mov r1, 2
    sub r0, r1
    push r0
    pop r1
    pop r0
    add r0, r1
    push r0
    pop r0
    mov [0x0001e240], r0
    
    ; Variable initialization "a"
    push 7
    mov r0, 3
    mov r1, 2
    mul r0, r1
    push r0
    push 5
    mov r0, 6
    mov r1, 3
    add r0, r1
    push r0
    pop r1
    pop r0
    mul r0, r1
    push r0
    pop r1
    pop r0
    sub r0, r1
    push r0
    pop r0
    mov r1, 4
    sub r0, r1
    push r0
    pop r1
    pop r0
    add r0, r1
    push r0
    pop r0
    mov [0x0001e240], r0
    
    ; Variable initialization "i"
    
    ; Infinite loop
    .inf_loop_0:
        ; TODO -> push parameters onto stack ...
        call func
        push 3
        ; <Object expression "i">
        ; </Object expression "i">
        pop r1
        pop r0
        mul r0, r1
        push r0
        pop r0
        add [0x0001e240], r0
    jmp inf_loop_0
    .inf_loop_0_break:
    
    push 0
    ret

; ================
