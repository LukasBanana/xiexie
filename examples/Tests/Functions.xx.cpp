// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Functions.xx.cpp
// XieXie generated source file
// Sun Jun 08 13:45:08 2014


#include <XieXie/Lang/StringCore.h>
#include <functional>
#include <memory>

// < Inline C++ Code >

	#include <iostream>
	#include <cstdlib>

// < /Inline C++ Code >
//! LinkedList class.
class LinkedList
{
    public:
        int data;
        std::shared_ptr< LinkedList > link;
        LinkedList() :
            data(0),
            link(nullptr)
        {
        }
        
        virtual ~LinkedList()
        {
        }
        
};

int global = 0;
//! Inc procedure.
//! \param[in] val Input parameter val.
void Inc(int& val)
{
    global += 1;
    val = global;
}

//! PrintText procedure.
//! \param[in] text Input parameter text.
void PrintText(std::string const text)
{
    // < Inline C++ Code >
 std::cout << text << std::endl; 
    // < /Inline C++ Code >
}

//! Print procedure.
//! \param[in] val Input parameter val.
//! \param[in] x Input parameter x.
void Print(int& val, float x)
{
    // < Inline C++ Code >
 std::cout << val << std::endl; 
    // < /Inline C++ Code >
}

//! Pause procedure.
void Pause()
{
    // < Inline C++ Code >
 system("pause"); 
    // < /Inline C++ Code >
}

//! MakeList procedure.
//! \param[in] num Input parameter num.
//! \return @.
std::shared_ptr< LinkedList > MakeList(int num)
{
    std::shared_ptr< LinkedList > list = std::make_shared< LinkedList >();
    (*list).data = num;
    if (num > 0)
    {
        (*list).link = MakeList(num - 1);
    }
    return list;
}

typedef std::function< void (int& val) > procType;
//! ForEach procedure.
//! \param[in] list Input parameter list.
//! \param[in] opProc Input parameter opProc.
void ForEach(std::shared_ptr< LinkedList > list, procType const opProc)
{
    // While Loop
    while (list != nullptr)
    {
        opProc((*list).data);
        list = (*list).link;
    }
    
}

//! Main procedure.
int main()
{
    std::shared_ptr< LinkedList > list = MakeList(10);
    PrintText("-- List --");
    ForEach(list, Print);
    PrintText("-- List --");
    ForEach(list, Inc);
    ForEach(list, Print);
    Pause();
    return 0;
}

// ================
