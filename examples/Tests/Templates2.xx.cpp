// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Templates2.xx.cpp
// XieXie generated source file
// Sun Jan 26 20:06:24 2014


#include <XieXie/Lang/StringCore.h>
#include <memory>
#include <vector>

//! __XX__TemplateInstance6__Vec3 template class.
class __XX__TemplateInstance6__Vec3
{
    public:
        typedef int T;
        static const unsigned int num = (5 >> 2);
        T x, y, z;
        virtual ~__XX__TemplateInstance6__Vec3()
        {
        }
        
};

//! __XX__TemplateInstance5__Vec3 template class.
class __XX__TemplateInstance5__Vec3
{
    public:
        typedef double T;
        static const unsigned int num = 3;
        T x, y, z;
        virtual ~__XX__TemplateInstance5__Vec3()
        {
        }
        
};

//! __XX__TemplateInstance4__Vec3 template class.
class __XX__TemplateInstance4__Vec3
{
    public:
        typedef float T;
        static const unsigned int num = 3;
        T x, y, z;
        virtual ~__XX__TemplateInstance4__Vec3()
        {
        }
        
};

//! __XX__TemplateInstance3__Test template class.
class __XX__TemplateInstance3__Test
{
    public:
        typedef std::shared_ptr< PrinterClass* >* T;
        T var;
        //! Foo procecure.
        //! \return int.
        int Foo()
        {
            if (true)
            {
                (***var).Print();
            }
            return 0;
        }
        
        virtual ~__XX__TemplateInstance3__Test()
        {
        }
        
};

//! __XX__TemplateInstance2__Test template class.
class __XX__TemplateInstance2__Test
{
    public:
        typedef PrinterClass* T;
        T var;
        //! Foo procecure.
        //! \return int.
        int Foo()
        {
            if (true)
            {
                (*var).Print();
            }
            return 0;
        }
        
        virtual ~__XX__TemplateInstance2__Test()
        {
        }
        
};

//! __XX__TemplateInstance1__Test template class.
class __XX__TemplateInstance1__Test
{
    public:
        typedef PrinterClass T;
        T var;
        //! Foo procecure.
        //! \return int.
        int Foo()
        {
            if (true)
            {
                var.Print();
            }
            return 0;
        }
        
        virtual ~__XX__TemplateInstance1__Test()
        {
        }
        
};

//! PrinterClass class.
class PrinterClass
{
    public:
        //! Print procecure.
        void Print()
        {
        }
        
        virtual ~PrinterClass()
        {
        }
        
};

//! Main procecure.
//! \param[in] Args Input parameter Args.
int main(int __XX__NumArgs, const char** __XX__ArgStrings)
{
    std::vector< std::string > Args;
    Args.resize(__XX__NumArgs);
    for (int i = 0; i < __XX__NumArgs; ++i)
    {
        Args[i] = std::string(*(__XX__ArgStrings++));
    }
    Test t1;
    Test t2;
    Test t3;
    typedef Vec3 Vec3f;
    typedef Vec3 Vec3d;
    typedef Vec3 Vec3i;
    return 0;
}

// ================
