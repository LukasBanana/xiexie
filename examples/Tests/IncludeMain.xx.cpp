// ./IncludeMain.xx.cpp
// XieXie generated source file
// Sat Jan 11 23:12:54 2014

#ifndef __XX____INCLUDEMAIN_XX_CPP__H__
#define __XX____INCLUDEMAIN_XX_CPP__H__

#include <XieXie/Lang/StringCore.h>

;
// < inline C++ code >
 #include <fstream> 
// < /inline C++ code >
namespace IO
{
//! File class.
class File
{
    public:
        //! AccessFlags flags enumeration.
        struct AccessFlags
        {
            // --- Auto-generated flags --- //
            typedef unsigned char DataType;
            static const DataType None     = 0;
            static const DataType All      = ~0;
            // --- Custom flags --- //
            static const DataType Binary   = (1 << 0);
            static const DataType End      = (1 << 1);
            static const DataType Append   = (1 << 2);
            static const DataType Truncate = (1 << 3);
        };
        
        //! Open procecure.
        //! \param[in] filename Input parameter filename.
        //! \return bool.
        bool Open(const std::string& filename, const AccessFlags::DataType& access = AccessFlags::None)
        {
            bool result = false;
            // < inline C++ code >

			stream.open(filename.c_str());
			result = stream.good();
		
            // < /inline C++ code >
            return result;
        }
        
        //! OpenRead procecure.
        //! \param[in] filename Input parameter filename.
        //! \return bool.
        bool OpenRead(const std::string& filename, const AccessFlags::DataType& access = AccessFlags::None)
        {
            bool result = false;
            // < inline C++ code >

			stream.open(filename.c_str(), std::ios_base::in);
			result = stream.good();
		
            // < /inline C++ code >
            return result;
        }
        
        //! OpenWrite procecure.
        //! \param[in] filename Input parameter filename.
        //! \return bool.
        bool OpenWrite(const std::string& filename, const AccessFlags::DataType& access = AccessFlags::None)
        {
            bool result = false;
            // < inline C++ code >

			stream.open(filename.c_str(), std::ios_base::out);
			result = stream.good();
		
            // < /inline C++ code >
            return result;
        }
        
        //! Close procecure.
        void Close()
        {
            // < inline C++ code >
 stream.close(); 
            // < /inline C++ code >
        }
        
        //! WriteBuffer procecure.
        //! \param[in] buffer Input parameter buffer.
        void WriteBuffer(const char* buffer, const unsigned int& size)
        {
            // < inline C++ code >
 stream.write(buffer, static_cast<std::streamsize>(size)); 
            // < /inline C++ code >
        }
        
        //! WriteByte procecure.
        //! \param[in] value Input parameter value.
        void WriteByte(const char& value)
        {
            // < inline C++ code >
 stream.write(&value, sizeof(value)); 
            // < /inline C++ code >
        }
        
        //! WriteUByte procecure.
        //! \param[in] value Input parameter value.
        void WriteUByte(const unsigned char& value)
        {
            // < inline C++ code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /inline C++ code >
        }
        
        //! WriteShort procecure.
        //! \param[in] value Input parameter value.
        void WriteShort(const short& value)
        {
            // < inline C++ code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /inline C++ code >
        }
        
        //! WriteUShort procecure.
        //! \param[in] value Input parameter value.
        void WriteUShort(const unsigned short& value)
        {
            // < inline C++ code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /inline C++ code >
        }
        
        //! WriteInt procecure.
        //! \param[in] value Input parameter value.
        void WriteInt(const int& value)
        {
            // < inline C++ code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /inline C++ code >
        }
        
        //! WriteUInt procecure.
        //! \param[in] value Input parameter value.
        void WriteUInt(const unsigned int& value)
        {
            // < inline C++ code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /inline C++ code >
        }
        
        //! WriteLong procecure.
        //! \param[in] value Input parameter value.
        void WriteLong(const long int& value)
        {
            // < inline C++ code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /inline C++ code >
        }
        
        //! WriteULong procecure.
        //! \param[in] value Input parameter value.
        void WriteULong(const unsigned long int& value)
        {
            // < inline C++ code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /inline C++ code >
        }
        
        //! WriteString procecure.
        //! \param[in] value Input parameter value.
        void WriteString(const std::string& value)
        {
            // < inline C++ code >
 stream.write(value.c_str(), sizeof(char)*value.size()); 
            // < /inline C++ code >
        }
        
        //! WriteWString procecure.
        //! \param[in] value Input parameter value.
        void WriteWString(const std::wstring& value)
        {
            // < inline C++ code >
 stream.write(reinterpret_cast<const char*>(value.c_str()), sizeof(wchar_t)*value.size()); 
            // < /inline C++ code >
        }
        
        //! WriteStringNL procecure.
        //! \param[in] value Input parameter value.
        void WriteStringNL(const std::string& value)
        {
            // < inline C++ code >

			stream.write(value.c_str(), sizeof(char)*value.size());
			stream.write("\n", sizeof(char));
		
            // < /inline C++ code >
        }
        
        //! WriteWStringNL procecure.
        //! \param[in] value Input parameter value.
        void WriteWStringNL(const std::string& value)
        {
            // < inline C++ code >

			stream.write(value.c_str(), sizeof(char)*value.size());
			stream.write(reinterpret_cast<const char*>(L"\n"), sizeof(wchar_t));
		
            // < /inline C++ code >
        }
        
        //! ReadBuffer procecure.
        //! \param[in] buffer Input parameter buffer.
        void ReadBuffer(char* buffer, const unsigned int& size)
        {
            // < inline C++ code >
 stream.read(buffer, static_cast<std::streamsize>(size)); 
            // < /inline C++ code >
        }
        
        virtual ~File()
        {
        }
        
    private:
        // < inline C++ code >
 std::fstream stream; 
        // < /inline C++ code >
};

} // /namespace IO

//! Main procecure.
//! \return int.
int main()
{
    IO::File file;
    file.OpenWrite("test.txt");
    int i = 0;
    // while loop
    while (i < 10)
    {
        file.WriteStringNL(Lang::__XX__Str("test ") + Lang::__XX__Str(i));
        i += 1;
    }
    
    return 0;
}

#endif
// ================
