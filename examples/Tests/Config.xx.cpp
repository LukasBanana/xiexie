// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Config.xx.cpp
// XieXie generated source file
// Fri Jan 17 20:29:41 2014


#include <memory>

;
std::shared_ptr< int > Test1(int const& x)
{
return std::make_shared< int >(x);
}

;
//! Test2 procecure.
//! \param[in] x Input parameter x.
//! \return .
std::shared_ptr< int > Test2(int const x)
{
    return std::make_shared< int >(x);
}

// ================
