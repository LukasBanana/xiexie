// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Expr2.xx.cpp
// XieXie generated source file
// Wed Feb 12 21:58:47 2014


#include <XieXie/Lang/StringCore.h>
#include <memory>

//! Test procedure.
template < bool b = (5 < 3) > void Test()
{
}

//! Main procedure.
//! \return int.
int main()
{
    std::shared_ptr< int > p1 = nullptr;
    int* p2 = nullptr;
    std::shared_ptr< int > p3 = std::make_shared< int >(5);
    int a = 3, b = 5, c = 7;
    int x = 1 & 2 | 3 ^ 4 + (5 - (a += 6 * 7)) - 8 + static_cast< unsigned int >(9) + b;
    a = (-(-(-b)));
    a = b + (++b);
    int h = 720675;
    std::string s1 = Lang::__XX__Str("Foo") + Lang::__XX__Str(x) + Lang::__XX__Str("Bar");
    std::string s2 = Lang::__XX__Str(3) + Lang::__XX__Str("Foo") + Lang::__XX__Str(x) + Lang::__XX__Str("Bar");
    bool b1 = a = 5 != 5 && a + (++a) > b == c || (5 + a < c == true) != false;
    bool b2 = p1 == p2 && p1 != nullptr;
    bool b3 = (-x) > 2 && (!b2) || Lang::__XX__Str("test") != Lang::__XX__Str(s1);
    if (b1 == true)
    {
    }
    if ((a > b) == true)
    {
    }
    if (p1 != nullptr)
    {
    }
    return 0;
}

// ================
