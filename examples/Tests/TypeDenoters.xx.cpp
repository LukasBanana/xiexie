// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/TypeDenoters.xx.cpp
// XieXie generated source file
// Sat Jan 25 19:24:56 2014


#include <memory>

float x = 0.0f, y = 0.5, z = 0.0f;
//! TestA class.
class TestA
{
    public:
        virtual ~TestA()
        {
        }
        
};

//! TestB class.
class TestB
{
    public:
        virtual ~TestB()
        {
        }
        
};

//! TestC class.
class TestC
{
    public:
        virtual ~TestC()
        {
        }
        
};

//! Main procecure.
//! \return int.
int main()
{
    TestA a;
    TestB b;
    //! c procecure.
    //! \param[in] x Input parameter x.
    //! \return <custom>.
    TestC c(int x)
    {
        return 0;
    }
    
    std::shared_ptr< int > p = nullptr;
    int* q = nullptr;
    std::shared_ptr< int* > r_obj = nullptr;
    std::shared_ptr< int* >& r = r_obj;
    r_obj = r;
    std::shared_ptr< int const** > ptr = nullptr;
    //! Foo class.
    class Foo
    {
        public:
            //! Bar procecure.
            void Bar()
            {
            }
            
            Foo* bla;
            int blub;
            Foo() :
                bla(nullptr),
                blub(0)
            {
            }
            
            virtual ~Foo()
            {
            }
            
    };
    
    std::shared_ptr< std::shared_ptr< Foo >* > foo = std::make_shared< std::shared_ptr< Foo >* >();
    (***foo).Bar();
    (*(***foo).bla).blub = 0;
    std::shared_ptr< Foo > foo2 = std::make_shared< Foo >();
    (*foo2).blub += 3;
    int s = 0;
    s = c(s);
    if ((ptr != nullptr && *ptr != nullptr && **ptr != nullptr) && (p != nullptr) && true)
    {
    }
    bool b1 = true;
    bool b2 = (p != nullptr);
    bool b3 = (r_obj != nullptr && *r_obj != nullptr);
    bool b4 = (ptr != nullptr && *ptr != nullptr && **ptr != nullptr);
    return 0;
}

// ================
