// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Classes.xx.cpp
// XieXie generated source file
// Tue Jan 14 23:33:50 2014

//! Vec3 class.
class Vec3
{
    public:
        Vec3(float x, float y, float z) :
            x(0.0f),
            y(0.0f),
            z(0.0f)
        {
            (*this).x = x;
            (*this).y = y;
            (*this).z = z;
        }
        
        float x, y, z;
        virtual ~Vec3()
        {
        }
        
};

//! TestClass class.
class TestClass
{
    public:
        //! TestProc procecure.
        //! \param[in] set Input parameter set.
        //! \param[in] opt Input parameter opt.
        void TestProc(Setting const set, Options::DataType const opt)
        {
            (*this).set = Setting::A;
            (*this).opt = opt;
        }
        
        Setting set;
        SubClass::Options::DataType opt;
        //! Setting enumeration.
        enum class Setting
        {
            A,
            B,
        };
        
        //! Options flags enumeration.
        struct Options
        {
            // --- Auto-Generated Flags --- //
            typedef unsigned char DataType;
            static const DataType None = 0;
            static const DataType All  = ~0;
            // --- Custom Flags --- //
            static const DataType Foo  = (1 << 0);
            static const DataType Bar  = (1 << 1);
        };
        
        //! SubClass class.
        class SubClass
        {
            public:
                //! Options flags enumeration.
                struct Options
                {
                    // --- Auto-Generated Flags --- //
                    typedef unsigned char DataType;
                    static const DataType None   = 0;
                    static const DataType All    = ~0;
                    // --- Custom Flags --- //
                    static const DataType FooSub = (1 << 0);
                    static const DataType BarSub = (1 << 1);
                };
                
                virtual ~SubClass()
                {
                }
                
        };
        
        TestClass() :
            opt(0u)
        {
        }
        
        virtual ~TestClass()
        {
        }
        
};

TestClass::Options::DataType opt = 0u;
TestClass obj;
obj.opt = TestClass::Options::Foo | TestClass::Options::Bar;
// ================
