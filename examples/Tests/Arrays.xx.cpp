// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Arrays.xx.cpp
// XieXie generated source file
// Sun Feb 23 16:30:25 2014


#include <XieXie/Lang/StringCore.h>
#include <array>
#include <functional>
#include <memory>
#include <vector>

std::array< int, 10 > list1 = { 0 };
std::array< std::array< int, 3 >, 6 > list2 = { 0 };
std::array< int, 6 > list3 = { 0 };
std::shared_ptr< std::array< int, 20 > > list4 = std::make_shared< std::array< int, 20 > >();
std::array< std::array< std::shared_ptr< int >, 20 >*, 5 > list5 = { 0 };
std::shared_ptr< std::array< std::function< void () >*, 12 > > list6 = std::make_shared< std::array< std::function< void () >*, 12 > >();
std::array< std::array< std::shared_ptr< int const > const, 20 >* const, 5 > list7 = { 0 };
std::array< int, 11 > list8 = { 0 };
std::shared_ptr< int const* const > const c0 = nullptr;
int* const** c1 = nullptr;
std::string const c2 = "test";
int* const c3 = nullptr;
std::array< int, 10 > staticArray = { 0 };
std::vector< int > dynamicArray;
std::shared_ptr< std::vector< int > > dynamicArrayPtr = std::make_shared< std::vector< int > >();
//! Main procedure.
int main()
{
    // Range-Based For Loop
    for (unsigned char i = 5; i <= 10; ++i)
    {
        list3[i - 5] = i;
    }
    
    // Range-Based For Loop
    for (char i = -5; i <= 5; ++i)
    {
        list8[i - -5] = i;
    }
    
    list2[0][0] = 3;
    int* p = (&list3[5 - 5]);
    p[0] += 2;
    return 0;
}

// ================
