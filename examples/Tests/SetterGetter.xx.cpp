// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/SetterGetter.xx.cpp
// XieXie generated source file
// Sat Feb 08 22:09:30 2014


#include <XieXie/Lang/StringCore.h>

//! Person class.
class Person
{
    public:
        int age;
        std::string name;
        Person() :
            age(0)
        {
        }
        
        virtual ~Person()
        {
        }
        
        //! GetAge procecure.
        //! \return .
        int const& GetAge()
        {
            return age;
        }
        
        //! SetAge procecure.
        //! \param[in] age Input parameter age.
        void SetAge(int const& age)
        {
            (*this).age = age;
        }
        
        //! GetName procecure.
        //! \return .
        std::string const& GetName()
        {
            return name;
        }
        
        //! SetName procecure.
        //! \param[in] name Input parameter name.
        void SetName(std::string const& name)
        {
            (*this).name = name;
        }
        
};

//! Main procecure.
int main()
{
    Person p;
    p.SetAge(23);
    p.SetName("Lukas");
    return 0;
}

// ================
