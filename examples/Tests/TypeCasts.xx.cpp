// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/TypeCasts.xx.cpp
// XieXie generated source file
// Tue Jan 14 20:43:03 2014


#include <memory>

float f = 0.5;
int i = 3;
int a = static_cast< int >(f);
int* p0 = nullptr;
float* p1 = dynamic_cast< float* >(p0);
std::shared_ptr< int > sp2 = std::make_shared< int >(50);
std::shared_ptr< float > sp3 = std::dynamic_pointer_cast< float >(sp2);
// ================
