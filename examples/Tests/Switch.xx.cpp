// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Switch.xx.cpp
// XieXie generated source file
// Mon Feb 03 22:33:28 2014


#include <XieXie/Lang/StringCore.h>

//! Test procecure.
//! \param[in] i Input parameter i.
void Test(int i)
{
}

//! Main procecure.
int main()
{
    static int const x = 15;
    int i = 0;
    std::string s;
    // Switch/Case Statement
    switch (i * 2)
    {
        case 0:
        {
            Test(1);
        }
        break;
        
        case 1:
        case 1 + 15:
        break;
        
        case 2:
        case 3:
        {
            Test(2);
        }
        
        case 4:
        {
            Test(3);
        }
        break;
        
        default:
        {
            Test(4);
        }
        break;
        
    }
    
    // Switch/Case Statement
    if ((s) == ("a"))
    {
        Test(1);
    }
    else if ((s) == ("a"))
    {
        Test(2);
    }
    else if ((s) == ("a"))
    {
        Test(3);
    }
    else
    {
        Test(4);
    }
    
    return 0;
}

// ================
