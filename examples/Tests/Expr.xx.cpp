// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Expr.xx.cpp
// XieXie generated source file
// Wed Feb 12 21:55:11 2014


#include <XieXie/Lang/StringCore.h>
#include <array>
#include <memory>

int g = 0;
//! Func procedure.
//! \param[in] x Input parameter x.
//! \return int.
int Func(int x)
{
    g += x;
    return g;
}

//! Obj class.
class Obj
{
    public:
        int a, b;
        Obj() :
            a(0),
            b(0)
        {
        }
        
        virtual ~Obj()
        {
        }
        
};

//! Array template class.
template < unsigned int num > class Array
{
    public:
        Array() :
            container({ 0 })
        {
        }
        
        virtual ~Array()
        {
        }
        
    private:
        std::array< int, 10 > container;
};

//! Main procedure.
//! \return int.
int main()
{
    int a = 5;
    int b = 2;
    float f = static_cast< float >((-(-(-3))));
    int x = 1 & 2 | 3 ^ 4 + (5 - (a += 6 * 7)) - 8 + static_cast< unsigned int >(9) + b;
    std::string s1 = "Test String 1";
    std::string s2 = Lang::__XX__Str("Test String 2: x equals ") + Lang::__XX__Str(x);
    std::string s3 = Lang::__XX__Str("Test String 3: x equals not ") + Lang::__XX__Str(static_cast< unsigned int >((~x)));
    std::string s4 = Lang::__XX__Str("Test String 4: x equals not ") + Lang::__XX__Str(x + 3 + (x * x));
    std::string s5 = Lang::__XX__Str("Test String 5: x equals not ") + Lang::__XX__Str((x - 3));
    std::string s6 = Lang::__XX__Str("Test String 6: x equals not ") + Lang::__XX__Str((x - 3)) + Lang::__XX__Str(" foo bar");
    s6 += Lang::__XX__Str("test") + Lang::__XX__Str(3) + Lang::__XX__Str("bla");
    std::shared_ptr< Obj > p = std::make_shared< Obj >(1, 2);
    std::array< int, 10 > array1 = { 0 };
    Array array2 = std::make_shared< Array >(10);
    if ((a > 5))
    {
    }
    return 0;
}

// ================
