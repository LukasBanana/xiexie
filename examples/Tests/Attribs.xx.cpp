// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Attribs.xx.cpp
// XieXie generated source file
// Sun Feb 09 15:45:48 2014


#include <XieXie/Lang/StringCore.h>

// < Inline C++ Code >
 #include <iostream> 
// < /Inline C++ Code >
// Structure Packing Alignment
#if defined(_MSC_VER)
#   pragma pack(push, packing)
#   pragma pack(1)
#   define __XX__PACK_STRUCT__
#elif defined(__GNUC__)
#   define __XX__PACK_STRUCT__ __attribute__((packed))
#else
#   define __XX__PACK_STRUCT__
#endif

//! Person class.
class Person
{
    public:
        //! ToString procecure.
        //! \return string.
        std::string ToString() const
        {
            return Lang::__XX__Str("Last Name: ") + Lang::__XX__Str(lastName) + Lang::__XX__Str(", First Name: ") + Lang::__XX__Str(firstName) + Lang::__XX__Str(", Age: ") + Lang::__XX__Str(age);
        }
        
        Person() :
            age(0)
        {
        }
        
        ~Person()
        {
        }
        
        //! GetAge procecure.
        //! \return .
        int const& GetAge() const
        {
            return age;
        }
        
        //! SetAge procecure.
        //! \param[in] age Input parameter age.
        void SetAge(int const& age)
        {
            (*this).age = age;
        }
        
        //! GetFirstName procecure.
        //! \return .
        std::string const& GetFirstName() const
        {
            return firstName;
        }
        
        //! SetFirstName procecure.
        //! \param[in] firstName Input parameter firstName.
        void SetFirstName(std::string const& firstName)
        {
            (*this).firstName = firstName;
        }
        
        //! GetLastName procecure.
        //! \return .
        std::string const& GetLastName() const
        {
            return lastName;
        }
        
        //! SetLastName procecure.
        //! \param[in] lastName Input parameter lastName.
        void SetLastName(std::string const& lastName)
        {
            (*this).lastName = lastName;
        }
        
    private:
        int age;
        std::string firstName, lastName;
} __XX__PACK_STRUCT__;

// /Structure Packing Alignment
#ifdef _MSC_VER
#   pragma pack(pop, packing)
#endif
#undef __XX__PACK_STRUCT__

//! Test procecure.
//! \param[in] i Input parameter i. By default 0.
//! \param[in] j Input parameter j. By default 0.
void Test(int i = 0, int j = 0)
{
}

//! Print procecure.
//! \param[in] text Input parameter text.
void Print(std::string const& text)
{
    // < Inline C++ Code >
 std::cout << text << std::endl; 
    // < /Inline C++ Code >
}

//! Pause procecure.
void Pause()
{
    // < Inline C++ Code >
 system("pause"); 
    // < /Inline C++ Code >
}

//! Main procecure.
int main()
{
    // Unrolled Range-Based For Loop
    {
        // Iteration 1
        unsigned char i = 1;
        {
            Test(0, i);
        }
        
        // Iteration 2
        i = 2;
        {
            Test(0, i);
        }
        
        // Iteration 3
        i = 3;
        {
            Test(0, i);
        }
        
    }
    Person p;
    p.SetAge(23);
    p.SetFirstName("Lukas");
    p.SetLastName("Hermanns");
    Print(p.ToString());
    Pause();
    return 0;
}

// ================
