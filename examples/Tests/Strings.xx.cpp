// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Strings.xx.cpp
// XieXie generated source file
// Sun Feb 23 23:40:26 2014


#include <XieXie/Lang/StringCore.h>
#include <vector>

std::string s1 = "\t\"\?";
std::string s2 = "C:\\Program Files\\XieXie\\Test.xx";
std::string s3 = "Hello \"XieXie\" programmer!\nTest";
//! Main procedure.
//! \param[in] args Input parameter args.
int main(int __XX__NumArgs, const char** __XX__ArgStrings)
{
    std::vector< std::string > args;
    args.resize(__XX__NumArgs);
    for (int i = 0; i < __XX__NumArgs; ++i)
    {
        args[i] = std::string(*(__XX__ArgStrings++));
    }
    std::string s4 = "test = -24";
    if ((!s4.empty()))
    {
        std::string subStr = s4.substr(s4.size() - 2, 2);
        s4 = Lang::__XX__Str(Lang::__XX__StrUpper(s4)) + Lang::__XX__Str(Lang::__XX__StrLower(s4)) + Lang::__XX__Str(s4.substr(0, 5));
        std::string tmp;
        unsigned int i = 0u;
        // While Loop
        while (i < s4.size())
        {
            tmp += s4[i];
            i += 1;
        }
        
    }
    return 0;
}

// ================
