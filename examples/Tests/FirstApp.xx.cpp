// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/FirstApp.xx.cpp
// XieXie generated source file
// Sun Jan 19 20:05:32 2014

#ifndef __XX__D__SOFTWAREENTWICKLUNG_C___HLC_TOOLS_XIEXIE_COMPILER_TRUNK_EXAMPLES_TESTS_FIRSTAPP_XX_CPP__H__
#define __XX__D__SOFTWAREENTWICKLUNG_C___HLC_TOOLS_XIEXIE_COMPILER_TRUNK_EXAMPLES_TESTS_FIRSTAPP_XX_CPP__H__

#include <XieXie/Lang/StringCore.h>
#include <memory>

;
// < Inline C++ Code >
 #include <fstream> 
// < /Inline C++ Code >
namespace IO
{
//! File class.
class File
{
    public:
        //! AccessFlags flags enumeration.
        struct AccessFlags
        {
            // --- Auto-Generated Flags --- //
            typedef unsigned char DataType;
            static const DataType None     = 0;
            static const DataType All      = ~0;
            // --- Custom Flags --- //
            static const DataType Binary   = (1 << 0);
            static const DataType End      = (1 << 1);
            static const DataType Append   = (1 << 2);
            static const DataType Truncate = (1 << 3);
        };
        
        //! Open procecure.
        //! \param[in] filename Input parameter filename.
        //! \param[in] access Input parameter access.
        //! \return bool.
        bool Open(std::string const filename, AccessFlags::DataType const access = AccessFlags::None)
        {
            bool result = false;
            // < Inline C++ Code >

			stream.open(filename.c_str());
			result = stream.good();
		
            // < /Inline C++ Code >
            return result;
        }
        
        //! OpenRead procecure.
        //! \param[in] filename Input parameter filename.
        //! \param[in] access Input parameter access.
        //! \return bool.
        bool OpenRead(std::string const filename, AccessFlags::DataType const access = AccessFlags::None)
        {
            bool result = false;
            // < Inline C++ Code >

			stream.open(filename.c_str(), std::ios_base::in);
			result = stream.good();
		
            // < /Inline C++ Code >
            return result;
        }
        
        //! OpenWrite procecure.
        //! \param[in] filename Input parameter filename.
        //! \param[in] access Input parameter access.
        //! \return bool.
        bool OpenWrite(std::string const filename, AccessFlags::DataType const access = AccessFlags::None)
        {
            bool result = false;
            // < Inline C++ Code >

			stream.open(filename.c_str(), std::ios_base::out);
			result = stream.good();
		
            // < /Inline C++ Code >
            return result;
        }
        
        //! Close procecure.
        void Close()
        {
            // < Inline C++ Code >
 stream.close(); 
            // < /Inline C++ Code >
        }
        
        //! WriteBuffer procecure.
        //! \param[in] buffer Input parameter buffer.
        //! \param[in] size Input parameter size.
        void WriteBuffer(char const* buffer, unsigned int const size)
        {
            // < Inline C++ Code >
 stream.write(buffer, static_cast<std::streamsize>(size)); 
            // < /Inline C++ Code >
        }
        
        //! WriteByte procecure.
        //! \param[in] value Input parameter value.
        void WriteByte(char const value)
        {
            // < Inline C++ Code >
 stream.write(&value, sizeof(value)); 
            // < /Inline C++ Code >
        }
        
        //! WriteUByte procecure.
        //! \param[in] value Input parameter value.
        void WriteUByte(unsigned char const value)
        {
            // < Inline C++ Code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /Inline C++ Code >
        }
        
        //! WriteShort procecure.
        //! \param[in] value Input parameter value.
        void WriteShort(short const value)
        {
            // < Inline C++ Code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /Inline C++ Code >
        }
        
        //! WriteUShort procecure.
        //! \param[in] value Input parameter value.
        void WriteUShort(unsigned short const value)
        {
            // < Inline C++ Code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /Inline C++ Code >
        }
        
        //! WriteInt procecure.
        //! \param[in] value Input parameter value.
        void WriteInt(int const value)
        {
            // < Inline C++ Code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /Inline C++ Code >
        }
        
        //! WriteUInt procecure.
        //! \param[in] value Input parameter value.
        void WriteUInt(unsigned int const value)
        {
            // < Inline C++ Code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /Inline C++ Code >
        }
        
        //! WriteLong procecure.
        //! \param[in] value Input parameter value.
        void WriteLong( const value)
        {
            // < Inline C++ Code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /Inline C++ Code >
        }
        
        //! WriteULong procecure.
        //! \param[in] value Input parameter value.
        void WriteULong( const value)
        {
            // < Inline C++ Code >
 stream.write(reinterpret_cast<const char*>(&value), sizeof(value)); 
            // < /Inline C++ Code >
        }
        
        //! WriteString procecure.
        //! \param[in] value Input parameter value.
        void WriteString(std::string const value)
        {
            // < Inline C++ Code >
 stream.write(value.c_str(), sizeof(char)*value.size()); 
            // < /Inline C++ Code >
        }
        
        //! WriteWString procecure.
        //! \param[in] value Input parameter value.
        void WriteWString(std::wstring const value)
        {
            // < Inline C++ Code >
 stream.write(reinterpret_cast<const char*>(value.c_str()), sizeof(wchar_t)*value.size()); 
            // < /Inline C++ Code >
        }
        
        //! WriteStringNL procecure.
        //! \param[in] value Input parameter value.
        void WriteStringNL(std::string const value)
        {
            // < Inline C++ Code >

			stream.write(value.c_str(), sizeof(char)*value.size());
			stream.write("\n", sizeof(char));
		
            // < /Inline C++ Code >
        }
        
        //! WriteWStringNL procecure.
        //! \param[in] value Input parameter value.
        void WriteWStringNL(std::string const value)
        {
            // < Inline C++ Code >

			stream.write(value.c_str(), sizeof(char)*value.size());
			stream.write(reinterpret_cast<const char*>(L"\n"), sizeof(wchar_t));
		
            // < /Inline C++ Code >
        }
        
        //! ReadBuffer procecure.
        //! \param[in] buffer Input parameter buffer.
        //! \param[in] size Input parameter size.
        void ReadBuffer(char* buffer, unsigned int const size)
        {
            // < Inline C++ Code >
 stream.read(buffer, static_cast<std::streamsize>(size)); 
            // < /Inline C++ Code >
        }
        
        virtual ~File()
        {
        }
        
    private:
        // < Inline C++ Code >
 std::fstream stream; 
        // < /Inline C++ Code >
};

} // /namespace IO

//! Person class.
class Person
{
    public:
        Person(std::string const n, int const a) :
            age(0)
        {
            name = n;
            age = a;
        }
        
        //! WritePerson procecure.
        //! \param[in] file Input parameter file.
        void WritePerson(std::shared_ptr< IO::File > file)
        {
            if (file != nullptr)
            {
                (*file).WriteStringNL("-- Person --");
                (*file).WriteStringNL("  Name: " + name);
                (*file).WriteStringNL("  Age: " + age);
                (*file).WriteStringNL("");
            }
        }
        
        std::string name;
        int age;
        virtual ~Person()
        {
        }
        
};

//! Main procecure.
//! \return int.
int main()
{
    std::shared_ptr< IO::File > file = std::make_shared< IO::File >();
    (*file).OpenWrite("FirstApp.Output.txt");
    std::shared_ptr< Person > p0 = std::make_shared< Person >("Lukas Hermanns", 23);
    std::shared_ptr< Person > p1 = std::make_shared< Person >("Max Mustermann", 99);
    std::shared_ptr< Person > p2 = std::make_shared< Person >("Bjarne Stroustrup", 64);
    (*p0).WritePerson(file);
    (*p1).WritePerson(file);
    (*p2).WritePerson(file);
    return 0;
}

#endif
// ================
