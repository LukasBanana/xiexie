// ..\..\..\trunk\examples\Tests/Events.xx.cpp
// XieXie generated source file
// Mon Feb 24 00:15:21 2014


#include <memory>

//! Window class.
class Window
{
    public:
        Window() :
            x(0),
            y(0),
            width(0),
            height(0)
        {
        }
        
        virtual ~Window()
        {
        }
        
        //! GetX procedure.
        //! \return .
        int const& GetX() const
        {
            return x;
        }
        
        //! SetX procedure.
        //! \param[in] x Input parameter x.
        void SetX(int const& x)
        {
            (*this).x = x;
        }
        
        //! GetY procedure.
        //! \return .
        int const& GetY() const
        {
            return y;
        }
        
        //! SetY procedure.
        //! \param[in] y Input parameter y.
        void SetY(int const& y)
        {
            (*this).y = y;
        }
        
        //! GetWidth procedure.
        //! \return .
        int const& GetWidth() const
        {
            return width;
        }
        
        //! SetWidth procedure.
        //! \param[in] width Input parameter width.
        void SetWidth(int const& width)
        {
            (*this).width = width;
        }
        
        //! GetHeight procedure.
        //! \return .
        int const& GetHeight() const
        {
            return height;
        }
        
        //! SetHeight procedure.
        //! \param[in] height Input parameter height.
        void SetHeight(int const& height)
        {
            (*this).height = height;
        }
        
    private:
        int x, y;
        int width, height;
        //! SetEventHandler procedure.
        //! \param[in] _place_holder_ Input parameter _place_holder_.
        void SetEventHandler(int _place_holder_)
        {
        }
        
};

//! WindowEvent class.
class WindowEvent
{
    public:
        WindowEvent(Window* window) :
            window(nullptr)
        {
            (*this).window = window;
        }
        
        virtual ~WindowEvent()
        {
        }
        
        //! GetWindow procedure.
        //! \return .
        Window* const& GetWindow() const
        {
            return window;
        }
        
    protected:
        Window* window;
};

//! ResizeEvent class.
//! \see WindowEvent
class ResizeEvent : public WindowEvent
{
    public:
        ResizeEvent(Window* window, int width, int height) :
            width(0),
            height(0)
        {
            (*this).width = width;
            (*this).height = height;
        }
        
        virtual ~ResizeEvent()
        {
        }
        
        //! GetWidth procedure.
        //! \return .
        int const& GetWidth() const
        {
            return width;
        }
        
        //! GetHeight procedure.
        //! \return .
        int const& GetHeight() const
        {
            return height;
        }
        
    private:
        int width, height;
};

//! MoveEvent class.
//! \see WindowEvent
class MoveEvent : public WindowEvent
{
    public:
        MoveEvent(Window* window, int x, int y) :
            x(0),
            y(0)
        {
            (*this).x = x;
            (*this).y = y;
        }
        
        virtual ~MoveEvent()
        {
        }
        
        //! GetX procedure.
        //! \return .
        int const& GetX() const
        {
            return x;
        }
        
        //! GetY procedure.
        //! \return .
        int const& GetY() const
        {
            return y;
        }
        
    private:
        int x, y;
};

//! WindowEventHandler class.
class WindowEventHandler
{
    public:
        //! OnResize procedure.
        //! \param[in] event Input parameter event.
        void OnResize(ResizeEvent const& event)
        {
        }
        
        //! OnMove procedure.
        //! \param[in] event Input parameter event.
        void OnMove(MoveEvent const& event)
        {
            if ((event.window != nullptr))
            {
                (*event.window).SetX(event.GetX());
                (*event.window).SetY(event.GetY());
            }
        }
        
        virtual ~WindowEventHandler()
        {
        }
        
};

//! Main procedure.
//! \return int.
int main()
{
    std::shared_ptr< Window > mainWindow = std::make_shared< Window >(640, 480);
    (*mainWindow).SetEventHandler(std::make_shared< WindowEventHandler >());
    return 0;
}

// ================
