// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Assert.xx.cpp
// XieXie generated source file
// Sat Feb 15 16:50:22 2014


#include <XieXie/Lang/AssertCore.h>

//! Main procedure.
int main()
{
    int x = 0;
    int* p = nullptr;
    #ifdef __XX__ENABLE_ASSERTIONS___
    __XX__ASSERT((p != nullptr));
    #endif
    #ifdef __XX__ENABLE_ASSERTIONS___
    __XX__ASSERT_INFO(x != 0, "x must not be zero");
    #endif
    return 0;
}

// ================
