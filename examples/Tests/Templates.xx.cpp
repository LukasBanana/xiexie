// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Templates.xx.cpp
// XieXie generated source file
// Tue Jan 21 21:51:10 2014


#include <XieXie/Lang/StringCore.h>
#include <memory>

//! Test template class.
template < unsigned int num = 0, std::string s > class Test
{
    public:
        int data;
        std::shared_ptr< Test > link;
        Test() :
            data(0),
            link(nullptr)
        {
        }
        
        virtual ~Test()
        {
        }
        
};

//! Vec3f class.
class Vec3f
{
    public:
        float x, y, z;
        Vec3f() :
            x(0.0f),
            y(0.0f),
            z(0.0f)
        {
        }
        
        virtual ~Vec3f()
        {
        }
        
};

//! Loop procecure.
template < unsigned int num > void Loop()
{
    unsigned int i = 0u;
    // While Loop
    while (i < num)
    {
    }
    
}

// ================
