// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Flags.xx.cpp
// XieXie generated source file
// Sun Feb 09 16:54:13 2014


#include <XieXie/Lang/FlagsCore.h>

namespace MyPackage
{
//! Flags flags enumeration.
struct Flags
{
    typedef unsigned char __XX__DataType;
    enum __XX__Enum : __XX__DataType
    {
        None  = 0,
        All   = 0x07,
        Flag0 = (1 << 0),
        Flag1 = (1 << 1),
        Flag2 = (1 << 2),
    };
    __XX__DEFINE_FLAGS_FUNCTIONS__(Flags)
    private:
    __XX__DataType __XX__BitField;
};
__XX__DEFINE_FLAGS_OPERATORS__(Flags)

//! TestFlags flags enumeration.
struct TestFlags
{
    typedef unsigned char __XX__DataType;
    enum __XX__Enum : __XX__DataType
    {
        None = 0,
        All  = 0x07,
        Foo  = (1 << 0),
        Bar  = (1 << 1),
        Blub = (1 << 2),
    };
    __XX__DEFINE_FLAGS_FUNCTIONS__(TestFlags)
    private:
    __XX__DataType __XX__BitField;
};
__XX__DEFINE_FLAGS_OPERATORS__(TestFlags)

} // /namespace MyPackage

//! LetterFlags flags enumeration.
struct LetterFlags
{
    typedef unsigned short int __XX__DataType;
    enum __XX__Enum : __XX__DataType
    {
        None  = 0,
        All   = 0x01ff,
        Flag0 = (1 << 0),
        Flag1 = (1 << 1),
        Flag2 = (1 << 2),
        Foo   = (1 << 3),
        Bar   = (1 << 4),
        Blub  = (1 << 5),
        A     = (1 << 6),
        B     = (1 << 7),
        C     = (1 << 8),
    };
    __XX__DEFINE_FLAGS_FUNCTIONS__(LetterFlags)
    private:
    __XX__DataType __XX__BitField;
};
__XX__DEFINE_FLAGS_OPERATORS__(LetterFlags)

//! Main procecure.
int main()
{
    int x = 0, y = 0;
    x += y + static_cast< int >(MyPackage::Flags(MyPackage::Flags::Flag0));
    MyPackage::Flags f = MyPackage::Flags(MyPackage::Flags::Flag0) | MyPackage::Flags(MyPackage::Flags::Flag1);
    LetterFlags f2 = LetterFlags(LetterFlags::Foo) | LetterFlags(LetterFlags::B) & LetterFlags(LetterFlags::C);
    f.Add(MyPackage::Flags(MyPackage::Flags::Flag0));
    f2.Add(LetterFlags(LetterFlags::Foo));
    f.Remove(MyPackage::Flags(MyPackage::Flags::Flag2));
    bool b1 = f2.Has(LetterFlags(LetterFlags::Bar)) && f.Has(MyPackage::Flags(MyPackage::Flags::Flag1));
    f = MyPackage::Flags(MyPackage::Flags::None);
    f.Add(MyPackage::Flags(MyPackage::Flags::Flag0));
    f.Add(MyPackage::Flags(MyPackage::Flags::Flag1));
    bool b2 = false;
    if (b2 = f.Has(MyPackage::Flags(MyPackage::Flags::Flag1)))
    {
        int x = 0;
    }
    return 0;
}

// ================
