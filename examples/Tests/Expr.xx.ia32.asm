; D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/ExpressionTest1.xx.ia32.asm
; XieXie generated source file
; Sun Dec 29 13:30:52 2013

; --- Function definition "func"  ---
func:
    push x
    pop eax
    mov ebx, 2
    mul eax, ebx
    push eax
    pop eax
    mov ebx, 3
    sub eax, ebx
    push eax
    ret

; --- Function definition "Main"  ---
Main:
    ; Variable initialization "x"
    push 1
    push 2
    ; TODO -> push parameters onto stack ...
    call func
    pop eax
    mov ebx, 4
    div eax, ebx
    push eax
    pop ebx
    pop eax
    mul eax, ebx
    push eax
    pop eax
    mov ebx, 2
    sub eax, ebx
    push eax
    pop ebx
    pop eax
    add eax, ebx
    push eax
    pop eax
    mov [0001e240h], eax
    
    ; Variable initialization "a"
    push 7
    mov eax, 3
    mov ebx, 2
    mul eax, ebx
    push eax
    push 5
    mov eax, 6
    mov ebx, 3
    add eax, ebx
    push eax
    pop ebx
    pop eax
    mul eax, ebx
    push eax
    pop ebx
    pop eax
    sub eax, ebx
    push eax
    pop eax
    mov ebx, 4
    sub eax, ebx
    push eax
    pop ebx
    pop eax
    add eax, ebx
    push eax
    pop eax
    mov [0001e240h], eax
    
    ; Variable initialization "i"
    
    ; Infinite loop
    .inf_loop_0:
        ; TODO -> push parameters onto stack ...
        call func
        push 3
        ; <Object expression "i">
        ; </Object expression "i">
        pop ebx
        pop eax
        mul eax, ebx
        push eax
        pop eax
        add [0001e240h], eax
    jmp inf_loop_0
    .inf_loop_0_break:
    
    push 0
    ret

// ===============
