// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Enums.xx.cpp
// XieXie generated source file
// Wed Feb 12 21:54:02 2014


#include <XieXie/Lang/EnumCore.h>
#include <XieXie/Lang/StringCore.h>
#include <string>

//! Types enumeration.
struct Types
{
    enum __XX__Enum
    {
        Unknown = 0,
        Foo = 5,
        Bar = 6,
        __XX__Uninitialized
    };
    static inline size_t Num()
    {
        return 3;
    }
    std::string Str() const
    {
        switch (__XX__Entry)
        {
            case Unknown: return "unknown";
            case Foo:     return "this is foo";
            case Bar:     return "and this is bar";
        }
        return "";
    }
    __XX__DEFINE_ENUM_DEFAULT_MEMBERS__(Types)
};

namespace GUI
{
//! RadioButtonStates enumeration.
struct RadioButtonStates
{
    enum __XX__Enum
    {
        Unknown,
        On,
        Off,
        __XX__Uninitialized
    };
    static inline size_t Num()
    {
        return 3;
    }
    std::string Str() const
    {
        switch (__XX__Entry)
        {
            case Unknown: return "Unknown";
            case On:      return "On";
            case Off:     return "Off";
        }
        return "";
    }
    __XX__DEFINE_ENUM_DEFAULT_MEMBERS__(RadioButtonStates)
};

} // /namespace GUI

//! Main procedure.
int main()
{
    GUI::RadioButtonStates stat;
    stat.Set(GUI::RadioButtonStates::On);
    std::string s = stat.Str();
    unsigned int n = stat.Num();
    ;
    int* p = nullptr;
    if ((p != 0))
    {
    }
    if (p != 0)
    {
    }
    if (n > 10)
    {
    }
    if ((!stat.Is(GUI::RadioButtonStates::Off)) || stat.Is(GUI::RadioButtonStates::On))
    {
    }
    return 0;
}

// ================
