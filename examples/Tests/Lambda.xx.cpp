// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Lambda.xx.cpp
// XieXie generated source file
// Sat Feb 08 20:50:20 2014


#include <functional>

//! Test class.
class Test
{
    public:
        typedef std::function< bool (int a) > ProcType;
        //! TestProc procecure.
        void TestProc()
        {
            ProcType f = [&](int a) -> bool
            {
                return a < x;
            }
            ;
            bool b = f(5);
        }
        
        int x;
        Test() :
            x(0)
        {
        }
        
        virtual ~Test()
        {
        }
        
};

//! Main procecure.
int main()
{
    Test t;
    t.TestProc();
    return 0;
}

// ================
