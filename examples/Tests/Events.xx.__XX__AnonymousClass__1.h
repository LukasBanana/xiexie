// ..\..\..\trunk\examples\Tests/Events.xx.__XX__AnonymousClass__1.h
// XieXie generated source file
// Mon Feb 24 00:15:21 2014

#ifndef __XX___________TRUNK_EXAMPLES_TESTS_EVENTS_XX___XX__ANONYMOUSCLASS__1_H__H__
#define __XX___________TRUNK_EXAMPLES_TESTS_EVENTS_XX___XX__ANONYMOUSCLASS__1_H__H__
//! __XX__AnonymousClass__1 class.
class __XX__AnonymousClass__1 : public WindowEventHandler
{
    public:
        //! OnResize procedure.
        //! \param[in] event Input parameter event.
        void OnResize(ResizeEvent const& event)
        {
            if ((event.window != nullptr))
            {
                if (event.width < 800)
                {
                    (*event.window).SetWidth(event.width);
                }
                else
                {
                    (*event.window).SetWidth(800);
                }
                (*event.window).SetHeight(event.height);
            }
        }
        
        virtual ~__XX__AnonymousClass__1()
        {
        }
        
};

#endif
// ================
