// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Scopes.xx.cpp
// XieXie generated source file
// Fri Jan 17 20:38:29 2014


#include <memory>

int x = 0;
{
    int x = 0;
}
namespace Video
{
namespace Renderer
{
//! HardwareBuffer class.
class HardwareBuffer
{
    public:
        virtual ~HardwareBuffer()
        {
        }
        
};

} // /namespace Video

} // /namespace Renderer

namespace Video
{
namespace Renderer
{
namespace OpenGL
{
//! GLHardwareBuffer class.
//! \see HardwareBuffer
class GLHardwareBuffer : public HardwareBuffer
{
    public:
        int y;
        GLHardwareBuffer() :
            y(0)
        {
        }
        
        virtual ~GLHardwareBuffer()
        {
        }
        
};

} // /namespace Video

} // /namespace Renderer

} // /namespace OpenGL

namespace Video
{
std::shared_ptr< Renderer::OpenGL::GLHardwareBuffer > hwBuffer = nullptr;
} // /namespace Video

Video::hwBuffer = std::make_shared< Video::Renderer::OpenGL::GLHardwareBuffer >();
//! ScopeClass class.
class ScopeClass
{
    public:
        //! ScopeProc procecure.
        void ScopeProc()
        {
            int a = 0;
            a += 5;
        }
        
        //! f flags enumeration.
        struct f
        {
            // --- Auto-Generated Flags --- //
            typedef unsigned char DataType;
            static const DataType None = 0;
            static const DataType All  = ~0;
            // --- Custom Flags --- //
            static const DataType a    = (1 << 0);
            static const DataType b    = (1 << 1);
            static const DataType c    = (1 << 2);
        };
        
        //! SubClass class.
        class SubClass
        {
            public:
                //! TestClass class.
                class TestClass
                {
                    public:
                        int c;
                        TestClass() :
                            c(0)
                        {
                        }
                        
                        virtual ~TestClass()
                        {
                        }
                        
                };
                
                //! ScopeClass class.
                class ScopeClass
                {
                    public:
                        int c;
                        ScopeClass f;
                        ScopeClass() :
                            c(0)
                        {
                        }
                        
                        virtual ~ScopeClass()
                        {
                        }
                        
                };
                
                ScopeClass::SubClass::TestClass d;
                ScopeClass e;
                virtual ~SubClass()
                {
                }
                
        };
        
        int test;
        f::DataType test_flags;
        SubClass sub;
        ScopeClass() :
            test(0),
            test_flags(0u)
        {
        }
        
        virtual ~ScopeClass()
        {
        }
        
};

ScopeClass::f::DataType test = 0u;
ScopeClass obj;
ScopeClass::SubClass obj2;
obj.ScopeProc();
obj.test += 3;
obj2.d.c += 5;
// ================
