// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/InitLists.xx.cpp
// XieXie generated source file
// Sat Feb 01 17:56:52 2014


#include <XieXie/Lang/StringCore.h>
#include <array>
#include <vector>

//! Test procecure.
//! \param[in] listInt Input parameter listInt.
//! \param[in] listString1 Input parameter listString1.
//! \param[in] listString2 Input parameter listString2.
//! \param[in] doubleArray1 Input parameter doubleArray1.
//! \param[in] doubleArray2 Input parameter doubleArray2.
void Test(std::vector< int > listInt, std::array< std::string, 2 > listString1, std::vector< std::string > listString2, std::vector< std::vector< int > > doubleArray1, std::vector< std::vector< int > > doubleArray2)
{
}

//! ToString procecure.
//! \param[in] n Input parameter n.
//! \return string.
std::string ToString(int n)
{
    std::string s;
    return s;
}

//! Main procecure.
int main()
{
    std::vector< std::string > array1 = { "foo", "bar" };
    std::array< int, 3 > array2 = { 1, 2, 3 };
    Test({ 1, 2, 3 }, { "test", ToString(5) }, array1, { { 1, 2, 3 }, { 6, 5, 4 } }, { array2, array2 });
    int i0 = array2[0];
    int i1 = array2[array2[0]];
    int i2 = array2[array2[i1]];
    return 0;
}

// ================
