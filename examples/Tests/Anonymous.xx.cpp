// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/Anonymous.xx.cpp
// XieXie generated source file
// Mon Jan 20 21:36:41 2014


#include <XieXie/Lang/StringCore.h>
#include <memory>

//! RootClass class.
class RootClass
{
    public:
        float f;
        RootClass() :
            f(0.0f)
        {
        }
        
        virtual ~RootClass()
        {
        }
        
};

//! BaseClass class.
//! \see RootClass
class BaseClass : public RootClass
{
    public:
        //! Foo procecure.
        void Foo()
        {
        }
        
        BaseClass() :
            x(0)
        {
        }
        
        virtual ~BaseClass()
        {
        }
        
    private:
        int x;
    public:
        //! Getter for "x" member variable.
        //! \see x
        //! \return Current value.
        inline const int& GetX() const
        {
            return x;
        }
        
};

//! TakeObject procecure.
//! \param[in] obj Input parameter obj.
void TakeObject(std::shared_ptr< BaseClass > obj)
{
    (*obj).Foo();
}

//! Main procecure.
//! \return int.
int main()
{
    std::shared_ptr< BaseClass > obj = std::make_shared< BaseClass >();
    TakeObject(std::make_shared< BaseClass >());
    return 0;
}

// ================
