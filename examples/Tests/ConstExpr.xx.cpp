// D:\SoftwareEntwicklung\C++\HLC\Tools\XieXie-Compiler\trunk\examples\Tests/ConstExpr.xx.cpp
// XieXie generated source file
// Wed Jan 29 23:23:54 2014

static int const x = 3;
static int const y = 2;
static int const z = -13;
static int const c1 = -11;
static int const c2 = 0.75;
static int const i1 = 135;
static int const i2 = 135;
static int const i3 = 135;
static int const i4 = 135;
//! Test procecure.
//! \param[in] a Input parameter a.
//! \param[in] b Input parameter b.
//! \param[in] c Input parameter c.
void Test(int a, int b, int c)
{
}

//! Main procecure.
int main()
{
    Test(3, -13, 3 + 2);
    return 0;
}

// ================
