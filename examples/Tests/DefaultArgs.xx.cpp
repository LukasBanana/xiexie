// ..\..\..\trunk\examples\Tests/DefaultArgs.xx.cpp
// XieXie generated source file
// Mon Feb 24 00:16:11 2014


#include <XieXie/Lang/StringCore.h>

//! Test procedure.
//! \param[in] x Input parameter x. By default 1.
//! \param[in] y Input parameter y. By default 2.0.
//! \param[in] z Input parameter z. By default 3.0.
//! \param[in] q Input parameter q. By default 4.0.
//! \param[in] s Input parameter s. By default "empty".
void Test(int x = 1, float y = 2.0, double z = 3.0, long double q = 4.0, std::string s = "empty")
{
}

//! Main procedure.
//! \return int.
int main()
{
    Test(1, 3.14);
    Test(1, 3.14);
    Test(1, 2.0, 3.0, 1.0 / 3.0);
    Test(6, 7, 3.0, 0.33);
    Test(1, 2.0, 3.0, 4.0, "test");
    float y = 0.0f;
    Test(1, y = 7.0);
    return 0;
}

// ================
