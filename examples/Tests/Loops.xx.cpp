// ..\..\..\trunk\examples\Tests/Loops.xx.cpp
// XieXie generated source file
// Mon Feb 24 00:13:47 2014


#include <array>
#include <memory>

//! LinkedList class.
class LinkedList
{
    public:
        int data;
        std::shared_ptr< LinkedList > link;
        LinkedList() :
            data(0),
            link(nullptr)
        {
        }
        
        virtual ~LinkedList()
        {
        }
        
};

//! MakeList procedure.
//! \param[in] num Input parameter num.
//! \return @.
std::shared_ptr< LinkedList > MakeList(int num)
{
    std::shared_ptr< LinkedList > list = std::make_shared< LinkedList >();
    (*list).data = num;
    if (num > 0)
    {
        (*list).link = MakeList(num - 1);
    }
    return list;
}

//! Main procedure.
//! \return int.
int main()
{
    int i = 0;
    // Infinite Loop
    while (true)
    {
        i += 1;
        if (i > 10)
        {
            break;
        }
    }
    
    // While Loop
    while (i < 10)
    {
    }
    
    // Until Loop
    while ( !( i == 10 ) )
    {
    }
    
    // Do/While Loop
    do
    {
    }
    while (i < 10);
    
    // Do/Until Loop
    do
    {
    }
    while ( !( i == 10 ) );
    
    // Range-Based For Loop
    for (unsigned char n = 5; n <= 12; ++n)
    {
    }
    
    // Range-Based For Loop
    for (char n = 6; n >= -3; --n)
    {
    }
    
    // Range-Based For Loop
    for (unsigned char n = 1; n <= 10; ++n)
    {
    }
    
    // Range-Based For Loop
    for (unsigned char n = 0; n <= 255; ++n)
    {
    }
    
    // Range-Based For Loop
    for (unsigned short int n = 0; n <= 256; ++n)
    {
    }
    
    // Range-Based For Loop
    for (short int n = -50; n <= 255; ++n)
    {
    }
    
    // Range-Based For Loop
    for (char n = -50; n <= 50; ++n)
    {
    }
    
    // Range-Based For Loop
    for (char n = -10; n <= -1; n += 2)
    {
    }
    
    // Range-Based For Loop
    for (char n = -1; n >= -10; n -= 2)
    {
    }
    
    // Range-Based For Loop
    for (unsigned int n = 1; n <= 100000000; ++n)
    {
    }
    
    // Range-Based For Loop
    for (unsigned long long int n = 1; n <= 10000000000000000; ++n)
    {
    }
    
    std::array< float, 3 > vec = { 0 };
    // Unrolled Range-Based For Loop
    {
        // Iteration 3
        unsigned char n = 3;
        {
            vec[n - 3] = static_cast< float >(n) * 4.0 + 2.0;
        }
        
        // Iteration 4
        n = 4;
        {
            vec[n - 3] = static_cast< float >(n) * 4.0 + 2.0;
        }
        
        // Iteration 5
        n = 5;
        {
            vec[n - 3] = static_cast< float >(n) * 4.0 + 2.0;
        }
        
    }
    return 0;
}

// ================
